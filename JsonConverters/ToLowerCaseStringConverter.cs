﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace Gensolve.Physio.Server.Service.MedicareOnline.JsonConverters
{
    public class ToLowerCaseStringConverter<T> : JsonConverter
    {
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer) => JToken.ReadFrom(reader).Value<string>();
        public override bool CanConvert(Type objectType) => typeof(T).Equals(objectType);
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) => serializer.Serialize(writer, value.ToString().ToLower());
    }
}
