﻿using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared;
using Gensolve.Utilities;
using System;
using System.Linq;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Extensions
{
    public static class BulkBillClaimStoreForwardClaimExtensions
    {
        /// <summary>
        /// Handles checking whihc url should be used to process the claim.
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static string GetBulkBillStoreForwardUrl(this BulkBillClaimStoreForwardClaim self)
        {
            if (self.ServiceTypeCode == ServiceTypeCodes.General
               && self.MatchesBulkBillGeneralMedicalServiceCondition())
            {
                //return MiscHelper.GetSystemSetting(SystemSetting.PRODA_MEDICARE_BULK_BILL_STORE_FORWARD_GENERAL_URL);
            }

            if (self.ServiceTypeCode == ServiceTypeCodes.Specialist
              && self.MatchesBulkBillSpecialistMedicalServiceCondition())
            {
                //return MiscHelper.GetSystemSetting(SystemSetting.PRODA_MEDICARE_BULK_BILL_STORE_FORWARD_SPECIALIST_URL);
            }

            if (self.ServiceTypeCode == ServiceTypeCodes.Pathology
              && self.MatchesBulkBillPathologyMedicalServiceCondition())
            {
                //return MiscHelper.GetSystemSetting(SystemSetting.PRODA_MEDICARE_BULK_BILL_STORE_FORWARD_PATHOLOGY_URL);
            }

            throw new Exception("Invalid Bulk Bill Claim");
        }

        /// <summary>
        /// Checks whether a claim has a referral or referral override code set or any of the child
        /// services has a self deemed code set. If any of these conditions met false to match required
        /// rules.
        /// </summary>
        /// <param name="self"></param>
        /// <returns>True conditions met.</returns>
        public static bool  MatchesBulkBillGeneralMedicalServiceCondition(this BulkBillClaimStoreForwardClaim self)
        {
            foreach(var medicalService in self.MedicalEvent)
            {
                if(medicalService.Referral != null 
                    || medicalService.ReferralOverrideCode.HasText()
                    || medicalService.Service.FirstOrDefault(e => e.SelfDeemedCode.HasText()) != null)
                {
                    // Only need one to be false.
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Checks whether a claim has a referral or referral override code set or any of the child
        /// services has a self deemed code set. If any of these conditions met false to match required
        /// rules.
        /// </summary>
        /// <param name="self"></param>
        /// <returns>True conditions met.</returns>
        public static bool MatchesBulkBillSpecialistMedicalServiceCondition(this BulkBillClaimStoreForwardClaim self)
        {
            foreach (var medicalService in self.MedicalEvent)
            {
                // If referral is set and referral>Type Code is set to S or ‘D’ perform validation checks and continue processing.If referral is set and
                // referral > Type Code is not set to S or D, an error will be returned and processing will stop
                // If Referral Override Code is set to either H, L, E or N, perform validation checks and continue processing.If Referral Override
                // Code is not set to either H, L, E or N, an error will be returned and processing will stop.
                if (medicalService.Referral == null
                    || !medicalService.Referral.TypeCode.MatchesAtLeastOne(ReferralTypeCodes.SpecialistAndAllied, ReferralTypeCodes.DiagnosticImaging)
                    || !medicalService.ReferralOverrideCode.MatchesAtLeastOne(ReferralOverrideCodes.Hospital, ReferralOverrideCodes.Lost, ReferralOverrideCodes.Emergency, ReferralOverrideCodes.NotRequired))
                {
                    return false;
                }

                // If Self Deemed Code is set to either SD or SS for at least one service per medical event,perform validation checks and continue
                // processing. If Self Deemed Code is not set to either SD or SS for at least one service per medical event, an error will be returned and processing will stop.
                var item = medicalService.Service.FirstOrDefault(e => e.SelfDeemedCode.MatchesAtLeastOne(SelfDeemedCodes.SelfDeemed, SelfDeemedCodes.SubstitutedService));
                if(item == null)
                {
                    return false;
                }
            }

            return true;
        }

        public static bool MatchesBulkBillPathologyMedicalServiceCondition(this BulkBillClaimStoreForwardClaim self)
        {
            foreach (var medicalService in self.MedicalEvent)
            {
                // If referral is set and referral>Type Code is set to S or ‘D’ perform validation checks and continue processing.If referral is set and
                // referral > Type Code is not set to S or D, an error will be returned and processing will stop
                // If Referral Override Code is set to either H, L, E or N, perform validation checks and continue processing.If Referral Override
                // Code is not set to either H, L, E or N, an error will be returned and processing will stop.
                if ((medicalService.Referral != null && !medicalService.Referral.TypeCode.MatchesAtLeastOne(ReferralTypeCodes.Pathology))
                    || !medicalService.ReferralOverrideCode.MatchesAtLeastOne(ReferralOverrideCodes.NotRequired)
                    || !medicalService.Service.MatchesPathologyServiceConditions())
                {
                    return false;
                }
            }

            return true;
        }

      
    }
}
