﻿using RestSharp;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Extensions
{
    public static class IRestResponseExtensions
    {
        public static bool ContentContains(this IRestResponse response, string text)
        {
            return response.Content.ToLower().Contains(text.ToLower());
        }
    }
}
