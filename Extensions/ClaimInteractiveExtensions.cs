﻿using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared;
using System;
using System.Linq;
using static Gensolve.Physio.Server.Service.MedicareOnline.Extensions.StringExtensions;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Extensions
{
    /// <summary>
    /// Adds extension methods for ClaimInteractive class.
    /// </summary>
    public static class ClaimInteractiveExtensions
    {
        /// <summary>
        /// Handles checking the claim to return which url should be called to process it.
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static string GetMedicarePatientClaimInteractiveUrl(this ClaimInteractive self)
        {
            if (self.Referral == null
                && !self.ReferralOverrideCode.HasText()
                && self.MatchesGeneralServiceConditions())
            {
                //return MiscHelper.GetSystemSetting(SystemSetting.PRODA_MEDICARE_PATIENT_CLAIM_INTERACTIVE_GENERAL_URL);
            }

            if (self.Referral != null
                && self.ReferralOverrideCode.MatchesAtLeastOne(ReferralOverrideCodes.Hospital, ReferralOverrideCodes.Lost,
                ReferralOverrideCodes.Emergency, ReferralOverrideCodes.NotRequired)
                && self.MatchesSpecialistServiceConditions())
            {
                //return MiscHelper.GetSystemSetting(SystemSetting.PRODA_MEDICARE_PATIENT_CLAIM_INTERACTIVE_SPECIALIST_URL);
            }

            if (self.Referral != null
                && self.ReferralOverrideCode.ToLower() == ReferralOverrideCodes.NotRequired.ToLower()
                && self.MatchesPathologyServiceConditions())
            {
                //return MiscHelper.GetSystemSetting(SystemSetting.PRODA_MEDICARE_PATIENT_CLAIM_INTERACTIVE_PATHOLOGY_URL);
            }

            throw new Exception("Invalid patient claim.");
        }

        /// <summary>
        /// Must not have a service which has a self deemed code set.
        /// </summary>
        /// <param name="patientClaimInteractive"></param>
        /// <returns></returns>
        public static bool MatchesGeneralServiceConditions(this ClaimInteractive self)
        {
            var item = self.MedicalEvent.FirstOrDefault(e => e.Service.FirstOrDefault(s => s.SelfDeemedCode.HasText()) != null);

            return item == null;
        }

        /// <summary>
        /// Must have at least one services which has a self deemed code of SD or SS.
        /// </summary>
        /// <param name="patientClaimInteractive"></param>
        /// <returns></returns>
        public static bool MatchesSpecialistServiceConditions(this ClaimInteractive self)
        {
            var item = self.MedicalEvent.FirstOrDefault(e => e.Service.FirstOrDefault(s =>
                s.SelfDeemedCode.MatchesAtLeastOne(SelfDeemedCodes.SelfDeemed, SelfDeemedCodes.SubstitutedService)) != null);

            return item != null;
        }


        /// <summary>
        /// Must have at least one service with SelfDeemedCode = SD
        /// and no service with SelfDeemedCode = SS
        /// </summary>
        /// <param name="patientClaimInteractive"></param>
        /// <returns></returns>
        public static bool MatchesPathologyServiceConditions(this ClaimInteractive self)
        {
            bool hasSelfDeemed = false;
            bool hasSubstitutedService = false;

            foreach (var medicalEvent in self.MedicalEvent)
            {
                foreach (var service in medicalEvent.Service)
                {
                    if (!hasSelfDeemed)
                    {
                        // only check if not set already
                        if (service.SelfDeemedCode.MatchesAtLeastOne(SelfDeemedCodes.SelfDeemed))
                            hasSelfDeemed = true;
                    }

                    if (!hasSubstitutedService)
                    {
                        if (service.SelfDeemedCode.MatchesAtLeastOne(SelfDeemedCodes.SubstitutedService))
                            hasSubstitutedService = true;
                    }

                }
            }

            return hasSelfDeemed & !hasSubstitutedService;
        }
    }
}
