﻿using System.Collections.Generic;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Extensions
{
    public static class ServiceExtensions
    {
        public static bool MatchesPathologyServiceConditions(this IList<Models.Requests.Shared.Service> self)
        {
            bool hasSelfDeemed = false;
            bool hasSubstitutedService = false;

            // If Self Deemed Code is set to SD for at least one service per medical event and no service has Self Deemed Code set to SS, perform
            // validation checks and continue processing.If Self Deemed Code is not set to SD for at least one service per medical event, or service has
            // Self Deemed Code set to SS, an error will be returned and processing will stop.
            foreach (var service in self)
            {
                if (!hasSelfDeemed)
                {
                    // only check if not set already
                    if (service.SelfDeemedCode.MatchesAtLeastOne(SelfDeemedCodes.SelfDeemed))
                        hasSelfDeemed = true;
                }

                if (!hasSubstitutedService)
                {
                    // only check if not set already
                    if (service.SelfDeemedCode.MatchesAtLeastOne(SelfDeemedCodes.SubstitutedService))
                        hasSubstitutedService = true;
                }
            }

            if (!hasSelfDeemed || hasSubstitutedService)
            {
                return false;
            }

            return true;
        }
    }
}
