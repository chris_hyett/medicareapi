﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Extensions
{
    public static class StringExtensions
    {
        public static bool MatchesAtLeastOne(this string self, params string[] items)
        {
            foreach (var item in items)
            {
                if (item.ToLower() == self.ToLower())
                {
                    return true;
                }
            }

            return false;
        }
        /// <summary>
        /// Handles extracting the text within a string between first 2 instances of a delimited
        /// </summary>
        /// <param name="text">Text containing delimited text.</param>
        /// <param name="charDelimiter">Char text delimited by.</param>
        /// <param name="textAfter">Text found after second delimiter instance.</param>
        /// <returns>Text found between first 2 instances of delimiter if it contains at least 2 else string.Empty.</returns>
        public static string ExtractTextBetweenFirstTwoCharDelimiters(this string text, char charDelimiter, out string textAfter)
        {
            textAfter = string.Empty;
            if (text.HasText())
            {
                // get position of first occurrence of the delimiter
                int first = text.IndexOf(charDelimiter);

                if (first > -1)
                {
                    {
                        // nowq see if it occurs again.
                        int second = text.IndexOf(charDelimiter, first + 1);

                        if (second > -1)
                        {
                            if (text.Length > second + 1)
                            {
                                textAfter = text.Substring(second + 1);
                            }

                            // so now return text between 2 occurrences.
                            return text.Substring(first + 1, (second - first) - 1);
                        }
                    }
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Handles extracting the text within a string between first 2 instances of a delimited
        /// and converts to decimal if possible.
        /// </summary>
        /// <param name="text">Text containing delimited text.</param>
        /// <param name="charDelimiter">Char text delimited by.</param>
        /// <param name="extractedNumber">ecimal valuew found between first 2 instances of delimiter.</param>
        /// <param name="textAfter">Text found after second delimiter instance.</param>
        /// <returns>True if decimal value found else false.</returns>
        public static bool ExtractDecimalBetweenFirstTwoCharDelimiters(this string text, char charDelimiter, out decimal extractedNumber, out string textAfter)
        {
            extractedNumber = 0;
            var extractedText = text.ExtractTextBetweenFirstTwoCharDelimiters(charDelimiter, out textAfter);

            if (decimal.TryParse(extractedText, out extractedNumber))
            {
                return true;
            }

            return false;
        }

        public static string ToCommaDelimited(this List<string> items)
        {
            if (items == null || items.Count == 0)
            {
                return string.Empty;
            }

            if (items.Count == 1)
            {
                return items[0];
            }

            string returnText = string.Empty;

            foreach (string item in items)
            {
                returnText += item.ToTrimmed() + ", ";
            }

            return returnText.Substring(0, returnText.Length - 2);
        }

        /// <summary>
        /// Handles extracting all the text parts after a paticular text part.
        /// E.g extract all text parts after Claim No:
        /// Lorem ipsum dolor Claim No: 12345 sit amet, Claim No: 54321 consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        /// should extract 12345 and 54321
        /// </summary>
        /// <param name="text"></param>
        /// <param name="textBefore"></param>
        /// <returns></returns>
        public static List<string> ExtractNextTextPartsAfter(this string text, string textPartBefore)
        {
            // Takes text and splits by textBeforePart
            // Start of each array part after first will contain parts to return.
            var textItems = text.Split(new string[] { textPartBefore }, StringSplitOptions.None);
            List<string> textItemsAfter = new List<string>();

            for (int partIndex = 1; partIndex < textItems.Count(); partIndex++)
            {
                var nextPart = textItems[partIndex].FirstNonEmptySpacePart().ToTrimmed();

                if (!string.IsNullOrEmpty(nextPart))
                {
                    textItemsAfter.Add(nextPart);
                }
            }

            // return all extracted text parts.
            return textItemsAfter;
        }

        public static string ExtractEverythingAfter(this string text, string textPartBefore, bool stopAtLineBreak = true)
        {
            // Takes text and splits by textBeforePart
            // Start of each array part after first will contain parts to return.
            var textItems = text.Split(new string[] { textPartBefore }, StringSplitOptions.None);

            if (text.HasText() && textPartBefore.HasText() && text.Contains(textPartBefore))
            {
                var startIndex = text.IndexOf(textPartBefore) + textPartBefore.Length;
                var textAfter = text.Substring(startIndex, text.Length - startIndex);

                // Check if only want text on same line not rest of text.
                if (stopAtLineBreak && textAfter.Contains(Environment.NewLine))
                {
                    var parts = textAfter.Split(new string[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);

                    return parts[0];
                }
                else
                {
                    // return all the rest of text after
                    return textAfter;
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Returns the number of times a text part appears in a text string
        /// </summary>
        /// <param name="text"></param>
        /// <param name="toContain"></param>
        /// <returns></returns>
        public static int ContainsCount(this string text, string toContain)
        {
            // Takes text and splits by textBeforePart
            // Start of each array part after first will contain parts to return.
            var textItems = text.Split(new string[] { toContain }, StringSplitOptions.None);

            return textItems.Count() - 1;
        }

        /// <summary>
        /// Splits text by " " and returns first part that is not empty space.
        /// E.G. '    Lorem ipsum dolor sit amet, consectetur adipiscing elit'
        /// return Lorem
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string FirstNonEmptySpacePart(this string text)
        {
            var splitParts = text.Split(" ".ToCharArray());

            foreach (string part in splitParts)
            {
                var trimmedPart = part.ToTrimmed();
                if (!string.IsNullOrEmpty(trimmedPart))
                {
                    return trimmedPart;
                }
            }

            return string.Empty;
        }

        public static bool IsValidEmail(this string mail)
        {
            if (string.IsNullOrEmpty(mail)) return false;
            try
            {
                System.Net.Mail.MailAddress validationEmail = new System.Net.Mail.MailAddress(mail);
            }
            catch (FormatException)
            {
                return false;
            }
            return true;
        }

        public static IEnumerable<string> SplitOnLength(this string input, int length)
        {
            int index = 0;
            while (index < input.Length)
            {
                if (index + length < input.Length)
                    yield return input.Substring(index, length);
                else
                    yield return input.Substring(index);

                index += length;
            }
        }

        public static string EscapeiCalChars(this string input)
        {
            return input.Replace(",", @"\,")
                        .Replace(";", @"\;")
                        .Replace("\\", "\\\\")
                        .Replace("\n", "\\n")
                        .Replace("\\N", "\\n");
        }

        public static string RemoveNonAlphabethicalCharacters(this string str)
        {
            Regex rgx = new Regex("[^a-zA-Z0-9]");
            return rgx.Replace(str, "");
        }

        public static string ZipMe(this string inputStr)
        {
            byte[] inputBytes = Encoding.UTF8.GetBytes(inputStr);
            string outputbase64 = String.Empty;
            using (var outputStream = new MemoryStream())
            {
                using (var gZipStream = new GZipStream(outputStream, CompressionMode.Compress))
                    gZipStream.Write(inputBytes, 0, inputBytes.Length);
                var outputBytes = outputStream.ToArray();
                outputbase64 = Convert.ToBase64String(outputBytes);
            }
            return outputbase64;
        }

        public static string UnzipMe(this string inputStr)
        {
            string decompressed = String.Empty;
            byte[] inputBytes = Convert.FromBase64String(inputStr);
            using (var inputStream = new MemoryStream(inputBytes))
            using (var gZipStream = new GZipStream(inputStream, CompressionMode.Decompress))
            using (var streamReader = new StreamReader(gZipStream))
            {
                decompressed = streamReader.ReadToEnd();
            }
            return decompressed;
        }

        public static string ToBaseSystem(this string inputStr, int radix = 36)
        {
            long decimalNumber = 0;
            if (!long.TryParse(inputStr, out decimalNumber)) { return "0"; }

            const int BitsInLong = 64;
            const string Digits = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            //The radix must be >= 2 and <= Digits.Length
            if (radix < 2 || radix > Digits.Length) { return "0"; }

            if (decimalNumber == 0) { return "0"; }

            int index = BitsInLong - 1;
            long currentNumber = Math.Abs(decimalNumber);
            char[] charArray = new char[BitsInLong];

            while (currentNumber != 0)
            {
                int remainder = (int)(currentNumber % radix);
                charArray[index--] = Digits[remainder];
                currentNumber = currentNumber / radix;
            }

            string result = new String(charArray, index + 1, BitsInLong - index - 1);
            if (decimalNumber < 0) { result = "-" + result; }

            return result;
        }

        /// <summary>
        /// Trims a string checks again null or empty string first.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string ToTrimmed(this string s)
        {
            if (!string.IsNullOrEmpty(s))
            {
                return s.Trim();
            }

            return s;
        }

        public static string GenerateRandomString(this string inputStr, int size)
        {
            Random random = new Random((int)DateTime.Now.Ticks);//thanks to McAden
            StringBuilder builder = new StringBuilder();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            return builder.ToString();
        }

        public static string TruncateWithEllipsis(this string s, int? maxLength = 100)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }


            if (!maxLength.HasValue)
            {
                maxLength = 100;
            }

            if (s.Length < maxLength)
            {
                return s;
            }

            return s.Substring(0, maxLength.GetValueOrDefault()) + "...";
        }

        public static string ToShortVersion(this string s)
        {
            string[] parts = s.Split('.');

            if (parts.Length >= 2 && parts.Length <= 4)
            {
                if (parts[0].Length == 1)
                {
                    parts[0] = " " + parts[0];
                }
                return parts[0] + '.' + parts[1];
            }

            return string.Empty;
        }

        public static bool IsValidShortVersion(this string s)
        {
            if (!string.IsNullOrEmpty(s))
            {
                string[] parts = s.Split('.');

                if (parts.Length == 2)
                {
                    return parts[0].IsValidShortVersionPart() &&
                          parts[1].IsValidShortVersionPart();
                }
            }

            return false;
        }

        public static bool IsValidShortVersionPart(this string s)
        {
            if (!string.IsNullOrEmpty(s))
            {
                s = s.Trim();

                try
                {
                    var partValue = int.Parse(s);

                    return partValue >= 0 && partValue <= 99;
                }
                catch
                {
                    return false;
                }
            }

            return false;
        }

        public static bool HasText(this string s)
        {
            return !string.IsNullOrEmpty(s);
        }

        public static HtmlString LineBreaksToBr(this string s)
        {
            if (!string.IsNullOrWhiteSpace(s))
            {
                return new HtmlString(s.Replace(System.Environment.NewLine, "<br/>"));
            }

            return new HtmlString(string.Empty);
        }


        public static decimal ToDecimal(this string s, decimal valueIfNull = -1)
        {
            if (!string.IsNullOrEmpty(s))
            {
                decimal returnValue;
                if (decimal.TryParse(s, out returnValue))
                {
                    return returnValue;
                }
            }

            return valueIfNull;
        }

        public static bool IsLetter(this char c)
        {
            string pattern = @"^[a-zA-Z]+$";
            Regex regex = new Regex(pattern);

            // Compare a string against the regular expression
            return regex.IsMatch(c.ToString());
        }

        /// <summary>
        /// Returns the first <paramref name="count"/> characters of a string, or the entire string if it
        /// is less than <paramref name="count"/> characters long.
        /// </summary>
        /// <param name="self">The string. Cannot be null.</param>
        /// <param name="count">The number of characters to return.</param>
        /// <returns>The first <paramref name="count"/> characters of the string.</returns>

        public static string GetFirstNChars(this string self, int count)
        {
            Contract.Requires(self != null);
            Contract.Requires(count >= 0);

            Contract.Ensures(Contract.Result<string>() != null);
            // ReSharper disable PossibleNullReferenceException
            Contract.Ensures(Contract.Result<string>().Length <= count);
            // ReSharper restore PossibleNullReferenceException

            if (self.Length <= count)
                return self;
            else
                return self.Substring(0, count);
        }

        /// <summary>
        /// Returns the last <paramref name="count"/> characters of a string, or the entire string if it
        /// is less than <paramref name="count"/> characters long.
        /// </summary>
        /// <param name="self">The string. Cannot be null.</param>
        /// <param name="count">The number of characters to return.</param>
        /// <returns>The last <paramref name="count"/> characters of the string.</returns>

        public static string GetLastNChars(this string self, int count)
        {
            Contract.Requires(self != null);
            Contract.Requires(count >= 0);

            Contract.Ensures(Contract.Result<string>() != null);
            // ReSharper disable PossibleNullReferenceException
            Contract.Ensures(Contract.Result<string>().Length <= count);
            // ReSharper restore PossibleNullReferenceException

            if (self.Length <= count)
                return self;
            else
                return self.Substring(self.Length - count, count);
        }

        public static string ToTextOrNull(this string self)
        {
            if (string.IsNullOrEmpty(self))
            {
                return null;
            }

            return self;
        }

        public static int? ToIntOrNull(this string self)
        {
            if (!string.IsNullOrEmpty(self))
            {
                int result;

                if (int.TryParse(self, out result))
                {
                    return result;
                }
            }

            return null;
        }

        public static string ToAllowedOrNull(this string self, string allowed)
        {
            if (string.IsNullOrEmpty(self))
            {
                return null;
            }

            if (self.ToLower() == allowed)
            {
                return allowed;
            }

            return null;
        }
    }
}
