﻿using System.Globalization;

namespace Gensolve.Physio.Server.Service.Utility
{
    public static class ExtensionMethods
    {
        public enum DifferenceValue
        {
            MilliSeconds,
            Seconds,
            Minutes,
            Hours,
            Days,
            Weeks,
            Months,
            Years
        }
        public static string ProperCase(this string sourceString)
        {
            if (string.IsNullOrWhiteSpace(sourceString)) return "";
            var valuearray = sourceString.ToLower().ToCharArray();
            var nextupper = true;
            for (var i = 0; i < valuearray.Length - 1; i++)
            {
                if (nextupper)
                {
                    valuearray[i] = char.Parse(valuearray[i].ToString(CultureInfo.InvariantCulture).ToUpper());
                    nextupper = false;
                }
                else
                {
                    switch (valuearray[i])
                    {
                        case ' ':
                        case '(':
                        case ')':
                        case '[':
                        case ']':
                        case '-':
                        case '.':
                        case ':':
                        case '\n':
                            {
                                nextupper = true;
                                break;
                            }
                        default:
                            {
                                nextupper = false;
                                break;
                            }
                    }
                }
            }
            return new string(valuearray);
        }
    }
}