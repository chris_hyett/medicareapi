﻿using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared.Claims.Responses;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Responses.Shared;
using RestSharp;
using System;
using System.Linq;


namespace Gensolve.Physio.Server.Service.MedicareOnline.ResponseHandlers
{
    public class BaseResponseHandler
    {
        public bool WasOK { get; set; } = false;

        public ServiceMessageResponse ServiceMessageResponse { get;set;}

        public ErrorResponse ErrorResponse { get; set; }

        public AuthenticationError AuthenticationError { get; set; }

        protected string GetTransactionId(IRestResponse response)
        {
            return GetHeaderValue(MedicareResponseHeaderKeys.TransactionIdKey, response);
        }

        protected string GetHeaderValue(string key, IRestResponse response)
        {
            var item = response.Headers.FirstOrDefault(e => e.Name == key);

            if (item != null)
            {
                return item.Value.ToString();
            }

            return string.Empty;
        }


        protected void SaveLog(string cardNumber, IRestResponse response)
        {
        }

    }
}
