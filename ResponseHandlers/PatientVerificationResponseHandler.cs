﻿using Gensolve.MedicareOnline;
using Gensolve.MedicareOnline.Interfaces;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Responses;
using Newtonsoft.Json;
using RestSharp;
using System;

namespace Gensolve.Physio.Server.Service.MedicareOnline.ResponseHandlers
{
    /// <summary>
    /// Defines class for parsing responses from calls to Medicare Patient Verification web service
    /// </summary>
    public class PatientVerificationResponseHandler : BaseResponseHandler, IMedicareOnlineVerificationResponseHandler
    {
        #region Members
        public PatientVerificationRequest patientVerificationRequest { get; set; }
        /// <summary>
        /// Strongly typed object from response.
        /// </summary>
        private PatientVerificationResponse patientVerificationResponse;
        private IRestResponse response;
        #endregion

        #region IMedicareOnlineResponseHandler
        /// <summary>
        /// Handles processing the response from the call and populating contents into patientVerificationResponse if 
        /// call was 200 OK
        /// </summary>
        /// <param name="response"></param>
        public void ProcessResponse(IRestResponse response)
        {
            this.response = response;

            if (response != null)
            {
                this.WasOK = response.StatusCode == System.Net.HttpStatusCode.OK && response.Content.ToLower().Contains("medicareStatus".ToLower());
                if (WasOK)
                {
                    patientVerificationResponse = JsonConvert.DeserializeObject<PatientVerificationResponse>(response.Content);
                }
                patientVerificationRequest = JsonConvert.DeserializeObject<PatientVerificationRequest>(response.Request.Body.Value.ToString());
                SaveLog(patientVerificationRequest.Patient.Medicare.MemberNumber, response);
            }
        }

        /// <summary>
        /// Handles parsing details from patientVerificationResponse to create a report for the user
        /// in the front end.
        /// </summary>
        /// <param name="config"></param>
        /// <returns>Report  on patient verification results.</returns>
        public string GenerateReport(IConfigurationManagerSettings config)
        {
            string report = string.Empty;

            if (patientVerificationResponse != null)
            {
                var MedicareStatusCode = string.Empty;

                if (patientVerificationResponse.MedicareStatus != null)
                {
                    MedicareStatusCode = patientVerificationResponse.MedicareStatus.Status.Code.ToString();
                    if (!string.IsNullOrEmpty(MedicareStatusCode))
                    {
                        if (MedicareStatusCode.Trim() == "0")
                        { report = report + "\nPatient Eligible for Medicare."; }
                        report = report + "\nMedicare Status Code: " + MedicareStatusCode;
                        if (MedicareStatusCode.Trim() != "0")
                        {
                            var MedicareErrorText = config.GetErrorMessage(int.Parse(MedicareStatusCode));
                            if (MedicareErrorText != null && MedicareErrorText.Trim().Length > 0)
                                report = report + "\n[ " + MedicareErrorText + " ]";
                        }
                    }
                }
                

                if(patientVerificationResponse.HealthFundStatus != null)
                {
                    var ConcessionStatusCode = patientVerificationResponse.HealthFundStatus.Status.Code.ToString();
                    if (ConcessionStatusCode != null)
                    {
                        if (ConcessionStatusCode.Trim() == "0")
                        { report = report + "\nPatient Eligible for Concession."; }
                        report = report + "\nConcession Status Code: " + ConcessionStatusCode;
                        MedicareStatusCode = patientVerificationResponse.HealthFundStatus.Status.Code.ToString();
                        if (MedicareStatusCode != "0")
                        {
                            var MedicareConcessionErrorText = config.GetErrorMessage(int.Parse(MedicareStatusCode));
                            if (MedicareConcessionErrorText != null && MedicareConcessionErrorText.Trim().Length > 0)
                                report = report + "\n[ " + MedicareConcessionErrorText + " ]";
                        }
                    }
                }

                if (patientVerificationResponse.MedicareStatus != null)
                {
                    if(patientVerificationResponse.MedicareStatus.CurrentMember != null)
                    {
                        var CurrentPatientFirstName = patientVerificationResponse.MedicareStatus.CurrentMember.GivenName;
                        if (CurrentPatientFirstName != null)
                            report = report + "\n\nCurrent Patient First Name: " + CurrentPatientFirstName;
                    }
                }

                if(patientVerificationResponse.MedicareStatus.CurrentMembership != null)
                {
                    var CurrentPatientMedicareCardNum = patientVerificationResponse.MedicareStatus.CurrentMembership.MemberNumber;
                    var CurrentPatientReferenceNum = patientVerificationResponse.MedicareStatus.CurrentMembership.MemberRefNumber;


                    if (CurrentPatientMedicareCardNum != null)
                        report = report + "\nCurrent Patient Medicare Card Num: " + CurrentPatientMedicareCardNum;
                    if (CurrentPatientReferenceNum != null)
                        report = report + "\nCurrent Patient Reference Num: " + CurrentPatientReferenceNum;
                }

            }
            return report;
        }
        #endregion
    }
}
