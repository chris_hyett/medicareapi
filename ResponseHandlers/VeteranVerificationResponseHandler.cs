﻿using Gensolve.MedicareOnline;
using Gensolve.MedicareOnline.Interfaces;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Responses;
using Newtonsoft.Json;
using RestSharp;

namespace Gensolve.Physio.Server.Service.MedicareOnline.ResponseHandlers
{
    /// <summary>
    /// Defines class for parsing responses from calls to Medicare Patient Verification web service
    /// </summary>
    public class VeteranVerificationResponseHandler : BaseResponseHandler, IMedicareOnlineVerificationResponseHandler
    {
        #region Members
        public VeteranVerificationRequest dvaVerificationRequest { get; set; }
        /// <summary>
        /// Strongly typed object from response.
        /// </summary>
        private DvaVerificationResponse dvaVerificationResponse;
        private IRestResponse response;
        #endregion

        #region IMedicareOnlineResponseHandler
        /// <summary>
        /// Handles processing the response from the call and populating contents into patientVerificationResponse if 
        /// call was 200 OK
        /// </summary>
        /// <param name="response"></param>
        public void ProcessResponse(IRestResponse response)
        {
            this.response = response;

            if(response != null)
            {
                this.WasOK = response.StatusCode == System.Net.HttpStatusCode.OK && response.Content.ToLower().Contains("veteranStatus".ToLower());

                if (WasOK)
                {
                    dvaVerificationResponse = JsonConvert.DeserializeObject<DvaVerificationResponse>(response.Content);
                }
                dvaVerificationRequest = JsonConvert.DeserializeObject<VeteranVerificationRequest>(response.Request.Body.Value.ToString());
                SaveLog(dvaVerificationRequest.Patient.VeteranMembership.VeteranNumber, response);
            }
        }

        /// <summary>
        /// Handles parsing details from patientVerificationResponse to create a report for the user
        /// in the front end.
        /// </summary>
        /// <param name="config"></param>
        /// <returns>Report  on patient verification results.</returns>
        public string GenerateReport(IConfigurationManagerSettings config)
        {
            var report = string.Empty;

            if (dvaVerificationResponse != null && WasOK)
            {
                var currentPatientFamilyName = dvaVerificationRequest.Patient.Identity.FamilyName;
                var currentPatientFirstName = dvaVerificationRequest.Patient.Identity.GivenName;
                var currentVeteranFileNum = dvaVerificationResponse.VeteranStatus.CurrentMembership.VeteranNumber;
                var veteranEntitlementCode = dvaVerificationResponse.VeteranStatus.CurrentMembership.EntitlementCode;

                report = (dvaVerificationResponse.VeteranStatus.Status.Code == 0) ? "Patient Eligible for Medicare." : dvaVerificationResponse.VeteranStatus.Status.Text;

                report = report + "\nStatus Code: " + dvaVerificationResponse.VeteranStatus.Status.Code.ToString();

                if (currentPatientFirstName != null)
                    report = report + "\n\nCurrent Patient First Name: " + currentPatientFirstName;
                if (currentPatientFamilyName != null)
                    report = report + "\nCurrent Patient Family Name: " + currentPatientFamilyName;
                if (currentVeteranFileNum != null)
                    report = report + "\nCurrent Veteran File Num: " + currentVeteranFileNum;
                if (veteranEntitlementCode != null)
                    report = report + "\nCurrent Entitlement Code: " + veteranEntitlementCode;
            }

            return report;
        }
        #endregion

    }
}
