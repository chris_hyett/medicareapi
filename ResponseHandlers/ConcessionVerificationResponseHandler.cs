﻿using Gensolve.MedicareOnline;
using Gensolve.MedicareOnline.Interfaces;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Responses;
using Newtonsoft.Json;
using RestSharp;

namespace Gensolve.Physio.Server.Service.MedicareOnline.ResponseHandlers
{
    public class ConcessionVerificationResponseHandler : BaseResponseHandler, IMedicareOnlineVerificationResponseHandler
    {
        #region Members
        public ConcessionVerificationRequest concessionVerificationRequest { get; set; }
        /// <summary>
        /// Strongly typed object from response.
        /// </summary>
        private ConcessionVerificationResponse concessionVerificationResponse;
        private IRestResponse response;
        #endregion

        #region IMedicareOnlineResponseHandler
        /// <summary>
        /// Handles processing the response from the call and populating contents into patientVerificationResponse if 
        /// call was 200 OK
        /// </summary>
        /// <param name="response"></param>
        public void ProcessResponse(IRestResponse response)
        {
            this.response = response;

   
            if (response != null)
            {
                this.WasOK = response.StatusCode == System.Net.HttpStatusCode.OK && response.Content.ToLower().Contains("concessionStatus".ToLower());

                if (WasOK)
                {
                    concessionVerificationResponse = JsonConvert.DeserializeObject<ConcessionVerificationResponse>(response.Content);
                }
                concessionVerificationRequest = JsonConvert.DeserializeObject<ConcessionVerificationRequest>(response.Request.Body.Value.ToString());
                SaveLog(concessionVerificationRequest.Patient.Medicare.MemberNumber, response);
            }

        }

        public string GenerateReport(IConfigurationManagerSettings config)
        {
            var opvReport = new PatientVerificationReport();

            if (concessionVerificationResponse != null && WasOK)
            {
                opvReport.ConcessionStatusCode = concessionVerificationResponse.ConcessionStatus.Status.Code.ToString();
                if (concessionVerificationResponse.ConcessionStatus.Status.Code != 0) 
                    opvReport.MedicareConcessionErrorText = concessionVerificationResponse.ConcessionStatus.Status.Text;

                opvReport.CurrentPatientFirstName = concessionVerificationRequest.Patient.Identity.GivenName;
                opvReport.CurrentPatientMedicareCardNum = concessionVerificationRequest.Patient.Medicare.MemberNumber;
                opvReport.CurrentPatientReferenceNum = concessionVerificationRequest.Patient.Medicare.MemberRefNumber;

                if (concessionVerificationResponse.MedicareStatus != null)
                {
                    opvReport.MedicareStatusCode = concessionVerificationResponse.MedicareStatus.Status.Code.ToString();
                    if (concessionVerificationResponse.MedicareStatus.Status.Code != 0)
                        opvReport.MedicareErrorText = concessionVerificationResponse.MedicareStatus.Status.Text;
                }
            }

            return opvReport.ToString();
        }
        #endregion
    }
}
