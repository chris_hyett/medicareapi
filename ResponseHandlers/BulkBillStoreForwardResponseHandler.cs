﻿using Gensolve.MedicareOnline.Interfaces;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Responses;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Responses.Shared;
using Newtonsoft.Json;
using RestSharp;

namespace Gensolve.Physio.Server.Service.MedicareOnline.ResponseHandlers
{
    public class BulkBillStoreForwardResponseHandler : BaseResponseHandler, IMedicareOnlineClaimResponseHandler
    {
        #region Members
        public BulkBillStoreForwardRequest bulkBillStoreForwardRequest { get; set; }
        /// <summary>
        /// Strongly typed object from response.
        /// </summary>
        private BulkBillStoreForwardResponse bulkBillStoreForwardResponse;
        private IRestResponse response;
        #endregion


        public void ProcessResponse(IRestResponse response)
        {
            this.response = response;

            if (response != null)
            {
                this.WasOK = response.StatusCode == System.Net.HttpStatusCode.OK && response.Content.ToLower().Contains("claimId".ToLower());

                if (WasOK)
                {
                    bulkBillStoreForwardResponse = JsonConvert.DeserializeObject<BulkBillStoreForwardResponse>(response.Content);
                }
                else
                {
                    if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                    {
                        AuthenticationError = JsonConvert.DeserializeObject<AuthenticationError>(response.Content);
                    }
                    else
                    {
                        ServiceMessageResponse = JsonConvert.DeserializeObject<ServiceMessageResponse>(response.Content);
                    }
                }
                bulkBillStoreForwardRequest = JsonConvert.DeserializeObject<BulkBillStoreForwardRequest>(response.Request.Body.Value.ToString());
               // SaveLog(bulkBillStoreForwardRequest.Claim.MedicalEvent..Medicare.MemberNumber, response);
            }
        }

        public IEsaResponse GenerateResult()
        {
            IEsaResponse result = new EsaResponse();

            var statusCode = this.response.StatusCode.ToString();
            var responseContent = "";

            if (this.ServiceMessageResponse != null)
            {
                statusCode =  this.ServiceMessageResponse.ErrorCode();
                responseContent += this.ServiceMessageResponse.ToMessageText();
            }

            if(this.AuthenticationError != null)
            {
                responseContent += this.AuthenticationError.Message;
            }

            if (this.ErrorResponse != null)
            {
                responseContent += this.ErrorResponse.Text;
            }

            result.XmlResponse = responseContent;
            result.TransactionId = GetTransactionId(response);
            result.XmlRequest = response.Request.Body.Value.ToString();
            result.StatusCode = statusCode;

            return result;
        }
    }
}
