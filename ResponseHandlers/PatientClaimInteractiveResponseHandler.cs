﻿using Gensolve.MedicareOnline.Interfaces;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Responses;
using Newtonsoft.Json;
using RestSharp;

namespace Gensolve.Physio.Server.Service.MedicareOnline.ResponseHandlers
{
    public class PatientClaimInteractiveResponseHandler : BaseResponseHandler, IMedicareOnlineClaimResponseHandler
    {

        #region Members
        public PatientClaimInteractiveRequest patientClaimInteractiveRequest { get; set; }
        /// <summary>
        /// Strongly typed object from response.
        /// </summary>
        private PatientClaimInteractiveResponse patientClaimInteractiveResponse;
        private IRestResponse response;
        #endregion


        public void ProcessResponse(IRestResponse response)
        {
            this.response = response;

            if (response != null)
            {
                this.WasOK = response.StatusCode == System.Net.HttpStatusCode.OK && response.Content.ToLower().Contains("claimAssessment".ToLower());

                if (WasOK)
                {
                    patientClaimInteractiveResponse = JsonConvert.DeserializeObject<PatientClaimInteractiveResponse>(response.Content);
                }
                patientClaimInteractiveRequest = JsonConvert.DeserializeObject<PatientClaimInteractiveRequest>(response.Request.Body.Value.ToString());
                SaveLog(patientClaimInteractiveRequest.PatientClaimInteractive.Patient.Medicare.MemberNumber, response);
            }
        }

        public IEsaResponse GenerateResult()
        {
            IEsaResponse result = new EsaResponse();

            result.XmlResponse = string.Format("RESPONSE-STATUSCODE:{0} {1}", this.response.StatusCode, this.WasOK ? "RESPONSE-CONTENT: " + result.Content : "");
            result.TransactionId = GetTransactionId(response);
            result.XmlRequest = response.Request.Body.Value.ToString();
            result.StatusCode = this.response.StatusCode.ToString();

            return result;
        }
    }
}
