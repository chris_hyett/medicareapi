﻿using FluentValidation;
using FluentValidation.Validators;
using Gensolve.Physio.Server.Service.MedicareOnline.Extensions;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using static Gensolve.Physio.Server.Service.Utility.ExtensionMethods;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Validators
{
    #region Custom Validator Class static wrapper extention methods
    public static partial class MedicareOnlineCustomValidations
    {

        public static List<int> GenderValueList = new List<int> { 1, 2, 3, 9 };

        //Validations
        public static bool ThrowFail<T>(T arg)
            => false;

        public static bool FamilyNameLength(string s)
            => s.Length >= 1 && s.Length <= 40;

        public static bool IsFutureDate(DateTime? date)
            => !date.HasValue || !(date > DateTime.Today);

        public static bool PostCodeIsValid(string s)
            => string.IsNullOrWhiteSpace(s) == true || !s.Split(new char[] { ' ' }).Where(a => new List<string> { "NSW", "VIC", "QLD", "ACT", "SA", "NT", "WA", "TAS", "NSW.", "VIC.", "QLD.", "ACT.", "SA.", "NT.", "WA.", "TAS", "N.S.W.", "V.I.C.", "Q.L.D.", "A.C.T.", "S.A.", "N.T.", "W.A.", "T.A.S.", "N.S.W", "V.I.C", "Q.L.D", "A.C.T", "S.A", "N.T", "W.A", "T.A.S" }.Any(b => b.Contains(a))).Any();

        public static IRuleBuilderOptions<T, TProperty> DateIsNotGreaterThan<T, TProperty>(this IRuleBuilder<T, TProperty> ruleBuilder, int periodLength, DifferenceValue periodType = DifferenceValue.Years)
            => ruleBuilder.SetValidator(new DateValueIsNotGreaterThanDate<T, TProperty>(DateTime.Now, periodLength, periodType));

        public static IRuleBuilderOptions<T, TProperty> CompareDateIsNotGreaterThan<T, TProperty>(this IRuleBuilder<T, TProperty> ruleBuilder, DateTime CompareDate, int periodLength, DifferenceValue periodType = DifferenceValue.Years)
            => ruleBuilder.SetValidator(new DateValueIsNotGreaterThanDate<T, TProperty>(CompareDate, periodLength, periodType));

        public static IRuleBuilderOptions<T, TProperty> StringLengthBetween<T, TProperty>(this IRuleBuilder<T, TProperty> ruleBuilder, int min, int max)
            => ruleBuilder.SetValidator(new StringLengthBetweenValidator<T, TProperty>(min, max));

        public static IRuleBuilderOptions<T, TProperty> IsValueInList<T, TProperty>(this IRuleBuilder<T, TProperty> ruleBuilder, List<int> Values)
            => ruleBuilder.SetValidator(new IsValueInListValidator<T, TProperty>(Values));

        public static IRuleBuilderOptions<T, TProperty> IsNumeric<T, TProperty>(this IRuleBuilder<T, TProperty> ruleBuilder)
            => ruleBuilder.SetValidator(new ValueIsNumericValidator<T, TProperty>());

        public static IRuleBuilderOptions<T, IList<TElement>> ListMustContainFewerThan<T, TElement>(this IRuleBuilder<T, IList<TElement>> ruleBuilder, int num)
            => ruleBuilder.SetValidator(new ListCountValidator<T, TElement>(num));

        public static IRuleBuilderOptions<T, TProperty> ClaimIsConsistent<T, TProperty>(this IRuleBuilder<T, TProperty> ruleBuilder, string stringUrl)
            => ruleBuilder.SetValidator(new ClaimIsConsistentValidator<T, TProperty>(stringUrl));

        public static bool MedicareMemberNumberIsValid(string s)
        {

            if (string.IsNullOrWhiteSpace(s) || s.Length < 10 || s.Substring(9, 1).StartsWith("0")) { return false; }

            var a = s.ToCharArray();
            int result = 0;
            for (int i = 0; i < 8; i++)
            {
                result += calcMedicareNumber(a[i].ToString(), i + 1);
            }
            var checkDigit = result % 10;
            var providedCheckDigit = int.Parse(s.Substring(8, 1));
            return providedCheckDigit == checkDigit;
        }

        public static bool VeteranNumberIsValid(string s)
        {
            if (string.IsNullOrWhiteSpace(s))
                return false;

            s = s.TrimEnd().ToUpper();

            if (s.Length < 3
                || s.StartsWith(" ")
                || s.Length == 9
                && char.IsNumber(Convert.ToChar(s.Substring(8, 1))))
                return false;

            var stateCodeValid = s.Take(1).Where(a => new List<string> { "N", "V", "Q", "W", "S", "T" }.Any(b => b.Contains(a))).Any();
            if (!stateCodeValid)
                return false;

            var warCodeTmp = s.Skip(1).Take(3).ToList();
            switch (warCodeTmp.Count)
            {
                case 3:
                    if (warCodeTmp[1] == ' ' || warCodeTmp[2] == ' ')
                    {
                        return false;
                    }
                    break;
                case 2:
                    if (warCodeTmp[1] == ' ')
                    {
                        return false;
                    }
                    break;
                default:
                    break;
            }
            var idx = 0;
            foreach (var item in warCodeTmp)
            {
                if (char.IsNumber(item)) { break; }

                idx++;
            }
            var warCode = s.Substring(1, idx);

            if (warCode.LastIndexOf(" ") != -1)
                return false;

            var validWarCodes = new List<string> {
                 " ",
                 "A",
                 "AGX",
                 "BUR",
                 "BW",
                 "CGW",
                 "CN",
                 "CNK",
                 "CNS",
                 "CNX",
                 "FIJ",
                 "GHA",
                 "GW",
                 "HKS",
                 "IND",
                 "IV",
                 "KM",
                 "KYA",
                 "MAL",
                 "MAU",
                 "MLS",
                 "MTX",
                 "MWI",
                 "NF",
                 "NG",
                 "NGR",
                 "NK",
                 "NRD",
                 "NSS",
                 "PK",
                 "RD",
                 "RDX",
                 "SA",
                 "SAX",
                 "SM",
                 "SR",
                 "SS",
                 "SWP",
                 "X"};

            return validWarCodes.Where(a => warCode.Equals(a)).Any();
        }

        private static int calcMedicareNumber(string s, int i)
        {
            int v = int.Parse(s);
            switch (i)
            {
                case 1:
                case 5:
                    {
                        return v;
                    }
                case 2:
                case 6:
                    {
                        return v * 3;
                    }
                case 3:
                case 7:
                    {
                        return v * 7;
                    }
                case 4:
                case 8:
                    {
                        return v * 9;
                    }
                default:
                    {
                        break;
                    }
            }
            return 0;

        }

        #endregion

        #region Custom Validator Class Definitions
        public class ValueIsNumericValidator<T, TProperty> : PropertyValidator<T, TProperty>
        {
            public ValueIsNumericValidator() { }
            public override string Name
                => "ValueIsNumericValidator";
            public override bool IsValid(ValidationContext<T> context, TProperty value)
                => int.TryParse(value.ToString(), out _);
            protected override string GetDefaultMessageTemplate(string errorCode)
                => "'{PropertyValue}' is not a numeric value."; //Change this
        }
        public class StringLengthBetweenValidator<T, TProperty> : PropertyValidator<T, TProperty>
        {
            private int _min;
            private int _max;
            public StringLengthBetweenValidator(int min = 1, int max = 1)
            {
                _min = min;
                _max = max;
            }
            public override string Name
                    => "StringLengthBetweenValidator";
            public override bool IsValid(ValidationContext<T> context, TProperty value)
            {
                context.MessageFormatter.AppendArgument("MinLength", _min);
                context.MessageFormatter.AppendArgument("MaxLength", _max);
                var str = value.ToString();
                return str.Length >= _min && str.Length <= _max;
            }
            protected override string GetDefaultMessageTemplate(string errorCode)
                => "'{PropertyName}' length less than {MinLength} or greater than {MaxLength}."; //Change this
        }
        public class IsValueInListValidator<T, TProperty> : PropertyValidator<T, TProperty>
        {
            private readonly List<int> _valuesList;
            public IsValueInListValidator(List<int> valuesList)
                => _valuesList = valuesList;
            public override string Name
                => "IsValueInListValidator";
            public override bool IsValid(ValidationContext<T> context, TProperty value)
                => _valuesList.Contains(Convert.ToInt32(value));
            protected override string GetDefaultMessageTemplate(string errorCode)
                => "'{PropertyName}' value '{PropertyValue} not valid."; //Change this
        }
        public class ListCountValidator<T, TCollectionElement> : PropertyValidator<T, IList<TCollectionElement>>
        {
            private int _max;
            public ListCountValidator(int max) { _max = max; }
            public override bool IsValid(ValidationContext<T> context, IList<TCollectionElement> list)
            {
                if (list != null && list.Count >= _max)
                {
                    context.MessageFormatter.AppendArgument("MaxElements", _max);
                    return false;
                }
                return true;
            }
            public override string Name
                => "ListCountValidator";
            protected override string GetDefaultMessageTemplate(string errorCode)
                => "{PropertyName} must contain fewer than {MaxElements} items. List contains {PropertyValue} items.";
        }
        public class DateValueIsNotGreaterThanDate<T, TProperty> : PropertyValidator<T, TProperty>
        {

            private int _periodLength = 0;
            private DifferenceValue _periodType = DifferenceValue.Years;
            private DateTime _d1 = DateTime.Now;
            public DateValueIsNotGreaterThanDate(DateTime Now, int periodLength, DifferenceValue periodType)
            {
                _periodLength = periodLength;
                _periodType = periodType;
                _d1 = Now;
            }
            public override string Name
                => "DateValueIsNotGreaterThanDate";
            public override bool IsValid(ValidationContext<T> context, TProperty value)
            {
                if (value == null) { return false; }
                var d2 = DateTime.Parse(value.ToString());
                int r = 0;

                switch (_periodType)
                {
                    case DifferenceValue.MilliSeconds:
                        r = Math.Abs(Convert.ToInt32((_d1 - d2).TotalMilliseconds));
                        break;
                    case DifferenceValue.Seconds:
                        r = Math.Abs(Convert.ToInt32((_d1 - d2).TotalSeconds));
                        break;
                    case DifferenceValue.Minutes:
                        r = Math.Abs(Convert.ToInt32((_d1 - d2).TotalMinutes));
                        break;
                    case DifferenceValue.Hours:
                        r = Math.Abs(Convert.ToInt32((_d1 - d2).TotalHours));
                        break;
                    case DifferenceValue.Days:
                        r = Math.Abs(Convert.ToInt32((_d1 - d2).TotalDays));
                        break;
                    case DifferenceValue.Weeks:
                        r = Math.Abs(Convert.ToInt32((_d1 - d2).TotalDays) / 7);
                        break;
                    case DifferenceValue.Months:

                        r = Math.Abs(12 * (_d1.Year - d2.Year) + _d1.Month - d2.Month);
                        break;
                    case DifferenceValue.Years:
                        r = Math.Abs(_d1.Year - d2.Year + 1);
                        break;
                    default:
                        break;
                }

                return r < _periodLength;

            }
            protected override string GetDefaultMessageTemplate(string errorCode)
                => "'{PropertyValue}' is not a date value.";
        }
        public class ClaimIsConsistentValidator<T, TProperty> : PropertyValidator<T, TProperty>
        {
            private string urlPath = "general";

            public ClaimIsConsistentValidator(string UrlPath) 
            {
                urlPath = UrlPath;
            }
            public override string Name => "ClaimIsConsistentValidator"; //Change this to class name

            public override bool IsValid(ValidationContext<T> context, TProperty value)
            {
                
                ClaimInteractive self = value as ClaimInteractive;

                if (urlPath.ToLower().Contains("general"))
                {
                    if (self.Referral != null
                    && self.ReferralOverrideCode.HasText()
                    && !self.MatchesGeneralServiceConditions())
                    {
                        context.AddFailure(ErrorMessage.Pciw_GeneralServiceNotValid);
                        return false;
                    }
                }
                if (urlPath.ToLower().Contains("specialist"))
                {
                    if (self.Referral == null
                    && self.ReferralOverrideCode.MatchesAtLeastOne(ReferralOverrideCodes.Hospital, ReferralOverrideCodes.Lost, ReferralOverrideCodes.Emergency, ReferralOverrideCodes.NotRequired)
                    && !self.MatchesSpecialistServiceConditions())
                    {
                        return false;
                    }
                }
                if (urlPath.ToLower().Contains("pathology"))
                {
                    if (self.Referral == null
                    && self.ReferralOverrideCode.ToLower() != ReferralOverrideCodes.NotRequired.ToLower()
                    && !self.MatchesPathologyServiceConditions())
                    {
                        return false;
                    }
                }
                return true;
            }
            protected internal string GetErrorMessage(string errorName)
            {
                return "";
            }
            protected override string GetDefaultMessageTemplate(string errorCode)
                => " "; 
        }
        #endregion
    }
}

/*
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Alternative child class validation
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
RuleFor(parent => parent.child.property).NotNull().When(parent => parent.child != null)

-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Custom Validator Definition Template
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
public class CustomActionValidator<T, TProperty> : PropertyValidator<T, TProperty>
{
    private object _arg; //Change type/name as required

    public CustomActionValidator(object arg) //Change type/name as required
    {
        _arg = arg;
    }
    public override string Name => "CustomActionValidator"; //Change this to class name

    public override bool IsValid(ValidationContext<T> context, TProperty value)
    {
        //Implement validation Logic
        //if ()
        //{
        //    context.MessageFormatter.AppendArgument("MessageHolderElementName", _arg); //Change message holder name as required
        //    return false;
        //}
        return true;
    }

    protected override string GetDefaultMessageTemplate(string errorCode)
        => "'{PropertyName}' message here"; //Change this
}

-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Example Template for Custom validators static wrapper
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
public static IRuleBuilderOptions<T, TProperty> CustomActionValidator<T, TProperty>(this IRuleBuilder<T, TProperty> ruleBuilder, object arg)
{
    return ruleBuilder.SetValidator(new CustomActionValidator<T, TProperty>(arg));
}

-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
An Alternate Simplistic aproach
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
public static IRuleBuilderOptionsConditions<T, IList<TElement>> ListMustContainFewerThan<T, TElement>(this IRuleBuilder<T, IList<TElement>> ruleBuilder, int num)
{

    return ruleBuilder.Custom((list, context) => {
        if (list.Count > num)
        {
            context.AddFailure("The list must contain 10 items or fewer");
        }
    });
}

*/