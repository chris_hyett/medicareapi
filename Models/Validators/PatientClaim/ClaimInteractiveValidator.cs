﻿using FluentValidation;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests;
using static Gensolve.Physio.Server.Service.MedicareOnline.Models.Validators.MedicareOnlineCustomValidations;
using static Gensolve.Physio.Server.Service.Utility.ExtensionMethods;
using System.Linq;
using System;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Validators
{
    public class ClaimInteractiveValidator : AbstractValidator<ClaimInteractive>
    {
        public ClaimInteractiveValidator()
        {
        
        }

    }
}
