﻿using FluentValidation;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Validators
{
    /// <summary>
    /// PICW = Patient Inline Claim Web Service
    /// </summary>
    public class PatientInlineClaimRequestValidator : AbstractValidator<PatientClaimInteractiveRequest>
    {
        public PatientInlineClaimRequestValidator(string urlString)
        {
            RuleFor(r => r.PatientClaimInteractive)
                .ClaimIsConsistent(urlString)
                    .WithErrorCode(ErrorCode.E2030)
                .SetValidator(new ClaimInteractiveValidator())
                ;

            
        }
    }
}
