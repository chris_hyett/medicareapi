﻿using FluentValidation;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared;
using static Gensolve.Physio.Server.Service.MedicareOnline.Models.Validators.MedicareOnlineCustomValidations;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Validators
{
    public class VerificationMedicareValidator : AbstractValidator<Medicare>
    {
        public VerificationMedicareValidator()
        {

            RuleFor(r => r.MemberNumber)
                .NotEmpty()
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.PtMedicareCardRequired)
                .NotNull()
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.PtMedicareCardRequired)
                .Length(10, 10)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.InvalidPtMedicareDetails)
                .Must(MedicareMemberNumberIsValid)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.InvalidPtMedicareDetails)
                ;

            RuleFor(r => r.MemberRefNumber)
                .NotEmpty()
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.PtMedicareIndividualNumberRequired)
                .NotNull()
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.PtMedicareIndividualNumberRequired)
                .Length(1, 1)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.InvalidPtMedicareIndividualNumber)
                .NotEqual("0")
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.InvalidPtMedicareIndividualNumber)
                .IsNumeric()
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.InvalidPtMedicareIndividualNumber)
                ;
        }
    }
}
