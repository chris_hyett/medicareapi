﻿using FluentValidation;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Validators
{
    public class VerificationHealthFundValidator : AbstractValidator<HealthFund>
    {
        public VerificationHealthFundValidator()
        {
            RuleFor(r => r.MemberNumber)
                .NotEmpty()
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.PtHealthFundNumberRequired)
                .NotNull()
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.PtHealthFundNumberRequired)
                .Length(1, 19)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.InvalidPtHealthFundNumber)
                .Matches(ValidationRegEx.OnlyAlphaNumeric)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.InvalidPtHealthFundNumber)
                ;

            RuleFor(r => r.MemberRefNumber)
                .NotEmpty()
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.InvalidPtHealthFundRefNumber)
                .NotNull()
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.InvalidPtHealthFundRefNumber)
                .Length(1, 2)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.InvalidPtHealthFundRefNumber)
                .Matches(ValidationRegEx.OnlyNumeric)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.InvalidPtHealthFundRefNumber)
                ;

            RuleFor(r => r.Organisation)
                .NotEmpty()
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.PtHealthFundOrgIdRequired)
                .NotNull()
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.PtHealthFundOrgIdRequired)
                .Length(3, 3)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.InvalidPtHealthFundOrgId)
                .Matches(ValidationRegEx.OnlyAlpha)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.InvalidPtHealthFundOrgId)
                ;
        }
    }
}
