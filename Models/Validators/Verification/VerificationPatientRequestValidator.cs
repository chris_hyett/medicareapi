﻿using FluentValidation;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests;
using static Gensolve.Physio.Server.Service.MedicareOnline.Models.Validators.MedicareOnlineCustomValidations;
using static Gensolve.Physio.Server.Service.Utility.ExtensionMethods;
using static Gensolve.PhysioData.Common.PhysioEnums;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Validators
{
    /// <summary>
    /// OPVW = Online Patient Verification Web Service
    /// </summary>
    public class VerificationPatientRequestValidator : AbstractValidator<PatientVerificationRequest>
    {
        private MedicarePatientValidatorType TypeCodeValue { get; set; }
        public VerificationPatientRequestValidator(MedicarePatientValidatorType typeCodeValue)
        {
            TypeCodeValue = typeCodeValue;

            if (TypeCodeValue != MedicarePatientValidatorType.OPV
            && TypeCodeValue != MedicarePatientValidatorType.PVM
            && TypeCodeValue != MedicarePatientValidatorType.PVF)
            {
                RuleFor(r => r)
                    .Must(ThrowFail)
                        .WithErrorCode(ErrorCode.E2030)
                        .WithMessage(ErrorMessage.InvalidTypeCode)
                    ;
            }

            RuleFor(r => r.TypeCode)
                .NotNull()
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.TypeCodeLength)
                .NotEmpty()
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.TypeCodeLength)
                .StringLengthBetween(3, 3)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.TypeCodeLength)
                ;

            When(r => r.DateOfService != null, () =>
            {
                RuleFor(r => r.DateOfService)
                    .Must(IsFutureDate)
                        .WithErrorCode(ErrorCode.E9202)
                        .WithMessage(ErrorMessage.DosFutureDate)
                    .DateIsNotGreaterThan(24, DifferenceValue.Months)
                        .WithErrorCode(ErrorCode.E9202)
                        .WithMessage(ErrorMessage.DosLt24Months)
                    ;
                RuleFor(r => r)
                    .Must(CompareDoS_DoBDates)
                        .WithErrorCode(ErrorCode.E2030)
                        .WithMessage(ErrorMessage.DosGtDob)
                    ;
            });

            RuleFor(r => r.Patient)
                .NotNull()
                .NotEmpty()
                .SetValidator(new VerificationPatientValidator(TypeCodeValue));
                ;
        }

        private bool CompareDoS_DoBDates(PatientVerificationRequest arg)
        {
            if (arg.Patient?.Identity == null || arg.Patient?.Identity?.DateOfBirth == null) return true;
            return arg.Patient.Identity.DateOfBirth <= arg.DateOfService;
        }
    }
}
