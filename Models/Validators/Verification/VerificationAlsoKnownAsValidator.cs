﻿using FluentValidation;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Validators
{
    public class VerificationAlsoKnownAsValidator : AbstractValidator<AliasIdentity>
    {
        public VerificationAlsoKnownAsValidator()
        {
            //Family Name
            RuleFor(r => r.FamilyName)
                .NotNull()
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.AliasFamilyNameNotSupplied)
                .NotEmpty()
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.AliasFamilyNameNotSupplied)
                .Length(1, 40)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.AliasFamilyNameLength)
                .Matches(ValidationRegEx.StartsWithAlpha)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.AliasFamilyNameValidation)
                .Matches(ValidationRegEx.AlphanumericNoDoubleSpace)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.AliasFamilyNameValidation)
                .Matches(ValidationRegEx.AlphanumericApostropheHyphen)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.AliasFamilyNameValidation)
                ;

            //GivenName            
            RuleFor(r => r.GivenName)
                .NotNull()
                    .WithErrorCode(ErrorCode.E2030)
                    .WithMessage(ErrorMessage.AliasGivenNameNotSupplied)
                .NotEmpty()
                    .WithErrorCode(ErrorCode.E2030)
                    .WithMessage(ErrorMessage.AliasGivenNameNotSupplied)
                .Length(1, 40)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.AliasGivenNameLength)
                .Matches(ValidationRegEx.StartsWithAlpha)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.AliasGivenNameValidation)
                .Matches(ValidationRegEx.AlphanumericNoDoubleSpace)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.AliasGivenNameValidation)
                .Matches(ValidationRegEx.AlphanumericApostropheHyphen)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.AliasGivenNameValidation)
                ;

        }
    }
}
