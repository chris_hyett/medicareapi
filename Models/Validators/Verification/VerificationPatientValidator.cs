﻿using FluentValidation;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared;
using static Gensolve.PhysioData.Common.PhysioEnums;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Validators
{
    public class VerificationPatientValidator : AbstractValidator<VerificationPatient>
    {
        private MedicarePatientValidatorType TypeCodeValue { get; set; }
        public VerificationPatientValidator(MedicarePatientValidatorType typeCodeValue)
        {

            TypeCodeValue = typeCodeValue;

            RuleFor(r => r.Identity)
                .NotEmpty()
                .NotNull()
                .SetValidator(new VerificationIdentityValidator())
            ;

            if (TypeCodeValue == MedicarePatientValidatorType.PVF
                || TypeCodeValue == MedicarePatientValidatorType.OPV)
            {
                RuleFor(r => r.AlsoKnownAs)
                   .SetValidator(new VerificationAlsoKnownAsValidator())
                   .When(r => r.AlsoKnownAs != null)
                ;
            };

            if (TypeCodeValue == MedicarePatientValidatorType.PVM
                 || TypeCodeValue == MedicarePatientValidatorType.OPV)
            {
                RuleFor(r => r.Medicare)
                    .NotNull()
                        .WithErrorCode(ErrorCode.E2030)
                        .WithMessage(ErrorMessage.MedicareRequired)
                    .NotEmpty()
                        .WithErrorCode(ErrorCode.E2030)
                        .WithMessage(ErrorMessage.MedicareRequired)
                    .SetValidator(new VerificationMedicareValidator())
                    ;
            }
            else
            {
                RuleFor(r => r.Medicare)
                    .Empty()
                        .WithErrorCode(ErrorCode.E2030)
                        .WithMessage(ErrorMessage.MedicareNotRequired)
                    ;
            }

            if (TypeCodeValue == MedicarePatientValidatorType.PVF
                || TypeCodeValue == MedicarePatientValidatorType.OPV)
            {
                RuleFor(r => r.HealthFund)
                    .NotNull()
                        .WithErrorCode(ErrorCode.E2030)
                        .WithMessage(ErrorMessage.HealthFundRequired)
                    .NotEmpty()
                        .WithErrorCode(ErrorCode.E2030)
                        .WithMessage(ErrorMessage.HealthFundRequired)
                    .SetValidator(new VerificationHealthFundValidator())
                ;
            }
            else
            {
                RuleFor(r => r.HealthFund)
                    .Empty()
                        .WithErrorCode(ErrorCode.E2030)
                        .WithMessage(ErrorMessage.HealthFundNotRequired)
                    ;
            }
        }
    }
}
