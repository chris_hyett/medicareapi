﻿using FluentValidation;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests;
using static Gensolve.Physio.Server.Service.MedicareOnline.Models.Validators.MedicareOnlineCustomValidations;
using static Gensolve.Physio.Server.Service.Utility.ExtensionMethods;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Validators
{
    public class ConcessionVerificationRequestValidator : AbstractValidator<ConcessionVerificationRequest>
    {
        public ConcessionVerificationRequestValidator()
        {
            
            When(r => r.DateOfService != null, () =>
            {

                RuleFor(r => r.DateOfService)
                    .Must(IsFutureDate)
                        .WithErrorCode(ErrorCode.E9202)
                        .WithMessage(ErrorMessage.DosFutureDate)
                    .DateIsNotGreaterThan(24, DifferenceValue.Months)
                        .WithErrorCode(ErrorCode.E9202)
                        .WithMessage(ErrorMessage.DosLt24Months)
                    ;
                RuleFor(r => r)
                    .Must(CompareDoS_DoBDates)
                        .WithErrorCode(ErrorCode.E2030)
                        .WithMessage(ErrorMessage.DosGtDob)
                    ;
            });

            RuleFor(r => r.Patient)
                .NotNull()
                .NotEmpty()
                .SetValidator(new ConcessionPatientValidator())
                ;

        }

        private bool CompareDoS_DoBDates(ConcessionVerificationRequest arg)
        {
            if (arg.Patient?.Identity == null || arg.Patient?.Identity?.DateOfBirth == null) return true;
            return arg.Patient.Identity.DateOfBirth <= arg.DateOfService;
        }
    }
}
