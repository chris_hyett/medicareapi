﻿using FluentValidation;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared;
using Gensolve.Physio.Server.Service.Utility;
using static Gensolve.Physio.Server.Service.MedicareOnline.Models.Validators.MedicareOnlineCustomValidations;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Validators
{
    public class ConcessionIdentityValidator : AbstractValidator<Identity>
    {
        public ConcessionIdentityValidator()
        {

            RuleFor(r => r.DateOfBirth)
                .NotNull()
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.DobRequired)
                .NotEmpty()
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.DobRequired)
                .Must(IsFutureDate)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.DobFutureDate)
                .DateIsNotGreaterThan(130, ExtensionMethods.DifferenceValue.Years)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.DobGt130)
                ;

            //Family Name
            RuleFor(r => r.FamilyName)
                .NotNull()
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.PtFamilyNameNotSupplied)
                .NotEmpty()
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.PtFamilyNameNotSupplied)
                .MinimumLength(1)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.PtFamilyNameLength)
                .MaximumLength(40)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.PtFamilyNameLength)
                .Matches(ValidationRegEx.StartsWithAlpha)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.PtFamilyNameValidation)
                .Matches(ValidationRegEx.AlphanumericNoDoubleSpace)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.PtFamilyNameValidation)
                .Matches(ValidationRegEx.AlphanumericApostropheHyphen)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.PtFamilyNameValidation)
                ;

            //GivenName            
            RuleFor(r => r.GivenName)
                .NotNull()
                    .WithErrorCode(ErrorCode.E2030)
                    .WithMessage(ErrorMessage.PtGivenNameNotSupplied)
                .NotEmpty()
                    .WithErrorCode(ErrorCode.E2030)
                    .WithMessage(ErrorMessage.PtGivenNameNotSupplied)
                .Length(1,40)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.PtGivenNameLength)
                .Matches(ValidationRegEx.StartsWithAlpha)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.PtGivenNameValidation)
                .Matches(ValidationRegEx.AlphanumericNoDoubleSpace)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.PtGivenNameValidation)
                .Matches(ValidationRegEx.AlphanumericApostropheHyphen)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.PtGivenNameValidation)
                ;            
        }
    }
}
