﻿using FluentValidation;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Validators
{
    public class ConcessionPatientValidator : AbstractValidator<ConcessionPatient>
    {
        public ConcessionPatientValidator()
        {
            RuleFor(r => r.Identity)
                .NotEmpty()
                .NotNull()
                .SetValidator(new ConcessionIdentityValidator())
            ;

            RuleFor(r => r.Medicare)
                .NotNull()
                    .WithErrorCode(ErrorCode.E2030)
                    .WithMessage(ErrorMessage.MedicareRequired)
                .NotEmpty()
                    .WithErrorCode(ErrorCode.E2030)
                    .WithMessage(ErrorMessage.MedicareRequired)
                ;
            RuleFor(r => r.Medicare)
                .SetValidator(new ConcessionMedicareValidator())
                ;

        }
    }
}
