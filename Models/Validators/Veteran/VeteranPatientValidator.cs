﻿using FluentValidation;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared;
using System;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Validators
{
    public class VeteranPatientValidator : AbstractValidator<VeteranPatient>
    {
        public VeteranPatientValidator()
        {
            RuleFor(r => r.Identity)
                .NotEmpty()
                .NotNull()
                .SetValidator(new VeteranIdentityValidator())
            ;

            RuleFor(r => r.ResidentialAddress)
                .SetValidator(new VeteranResidentialAddressValidator())
            ;

            RuleFor(r => r.VeteranMembership)
                .SetValidator(new VeteranMembershipValidator())
            ;
        }
    }
}
