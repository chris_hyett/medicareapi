﻿using FluentValidation;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests;


namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Validators
{
    public class VeteranVerificationRequestValidator : AbstractValidator<VeteranVerificationRequest>
    {
        public VeteranVerificationRequestValidator()
        {
            RuleFor(r => r.Patient)
                .NotNull()
                .NotEmpty()
                .SetValidator(new VeteranPatientValidator());
            ;
        }
    }
}
