﻿using FluentValidation;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared;
using Gensolve.Physio.Server.Service.Utility;
using static Gensolve.Physio.Server.Service.MedicareOnline.Models.Validators.MedicareOnlineCustomValidations;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Validators
{
    public class VeteranIdentityValidator : AbstractValidator<Identity>
    {
        public VeteranIdentityValidator()
        {
            //Date of Birth
            RuleFor(r => r.DateOfBirth)
                .NotNull()
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.DobRequired)
                .NotEmpty()
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.DobRequired)
                .Must(IsFutureDate)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.DobFutureDate)
                .DateIsNotGreaterThan(130, ExtensionMethods.DifferenceValue.Years)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.DobGt130)
                ;

            //Family Name
            RuleFor(r => r.FamilyName)
                .NotNull()
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.PtFamilyNameNotSupplied)
                .NotEmpty()
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.PtFamilyNameNotSupplied)
                .Length(1,40)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.PtFamilyNameLength)
                .Matches(ValidationRegEx.StartsWithAlpha)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.PtFamilyNameValidation)
                .Matches(ValidationRegEx.AlphanumericNoDoubleSpace)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.PtFamilyNameValidation)
                .Matches(ValidationRegEx.AlphanumericApostropheHyphen)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.PtFamilyNameValidation)
                ;

            //GivenName            
            RuleFor(r => r.GivenName)
                .NotNull()
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.PtGivenNameLength)
                .NotEmpty()
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.PtGivenNameLength)
                .WithErrorCode(ErrorCode.E2030)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.PtGivenNameNotSupplied)
                .Length(1,40)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.PtGivenNameLength)
                .Matches(ValidationRegEx.StartsWithAlpha)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.PtGivenNameValidation)
                .Matches(ValidationRegEx.AlphanumericNoDoubleSpace)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.PtGivenNameValidation)
                .Matches(ValidationRegEx.AlphanumericApostropheHyphen)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.PtGivenNameValidation)
                ;

            //SecondInitial
            When(r => !string.IsNullOrWhiteSpace(r.SecondInitial), () =>
            {
                RuleFor(r => r.SecondInitial)
                    .Length(1, 1)
                        .WithErrorCode(ErrorCode.E2030)
                        .WithMessage(ErrorMessage.PtSecondInitialLength)
                    .Matches(ValidationRegEx.StartsWithAlpha)
                        .WithErrorCode(ErrorCode.E2030)
                        .WithMessage(ErrorMessage.PtSecondInitialValidation)
                    ;
            });

            //Gender
            RuleFor(r => r.Sex)
                .NotNull()
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.PtGender)
                .NotEmpty()
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.PtGender)
                .IsValueInList(GenderValueList)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.PtGender)
                ;
        }
    }
}
