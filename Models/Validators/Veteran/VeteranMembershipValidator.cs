﻿using FluentValidation;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared;
using static Gensolve.Physio.Server.Service.MedicareOnline.Models.Validators.MedicareOnlineCustomValidations;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Validators
{
    public class VeteranMembershipValidator:AbstractValidator<VeteranMembership>
    {
        public VeteranMembershipValidator() {

            RuleFor(r => r.VeteranNumber)
                .NotNull()
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.VeteranNumberValidation)
                .NotEmpty()
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.VeteranNumberValidation)
                .Must(VeteranNumberIsValid)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.VeteranNumberValidation)
            ;
        }
    }
}
