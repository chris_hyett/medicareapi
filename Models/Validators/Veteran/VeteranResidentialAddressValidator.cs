﻿using FluentValidation;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared;
using static Gensolve.Physio.Server.Service.MedicareOnline.Models.Validators.MedicareOnlineCustomValidations;


namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Validators
{
    public class VeteranResidentialAddressValidator : AbstractValidator<ResidentialAddress>
    {
        public VeteranResidentialAddressValidator() {

            //Locality
            RuleFor(r => r.Locality)
                .NotEmpty()
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.ResidentialAddressLocalityPostcodeRequired)
                .NotNull()
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.ResidentialAddressLocalityPostcodeRequired)
                .Matches(ValidationRegEx.AlphanumericApostropheHyphen)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.ResidentialAddressLocalityValidation)
                .Must(PostCodeIsValid)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.ResidentialAddressLocalityIncludesStateCode)
            ;

            //Postcode
            RuleFor(r => r.Postcode)
                .NotEmpty()
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.ResidentialAddressLocalityPostcodeRequired)
                .NotNull()
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.ResidentialAddressLocalityPostcodeRequired)
                .NotEqual("0000")
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.ResidentialAddressPostCodeMustNot0000)
                .Matches(ValidationRegEx.OnlyNumeric)
                    .WithErrorCode(ErrorCode.E9202)
                    .WithMessage(ErrorMessage.ResidentialAddressPostCodeValidation)
            ;
        }
    }
}
