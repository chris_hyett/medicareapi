﻿using FluentValidation;
using Gensolve.Physio.Server.Service.MedicareOnline;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared;
using System.Linq;

namespace MedicareAPI.Models.Validators.BulkBill
{
    public class BulkBillClaimStoreForwardClaimValidator : AbstractValidator<BulkBillClaimStoreForwardClaim>
    {
        public BulkBillClaimStoreForwardClaimValidator()
        {
            /// FAIL with Code and Error
            /// If the value of ServiceTypeCode = "O"
            /// And there are ANY  MedicalEvent(s) that have
            /// Referral.TypeCode contains a value OR
            /// ReferralOverrideCode contains a value OR
            /// ANY Service(s) have the SelfDeemedCode contains a value
            RuleFor(r => r.ServiceTypeCode)
                .Equal("O")
                    .WithErrorCode(ErrorCode.E2030)
                    .WithMessage(ErrorMessage.BulkBillInconsistentGeneralService)
                .When(r => r.MedicalEvent.Any(
                        a => !string.IsNullOrEmpty(a.Referral.TypeCode)
                          || !string.IsNullOrEmpty(a.ReferralOverrideCode)
                          || a.Service.Any(s => !string.IsNullOrEmpty(s.SelfDeemedCode))));

        }
    }
}
