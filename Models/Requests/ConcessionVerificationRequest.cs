﻿using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared;
using Newtonsoft.Json;
using System;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests
{
    [JsonObject(MemberSerialization.OptIn)]
    public class ConcessionVerificationRequest
    {
        [JsonProperty(Order = 1)]
        public ConcessionPatient Patient { get; set; }

        [JsonProperty(Order = 2)]
        public DateTime? DateOfService { get; set; }
    }
}
