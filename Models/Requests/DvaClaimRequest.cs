﻿using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared;
using Newtonsoft.Json;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests
{
    [JsonObject(MemberSerialization.OptIn)]
    public class DvaClaimRequest
    {
        [JsonProperty(Order = 1)]
        public DvaClaim Claim { get; set; }
    }
}
