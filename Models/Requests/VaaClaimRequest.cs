﻿using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared.VAA;
using Newtonsoft.Json;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests
{
    [JsonObject(MemberSerialization.OptIn)]
    public class VaaClaimRequest
    {
        [JsonProperty(Order = 1)]
        public VaaClaim Claim { get; set; }
    }
}
