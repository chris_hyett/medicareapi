﻿using Gensolve.Physio.Server.Service.MedicareOnline.JsonConverters;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared;
using Newtonsoft.Json;
using System;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests
{
    [JsonObject(MemberSerialization.OptIn)]
    public class PatientVerificationRequest
    {
        [JsonProperty(Order = 1)]
        public VerificationPatient Patient { get; set; }

        [JsonProperty(Order = 2)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string TypeCode { get; set; }

        [JsonProperty(Order = 3)]
        public DateTime? DateOfService { get; set; }
    }
}
