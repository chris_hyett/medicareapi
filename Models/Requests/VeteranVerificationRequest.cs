﻿using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared;
using Newtonsoft.Json;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests
{
    [JsonObject(MemberSerialization.OptIn)]
    public class VeteranVerificationRequest
    {
        [JsonProperty(Order = 1)]
        public VeteranPatient Patient { get; set; }

    }
}
