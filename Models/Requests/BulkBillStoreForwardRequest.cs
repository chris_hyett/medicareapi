﻿using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared;
using Newtonsoft.Json;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests
{
    [JsonObject(MemberSerialization.OptIn)]
    public class BulkBillStoreForwardRequest
    {
        [JsonProperty(Order = 1)]
        public BulkBillClaimStoreForwardClaim Claim { get; set; }
    }
}
