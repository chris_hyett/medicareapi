﻿using System;
using System.Collections.Generic;
using Gensolve.Physio.Server.Service.MedicareOnline.JsonConverters;
using Newtonsoft.Json;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared
{
    [JsonObject(MemberSerialization.OptIn)]
    public class MedicalEvent
    {
        [JsonProperty(Order = 1)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string Id { get; set; }

        [JsonProperty(Order = 2)]
        public string MedicalEventDate { get; set; }

        [JsonProperty(Order = 3)]
        public string MedicalEventTime { get; set; }

        [JsonProperty(Order = 4)]
        public List<Service> Service { get; set; }
    }

}
