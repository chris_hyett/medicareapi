﻿using Newtonsoft.Json;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Claimant
    {
        [JsonProperty(Order = 1)]
        public Identity Identity { get; set; }

        [JsonProperty(Order = 2)]
        public Medicare Medicare { get; set; }

        [JsonProperty(Order = 3)]
        public EftDetails EftDetails { get; set; }

        [JsonProperty(Order = 4)]
        public ResidentialAddress ResidentialAddress { get; set; }

        [JsonProperty(Order = 5)]
        public ContactDetails ContactDetails { get; set; }
    }
}
