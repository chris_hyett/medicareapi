﻿using Gensolve.Physio.Server.Service.MedicareOnline.JsonConverters;
using Newtonsoft.Json;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Provider
    {
        [JsonProperty(Order = 1)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string ProviderNumber { get; set; }
    }
}
