﻿using System;
using System.Collections.Generic;
using Gensolve.Physio.Server.Service.MedicareOnline.JsonConverters;
using Newtonsoft.Json;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared
{
    [JsonObject(MemberSerialization.OptIn)]
    public class ClaimInteractive
    {
        [JsonProperty(Order = 1)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string AccountPaidInd { get; set; }

        [JsonProperty(Order = 2)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string AccountReferenceId { get; set; }

        [JsonProperty(Order = 3)]
        public DateTime? AuthorisationDate { get; set; }

        [JsonProperty(Order = 4)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string ReferralOverrideCode { get; set; }

        [JsonProperty(Order = 5)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string SubmissionAuthorityInd { get; set; }

        [JsonProperty(Order = 6)]
        public ClaimInteractivePatient Patient { get; set; }

        [JsonProperty(Order = 7)]
        public Referral Referral { get; set; }

        [JsonProperty(Order = 8)]
        public Claimant Claimant { get; set; }

        [JsonProperty(Order = 9)]
        public List<MedicalEvent> MedicalEvent { get; set; }

        [JsonProperty(Order = 10)]
        public PayeeProvider PayeeProvider { get; set; }

        [JsonProperty(Order = 11)]
        public ServiceProvider ServiceProvider { get; set; }

        [JsonIgnore()]
        public bool HadClaimantData { get; set; }
    }

}
