﻿using Gensolve.Physio.Server.Service.MedicareOnline.JsonConverters;
using Newtonsoft.Json;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared.VAA
{
    [JsonObject(MemberSerialization.OptIn)]
    public class VaaService 
    {
        [JsonProperty(Order = 1)]
        public string Id { get; set; }

        [JsonProperty(Order = 2)]
        public string AccountReferenceNumber { get; set; }

        [JsonProperty(Order = 3)]
        public string AdmissionDate { get; set; }

        [JsonProperty(Order = 4)]
        public string ChargeAmount { get; set; }

        [JsonProperty(Order = 5)]
        public string DischargeDate { get; set; }

        [JsonProperty(Order = 6)]
        public int? DistanceKilometres { get; set; }

        [JsonProperty(Order = 7)]
        public string DuplicateServiceOverrideInd { get; set; }

        [JsonProperty(Order = 8)]
        public string ItemNumber { get; set; }

        [JsonProperty(Order = 9)]
        public string MultipleProcedureOverrideInd { get; set; }

        [JsonProperty(Order = 10)]
        public string NumberOfPatientsSeen { get; set; }

        [JsonProperty(Order = 11)]
        public string NumberOfTeeth { get; set; }

        [JsonProperty(Order = 12)]
        public string OpticalScriptCode { get; set; }

        [JsonProperty(Order = 13)]
        public string RestrictiveOverrideCode { get; set; }

        [JsonProperty(Order = 14)]
        public string SecondDeviceInd { get; set; }

        [JsonProperty(Order = 15)]
        public string SelfDeemedCode { get; set; }

        [JsonProperty(Order = 16)]
        public string Text { get; set; }

        [JsonProperty(Order = 17)]
        public string TimeDuration { get; set; }

        [JsonProperty(Order = 18)]
        public string ToothNumber { get; set; }

        [JsonProperty(Order = 19)]
        public string UpperLowerJawCode { get; set; }
    }
}
