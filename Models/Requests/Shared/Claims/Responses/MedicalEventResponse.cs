﻿using Newtonsoft.Json;
using System.Collections.Generic;


namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared.Claims.Responses
{
    [JsonObject("MedicalEvent")]
    public class MedicalEventResponse
    {
        [JsonProperty("service")]
        public List<Service> Service { get; set; }

        [JsonProperty("eventDate")]
        public string EventDate { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }
    }
}
