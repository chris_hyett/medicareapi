﻿using Newtonsoft.Json;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared.Claims.Responses
{
    [JsonObject("Patient")]
    class PatientResponse
    {
        [JsonProperty("currentMembership")]
        public CurrentMembershipResponse CurrentMembership { get; set; }
    }
}
