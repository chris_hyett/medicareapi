﻿using Newtonsoft.Json;
using System.Collections.Generic;


namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared.Claims.Responses
{
    [JsonObject("ClaimAssessment")]
    public class ClaimAssessmentResponse
    {
        [JsonProperty("claimant")]
        public Claimant Claimant { get; set; }

        [JsonProperty("patient")]
        public ConcessionPatient Patient { get; set; }

        [JsonProperty("medicalEvent")]
        public List<MedicalEventResponse> MedicalEvent { get; set; }

        [JsonProperty("error")]
        public ErrorResponse Error { get; set; }

        [JsonProperty("claimId")]
        public string ClaimId { get; set; }
    }

}
