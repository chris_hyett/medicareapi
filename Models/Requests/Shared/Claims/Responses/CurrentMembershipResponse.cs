﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared.Claims.Responses
{
    [JsonObject("CurrentMembership")]
    public class CurrentMembershipResponse
    {
        [JsonProperty("memberNumber")]
        public string MemberNumber { get; set; }

        [JsonProperty("memberRefNumber")]
        public string MemberRefNumber { get; set; }
    }
}
