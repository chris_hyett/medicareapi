﻿using Gensolve.Physio.Server.Service.MedicareOnline.JsonConverters;
using Newtonsoft.Json;
using System;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared
{

    [JsonObject(MemberSerialization.OptIn)]
    public partial class Identity
    {
        [JsonProperty(Order = 1)]
        public DateTime? DateOfBirth { get; set; }

        [JsonProperty(Order = 2),
         JsonConverter(typeof(ToProperCaseStringConverter<string>))]
        public string FamilyName { get; set; }

        [JsonProperty(Order = 3),
         JsonConverter(typeof(ToProperCaseStringConverter<string>))]
        public string GivenName { get; set; }

        [JsonProperty(Order = 4),
         JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string SecondInitial { get; set; }

        [JsonProperty(Order = 5),
        JsonConverter(typeof(ToProperCaseStringConverter<string>))]
        public string Sex { get; set; }
    }
}
