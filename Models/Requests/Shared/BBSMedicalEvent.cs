﻿using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared.Claims;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared
{
    [JsonObject(MemberSerialization.OptIn)]
    public class BBSMedicalEvent 
    {
        public BBSMedicalEvent() : base()
        {
            Service = new List<Service>();
        }

        [JsonProperty(Order = 1)]
        public string Id { get; set; }

        [JsonProperty(Order = 2)]
        public string AuthorisationDate { get; set; }

        [JsonProperty(Order = 3)]
        public string CreateDateTime { get; set; }

        [JsonProperty(Order = 4)]
        public string MedicalEventDate { get; set; }

        [JsonProperty(Order = 5)]
        public string MedicalEventTime { get; set; }

        [JsonProperty(Order = 6)]
        public BulkBillingPatient Patient { get; set; }

        [JsonProperty(Order = 7)]
        public Referral Referral { get; set; }

        [JsonProperty(Order = 8)]
        public string ReferralOverrideCode { get; set; }

        [JsonProperty(Order = 9)]
        public List<Service> Service { get; set; }

        [JsonProperty(Order = 10)]
        public string SubmissionAuthorityInd { get; set; }
    }
}
