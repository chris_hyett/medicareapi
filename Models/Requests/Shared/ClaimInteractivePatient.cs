﻿using Newtonsoft.Json;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class ClaimInteractivePatient: Patient
    {
        [JsonProperty(Order = 2)]
        public AliasIdentity AlsoKnownAs { get; set; }

        [JsonProperty(Order = 3)]
        public Medicare Medicare { get; set; }

        [JsonProperty(Order = 4)]
        public HealthFund HealthFund { get; set; }        
    }
}