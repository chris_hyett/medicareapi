﻿using Newtonsoft.Json;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared
{
    [JsonObject(MemberSerialization.OptIn)]
    public class AcceptedDisability
    {
        [JsonProperty(Order = 1)]
        public string Code { get; set; }

        [JsonProperty(Order = 2)]
        public string Ind { get; set; }
    }
}
