﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared.VAA
{
    [JsonObject(MemberSerialization.OptIn)]
    public class VaaMedicalEvent
    {
        public VaaMedicalEvent()
        {
            this.Patient = new VeteranPatient();
            this.Service = new List<VaaService>();
        }

        [JsonProperty(Order = 1)]
        public string Id { get; set; }

        [JsonProperty(Order = 2)]
        public AcceptedDisability AcceptedDisability { get; set; }

        [JsonProperty(Order = 3)]
        public string AuthorisationDate { get; set; }

        [JsonProperty(Order = 4)]
        public string BreakInEpisodeEndDate { get; set; }

        [JsonProperty(Order = 5)]
        public string BreakInEpisodeOfCareNumber { get; set; }

        [JsonProperty(Order = 6)]
        public string BreakInEpisodeStartDate { get; set; }

        [JsonProperty(Order = 7)]
        public string NumberOfCNCHours { get; set; }

        [JsonProperty(Order = 8)]
        public string NumberOfCNCVisits { get; set; }

        [JsonProperty(Order = 9)]
        public string CreateDateTime { get; set; }

        [JsonProperty(Order = 10)]
        public string NumberOfENHours { get; set; }

        [JsonProperty(Order = 11)]
        public string NumberOfENVisits { get; set; }

        [JsonProperty(Order = 12)]
        public string FacilityId { get; set; }

        [JsonProperty(Order = 13)]
        public string MedicalEventDate { get; set; }

        [JsonProperty(Order = 14)]
        public string MedicalEventTime { get; set; }

        [JsonProperty(Order = 15)]
        public string NumberOfNSSHours { get; set; }

        [JsonProperty(Order = 16)]
        public string NumberOfNSSVisits { get; set; }

        [JsonProperty(Order = 17)]
        public VeteranPatient Patient { get; set; }

        [JsonProperty(Order = 18)]
        public Referral Referral { get; set; }

        [JsonProperty(Order = 19)]
        public string ReferralOverrideCode { get; set; }

        [JsonProperty(Order = 20)]
        public string NumberOfRNVisits { get; set; }

        [JsonProperty(Order = 21)]
        public List<VaaService> Service { get; set; }

        [JsonProperty(Order =22)]
        public string SubmissionAuthorityInd { get; set; }
    }
}
