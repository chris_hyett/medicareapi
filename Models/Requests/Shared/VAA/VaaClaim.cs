﻿using System.Collections.Generic;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared.VAA
{
    public class VaaClaim
    {
        public VaaClaim()
        {
            MedicalEvent = new List<VaaMedicalEvent>();
            PayeeProvider = new PayeeProvider();
            ServiceProvider = new ServiceProvider();
        }

        public string HospitalInd { get; set; }

        public List<VaaMedicalEvent> MedicalEvent { get; set; }

        public PayeeProvider PayeeProvider { get; set; }

        public ServiceProvider ServiceProvider { get; set; }

        public string ServiceTypeCode { get; set; }

    }
}
