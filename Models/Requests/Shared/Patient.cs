﻿using Newtonsoft.Json;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Patient
    {
        [JsonProperty(Order = 1)]
        public Identity Identity { get; set; }
    }
}
