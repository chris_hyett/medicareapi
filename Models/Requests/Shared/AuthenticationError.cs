﻿namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared
{
    public class AuthenticationError
    {
        public string Code { get; set; }
        public string CodeType { get; set; }
        public string Message { get; set; }
    }
}
