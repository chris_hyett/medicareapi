﻿using Gensolve.Physio.Server.Service.MedicareOnline.JsonConverters;
using Newtonsoft.Json;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Service
    {
        [JsonProperty(Order = 1)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string Id { get; set; }

        [JsonProperty(Order = 2)]
        public string AccessionDateTime { get; set; }

        [JsonProperty(Order = 3)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string AftercareOverrideInd { get; set; }

        [JsonProperty(Order = 4)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string ChargeAmount { get; set; }

        [JsonProperty(Order = 5)]
        public string CollectionDateTime { get; set; }

        [JsonProperty(Order = 6)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string DuplicateServiceOverrideInd { get; set; }

        [JsonProperty(Order = 7)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string FieldQuantity { get; set; }

        [JsonProperty(Order = 8)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string ItemNumber { get; set; }

        /// <summary>
        /// Location Specific Practice Number for diagnostic and radiation oncology that is 
        /// specific to an individual location for the purpose of claiming a Medicare benefit
        /// </summary>
        [JsonProperty(Order = 9)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string LspNumber { get; set; }

        [JsonProperty(Order = 10)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string MultipleProcedureOverrideInd { get; set; }

        [JsonProperty(Order = 11)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string NumberOfPatientsSeen { get; set; }

        [JsonProperty(Order = 12)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string RestrictiveOverrideCode { get; set; }

        [JsonProperty(Order = 13)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string Rule3ExemptInd { get; set; }

        [JsonProperty(Order = 14)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string S4b3ExemptInd { get; set; }

        [JsonProperty(Order = 15)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string ScpId { get; set; }

        [JsonProperty(Order = 16)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string SelfDeemedCode { get; set; }

        [JsonProperty(Order = 17)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string Text { get; set; }

        [JsonProperty(Order = 18)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string TimeDuration { get; set; }

        [JsonProperty(Order = 19)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string PatientContribAmount { get; set; }

        [JsonProperty(Order = 20)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string FacilityId { get; set; }

        [JsonProperty(Order = 21)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string HospitalInd { get; set; }

        [JsonProperty(Order = 22)]
        public string AccountReferenceNumber { get; set; }

        [JsonProperty(Order = 23)]
        public int? DistanceKilometres { get; set; }

        [JsonProperty(Order = 24)]
        public Status Error { get; set; }
    }
}
