﻿using Newtonsoft.Json;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared
{
    [JsonObject(MemberSerialization.OptIn)]
    public partial class VeteranPatient: Patient
    {
        public VeteranPatient()
        {
            ResidentialAddress = new ResidentialAddress();
            VeteranMembership = new VeteranMembership();
        }

        [JsonProperty(Order = 2)]
        public ResidentialAddress ResidentialAddress { get; set; }

        [JsonProperty(Order = 3)]
        public VeteranMembership VeteranMembership { get; set; }

    }
}