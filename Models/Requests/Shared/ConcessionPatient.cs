﻿using Newtonsoft.Json;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared
{

    [JsonObject(MemberSerialization.OptIn)]
    public partial class ConcessionPatient
    {
        [JsonProperty(Order = 1)]       
        public Identity Identity { get; set; }

        [JsonProperty(Order = 2)]
        public Medicare Medicare { get; set; }
    }
}