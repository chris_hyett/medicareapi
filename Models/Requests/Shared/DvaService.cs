﻿using Newtonsoft.Json;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared
{
    [JsonObject(MemberSerialization.OptIn)]
    public class DvaService : Service
    {
        [JsonProperty(Order = 1)]   
        public string AccountReferenceNumber { get; set; }

        [JsonProperty(Order = 2)]
        public int DistanceKilometres { get; set; }
    }
}
