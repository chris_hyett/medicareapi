﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared
{
    [JsonObject(MemberSerialization.OptIn)]
    public class BulkBillClaimStoreForwardClaim
    {
        public BulkBillClaimStoreForwardClaim()
        {
            MedicalEvent = new List<BBSMedicalEvent>();
            ServiceProvider = new ServiceProvider();
        }

        [JsonProperty(Order = 1)]
        public string FacilityId { get; set; }

        [JsonProperty(Order = 2)]
        public string HospitalInd { get; set; }

        [JsonProperty(Order = 3)]
        public List<BBSMedicalEvent> MedicalEvent { get; set; }

        [JsonProperty(Order = 4)]
        public PayeeProvider PayeeProvider { get; set; }

        [JsonProperty(Order = 5)]
        public ServiceProvider ServiceProvider { get; set; }

        /// <summary>
        /// P = Pathology 
        /// S = Specialist
        /// O = General
        /// </summary>
        [JsonProperty(Order = 6)]
        public string ServiceTypeCode { get; set; }
    }
}
