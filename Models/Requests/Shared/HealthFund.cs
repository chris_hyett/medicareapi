﻿using Gensolve.Physio.Server.Service.MedicareOnline.JsonConverters;
using Newtonsoft.Json;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared
{
    [JsonObject(MemberSerialization.OptIn)]
    public class HealthFund
    {
        [JsonProperty(Order = 1)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string MemberNumber { get; set; }

        [JsonProperty(Order = 2)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string MemberRefNumber { get; set; }

        [JsonProperty(Order = 3),
         JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string Organisation { get; set; }
    }
}
