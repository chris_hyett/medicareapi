﻿using Gensolve.Physio.Server.Service.MedicareOnline.JsonConverters;
using Newtonsoft.Json;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Status
    {
        [JsonProperty(Order = 1)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public int Code { get; set; }

        [JsonProperty(Order = 2)]
        [JsonConverter(typeof(ToProperCaseStringConverter<string>))]
        public string Text { get; set; }
    }
}
