﻿using Gensolve.Physio.Server.Service.MedicareOnline.JsonConverters;
using Newtonsoft.Json;
using System;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Referral
    {
        [JsonProperty(Order = 1)]
        public DateTime? IssueDate { get; set; }

        [JsonProperty(Order = 2)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public int Period { get; set; }

        [JsonProperty(Order = 3)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string PeriodCode { get; set; }

        [JsonProperty(Order = 4)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string TypeCode { get; set; }

        [JsonProperty(Order = 5)]
        public Provider Provider { get; set; }
    }
}
