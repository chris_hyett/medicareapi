﻿using Newtonsoft.Json;
using System.Collections.Generic;


namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared
{
    [JsonObject(MemberSerialization.OptIn)]
    public class DvaClaim
    {
        public DvaClaim()
        {
            MedicalEvent = new List<DvaMedicalEvent>();
            ServiceProvider = new ServiceProvider();
            PayeeProvider = new PayeeProvider();
        }

        [JsonProperty(Order = 1)]
        public string HospitalInd { get; set; }

        [JsonProperty(Order = 2)]
        public string ServiceTypeCode { get; set; }

        [JsonProperty(Order = 3)]
        public List<DvaMedicalEvent> MedicalEvent { get; set; }

        [JsonProperty(Order = 4)]
        public ServiceProvider ServiceProvider { get; set; }

        [JsonProperty(Order = 5)]
        public PayeeProvider PayeeProvider { get; set; }
    }
}
