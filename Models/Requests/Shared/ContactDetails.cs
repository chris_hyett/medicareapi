﻿using Gensolve.Physio.Server.Service.MedicareOnline.JsonConverters;
using Newtonsoft.Json;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared
{
    [JsonObject(MemberSerialization.OptIn)]
    public class ContactDetails
    {
        [JsonProperty(Order = 1 )]
        public string EmailAddress { get; set; }

        [JsonProperty(Order = 2)]
        [JsonConverter(typeof(ToProperCaseStringConverter<string>))]
        public string Name { get; set; }

        [JsonProperty(Order = 3)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string PhoneNumber { get; set; }
    }

}
