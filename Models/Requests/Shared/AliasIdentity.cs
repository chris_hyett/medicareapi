﻿using Gensolve.Physio.Server.Service.MedicareOnline.JsonConverters;
using Newtonsoft.Json;
using System;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared
{

    [JsonObject(MemberSerialization.OptIn)]
    public partial class AliasIdentity:Identity
    {
    }
}
