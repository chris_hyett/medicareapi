﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared
{
    [JsonObject(MemberSerialization.OptIn)]
    public class DvaMedicalEvent
    {
        public DvaMedicalEvent()
        {
            this.Patient = new VeteranPatient();
            this.Service = new List<Service>();
        }

        [JsonProperty(Order = 1)]
        public string Id { get; set; }

        [JsonProperty(Order = 2)]
        public string AuthorisationDate { get; set; }

        [JsonProperty(Order = 3)]
        public string CreateDateTime { get; set; }

        [JsonProperty(Order = 4)]
        public string FacilityId { get; set; }

        [JsonProperty(Order = 5)]
        public string MedicalEventDate { get; set; }

        [JsonProperty(Order = 6)]
        public string MedicalEventTime { get; set; }

        [JsonProperty(Order = 7)]
        public string ReferralOverrideCode { get; set; }

        [JsonProperty(Order = 8)]
        public string SubmissionAuthorityInd { get; set; }

        [JsonProperty(Order = 9)]
        public string TreatmentLocationCode { get; set; }

        [JsonProperty(Order = 10)]
        public AcceptedDisability AcceptedDisability { get; set; }

        [JsonProperty(Order = 11)]
        public Referral Referral { get; set; }

        [JsonProperty(Order = 12)]
        public VeteranPatient Patient { get; set; }

        [JsonProperty(Order = 3)]
        public List<Service> Service { get; set; }
    }
}
