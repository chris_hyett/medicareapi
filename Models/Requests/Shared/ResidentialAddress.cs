﻿using Gensolve.Physio.Server.Service.MedicareOnline.JsonConverters;
using Newtonsoft.Json;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared
{
    [JsonObject(MemberSerialization.OptIn)]
    public class ResidentialAddress
    {
        [JsonProperty(Order = 1)]
        [JsonConverter(typeof(ToProperCaseStringConverter<string>))]
        public string AddressLineOne { get; set; }
        [JsonProperty(Order = 2)]

        [JsonConverter(typeof(ToProperCaseStringConverter<string>))]
        public string AddressLineTwo { get; set; }

        [JsonProperty(Order = 3)]
        [JsonConverter(typeof(ToProperCaseStringConverter<string>))]
        public string Locality { get; set; }

        [JsonProperty(Order = 4)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string Postcode { get; set; }
    }
}
