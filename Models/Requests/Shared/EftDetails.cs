﻿using Gensolve.Physio.Server.Service.MedicareOnline.JsonConverters;
using Newtonsoft.Json;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared
{
    [JsonObject(MemberSerialization.OptIn)]
    public class EftDetails
    {
        [JsonProperty(Order = 1)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string AccountName { get; set; }

        [JsonProperty(Order = 2)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string AccountNumber { get; set; }

        [JsonProperty(Order = 3)]
        [JsonConverter(typeof(ToUpperCaseStringConverter<string>))]
        public string BsbCode { get; set; }
    }
}
