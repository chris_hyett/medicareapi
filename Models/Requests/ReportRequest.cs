﻿using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared;
using Newtonsoft.Json;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests
{
    [JsonObject(MemberSerialization.OptIn)]
    public class ReportRequest
    {
        public ReportRequest()
        {
            PayeeProvider = new PayeeProvider();
        }

        [JsonProperty(Order = 1)]
        public PayeeProvider PayeeProvider { get; set; }

        [JsonProperty(Order = 2)]
        public string ClaimId { get; set; }

        [JsonProperty(Order = 3)]
        public string LodgementDate { get; set; }
    }
}
