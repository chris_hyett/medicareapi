﻿using AutoMapper;
using Gensolve.MedicareOnline;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared;
using Gensolve.Utilities;
using System;
using System.Collections.Generic;
using static Gensolve.PhysioData.Common.PhysioEnums;
using physioData = Gensolve.PhysioData.Common.Medicare;
using xmlData = Gensolve.MedicareOnline.MedicareOnlineRequestXml;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.MappingProfiles
{
    public class MedicareApiMappingProfile : Profile
    {
        public MedicareApiMappingProfile()
        {
            BulkBillPaymentReport();
            BulkBillProcessingReport();
            DirectBillClaim();
            DvaMedicalPaperlessClaims();
            DvaPaymentReport();
            DvaProcessingReport();
            EnterpriseConcessionVerification();
            EnterpriseVeteranVerificationRequest();
            OnlineConcessionVerification();
            OnlinePatientVerification();
            OnlineVeteranVerification();
            PatientClaimInteractive();
            PatientClaimStoredAndForward();
            VaaClaim();
        }
        #region Online
        #region OnlineConcessionVerification
        protected void OnlineConcessionVerification()
        {
            CreateMap<OCEVerification, ConcessionVerificationRequest>()
                .ForMember(dest => dest.DateOfService, opt => opt.MapFrom(src => src.ServiceDate.MapToMedicareDateTime()))
                .ForMember(dest => dest.Patient, opt => opt.MapFrom(src => src))
                ;

            CreateMap<OCEVerification, ConcessionPatient>()
                .ForMember(dest => dest.Identity, opt => opt.MapFrom(src => src))
                .ForMember(dest => dest.Medicare, opt => opt.MapFrom(src => src))
                ;

            CreateMap<OCEVerification, Identity>()
                .ForMember(dest => dest.DateOfBirth, opt => opt.MapFrom(src => src.PatientDateOfBirth.MapToMedicareDateTime()))
                .ForMember(dest => dest.FamilyName, opt => opt.MapFrom(src => src.PatientFamilyName))
                .ForMember(dest => dest.GivenName, opt => opt.MapFrom(src => src.PatientFirstName))
                .ForMember(dest => dest.SecondInitial, opt => opt.Ignore())
                .ForMember(dest => dest.Sex, opt => opt.Ignore())
                .AfterMap((src, dest) => dest.GivenName = string.IsNullOrWhiteSpace(dest.GivenName) && !string.IsNullOrWhiteSpace(dest.FamilyName) ? "Onlyname" : dest.GivenName)

                ;

            CreateMap<OCEVerification, Medicare>()
                .ForMember(dest => dest.MemberNumber, opt => opt.MapFrom(src => src.PatientMedicareCardNum))
                .ForMember(dest => dest.MemberRefNumber, opt => opt.MapFrom(src => src.PatientReferenceNum))
                ;
        }
        #endregion
        #region OnlinePatientVerification
        protected void OnlinePatientVerification()
        {
            CreateMap<PatientVerification, PatientVerificationRequest>()
                .ForMember(dest => dest.DateOfService, opt => opt.MapFrom(src => src.EarliestDateOfService))
                .ForMember(dest => dest.TypeCode, opt => opt.MapFrom(src => src.OPVTypeCde))
                .ForMember(dest => dest.Patient, opt => opt.MapFrom(src => src))
                ;

            CreateMap<PatientVerification, VerificationPatient>()
                .ForMember(dest => dest.Identity, opt => opt.MapFrom(src => src))
                .ForMember(dest => dest.Medicare, opt => opt.MapFrom(src => (MedicarePatientValidatorType)Enum.Parse(typeof(MedicarePatientValidatorType), src.OPVTypeCde) != MedicarePatientValidatorType.PVF ? src : null))
                .ForMember(dest => dest.HealthFund, opt => opt.MapFrom(src => (MedicarePatientValidatorType)Enum.Parse(typeof(MedicarePatientValidatorType), src.OPVTypeCde) != MedicarePatientValidatorType.PVM ? src : null))
                .ForMember(dest => dest.AlsoKnownAs, opt => opt.MapFrom(src => (MedicarePatientValidatorType)Enum.Parse(typeof(MedicarePatientValidatorType), src.OPVTypeCde) != MedicarePatientValidatorType.PVF && !string.IsNullOrWhiteSpace(src.PatientAliasFamilyName) ? src : null))
                ;

            CreateMap<PatientVerification, Identity>()
                .ForMember(dest => dest.DateOfBirth, opt => opt.MapFrom(src => src.PatientDateOfBirth.MapToMedicareDateTime()))
                .ForMember(dest => dest.FamilyName, opt => opt.MapFrom(src => src.PatientFamilyName))
                .ForMember(dest => dest.GivenName, opt => opt.MapFrom(src => src.PatientFirstName))
                .ForMember(dest => dest.SecondInitial, opt => opt.Ignore())
                .ForMember(dest => dest.Sex, opt => opt.ConvertUsing(new GenderFormatter(), src => src.Gender))
                .AfterMap((src, dest) => dest.GivenName = string.IsNullOrWhiteSpace(dest.GivenName) && !string.IsNullOrWhiteSpace(dest.FamilyName) ? "Onlyname" : dest.GivenName)
                ;

            CreateMap<PatientVerification, AliasIdentity>()
                .ForMember(dest => dest.FamilyName, opt => opt.MapFrom(src => src.PatientAliasFamilyName))
                .ForMember(dest => dest.GivenName, opt => opt.MapFrom(src => src.PatientAliasFirstName))
                .ForMember(dest => dest.DateOfBirth, opt => opt.Ignore())
                .ForMember(dest => dest.Sex, opt => opt.Ignore())
                .ForMember(dest => dest.SecondInitial, opt => opt.Ignore())
                .AfterMap((src, dest) => dest.GivenName = string.IsNullOrWhiteSpace(dest.GivenName) && !string.IsNullOrWhiteSpace(dest.FamilyName) ? "Onlyname" : dest.GivenName)
                ;
            ;

            CreateMap<PatientVerification, Medicare>()
                .ForMember(dest => dest.MemberNumber, opt => opt.MapFrom(src => src.PatientMedicareCardNum))
                .ForMember(dest => dest.MemberRefNumber, opt => opt.MapFrom(src => src.PatientReferenceNum))
                ;

            CreateMap<PatientVerification, HealthFund>()
                .ForMember(dest => dest.MemberNumber, opt => opt.MapFrom(src => src.PatientMedicareCardNum))
                .ForMember(dest => dest.MemberRefNumber, opt => opt.MapFrom(src => src.PatientReferenceNum))
                .ForMember(dest => dest.Organisation, opt => opt.MapFrom(src => src.ServicingProviderNum))
                ;
        }
        #endregion
        #region OnlineVeteranVerification        
        protected void OnlineVeteranVerification()
        {
            CreateMap<DVAVerification, VeteranVerificationRequest>()
                .ForMember(dest => dest.Patient, opt => opt.MapFrom(src => src))
                ;

            CreateMap<DVAVerification, VeteranPatient>()
                .ForMember(dest => dest.Identity, opt => opt.MapFrom(src => src))
                .ForMember(dest => dest.ResidentialAddress, opt => opt.MapFrom(src => src))
                .ForMember(dest => dest.VeteranMembership, opt => opt.MapFrom(src => src))
                ;

            CreateMap<DVAVerification, Identity>()
                .ForMember(dest => dest.DateOfBirth, opt => opt.MapFrom(src => src.PatientDateOfBirth.MapToMedicareDateTime()))
                .ForMember(dest => dest.FamilyName, opt => opt.MapFrom(src => src.PatientFamilyName))
                .ForMember(dest => dest.GivenName, opt => opt.MapFrom(src => src.PatientFirstName))
                .ForMember(dest => dest.SecondInitial, opt => opt.MapFrom(src => src.PatientSecondInitial.ToTextOrNull()))
                .ForMember(dest => dest.Sex, opt => opt.ConvertUsing(new GenderFormatter(), src => src.PatientGender))
                .AfterMap((src, dest) => dest.GivenName = string.IsNullOrWhiteSpace(dest.GivenName) && !string.IsNullOrWhiteSpace(dest.FamilyName) ? "Onlyname" : dest.GivenName)
                ;

            CreateMap<DVAVerification, ResidentialAddress>()
                .ForMember(dest => dest.Locality, opt => opt.MapFrom(src => src.PatientAddressLocality))
                .ForMember(dest => dest.Postcode, opt => opt.MapFrom(src => src.PatientAddressPostcode))
                ;

            CreateMap<DVAVerification, VeteranMembership>()
                .ForMember(dest => dest.VeteranNumber, opt => opt.MapFrom(src => src.VeteranFileNum))
                ;

        }
        #endregion
        #endregion

        #region Enterprise
        #region EnterpriseConcessionVerification
        protected void EnterpriseConcessionVerification() { }
        #endregion
        #region EnterpriseVeteranVerificationRequest
        protected void EnterpriseVeteranVerificationRequest() { }
        #endregion
        #endregion

        #region Billing
        #region DirectBillClaim
        protected void DirectBillClaim() { }
        #endregion
        #region BulkBillPaymentReport
        protected void BulkBillPaymentReport() { }
        #endregion
        #region BulkBillProcessingReport
        protected void BulkBillProcessingReport() { }
        #endregion
        #endregion

        #region Claim
        #region PatientClaimInteractive
        protected void PatientClaimInteractive()
        {
            //Allows mutiple source class objects (PatientClaim & MedicareOnlineClaim)
            //to map to a single returned PatientClaimInteractiveRequest

            // PatientClaim Mapping definitions
            CreateMap<xmlData.PatientClaim, PatientClaimInteractiveRequest>()
                .ForMember(dest => dest.PatientClaimInteractive, opt => opt.ConvertUsing(new ClaimInteractiveXmlDataConverter(), src => src))
                .ForAllOtherMembers(opt => opt.Ignore())
                ;

            // MedicareOnlineClaim Mapping definitions
            CreateMap<physioData.MedicareOnlineClaim, PatientClaimInteractiveRequest>()
                .ForMember(dest => dest.PatientClaimInteractive, opt => opt.MapFrom(src => src))
                .ForAllOtherMembers(opt => opt.Ignore())
                ;

            CreateMap<physioData.MedicareOnlineClaim, ClaimInteractive>()
                .ForMember(dest => dest.AuthorisationDate, opt => opt.MapFrom(src => src.AuthorisationDate))
                .ForMember(dest => dest.ReferralOverrideCode, opt => opt.MapFrom(src => src.ReferralOverrideTypeCde.MapEnumDescription()))
                .ForMember(dest => dest.PayeeProvider, opt => opt.MapFrom(src => src))
                .ForMember(dest => dest.ServiceProvider, opt => opt.MapFrom(src => src))
                .ForMember(dest => dest.Referral, opt => opt.ConvertUsing(new ClaimReferralConverter(), src => src))
                .ForMember(dest => dest.Claimant, opt =>
                {
                    opt.PreCondition((src, dest, context) => !dest.HadClaimantData && dest?.Claimant?.ResidentialAddress != null);
                    opt.ConvertUsing(new ClaimInteractivePhysioDataConverter(), src => src);
                })
                .ForAllOtherMembers(opt => opt.Ignore())
                ;

            CreateMap<physioData.MedicareOnlineClaim, ServiceProvider>()
                .ForMember(dest => dest.ProviderNumber, opt => opt.MapFrom(src => src.ServicingProviderNum));
            
            CreateMap<physioData.MedicareOnlineClaim, PayeeProvider>()
                .ForMember(dest => dest.ProviderNumber, opt => opt.MapFrom(src => src.PayeeProviderNum));
        }

        #endregion
        #region PatientClaimStoredAndForward
        protected void PatientClaimStoredAndForward() { }
        #endregion
        #region VaaClaim
        protected void VaaClaim() { }
        #endregion
        #endregion

        #region DVA
        #region DvaMedicalPaperlessClaims
        protected void DvaMedicalPaperlessClaims() { }
        #endregion
        #region DvaPaymentReport
        protected void DvaPaymentReport() { }
        #endregion
        #region DvaProcessingReport
        protected void DvaProcessingReport() { }
        #endregion
        #endregion

        #region Custom Converters
        protected internal class CurrencyFormatter : IValueConverter<decimal, string>
        {
            public string Convert(decimal source, ResolutionContext context)
                => source.ToString("c");
        }

        protected internal class GenderFormatter : IValueConverter<string, string>
        {
            public string Convert(string sourceMember, ResolutionContext context) => sourceMember.MapGenderToSex();
        }

        protected internal class ClaimInteractivePhysioDataConverter : IValueConverter<physioData.MedicareOnlineClaim, Claimant>
        {
            public Claimant Convert(physioData.MedicareOnlineClaim medicareOnlineClaim, ResolutionContext context)
            {
                return new Claimant
                {
                    ResidentialAddress = new ResidentialAddress
                    {
                        Locality = medicareOnlineClaim.PatientAddressLocality,
                        Postcode = medicareOnlineClaim.PatientAddressPostcode
                    }
                };
            }
        }

        protected internal class ClaimInteractiveXmlDataConverter : IValueConverter<xmlData.PatientClaim, ClaimInteractive>
        {
            public ClaimInteractive Convert(xmlData.PatientClaim xmlData, ResolutionContext context)
            {
                
                var patientClaim = new ClaimInteractive
                {
                    AccountPaidInd = xmlData.AccountPaidInd,
                    SubmissionAuthorityInd = xmlData.SubmissionAuthorityInd,
                    Claimant = new Claimant()
                };
                

                patientClaim.HadClaimantData = false;

                if (xmlData.Claimant != null && xmlData.Claimant.QuotedMedicareCardNum != null)
                {
                    patientClaim.HadClaimantData = true;

                    patientClaim.Claimant.Medicare = new Medicare
                    {
                        MemberNumber = xmlData?.Claimant?.QuotedMedicareCardNum ?? "" + xmlData?.Claimant?.QuotedMedicareCardIssueNum ?? "",
                        MemberRefNumber = xmlData?.Claimant?.QuotedSubnumerate
                    };
                    patientClaim.Claimant.Identity = new Identity
                    {
                        FamilyName = xmlData?.Claimant?.LastName,
                        GivenName = xmlData?.Claimant?.FirstName,
                        DateOfBirth = xmlData?.Claimant?.DateOfBirth.MapToMedicareDateTime()
                    };
                    patientClaim.Claimant.ContactDetails = new ContactDetails
                    {
                        PhoneNumber = xmlData.Claimant.PhoneNum
                    };
                    patientClaim.Claimant.ResidentialAddress = new ResidentialAddress
                    {
                        AddressLineOne = xmlData?.Claimant?.Address?.AddressLine1,
                        AddressLineTwo = xmlData?.Claimant?.Address?.AddressLine2,
                        Locality = xmlData?.Claimant?.Address?.Locality,
                        Postcode = xmlData?.Claimant?.Address?.PostCode,
                    };

                    if (xmlData?.Payment?.PaymentEftClaimant != null)
                    {
                        patientClaim.Claimant.EftDetails = new EftDetails
                        {
                            AccountName = xmlData.Payment.PaymentEftClaimant.AccountName,
                            AccountNumber = xmlData.Payment.PaymentEftClaimant.AccountNum,
                            BsbCode = xmlData.Payment.PaymentEftClaimant.BSBCode
                        };
                    }
                }
                if (xmlData.MedicalEvent != null && xmlData?.MedicalEvent.Count > 0)
                {
                    patientClaim.MedicalEvent = new List<MedicalEvent>();

                    var patientXml = xmlData.MedicalEvent[0].Patient;

                    if (patientXml != null)
                    {
                        patientClaim.Patient = new ClaimInteractivePatient
                        {
                            Medicare = new Medicare
                            {
                                MemberNumber = (patientXml?.QuotedMedicareCardNum ?? "") + (patientXml?.QuotedMedicareCardIssueNum ?? ""),
                                MemberRefNumber = patientXml?.QuotedSubnumerate
                            },
                            Identity = new Identity
                            {
                                DateOfBirth = patientXml?.DateOfBirth.MapToMedicareDateTime(),
                                FamilyName = patientXml?.LastName,
                                GivenName = patientXml?.FirstName,
                            },

                        };
                        foreach (var medicalEventXml in xmlData.MedicalEvent)
                        {
                            patientClaim.MedicalEvent.Add(MapMedicalEventXmlToMedicalEvent(medicalEventXml));
                        }
                    }

                    if (!patientClaim.HadClaimantData)
                    {
                        // https://healthsoftware.humanservices.gov.au/claiming/ext-vnd/node/4036
                        // Claimant is now mandatory in new web services so if no claimant
                        // Map claimant info from patient data.
                        patientClaim.Claimant.Medicare = new Medicare
                        {
                            MemberNumber = patientXml.QuotedMedicareCardNum + patientXml.QuotedMedicareCardIssueNum,
                            MemberRefNumber = patientXml.QuotedSubnumerate
                        };

                        patientClaim.Claimant.Identity = new Identity
                        {
                            DateOfBirth = patientXml.DateOfBirth.MapToMedicareDateTime(),
                            FamilyName = patientXml.LastName,
                            GivenName = patientXml.FirstName
                        };
                    }
                   
                }
                return patientClaim;
            }
            protected MedicalEvent MapMedicalEventXmlToMedicalEvent(Gensolve.MedicareOnline.POCO.MedicalEvent medicalEventXml)
            {
                return new MedicalEvent()
                {
                    Id = medicalEventXml.Id,
                    MedicalEventDate = medicalEventXml.MedicalEventDate.MapToMedicareDate(),
                    MedicalEventTime = medicalEventXml.MedicalEventTime.MapMedicalEventTime(),

                    Service = MapServicesXmlToServices(medicalEventXml)
                };
            }
            protected List<Requests.Shared.Service> MapServicesXmlToServices(Gensolve.MedicareOnline.POCO.MedicalEvent medicalEventXml)
            {
                var serviceList = new List<Requests.Shared.Service>();
                foreach (var serviceXml in medicalEventXml.Service)
                {
                    serviceList.Add(new Requests.Shared.Service
                    {
                        Id = serviceXml.Id,
                        AccessionDateTime = serviceXml.AccessionDateTime.MapToMedicareDateTime().ToString().ToTextOrNull(),
                        AftercareOverrideInd = serviceXml.AftercareOverrideInd,
                        ChargeAmount = serviceXml.Charge,
                        CollectionDateTime = serviceXml.CollectionDateTime.MapToMedicareDateTime().ToString().ToTextOrNull(),
                        DuplicateServiceOverrideInd = serviceXml.DuplicateServiceOverrideInd.ToTextOrNull(),
                        FieldQuantity = serviceXml.FieldQty.ToTextOrNull(),
                        ItemNumber = serviceXml.Item,
                        LspNumber = serviceXml.LspNum.ToTextOrNull(),
                        MultipleProcedureOverrideInd = serviceXml.MultipleProcedureOverrideInd.ToTextOrNull(),
                        NumberOfPatientsSeen = serviceXml.NoOfPatientsSeen,
                        RestrictiveOverrideCode = serviceXml.RestrictiveOverrideCde.ToTextOrNull(),
                        Rule3ExemptInd = serviceXml.Rule3ExemptInd,
                        S4b3ExemptInd = serviceXml.S4b3ExemptInd,
                        SelfDeemedCode = serviceXml.SelfDeemedCde,
                        Text = serviceXml.Text,
                        TimeDuration = serviceXml.TimeDuration.ToTextOrNull()
                    });
                }
                return serviceList;
            }

        }

        protected internal class ClaimReferralConverter : IValueConverter<physioData.MedicareOnlineClaim, Referral>
        {
            public Referral Convert(physioData.MedicareOnlineClaim medicareOnlineClaim, ResolutionContext context)
            {
                return new Referral
                {
                    IssueDate = medicareOnlineClaim.ReferralIssueDate,
                    Period = medicareOnlineClaim.PeriodTypeText.MapStringToNumber<int>(),
                    PeriodCode = medicareOnlineClaim.ReferralPeriodType.MapEnumDescription(),
                    TypeCode = medicareOnlineClaim.serviceType.MapEnumDescription(),
                    Provider = new Provider
                    {
                        ProviderNumber = medicareOnlineClaim.ReferringProviderNum
                    }
                };
            }
        }
        protected MedicalEvent MapMedicalEventXmlToMedicalEvent(Gensolve.MedicareOnline.POCO.MedicalEvent medicalEventXml)
        {
            return new MedicalEvent()
            {
                Id = medicalEventXml.Id,
                MedicalEventDate = medicalEventXml.MedicalEventDate.MapToMedicareDate(),
                MedicalEventTime = medicalEventXml.MedicalEventTime.MapMedicalEventTime(),

                Service = MapServicesXmlToServices(medicalEventXml)
            };
        }
        protected List<Requests.Shared.Service> MapServicesXmlToServices(Gensolve.MedicareOnline.POCO.MedicalEvent medicalEventXml)
        {
            var serviceList = new List<Requests.Shared.Service>();
            foreach (var serviceXml in medicalEventXml.Service)
            {
                serviceList.Add(new Requests.Shared.Service
                {
                    Id = serviceXml.Id,
                    AccessionDateTime = serviceXml.AccessionDateTime.MapToMedicareDateTime().ToString().ToTextOrNull(),
                    AftercareOverrideInd = serviceXml.AftercareOverrideInd,
                    ChargeAmount = serviceXml.Charge,
                    CollectionDateTime = serviceXml.CollectionDateTime.MapToMedicareDateTime().ToString().ToTextOrNull(),
                    DuplicateServiceOverrideInd = serviceXml.DuplicateServiceOverrideInd.ToTextOrNull(),
                    FieldQuantity = serviceXml.FieldQty.ToTextOrNull(),
                    ItemNumber = serviceXml.Item,
                    LspNumber = serviceXml.LspNum.ToTextOrNull(),
                    MultipleProcedureOverrideInd = serviceXml.MultipleProcedureOverrideInd.ToTextOrNull(),
                    NumberOfPatientsSeen = serviceXml.NoOfPatientsSeen,
                    RestrictiveOverrideCode = serviceXml.RestrictiveOverrideCde.ToTextOrNull(),
                    Rule3ExemptInd = serviceXml.Rule3ExemptInd,
                    S4b3ExemptInd = serviceXml.S4b3ExemptInd,
                    SelfDeemedCode = serviceXml.SelfDeemedCde,
                    Text = serviceXml.Text,
                    TimeDuration = serviceXml.TimeDuration.ToTextOrNull()
                });
            }
            return serviceList;
        }

    }

    #endregion
}


public static class MapperExtensions
{
    internal static readonly string MedicareFormat = "ddMMyyyy";
    internal static readonly string MedicareDateFormat = "yyyy-MM-dd";

    public static IMultiMapBuilder<TDestination> StartMultiSourceMapping<TDestination>(this IMapper mapper, object source) => new MultiMapBuilder<TDestination>(mapper, source);

    public static string MapGenderToSex(this string gender)
    {
        if (gender == "M") { return "1"; }
        if (gender == "F") { return "2"; }
        if (gender == "O") { return "3"; }
        return "9";
    }
    public static string MapEnumDescription(this Enum enumeration)
    {
        return EnumResourceAttributeReader.GetAttributeValue<EnumResourceAttribute, string>(enumeration);
    }
    public static T MapStringToNumber<T>(this string v, bool RoundResult = false)
    {
        object o = new object();
        try
        {
            decimal.TryParse(v, out decimal n);
            switch (Type.GetTypeCode(typeof(T)))
            {
                case TypeCode.SByte:
                case TypeCode.Byte:
                case TypeCode.Int16:
                case TypeCode.UInt16:
                case TypeCode.Int32:
                case TypeCode.UInt32:
                case TypeCode.Int64:
                case TypeCode.UInt64:
                    o = decimal.Truncate(n);
                    if (RoundResult)
                    {
                        o = decimal.Round(n);
                    }
                    break;
                case TypeCode.Single:
                case TypeCode.Double:
                case TypeCode.Decimal:
                    o = n;
                    break;
                default:
                    break;
            }
            return (T)Convert.ChangeType(o, typeof(T));
        }
        catch (Exception)
        {
            return default;
        }
    }
    public static string ToTextOrNull(this string self)
    {
        if (string.IsNullOrEmpty(self))
        {
            return null;
        }

        return self;
    }
    public static DateTime? MapToMedicareDateTime(this string date)
    {
        try
        {
            return DateTime.ParseExact(date, MedicareFormat, System.Globalization.CultureInfo.InvariantCulture);
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static string MapToMedicareDate(this string date)
    {
        try
        {
            return DateTime.ParseExact(date, MedicareFormat, System.Globalization.CultureInfo.InvariantCulture).ToString(MedicareDateFormat);
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static string MapCreateDateTime(this string date, string time)
    {
        if (string.IsNullOrEmpty(date) || string.IsNullOrEmpty(time))
        {
            return null;
        }
        try
        {
            var medicalEventDate = DateTime.ParseExact(date, MedicareFormat, System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
            var medicalEventTime = MapMedicalEventTime(time);

            var createdDateTime = medicalEventDate + "T" + medicalEventTime;
            return createdDateTime;

        }
        catch { return null; }

    }
    public static string MapMedicalEventTime(this string date)
    {
        try
        {
            if (string.IsNullOrEmpty(date))
            {
                return "00:00:00+09:30";
                // TODO NEED TO GRAB TIMEZONE FROM SITE TO ADD UTC OFFSET TO TIME FOR SITE LOCATION
            }
            return null;
        }
        catch
        {
            return null;
        }
    }
    public static string MapDateOffsetToDateTime(this string date)
    {
        try
        {
            return DateTimeOffset.ParseExact(date, "yyyy-MM-ddzzz", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-ddTHH:mm:ss.fffffffK");
        }
        catch
        {
            return null;
        }
    }
    public static DateTime? MapDateOffsetStringToDate(this string date)
    {
        try
        {
            return DateTimeOffset.ParseExact(date, "yyyy-MM-ddzzz", System.Globalization.CultureInfo.InvariantCulture).DateTime;
        }
        catch
        {
            return null;
        }
    }
    public interface IMultiMapBuilder<T>
    {
        IMultiMapBuilder<T> Then<TSource>(TSource source);
        T Map();
    }

    public class MultiMapBuilder<T> : IMultiMapBuilder<T>
    {
        private readonly IMapper _mapper;
        private readonly T _mappedObject;
        public MultiMapBuilder(IMapper mapper, object source)
        {
            _mapper = mapper;
            _mappedObject = mapper.Map<T>(source);
        }
        public IMultiMapBuilder<T> Then<TSource>(TSource source)
        {
            _mapper.Map(source, _mappedObject);
            return this;
        }

        public T Map()
        {
            return _mappedObject;
        }
    }
}
