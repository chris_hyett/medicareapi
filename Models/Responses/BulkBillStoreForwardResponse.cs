﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Responses
{
    public class BulkBillStoreForwardResponse
    {
        [JsonProperty("claimId")]
        public string ClaimId { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }
    }
}
