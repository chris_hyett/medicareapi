﻿using Gensolve.Physio.Server.Service.MedicareOnline.Models.Responses.Shared;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Responses
{
    public class ConcessionVerificationResponse
    {
        [JsonProperty("medicareStatus")]
        public MedicareStatus MedicareStatus { get; set; }

        [JsonProperty("ConcessionStatus")]
        public ConcessionStatus ConcessionStatus { get; set; }
    }
}
