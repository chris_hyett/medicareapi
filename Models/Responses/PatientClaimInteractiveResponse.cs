﻿using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests.Shared.Claims.Responses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Responses
{
    public class PatientClaimInteractiveResponse
    {
        [JsonProperty("claimAssessment")]
        public ClaimAssessmentResponse ClaimAssessment { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }
    }
}
