﻿using Gensolve.Physio.Server.Service.MedicareOnline.Models.Responses.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Responses
{
    public class DvaVerificationResponse
    {
        public VeteranStatus VeteranStatus { get; set; }
    }
}
