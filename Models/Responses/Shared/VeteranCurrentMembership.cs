﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Responses.Shared
{
    public class VeteranCurrentMembership
    {
        public string VeteranNumber { get; set; }
        public string EntitlementCode { get; set; }
    }
}
