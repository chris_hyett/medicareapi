﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Responses.Shared
{
    public class ServiceMessageResponse
    {
        public string HighestSeverity { get; set; }
        public List<ServiceMessage> ServiceMessage { get; set; }

        public string ToMessageText()
        {
            var messageText = string.Empty;
            foreach(var message in this.ServiceMessage)
            {
                messageText += "Severity:" + message.Severity + Environment.NewLine +
                    "Code:" + this.ServiceMessage[0].Code + Environment.NewLine +
                    "Reason:" + this.ServiceMessage[0].Reason;
            }

            return messageText;
        }


        public string ErrorCode()
        {
            if(this.ServiceMessage.Count > 0)
            {
                return this.ServiceMessage[0].Code;
            }

            return string.Empty;
        }
    }
}
