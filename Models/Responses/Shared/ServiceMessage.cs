﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Responses.Shared
{
    public class ServiceMessage
    {
        public string Code { get; set; }
        public string Severity { get; set; }
        public string Reason { get; set; }
    }
}
