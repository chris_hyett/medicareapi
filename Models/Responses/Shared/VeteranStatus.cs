﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Responses.Shared
{
    public class VeteranStatus
    {
        public VeteranCurrentMembership CurrentMembership { get; set; }
        public CurrentMember CurrentMember { get; set; }
        public Status Status { get; set; }
        public string ProcessDate { get; set; }
    }
}
