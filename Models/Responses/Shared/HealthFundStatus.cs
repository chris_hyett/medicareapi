﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Responses.Shared
{
    public class HealthFundStatus
    {
        [JsonProperty("status")]
        public Status Status { get; set; }

        [JsonProperty("currentMembership")]
        public CurrentMembership CurrentMembership { get; set; }

        [JsonProperty("currentMember")]
        public CurrentMember CurrentMember { get; set; }

        [JsonProperty("processDate")]
        public string ProcessDate { get; set; }
    }
}
