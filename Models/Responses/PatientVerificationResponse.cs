﻿using Gensolve.Physio.Server.Service.MedicareOnline.Models.Responses.Shared;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gensolve.Physio.Server.Service.MedicareOnline.Models.Responses
{
    public class PatientVerificationResponse
    {
        [JsonProperty("medicareStatus")]
        public MedicareStatus MedicareStatus { get; set; }

        [JsonProperty("healthFundStatus")]
        public HealthFundStatus HealthFundStatus { get; set; }

        [JsonProperty("processDate")]
        public string ProcessDate { get; set; }
    }
}
