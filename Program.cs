﻿using Gensolve.MedicareOnline.MedicareOnlineRequestXml;
using Gensolve.PhysioData.Common.Medicare;
using System;


namespace Gensolve.Physio.Server.Service.MedicareOnline
{
    class Program
    {
        static readonly MedicareOnlineService MOS = new MedicareOnlineService();
        static void Main()
        {
            //OnlinePatientVerification();



            PatientClaimInteractive();




            Console.ReadKey();
        }
        protected static void OnlinePatientVerification()
        {
            //Patient Verification
            var patientVerification = new Gensolve.MedicareOnline.PatientVerification
            {

                OPVTypeCde = "PVM",
                EarliestDateOfService = DateTime.Now.AddDays(-2).ToUniversalTime().ToString(),
                PatientMedicareCardNum = "2954077821",
                PatientReferenceNum = "8",
                ServicingProviderNum = "ABC",
                PatientFamilyName = "Hyett",
                PatientFirstName = "Chris",
                PatientDateOfBirth = DateTime.Parse("29/07/1966").ToString("ddMMyyyy", System.Globalization.CultureInfo.InvariantCulture),
                Gender = "M",
                PatientAliasFamilyName = "Brown",
                //PatientAliasFirstName = "Chris"
            };
            MOS.OnlinePatientVerification(patientVerification);
        }
        protected static void PatientClaimInteractive()
        {
            var xmlData = new PatientClaim
            {
                AccountPaidInd = "123456",
                SubmissionAuthorityInd = "1400",
                Payment = new Payment
                {

                    PaymentEftClaimant = new PaymentEftClaimant
                    {
                        AccountName = "PayMe",
                        BSBCode = "bbsCode",
                        AccountNum = "12-1234-123456-123"
                    }
                },
                Claimant = new Claimant
                {
                    QuotedMedicareCardNum = "295407782",
                    QuotedMedicareCardIssueNum = "1400",
                    QuotedSubnumerate = "1400",
                    LastName = "Hyett1400",
                    FirstName = "Chris1400",
                    DateOfBirth = DateTime.Parse("29/07/1966").ToString("ddMMyyyy", System.Globalization.CultureInfo.InvariantCulture),
                    PhoneNum = "12312345",
                    Address = new Address
                    {
                        AddressLine1 = "A Street",
                        AddressLine2 = null,
                        Locality = "Perth",
                        PostCode = "1234"
                    }                    
                },
                MedicalEvent = new System.Collections.Generic.List<Gensolve.MedicareOnline.POCO.MedicalEvent> {
                    new Gensolve.MedicareOnline.POCO.MedicalEvent
                    {
                     Id = "1500",
                     MedicalEventDate = DateTime.Now.AddDays(-20).ToUniversalTime().ToString(),
                     MedicalEventTime = DateTime.Now.TimeOfDay.ToString(),
                     ReferralOverrideType = "4",
                         Patient = new Gensolve.MedicareOnline.POCO.Patient
                         {
                             QuotedMedicareCardNum = "295407782",
                             QuotedMedicareCardIssueNum = "1500",
                             QuotedSubnumerate = "1500",
                             LastName = "Hyett1500",
                             FirstName = "Chris1500",
                             DateOfBirth = DateTime.Parse("29/07/1966").ToString("ddMMyyyy", System.Globalization.CultureInfo.InvariantCulture),
                             Gender = "M"
                         },
                         Service = new System.Collections.Generic.List<Gensolve.MedicareOnline.POCO.Service>
                         {
                            new Gensolve.MedicareOnline.POCO.Service
                            {
                                 Id = "1501",
                                 AccessionDateTime = DateTime.Now.AddDays(-20).ToUniversalTime().ToString(),
                                 AftercareOverrideInd = "1",
                                 ChargeAmount = "10.00",
                                 CollectionDateTime = DateTime.Now.AddDays(-20).ToUniversalTime().ToString(),
                                 DuplicateServiceOverrideInd = "Test",
                                 FieldQty = "10",
                                 Item = "1",
                                 LspNum = "1",
                                 MultipleProcedureOverrideInd = "20",
                                 NoOfPatientsSeen = "2",
                                 RestrictiveOverrideCde = "ABC",
                                 Rule3ExemptInd = "1",
                                 S4b3ExemptInd = "2",
                                 SelfDeemedCde = "ABC",
                                 Text = "Test",
                                 TimeDuration = "1"
                            },
                            new Gensolve.MedicareOnline.POCO.Service
                            {
                                Id = "1502",
                                AccessionDateTime = DateTime.Now.AddDays(-20).ToUniversalTime().ToString(),
                                AftercareOverrideInd = "1",
                                ChargeAmount = "10.00",
                                CollectionDateTime = DateTime.Now.AddDays(-20).ToUniversalTime().ToString(),
                                DuplicateServiceOverrideInd = "Test",
                                FieldQty = "10",
                                Item = "1",
                                LspNum = "1",
                                MultipleProcedureOverrideInd = "20",
                                NoOfPatientsSeen = "2",
                                RestrictiveOverrideCde = "ABC",
                                Rule3ExemptInd = "1",
                                S4b3ExemptInd = "2",
                                SelfDeemedCde = "ABC",
                                Text = "Test",
                                TimeDuration = "1"
                            },
                         },
                    },
                    new Gensolve.MedicareOnline.POCO.MedicalEvent
                    {
                    Id = "1800",
                    MedicalEventDate = DateTime.Now.AddDays(-20).ToUniversalTime().ToString(),
                    MedicalEventTime = DateTime.Now.TimeOfDay.ToString(),
                        Patient = new Gensolve.MedicareOnline.POCO.Patient
                        {
                            QuotedMedicareCardNum = "295407782",
                            QuotedMedicareCardIssueNum = "1800",
                            QuotedSubnumerate = "1800",
                            LastName = "Hyett1800",
                            FirstName = "Chris1800",
                            DateOfBirth = DateTime.Parse("29/07/1966").ToString("ddMMyyyy", System.Globalization.CultureInfo.InvariantCulture),
                            Gender = "M"
                        },
                        Service = new System.Collections.Generic.List<Gensolve.MedicareOnline.POCO.Service>
                        {
                           new Gensolve.MedicareOnline.POCO.Service
                           {
                                Id = "1801",
                                AccessionDateTime = DateTime.Now.AddDays(-20).ToUniversalTime().ToString(),
                                AftercareOverrideInd = "1",
                                ChargeAmount = "10.00",
                                CollectionDateTime = DateTime.Now.AddDays(-20).ToUniversalTime().ToString(),
                                DuplicateServiceOverrideInd = "Test",
                                FieldQty = "10",
                                Item = "1",
                                LspNum = "1",
                                MultipleProcedureOverrideInd = "20",
                                NoOfPatientsSeen = "2",
                                RestrictiveOverrideCde = "ABC",
                                Rule3ExemptInd = "1",
                                S4b3ExemptInd = "2",
                                SelfDeemedCde = "ABC",
                                Text = "Test",
                                TimeDuration = "1"
                           },
                           new Gensolve.MedicareOnline.POCO.Service
                           {
                               Id = "1802",
                               AccessionDateTime = DateTime.Now.AddDays(-20).ToUniversalTime().ToString(),
                               AftercareOverrideInd = "1",
                               ChargeAmount = "10.00",
                               CollectionDateTime = DateTime.Now.AddDays(-20).ToUniversalTime().ToString(),
                               DuplicateServiceOverrideInd = "Test",
                               FieldQty = "10",
                               Item = "1",
                               LspNum = "1",
                               MultipleProcedureOverrideInd = "20",
                               NoOfPatientsSeen = "2",
                               RestrictiveOverrideCde = "ABC",
                               Rule3ExemptInd = "1",
                               S4b3ExemptInd = "2",
                               SelfDeemedCde = "ABC",
                               Text = "Test",
                               TimeDuration = "1"
                           },
                        }
                    }
                }
            };

            var medicareOnlineClaim = new ESAMedicareOnlineClaimObjectFactory().GetMedicareOnlineClaim();
            medicareOnlineClaim.AuthorisationDate = DateTime.Now;
            medicareOnlineClaim.PatientFamilyName = "Hyett001";
            medicareOnlineClaim.PatientFirstName = "Chris001";
            medicareOnlineClaim.PatientDateOfBirth = DateTime.Parse("29/07/1966");
            medicareOnlineClaim.PatientGender = "M";
            medicareOnlineClaim.PatientAddressLocality = "Perth 001";
            medicareOnlineClaim.PatientAddressPostcode = "1234";

            medicareOnlineClaim.ReferralOverrideTypeCde = PhysioData.Common.PhysioEnums.MedicareReferralOverride.Hospital;
            medicareOnlineClaim.ReferralIssueDate = DateTime.Parse("30/11/2021");
            medicareOnlineClaim.PeriodTypeText = "09";
            medicareOnlineClaim.ReferringProviderNum = "12345";
            medicareOnlineClaim.ReferralPeriodType = PhysioData.Common.PhysioEnums.MedicareReferralPeriodType.Standard;
            medicareOnlineClaim.serviceType = PhysioData.Common.PhysioEnums.MedicareServiceType.Specialist;
            medicareOnlineClaim.PayeeProviderNum = "1098";
            medicareOnlineClaim.ServicingProviderNum = "123456";




            MOS.PatientClaimInteractive(xmlData, medicareOnlineClaim);
        }
    }

}
