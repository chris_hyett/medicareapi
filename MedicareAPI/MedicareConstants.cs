﻿namespace Gensolve.Physio.Server.Service.MedicareOnline
{
    public class MedicareConstants
    {
        public const string AllowedHospitalValue = "Y";
        public const string SuccessStatusCode = "0";
    }

    public class MedicareFormatConstants
    {
        public const string DateFormat = "yyyy-MM-dd";
    }

    public class MedicareHeaderConstants
    {
        public const string DhsCorrelationId = "dhs-correlationid";
        public const string DhsSubjectIdType = "dhs-subjectidtype";
        public const string DhsProductid = "dhs-productid";
        public const string DhsAuditIdType = "dhs-auditidtype";
        public const string DhsMessageId = "dhs-messageid";
        public const string DhsSubjectId = "dhs-subjectid";
        public const string DhsAuditId = "dhs-auditid";
        public const string Authorization = "authorization";
        // apiKey located in header Client ID in Medicare
        public const string XIbmClientId = "x-ibm-client-id";
    }

    public class MedicareSubjectConstants
    {
        public const string NA = "N/A";
        public const string MedicareCard = "Medicare Card";
        public const string VeteranNumber = "Veteran Number";
    }
    public class ReferralOverrideCodes
    {
        public const string Hospital = "H";
        public const string Lost = "L";
        public const string Emergency = "E";
        public const string NotRequired = "N";
    }
    public class ReferralTypeCodes
    {
        public const string Pathology = "P";
        public const string DiagnosticImaging = "D";
        public const string SpecialistAndAllied = "S";
    }

    public class SelfDeemedCodes
    {
        public const string SelfDeemed = "SD";
        public const string SubstitutedService = "SS";
    }

    public class ServiceTypeCodes
    {
        public const string General = "O";
        public const string Specialist = "S";
        public const string Pathology = "P";
    }
    public class MedicareAuditConstants
    {
        public const string LocationId = "Location Id";
    }

    public class MedicareResponseHeaderKeys
    {
        public const string TransactionIdKey = "X-Global-Transaction-ID";
    }

    public class MedicareApiProcessTypes
    {
        public const string ApiRequest = "Request";
        public const string ApiResponse = "Response";
    }

    public class ValidationRegEx
    {
        /* Must Start with a Letter of the Alphabet - not space; punctuation;  0-9 */
        public const string StartsWithAlpha = @"\A[a-zA-Z]+";

        /* Only Letters of the Alphabet - not space; punctuation;  0-9 */
        public const string OnlyAlpha = @"^[a-zA-Z]+$";

        /* Only Numeric - 0-9 */
        public const string OnlyNumeric = @"^[0-9]+$";

        /* Only Letters of the Alphabet or Numbers - not space; punctuation*/
        public const string OnlyAlphaNumeric = @"^[a-zA-Z0-9]+$";

        /* Only Alphanumeric -'., characters and only 1 space between words */
        public const string AlphanumericNoDoubleSpace = @"^([A-Za-z0-9\-']+ )+[A-Za-z0\-9\-']+$|^[A-Za-z0-9\-']+$";

        /* Allow no ending space; no ' followed by space; no - follwed by space; No -' or '-   */
        public const string AlphanumericApostropheHyphen = @"^[a-zA-Z0-9]+(?:[-' ][a-zA-Z0-9]+)*$";           /* Allow no ending space; no ' followed by space; no - follwed by space; No -' or '-   */
    }
    public class ErrorCode
    {
        //Medicare Defined Error Codes E_
        public static string E9202 = "9202";
        public static string E2025 = "2025";
        public static string E2030 = "2030";
        public static string E2032 = "2032";
    }
    //Medicare Defined Error Messages M_
    // Note theses may be changed by Medicare in the future so defined here and used all over :)
    public class ErrorMessage
    {
        public const string TypeCodeLength = "Invalid value of [{PropertyValue}] supplied for Patient Verification Type Code. The value supplied must be PVM (Medicare Only), PVF (Fund Only) or OPV (Medicare and Fund).";
        public const string InvalidTypeCode = "The details in this request are inconsistent with the service called. Amend and resubmit.";
        //
        public const string DobRequired = "Patient Date of Birth must be supplied.";
        public const string DosGtDob = "Invalid value of [{PropertyValue}] supplied for Date of Service. The date supplied must be on or after Patient Date of Birth. ";
        public const string DosFutureDate = "Invalid value of [{PropertyValue}] supplied for Date of Service. The date supplied must not be in the future. ";
        public const string DosLt24Months = "Invalid value of [{PropertyValue}] supplied for Date of Service. The date supplied must be less than two years before the date the request is received. ";
        public const string DobFutureDate = "Invalid value of [{PropertyValue}] supplied for Patient Date of Birth. The date supplied must not be in the future.";
        public const string DobGt130 = "Invalid value of [{PropertyValue}] supplied for Patient Date of Birth. The date supplied must not be greater than 130 years ago.";
        public const string PtFamilyNameNotSupplied = "Patient Family Name must be supplied. If the patient only has one name, their name must be supplied in this field and the text *Onlyname* must be supplied in the Patient Given Name field.";
        public const string PtFamilyNameLength = "Invalid value of [{ PropertyValue}] supplied for Patient Family Name. The value supplied must not be less than 1 or more than 40 characters long.";
        public const string PtFamilyNameValidation = "Invalid value of [{PropertyValue}] supplied for Patient Family Name. The value supplied must contain alpha (A-Z and a-z), numeric (0-9), space ( ), apostrophe (') and hyphen (-) characters only. Spaces must not appear before or after apostrophes, hyphens, other spaces or the supplied value. The value must contain at least one alpha or numeric character.";
        public const string PtGivenNameNotSupplied = "Patient Given Name must be supplied. If the patient only has one name, their name must be supplied in this field and the text *Onlyname* must be supplied in the Patient Given Name field.";
        public const string PtGivenNameLength = "Invalid value of [{ PropertyValue}] supplied for Patient Given Name. The value supplied must not be less than 1 or more than 40 characters long.";
        public const string PtGivenNameValidation = "Invalid value of [{PropertyValue}] supplied for Patient Given Name. The value supplied must contain alpha (A-Z and a-z), numeric (0-9), space ( ), apostrophe (') and hyphen (-) characters only. Spaces must not appear before or after apostrophes, hyphens, other spaces or the supplied value. The value must contain at least one alpha or numeric character.";
        public const string PtSecondInitialLength = "Invalid value of [{PropertyValue}] supplied for Patient Second Initial. The value supplied must be an alpha (A-Z, a-z).";
        public const string PtSecondInitialValidation = "Invalid value of [{PropertyValue}] supplied for Patient Second Initial. The value supplied must be an alpha (A-Z, a-z).";
        public const string PtGender = "Invalid value of [{PropertyValue}] supplied for Patient's Sex. The value supplied must be 1 (Male), 2 (Female), 3 (Other), or 9 (Not stated/inadequately described).";
        //
        public const string MedicareRequired = "Patient Medicare Details must be supplied if Patient Verification Type Code is set to OPV (Medicare and Fund) or PVM (Medicare Only). ";
        public const string MedicareNotRequired = "Patient Medicare Details must not be supplied if Patient Verification Type Code is set to PVF (Fund Only). ";
        public const string HealthFundRequired = "Patient Health Fund Details must be supplied if Patient Verification Type Code is set to OPV (Medicare and Fund) or PVF (Fund Only). ";
        public const string HealthFundNotRequired = "Patient Health Fund Details must not be supplied if Patient Verification Type Code is set to PVM (Medicare Only) ";
        //
        public const string AliasFamilyNameNotSupplied = "Also Known As Family Name must be supplied. If the patient only has one name, their name must be supplied in this field and the text *Onlyname* must be supplied in the Patient Given Name field.";
        public const string AliasFamilyNameLength = "Invalid value of [{ PropertyValue}] supplied for Also Known As Family Name. The value supplied must not be less than 1 or more than 40 characters long.";
        public const string AliasFamilyNameValidation = "Invalid value of [{PropertyValue}] supplied for Also Known As Family Name. The value supplied must contain alpha (A-Z and a-z), numeric (0-9), space ( ), apostrophe (') and hyphen (-) characters only. Spaces must not appear before or after apostrophes, hyphens, other spaces or the supplied value. The value must contain at least one alpha or numeric character.";
        public const string AliasGivenNameNotSupplied = "Also Known As Given Name must be supplied. If the patient only has one name, their name must be supplied in this field and the text *Onlyname* must be supplied in the Patient Given Name field.";
        public const string AliasGivenNameLength = "Invalid value of [{ PropertyValue}] supplied for Also Known As Given Name. The value supplied must not be less than 1 or more than 40 characters long.";
        public const string AliasGivenNameValidation = "Invalid value of [{PropertyValue}] supplied for Also Known As Given Name. The value supplied must contain alpha (A-Z and a-z), numeric (0-9), space ( ), apostrophe (') and hyphen (-) characters only. Spaces must not appear before or after apostrophes, hyphens, other spaces or the supplied value. The value must contain at least one alpha or numeric character.";
        //
        public const string PtMedicareCardRequired = "Patient Medicare Card Number must be supplied.";
        public const string InvalidPtMedicareDetails = "Invalid value of [{PropertyValue}] supplied for Patient Medicare Card Number. The value supplied must be numeric and conform to the Medicare Card check digit routine. The 10th character (Card Issue Number) must not be set to zero.";
        public const string PtMedicareIndividualNumberRequired = "Patient Medicare Individual Reference Number must be supplied.";
        public const string InvalidPtMedicareIndividualNumber = "Invalid value of [{PropertyValue}] supplied for Patient Medicare Individual Reference Number. The value supplied must be numeric (1-9) and must not be set to zero.";
        //
        public const string PtHealthFundNumberRequired = "Patient Health Fund Member Number must be supplied.";
        public const string InvalidPtHealthFundNumber = "Invalid value of [{PropertyValue}] supplied for Patient Health Fund Member Number.The value supplied must be 1-19 alphanumeric characters.";
        public const string InvalidPtHealthFundRefNumber = "Invalid value of [{PropertyValue}] supplied for Health Fund Universal Patient Identifier (UPI). The value supplied must be single or double numeric values including zeros (00, 01, to 09 and 0 to 99).";
        public const string PtHealthFundOrgIdRequired = "Fund Brand Id must be supplied.";
        public const string InvalidPtHealthFundOrgId = "Invalid value of [{PropertyValue}] supplied for Fund Brand Id.The value supplied must be three alpha (A-Z) characters.";
        //
        public const string ResidentialAddressLocalityPostcodeRequired = "Address details supplied are incomplete. When a residential address is supplied, Locality and Postcode are required.";
        public const string ResidentialAddressLocalityIncludesStateCode = "Invalid value of [{PropertyValue}] supplied for Residential Address Locality. The value supplied must not include the State code.";
        public const string ResidentialAddressLocalityValidation = "Invalid value of [{PropertyValue}] supplied for Residential Address Locality. The value supplied must contain alpha (A-Z and a-z), numeric (0-9), space ( ), apostrophe (') and hyphen (-) characters only. Spaces must not appear before or after apostrophes, hyphens, other spaces or the supplied value. The value must contain at least one alpha or numeric character.";
        public const string ResidentialAddressPostCodeMustNot0000 = "Invalid value of [{PropertyValue}] supplied for Residential Address Postcode. The value supplied must not be 0000.";
        public const string ResidentialAddressPostCodeValidation = "Invalid value of [{PropertyValue}] supplied for Residential Address Postcode. The value supplied must be numeric (0-9) characters only.";
        //
        public const string VeteranNumberValidation = "Invalid value of [{PropertyValue}] supplied for Veteran Number. The value supplied must be in the format of - State code (1 uppercase alpha character), War code (up to 3 uppercase alpha characters or 1 space), Numeric Field (up to 6 numeric characters) and Dependant indicator (optional, 1 uppercase alpha character). The Veteran file number occupies up to 9 Characters in total.";
        //
        public const string BulkBillPostfix = " Error located in Medical Event {m}.";
        public const string BulkBillIncorrectTypeCode = "The details in this claim are inconsistent with the service called. Amend and resubmit.";
        public const string BulkBillInconsistentGeneralService = "The details in this claim are inconsistent with the service called. A General Service does not require a Referral Type Code, Referral Override Code or a Self Deemed Code. Amend and resubmit. Error located in Medical Event [{m}]. Note: {m} = Every medical event that is in error e.g. Error located in Medical Event [01, 02, 03….] etc ";
        public const string BulkBillInconsistentSpecialistService = "The details in this claim are inconsistent with the service called. A Specialist Service must contain a Diagnostic Request, Specialist Referral, Referral Override Code of H, L, E or N or a Self Deemed Code of SD or SS against at least one service per medical event. Amend and resubmit. Error located in Medical Event [{m}]. Note: {m} = Every medical event that is in error e.g. Error located in Medical Event [01, 02, 03….]etc ";
        public const string BulkBillInconsistentPathologyService = "The details in this claim are inconsistent with the service called. A Pathology Service must contain a Pathology Request, Referral Override Code of N or a Self Deemed Code of SD against at least one service per medical event. Amend and resubmit. Error located in Medical Event [{m}]. Note: {m} = Every medical event that is in error e.g. Error located in Medical Event [01, 02, 03….] etc ";
        public const string BulkBillExceeds80MedicalEvents = "A maximum of 80 Medical Events are allowed per Bulk Bill Claim. Additional Medical Events will need to be transmitted in a separate Bulk Bill Claim. ";
        public const string BulkBillExceeds14Services = "A maximum of 14 Services are allowed per Medical Event. Additional services will need to be transmitted in a separate Medical Event." + BulkBillPostfix;
        public const string BulkBillRequiresUniqueMedicalEventId = "Invalid value of [{2}] supplied for Medical Event Id. The value supplied must be unique within the claim. Note: the [{2}] value will return a list of Duplicate Id's within the claim. Each 'Duplicate Id' will be separated by a comma. Where there is only 1 'Duplicate Id' found, the supplied value will reflect the one 'Duplicate Id' value. ";
        public const string BulkBillIncorrectSequence = "Medical Event Id/s supplied are out of sequence. The value/s supplied must begin with 01 and increase by one as each Medical Event is added. ";
        public const string BulkBillMustBeUniqueClaim = "Invalid value of [{2}] supplied for Service Id. The value supplied must be unique within the claim. Note: the [{2}] value will return a list of Duplicate Id's within the claim. Each 'Duplicate Id' will be separated by a comma. Where there is only 1 'Duplicate Id' found, the supplied value will reflect the one 'Duplicate Id' value. ";
        public const string BulkBillInvalidFacilityFormat = "The value supplied must be in the format of - Provider stem (6 digit number), 1 Practice Location character, 1 Check Digit. ";
        public const string BulkBillRequiredHospitalIndicator = "If Hospital Indicator is set to Y (In Hospital) then Facility Id must be supplied.";
        public const string BulkBillInvalidServiceProvider = "Invalid value of [{PropertyValue}] supplied for Servicing Provider Number. The value supplied must be in the format of - Provider stem (6 digit number), 1 Practice Location character, 1 Check Digit.  ";
        public const string BulkBillServiceProviderMatchesPayeeProvider = "The Servicing Provider must not be the same person as the Payee Provider. If they are the same person only supply the Servicing Provider";
        public const string BulkBillInvalidPayeeProvider = "Invalid value of [{PropertyValue}] supplied for Payee Provider Number. The value supplied must be in the format of - Provider stem (6 digit number), 1 Practice Location character, 1 Check Digit. ";
        public const string BulkBillInvlidAuthorisationDate = "Invalid value of [{PropertyValue}] supplied for Authorisation Date. The date supplied must not be in the future." + BulkBillPostfix;
        public const string BulkBillAuthorisationDateBeforeMedicalEventDate = "Authorisation Date must not be before Medical Event Date." + BulkBillPostfix;
        public const string BulkBillFutureClaimDate = "Invalid value of [{PropertyValue}] supplied for Medical Event Creation Date Time. The date-time supplied must not be in the future." + BulkBillPostfix;
        public const string BulkBillFutureMedicalEventCreateDate = "Medical Event Creation Date Time must not be before Medical Event Date and Medical Event Time (if supplied)." + BulkBillPostfix;
        public const string BulkBillTimeZoneMedicalEventDate = "Invalid value of [{PropertyValue}] supplied for Medical Event Creation Date Time. The date-time supplied must reflect the time zone in Australia, the transaction was created in.  Valid time zone is UTC + local offset." + BulkBillPostfix;
        public const string BulkBillFutureMedicalEventDate = "Invalid value of [{PropertyValue}] supplied for Medical Event Date. The date supplied must not be in the future." + BulkBillPostfix;
        public const string BulkBillMedicalEventDateGreater24months = "Invalid value of [{PropertyValue}] supplied for Medical Event Date. The date supplied must be less than two years before the date the request is received." + BulkBillPostfix;
        public const string BulkBillFutureMedicalEventTime = "Invalid value of [{PropertyValue}] supplied for Medical Event Time. The time supplied must not be in the future." + BulkBillPostfix;
        public const string BulkBillTimeZoneMedicalEventTime = "Invalid value of [{PropertyValue}] supplied for Medical Event Time. The time supplied must reflect the time zone in Australia, the transaction was created in. Valid time zone is UTC + local offset." + BulkBillPostfix;
        public const string BulkBillInvalidReferalOverrideCode = "Invalid value of [{PropertyValue}] supplied for Referral Override Code. The value supplied must be either L (Lost), E (Emergency), H (Hospital) or N (Not required)." + BulkBillPostfix;
        public const string BulkBillReferalOverrideAdditionalInformationRequired = "Additional information required. If Referral Override Code is set to H (Hospital) then Hospital Indicator must be set to Y (In Hospital)." + BulkBillPostfix;
        public const string BulkBillInvalidSubmissionAuthority = "Submission Authority Indicator must be set to Y (Authorised) indicating that the Medicare benefit was assigned by the patient, who retained a copy of the DB4 form." + BulkBillPostfix;
        public const string BulkBillReferalDetailsNotRequired = "Referral Details must not be set if Referral Override Code is set." + BulkBillPostfix;
        public const string BulkBillFututreIssueDate = "Invalid value of [{PropertyValue}] supplied for Referral Issue Date. The date supplied must not be in the future." + BulkBillPostfix;
        public const string BulkBillIssueDateAfterMedicalEventDate = "Referral Issue Date must not be after Medical Event Date." + BulkBillPostfix;
        public const string BulkBillIssueDateBeforeDateOfBirth = "Referral Issue Date must not be before Patient Date of Birth." + BulkBillPostfix;
        public const string BulkBillInvalidReferalPeriod = "Invalid value of [{PropertyValue}] supplied for Referral Period. The value supplied must be numeric (1 - 98), representing months." + BulkBillPostfix;
        public const string BulkBillReferalPeriodAdditionalInformationRequired = "Additional information required. If Referral Period Code is set to N (Non Standard) then Referral Period must be supplied." + BulkBillPostfix;
        public const string BulkBillInvalidReferalPeriodCode = "Invalid value of [{PropertyValue}] supplied for Referral Period Code. The value supplied must be either S (Standard - 12 months for a GP, 3 months for Specialist), N (Non Standard), or I (Indefinite)." + BulkBillPostfix;
        public const string BulkBillReferalPeriodNotRequired = "Referral Period must only be set when Referral Period Code is set to N (Non Standard)." + BulkBillPostfix;
        public const string BulkBillInvalidReferalTypeCode = "Invalid value of [{PropertyValue}] supplied for Referral Type Code. The value supplied must be either P (Pathology), D (Diagnostic Imaging) or S (Specialist)." + BulkBillPostfix;
        public const string BulkBillReferralPeriodCodeNotRequiredCriteria = "Referral Period and Referral Period Code must only be set when Referral Type Code is set to S (Specialist)." + BulkBillPostfix;
        public const string BulkBillReferalTypeCodeAdditionalInformationRequired = "Additional information required. If Referral Type Code is set to S (Specialist) then Referral Period Code must be supplied." + BulkBillPostfix;
        public const string BulkBillReferalTypeCodeServiceTypeCodeMustEqualP = "Service Type Code must be set to P (Pathology) when Referral Type Code is set to P." + BulkBillPostfix;
        public const string BulkBillServiceTypeCodeShouldEqualS = "Service Type Code must be set to S (Specialist) when Referral Type Code is set to D (Diagnostic Imaging) or S (Specialist)." + BulkBillPostfix;
        public const string BulkBillInvalidReferringProviderNumber = "Invalid value of [{PropertyValue}] supplied for Referring Provider Number. The value supplied must be in the format of - Provider stem (6 digit number), 1 Practice Location character, 1 Check Digit." + BulkBillPostfix;
        public const string BulkBillReferringProviderNumberMatchesPayeeProviderNumber = "Referring Provider Number must not be the same as the Payee Provider Number." + BulkBillPostfix;
        public const string BulkBillReferringProviderNumberMatchesServicingProviderNumber = "Referring Provider Number must not be the same as the Servicing Provider Number. ";
        public const string BulkBillDoBRequired = DobRequired + BulkBillPostfix;
        public const string BulkBillDobFutureDate = DobFutureDate + BulkBillPostfix;
        public const string BulkBillDobGt130 = DobGt130 + BulkBillPostfix;
        public const string BulkBillDobAfterMedicalEvent = "Medical Event Date must not be before Patient Date of Birth." + BulkBillPostfix;
        public const string BulkBillPtFamilyNameNotSupplied = PtFamilyNameNotSupplied + BulkBillPostfix;
        public const string BulkBillPtFamilyNameLength = PtFamilyNameLength + BulkBillPostfix;
        public const string BulkBillPtFamilyNameValidation = PtFamilyNameValidation + BulkBillPostfix; 
        public const string BulkBillPtGivenNameNotSupplied = PtGivenNameNotSupplied + BulkBillPostfix;
        public const string BulkBillPtGivenNameLength = PtGivenNameLength + BulkBillPostfix;
        public const string BulkBillPtGivenNameValidation = PtGivenNameValidation + BulkBillPostfix;
        public const string BulkBillInvalidPtMedicareDetails  = InvalidPtMedicareDetails + BulkBillPostfix;
        public const string BulkBillPtMedicareCardRequired = PtMedicareCardRequired + BulkBillPostfix;
        public const string BulkBillPtMedicareIndividualNumberRequired = PtMedicareIndividualNumberRequired + BulkBillPostfix;
        public const string BulkBillInvalidPtMedicareIndividualNumber = InvalidPtMedicareIndividualNumber + BulkBillPostfix;
        //
        public const string Pciw_GeneralServiceNotValid = "The details in this claim are inconsistent with the service called. A General Service does not require a Referral Type Code, Referral Override Code or a Self Deemed Code. Amend and resubmit.";
        public const string Pciw_SpecialistServiceNotValid = "The details in this claim are inconsistent with the service called. A Specialist Service must contain a Diagnostic Request, Specialist Referral, Referral Override Code of H, L, E or N or a Self Deemed Code of SD or SS against at least one service. Amend and resubmit.";
        public const string Pciw_PathologyServiceNotValid = "The details in this claim are inconsistent with the service called. A Pathology Service must contain a Pathology Request, Referral Override Code of N or a Self Deemed Code of SD against at least one service. Amend and resubmit.";
    }
}