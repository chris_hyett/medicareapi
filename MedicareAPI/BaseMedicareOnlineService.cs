﻿using AutoMapper;
using FluentValidation;
using FluentValidation.Results;
using Gensolve.Physio.Server.Service.MedicareOnline.JsonConverters;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;

namespace Gensolve.Physio.Server.Service.MedicareOnline
{

    public class BaseMedicareOnlineService
    {
        protected IMapper _mapper;
        protected IValidator _validator;
        protected ValidationResult _validationResult;
        public JsonSerializerSettings GenerateJsonSerializerSettings()
        {
            return new JsonSerializerSettings
            {
                ContractResolver = new DefaultContractResolver
                {
                    NamingStrategy = new CamelCaseNamingStrategy()
                },
                NullValueHandling = NullValueHandling.Ignore,
                Formatting = Formatting.Indented,
                DateFormatString = "yyyy-MM-dd",
                DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                Converters = new List<JsonConverter>
                {
                    new StringConverter<bool>(),
                    new StringConverter<int>(),
                    new StringConverter<long>()
                }
            };
        }
    }
}