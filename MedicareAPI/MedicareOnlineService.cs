﻿using AutoMapper;
using Gensolve.MedicareOnline;
using Gensolve.MedicareOnline.Interfaces;
using Gensolve.MedicareOnline.POCO;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.MappingProfiles;
using Gensolve.Physio.Server.Service.MedicareOnline.Models.Requests;
using Gensolve.PhysioData.Common.Medicare;
using Newtonsoft.Json;
using System;
using static Gensolve.PhysioData.Common.PhysioEnums;

namespace Gensolve.Physio.Server.Service.MedicareOnline
{
    public class MedicareOnlineService : BaseMedicareOnlineService, IMedicareOnlineService
    {
        public MedicareOnlineService()
        {
            _mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<MedicareApiMappingProfile>();
            }).CreateMapper();
        }

        public void OnlinePatientVerification(PatientVerification patientVerification)
        {
            //Mapping here
            var patientVerificationRequest = _mapper.Map<PatientVerification, PatientVerificationRequest>(patientVerification);

            //Validation here
            _validationResult = new Models.Validators.VerificationPatientRequestValidator((MedicarePatientValidatorType)Enum.Parse(typeof(MedicarePatientValidatorType), patientVerificationRequest.TypeCode)).Validate(patientVerificationRequest);

            //Validation error management
            if (!_validationResult.IsValid)
            {
                throw new Exception(_validationResult.ToString());
            }

            EmulateSending(patientVerificationRequest);
        }
        public void OnlineConcessionVerification(OCEVerification oceVerification)
        {

        }
        public void OnlineVeteranVerification(DVAVerification dvaVerification) 
        { 
        }
        public void PatientClaimInteractive(Gensolve.MedicareOnline.MedicareOnlineRequestXml.PatientClaim xmlData, MedicareOnlineClaim medicareOnlineClaim) 
        {
            //Mapping here
            var patientClaimInteractiveRequest = _mapper
                .StartMultiSourceMapping<PatientClaimInteractiveRequest>(xmlData)
                .Then(medicareOnlineClaim)
                .Map();

            //Validation here
            _validationResult = new Models.Validators.PatientInlineClaimRequestValidator("specialist").Validate(patientClaimInteractiveRequest);

            //Validation error management
            if (!_validationResult.IsValid)
            {
                throw new Exception(_validationResult.ToString());
            }

            EmulateSending(patientClaimInteractiveRequest);
        }
        public void DirectBillClaim(DirectBillClaim xmlData, MedicareOnlineClaim medicareOnlineClaim)
        { 
        }
        protected internal void EmulateSending<T>(T dto)
        {
            var json = JsonConvert.SerializeObject(dto, GenerateJsonSerializerSettings());
            Console.WriteLine(json);
        }

    }
}
