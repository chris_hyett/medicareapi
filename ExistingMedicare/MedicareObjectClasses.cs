﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using Gensolve.MedicareOnline.Interfaces;
using Gensolve.MedicareOnline.ObjectClasses;
using Gensolve.PhysioData.Common.Medicare;


// ReSharper disable InconsistentNaming

namespace Gensolve.MedicareOnline
{
    #region Utilities
    public interface IClaimHeader : IObjectHeader
    {
        string TransactionId { get; set; }
    }

    public interface IObjectHeader
    {
        string BusinessObjectType { get; }
        string PathOfParent { get; set; }
        string ObjectId { get; set; }
    }
    public class PropertyCopier<TParent, TChild> where TParent : class where TChild : class
    {
        public static void Copy(TParent parent, TChild child)
        {
            var parentProperties = parent.GetType().GetProperties();
            var childProperties = child.GetType().GetProperties();
            foreach (var parentProperty in parentProperties)
            {
                foreach (var childProperty in childProperties)
                {
                    try
                    {
                        if (parentProperty.Name == childProperty.Name &&
                            parentProperty.PropertyType == childProperty.PropertyType)
                        {
                            childProperty.SetValue(child, parentProperty.GetValue(parent));
                            break;
                        }
                    }
                    catch (Exception e)
                    {
                        ;
                    }
                }
            }
        }
    }

    #endregion

    #region Allied Multiple
    [Serializable]
    public class DvaAlliedHealthClaim32 : IClaimHeader
    {
        public DvaAlliedHealthClaim32()
        {
            
        }
        public DvaAlliedHealthClaim32(DvaClaim alliedClaim)
        {
            PropertyCopier<DvaClaim, DvaAlliedHealthClaim32>.Copy(alliedClaim, this);

            foreach (var voucher in alliedClaim.Vouchers)
            {
                var voucher32 = new DvaAlliedHealthVoucher32(voucher.ObjectId);
                try
                {
                    PropertyCopier<DvaVoucher, DvaAlliedHealthVoucher32>.Copy(voucher, voucher32);
                }
                catch (Exception e)
                {
                }
                this.AddVoucher(voucher32);

            }
            foreach (var service in alliedClaim.Services)
            {
                var service32 = new DvaAlliedHealthService32(service.ParentVoucherId);
                try
                {
                    PropertyCopier<DvaService, DvaAlliedHealthService32>.Copy(service, service32);
                }
                catch (Exception exception)
                {
                    throw exception;
                }
                this.AddService(service32);
            }
        }

        private string _authorisationDate;
        private string _claimCertifiedDate;

        public string BusinessObjectType { get { return string.Empty; } set { } }
        public string PathOfParent { get; set; }
        public string ObjectId { get; set; }
        public string TransactionId { get; set; }

        public string AuthorisationDate
        {
            get { return _authorisationDate; }
            set { if (MedOnlineUtils.FormatDateIsValid(value)) _authorisationDate = value; }
        }
        public string ClaimCertifiedInd { get; set; }

        public string ClaimCertifiedDate
        {
            get { return _claimCertifiedDate; }
            set { if (MedOnlineUtils.FormatDateIsValid(value)) _claimCertifiedDate = value; }
        }

        public string PmsClaimId { get; set; }
        public string PayeeProviderNum { get; set; }

        public List<DvaAlliedHealthVoucher32> Vouchers { get; private set; }
        public List<DvaAlliedHealthService32> Services { get; private set; }

        public void AddVoucher(DvaAlliedHealthVoucher32 bbVoucher)
        {
            if (Vouchers == null)
                Vouchers = new List<DvaAlliedHealthVoucher32>();
            Vouchers.Add(bbVoucher);
        }

        public void AddService(DvaAlliedHealthService32 bbService)
        {
            if (Services == null)
                Services = new List<DvaAlliedHealthService32>();
            Services.Add(bbService);
        }

    }

    [Serializable]
    public class DvaAlliedHealthService32 : IObjectHeader
    {
        private string _parentVoucherId;
        private string _serviceText;

        public DvaAlliedHealthService32(string parentVoucherId)
        {
            _parentVoucherId = parentVoucherId;
        }

        public string AccountReferenceNum { get; set; }
        public string AdmissionDate { get; set; }

        public decimal ChargeAmount { get; set; }
        public string DateOfService { get; set; }
        public string DischargeDate { get; set; }
        public string DistanceKms { get; set; }
        public string DuplicateServiceOverrideInd { get; set; }
        public string EquipmentId { get; set; }
        public string ItemNum { get; set; }
        public string MultipleProcedureOverrideInd { get; set; }
        public decimal NoOfPatientsSeen { get; set; }

        public string ObjectId
        {
            get; set;
        }

        public string BusinessObjectType { get { return "VAAService"; } set { } }

        public string PathOfParent
        {
            get { return "./" + _parentVoucherId; }
            set { _parentVoucherId = value.Replace("./", string.Empty).Replace('\n', ' '); }
        }

        public string SecondDeviceInd { get; set; }
        public string SelfDeemedCde { get; set; }
        public string ServiceId { get; set; }

        public string ServiceText
        {
            get
            {
                return _serviceText;
            }
            set {
                var serviceText = value.Replace('\n', ' ').Replace(">", String.Empty).Replace("<", String.Empty).Replace("*", String.Empty);
                _serviceText = serviceText.Length > 50 ? serviceText.Substring(0,50) : serviceText ;
            }
        }
            
        public string TimeOfService { get; set; }
        public string HospitalInd { get; set; }
    }

    [Serializable]
    public class DvaAlliedHealthVoucher32 : IObjectHeader
    {
        #region Fields
        private string _referralOverrideTypeCde;
        private string _patientDateOfBirth;

        private string _referralIssueDate;
        private string _requestIssueDate;
        private string _referringProviderNum;
        private string _referralPeriodTypeCde;
        private string _requestingProviderNum;
        private string _requestTypeCde;
        #endregion

        public string BusinessObjectType { get { return "VAAVoucher"; } set { } }
        public string PathOfParent { get; set; }

        public DvaAlliedHealthVoucher32(string objectId)
        {
            ObjectId = objectId;
        }

        public string ObjectId { get; set; }

        public string AcceptedDisabilityInd { get; set; }
        public string AcceptedDisabilityText { get; set; }
        public string HospitalInd { get; set; }
        public string PatientAddressLocality { get; set; }
        public string PatientAddressPostcode { get; set; }
        public string PatientAliasFirstName { get; set; }
        public string PatientAliasFamilyName { get; set; }

        public string PatientDateOfBirth
        {
            get { return _patientDateOfBirth; }
            set { if (MedOnlineUtils.FormatDateIsValid(value)) _patientDateOfBirth = value; }
        }
        public string PatientFamilyName { get; set; }
        public string PatientFirstName { get; set; }
        public string PatientGender { get; set; }
        public string PatientSecondInitial { get; set; }
        public string ReferralIssueDate { get; set; }
        public string ReferralOverrideTypeCde { get; set; }
        public string ReferralPeriod { get; set; }
        public string ReferralDateFrom { get; set; }
        public string ReferralPeriodTypeCde { get; set; }
        public string ReferringProviderNum { get; set; }
        public string RequestingProviderNum { get; set; }
        public string RequestIssueDate { get; set; }
        public string RequestOverrideTypeCde { get; set; }
        public string ServiceTypeCde { get; set; }
        public string ServicingProviderNum { get; set; }
        public string VeteranFileNum { get; set; }
        public string VoucherId { get; set; }
    }
    #endregion

    #region Bulk Bill Multiple
    [Serializable]
    public class BulkBillClaim32 : IClaimHeader
    {
        public BulkBillClaim32(Gensolve.PhysioData.Common.Medicare.BulkBillClaim bbClaim)
        {
            PropertyCopier<BulkBillClaim, BulkBillClaim32>.Copy(bbClaim, this);

            foreach (var voucher in bbClaim.Vouchers)
            {
                var voucher32 = new BulkBillVoucher32(voucher.ObjectId);
                try
                {
                    PropertyCopier<BulkBillVoucher, BulkBillVoucher32>.Copy(voucher, voucher32);
                }
                catch (Exception e)
                {


                }
                this.AddVoucher(voucher32);

            }
            foreach (var service in bbClaim.Services)
            {
                var service32 = new BulkBillService32(this.ObjectId);
                try
                {
                    PropertyCopier<BulkBillService, BulkBillService32>.Copy(service, service32);
                }
                catch (Exception exception)
                {

                    throw exception;
                }
                this.AddService(service32);
            }
        }

        private string _authorisationDate;
        private string _payeeProviderNum;

        public string BusinessObjectType
        {
            get { return string.Empty; }
            private set { }
        }

        public string PathOfParent { get; set; }
        public string ObjectId { get; set; }

        #region Header Data Elements

        public string AuthorisationDate
        {
            get { return _authorisationDate; }
            set { if (MedOnlineUtils.FormatDateIsValid(value)) _authorisationDate = value; }
        }

        public string PmsClaimId { get; set; }
        public string ServicingProviderNum { get; set; }

        public string PayeeProviderNum
        {
            get { return _payeeProviderNum == ServicingProviderNum ? null : _payeeProviderNum; }
            set { _payeeProviderNum = value; }
        }

        public string ServiceTypeCde { get; set; }
        public string TransactionId { get; set; }

        public List<BulkBillVoucher32> Vouchers { get; private set; }
        public List<BulkBillService32> Services { get; private set; }

        public void AddVoucher(BulkBillVoucher32 bbVoucher)
        {
            if (Vouchers == null)
                Vouchers = new List<BulkBillVoucher32>();
            Vouchers.Add(bbVoucher);
        }

        public void AddService(BulkBillService32 bbService)
        {
            if (Services == null)
                Services = new List<BulkBillService32>();
            Services.Add(bbService);
        }
        #endregion

        public bool Validate()
        {
            var isValid = true;
            foreach (var voucher in Vouchers)
            {
                isValid &= !string.IsNullOrEmpty(voucher.ReferralIssueDate);
                isValid &= !string.IsNullOrEmpty(voucher.ReferringProviderNum);
                isValid &= !string.IsNullOrEmpty(voucher.ReferralPeriodTypeCde);
            }
            return isValid;
        }
    }

    [Serializable]
    public class BulkBillVoucher32 : IObjectHeader
    {
        #region Fields
        private string _referralOverrideTypeCde;
        private string _patientDateOfBirth;

        private string _referralIssueDate;
        private string _requestIssueDate;
        private string _referringProviderNum;
        private string _referralPeriodTypeCde;
        private string _requestingProviderNum;
        private string _requestTypeCde;
        private string _dateOfService;
        #endregion

        public BulkBillVoucher32(string objectId)
        {
            ObjectId = objectId;
        }

        public string BusinessObjectType { get { return "Voucher"; } set { } }
        public string PathOfParent { get { return "."; } set { } }

        public string ObjectId { get; set; }

        public string BenefitAssignmentAuthorised { get { return "Y"; } set { } }


        /// <summary>
        /// date
        /// </summary>
        public string PatientDateOfBirth
        {
            get { return ReturnDateInMedicareFormat(_patientDateOfBirth); }
            set { if (MedOnlineUtils.FormatDateIsValid(value)) _patientDateOfBirth = value; }
        }

        public string PatientFamilyName { get; set; }

        public string PatientFirstName { get; set; }

        public string PatientMedicareCardNum { get; set; }

        public string PatientReferenceNum { get; set; }

        public string DateOfService
        {
            get { return ReturnDateInMedicareFormat(_dateOfService); }
            set { if (MedOnlineUtils.FormatDateIsValid(value)) _dateOfService = value; }
        }

        #region Referral Override 1 of 3
        public string ReferralOverrideTypeCde
        {
            get { return _referralOverrideTypeCde; }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    ClearReferringDetails();
                    ClearRequestsDetails();
                }
                _referralOverrideTypeCde = value;
            }
        }
        #endregion

        #region Referral Items 2 of 3
        public string ReferringProviderNum
        {
            get { return _referringProviderNum; }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    ClearOverrideCodeDetails();
                    ClearRequestsDetails();
                }
                _referringProviderNum = value;
            }
        }

        public string ReferralIssueDate
        {
            get { return ReturnDateInMedicareFormat(_referralIssueDate); }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    ClearOverrideCodeDetails();
                    ClearRequestsDetails();
                }
                if (MedOnlineUtils.FormatDateIsValid(value)) _referralIssueDate = value;
            }
        }

        public string ReferralPeriodTypeCde
        {
            get { return _referralPeriodTypeCde; }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    ClearOverrideCodeDetails();
                    ClearRequestsDetails();
                }
                _referralPeriodTypeCde = value;
            }
        }
        #endregion

        #region Requesting Items 3 of 3
        public string RequestingProviderNum
        {
            get { return _requestingProviderNum; }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    ClearOverrideCodeDetails();
                    ClearReferringDetails();
                }
                _requestingProviderNum = value;
            }
        }

        public string RequestIssueDate
        {
            get { return ReturnDateInMedicareFormat(_requestIssueDate); }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    ClearOverrideCodeDetails();
                    ClearReferringDetails();
                }
                if (!MedOnlineUtils.FormatDateIsValid(value)) _requestIssueDate = value;
            }
        }

        public string RequestTypeCde
        {
            get { return _requestTypeCde; }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    ClearOverrideCodeDetails();
                    ClearReferringDetails();
                }
                _requestTypeCde = value;
            }
        }
        #endregion 

        #region Private Methods

        private void ClearRequestsDetails()
        {
            _requestIssueDate = String.Empty;
            _requestTypeCde = String.Empty;
            _requestingProviderNum = String.Empty;
        }

        private void ClearReferringDetails()
        {
            _referralIssueDate = String.Empty;
            _referralPeriodTypeCde = String.Empty;
            _referringProviderNum = String.Empty;
        }

        private void ClearOverrideCodeDetails()
        {
            _referralPeriodTypeCde = String.Empty;
        }

        private string ReturnDateInMedicareFormat(string value)
        {
            if (string.IsNullOrEmpty(value))
                return string.Empty;

            return value.Length > 7 ? value.Substring(0, 8) : string.Empty;
        }
       
        #endregion
    }

    [Serializable]
    public class BulkBillService32 : IObjectHeader
    {
        #region Fields
        private string _parentVoucherId;
        private string _accessionDateTime;
        private string _collectionDateTime;
        private string _dateOfService;
        private decimal _chargeAmount;
        private string _serviceText;

        #endregion

        public BulkBillService32(string parentVoucherId)
        {
            _parentVoucherId = parentVoucherId;
        }

        /// <summary>
        /// date
        /// </summary>
        public string DateOfService
        {
            get { return ReturnDateInMedicareFormat(_dateOfService); }
            set { if (FormatDateIsValid(value)) _dateOfService = value; }
        }

        public string BusinessObjectType { get { return "Service"; } set { } }
        public string PathOfParent
        {
            get { return "./" + _parentVoucherId; }
            set { _parentVoucherId = value.Replace("./", string.Empty); }
        }
        public string ObjectId { get; set; }

        /// <summary>
        /// DDMMYYYYHHMM or DDMMYYYY HHMMSS
        /// </summary>
        public string AccessionDateTime
        {
            get { return _accessionDateTime; }
            set { _accessionDateTime = value; }
        }

        public string AfterCareOverrideInd { get; set; }

        public decimal ChargeAmount
        {
            get { if (_chargeAmount > 0 && _chargeAmount < 100) throw new MedicareException("Amount must be at least 100 cents"); return _chargeAmount; }
            set { _chargeAmount = value; }
        }

        /// <summary>
        /// DDMMYYYYHHMM or DDMMYYYY HHMMSS
        /// </summary>
        public string CollectionDateTime
        {
            get { return _collectionDateTime; }
            set { _collectionDateTime = value; }
        }

        public string DuplicateServiceOverrideInd { get; set; }
        public short EquipmentId { get; set; }
        public short FieldQuantity { get; set; }
        public string HospitalInd { get; set; }
        public string ItemNum { get; set; }
        public decimal LSPNum { get; set; }
        public string MultipleProcedureOverrideInd { get; set; }
        public short NoOfPatientsSeen { get; set; }
        public string RestrictiveOverrideCde { get; set; }
        public string Rule3ExemptInd { get; set; }
        public string S4b3ExemptInd { get; set; }
        public string SelfDeemedCde { get; set; }

        public string ServiceText
        {
            get
            {
                var serviceText = Regex.Replace(_serviceText, @"[^\w\s]", string.Empty).Replace('\n', ' ');
                return serviceText.Length > 50 ? serviceText.Substring(0, 49) : serviceText;
            }
            set { _serviceText = value; }
        }

        public short TimeDuration { get; set; }

        private string ReturnDateInMedicareFormat(string value)
        {
            return value.Length > 7 ? value.Substring(0, 8) : string.Empty;
        }

        private static bool FormatDateIsValid(string value)
        {
            if (string.IsNullOrEmpty(value))
                return true;

            var valid = true;
            try
            {
                var day = short.Parse(value.Substring(0, 2));
                if (day < 0 || day > 31)
                    valid = false;
                var month = short.Parse(value.Substring(2, 2));
                if (month < 0 || month > 12)
                    valid = false;
                var year = short.Parse(value.Substring(4, 4));
                if (year < 1900 || year > 2100)
                    valid = false;
            }
            catch (Exception)
            {
                throw new Exception("Bad format exception only DDMMYYYY allowed.");
            }
            if (!valid)
                throw new Exception("Bad format exception only DDMMYYYY allowed.");
            return true;
        }

    }
    #endregion

    #region Paperless Multiple
    [Serializable]
    public class DvaPaperlessClaim32 : DvaAlliedHealthClaim32, IClaimHeader
    {
        public DvaPaperlessClaim32(DvaPaperlessClaim paperlessClaim)
        {
            PropertyCopier<DvaPaperlessClaim, DvaPaperlessClaim32>.Copy(paperlessClaim, this);

            foreach (var voucher in paperlessClaim.Vouchers)
            {
                var voucher32 = new DvaPaperlessVoucher32(voucher.ObjectId);
                try
                {
                    PropertyCopier<DvaPaperlessVoucher, DvaPaperlessVoucher32>.Copy(voucher, voucher32);
                }
                catch (Exception e)
                {
                    throw e;
                }
                this.AddVoucher(voucher32);

            }
            foreach (var service in paperlessClaim.Services)
            {
                var service32 = new DvaPaperlessService32(service.ParentVoucherId);
                try
                {
                    PropertyCopier<DvaPaperlessService, DvaPaperlessService32>.Copy(service, service32);
                }
                catch (Exception exception)
                {
                    throw exception;
                }
                this.AddService(service32);
            }
        }

        public string ServiceTypeCde { get { return "S"; } set { } }
        public string BusinessObjectType { get { return string.Empty; } set { } }

        public string ServicingProviderNum { get; set; }

        public new List<DvaPaperlessVoucher32> Vouchers;
        public new List<DvaPaperlessService32> Services;

        public void AddVoucher(DvaPaperlessVoucher32 bbVoucher)
        {
            if (Vouchers == null)
                Vouchers = new List<DvaPaperlessVoucher32>();
            Vouchers.Add(bbVoucher);
        }

        public void AddService(DvaPaperlessService32 bbService)
        {
            if (Services == null)
                Services = new List<DvaPaperlessService32>();
            Services.Add(bbService);
        }
    }

    [Serializable]
    public class DvaPaperlessService32 : DvaAlliedHealthService32
    {
        public DvaPaperlessService32(string parentVoucherId) : base(parentVoucherId)
        {
        }

        public string AfterCareOverrideInd { get { return "N"; } set {} }

        public new string BusinessObjectType { get { return "DVAService"; } set { } }
        public string FieldQuantity { get; set; }
        public string LSPNum { get; set; }
    }

    [Serializable]
    public class DvaPaperlessVoucher32 : DvaAlliedHealthVoucher32
    {
        private string _dateOfService;

        public DvaPaperlessVoucher32(string objectId) : base(objectId)
        {
        }

        
        public new string BusinessObjectType { get { return "DVAVoucher"; } set { } }

        public string DateOfService
        {
            get { return _dateOfService??DateTime.Now.ToString("ddMMyyyy"); }
            set { _dateOfService = value; }
        }

        public string TreatmentLocationCde { get; set; }
    }
    #endregion
    [Serializable]
    public class BillClaim : IMedicareOnlineObject 
    {
        public string PmsClaimId { set; get; }
        public string ServicingProviderNum { set; get; }
        public string PayeeProviderNum { set; get; }
        public string ServiceTypeCde { set; get; }
        public string DateOfTransmission { set; get; }
        public string TransactionId { set; get; }
        public List<Voucher> vouchers { set; get; }
        public List<Service> Services;

        public BillClaim()
        {
            vouchers = new List<Voucher>();
        }

        public IList<_PropertyInfo> GetVoucherProperties()
        {
            return Utils.FilterPropertiesByExcludingList(vouchers.First().GetType().GetProperties(), new List<string> ());
        }

        public static bool ServicePropertyIsNotExcluded(_PropertyInfo item)
        {
            return !item.Name.Equals("ServiceId")
                   && !item.Name.Equals("DateOfService")
                   && !item.Name.Equals("SecondDeviceInd");
        }

        public static bool VoucherPropertyIsNotExcluded(_PropertyInfo item)
        {
            return !item.Name.Equals("VoucherId");
        }

        public static Type GetVoucherType()
        {
            return Voucher.GetVoucherType();
        }
    }

    [Serializable]
    public class DVAPaperLessStrmLinedClaim : IDvaClaimProperties, IMedicareOnlineObject
    {
        public string AuthorisationDate { set; get; }
        public string ClaimCertifiedInd { set; get; }
        public string ClaimCertifiedDate { set; get; }
        public string PmsClaimId { set; get; }
        public string PayeeProviderNum { set; get; }
        public string ServiceTypeCde { set; get; }
        public string ServicingProviderNum { set; get; }
        public string TransactionId { set; get; }
        public List<Voucher> vouchers { set; get; }
        public List<Service> Services;

        public DVAPaperLessStrmLinedClaim()
        {
            vouchers = new List<Voucher>();
        }

        private readonly List<string> voucherPropertiesList = new List<string> { "AcceptedDisabilityInd", "AcceptedDisabilityText", "HospitalInd",
            "PatientAddressLocality", "PatientAddressPostcode", "PatientAliasFirstName", "PatientAliasFamilyName", "PatientDateOfBirth",
            "PatientFamilyName", "PatientFirstName", "PatientGender", "ReferralIssueDate", "ReferralOverrideTypeCde", "ReferralPeriod",
            "ReferralPeriodTypeCde", "DateOfService", "ReferringProviderNum", "ServiceTypeCde", "ServicingProviderNum", "TreatmentLocationCde",
            "VeteranFileNum", "VoucherId", "PatientAliasFirstName", "PatientAliasFamilyName", "PatientAddressLocality", "PatientAddressPostcode",
            "TimeOfService"
        };

        public IList<_PropertyInfo> GetVoucherProperties()
        {
            return Utils.FilterPropertiesByList(vouchers.First().GetType().GetProperties(), voucherPropertiesList);
        }

        public IList<_PropertyInfo> GetClaimProperties()
        {
            return Utils.FilterPropertiesByExcludingList( GetType().GetProperties(), new List<string> { "vouchers" });
        }

        public Type GetVoucherType()
        {
            return Voucher.GetVoucherType();
        }
    }

    [Serializable]
    public class DVAAlliedHealthClaim : IDvaClaimProperties, IMedicareOnlineObject
    {        
        public string AuthorisationDate { set; get; }
        public string ClaimCertifiedInd { set { } get { return "Y"; } }
        public string ClaimCertifiedDate { set; get; }
        public string PmsClaimId { set; get; }
        public string PayeeProviderNum { set; get; }

        public string TransactionId { set; get; }
        

        public List<Voucher> vouchers { set; get; }

        public DVAAlliedHealthClaim()
        {
            vouchers = new List<Voucher>();
        }

        /// <summary>
        /// According to medicare a Voucher with J ServiceTypeCde can only have one of this fields
        /// they are optional, conditional or mandatory, but no more than this fields.
        /// </summary>
        private static readonly List<string> voucherPropertiesList = new List<string> { "AcceptedDisabilityInd", "AcceptedDisabilityText", "HospitalInd",
            "PatientAddressLocality", "PatientAddressPostcode", "PatientAliasFirstName", "PatientAliasFamilyName", "PatientDateOfBirth", "PatientFamilyName",
            "PatientFirstName", "PatientGender", "PatientSecondInitial", "ReferralIssueDate", "ReferralOverrideTypeCde", "ReferralPeriod", "ReferralPeriodTypeCde",
            "ReferringProviderNum", "RequestingProviderNum", "RequestIssueDate", "RequestOverrideTypeCde", "ServiceTypeCde", "ServicingProviderNum",
            "VeteranFileNum", "VoucherId"
        };
        

        public List<Service> Services;

        public IList<_PropertyInfo> GetVoucherProperties()
        {
            return Utils.FilterPropertiesByList(vouchers.First().GetType().GetProperties(), voucherPropertiesList);
        }

        public IList<_PropertyInfo> GetClaimProperties()
        {
            return new List<_PropertyInfo>(GetType().GetProperties());
        }

        public Type GetVoucherType()
        {
            return Voucher.GetVoucherType();
        }

        public static bool VoucherPropertyIsExcluded(_PropertyInfo item)
        {
            return item.Name.Equals("VoucherId");
        }                
    }
    
    [Serializable]
    public class PatientClaim : Verification, IPatientDetails, IMedicareOnlineObject 
    {
        public string AccountPaidInd { set; get; }
        public string AccountReferenceId { set; get; }
        public string BankAccountName { set; get; }
        public string BankAccountNum { set; get; }
        public string BSBCode { set; get; }
        public string ClaimantAddressLine1 { set; get; }
        public string ClaimantAddressLine2 { set; get; }
        public string ClaimantAddressPostcode { set; get; }
        public string ClaimantAddressLocality { set; get; }
        public string ClaimantDateOfBirth { set; get; }
        public string ClaimantFirstName { set; get; }
        public string ClaimantFamilyName { set; get; }
        public string ClaimantMedicareCardNum { set; get; }
        public string ClaimantMedicareCardIssueNum { set; get; }
        public string ClaimantReferenceNum { set; get; }
        public string ClaimSubmissionAuthorised { set; get; }
        public string ContactPhoneNum { set; get; }
        public string DateOfLodgement { set; get; }
        public string PatientMedicareCardNum { set; get; }
        public string PatientMedicareCardIssueNum { set; get; }
        public string PatientReferenceNum { set; get; }
        public string PayeeProviderNum { set; get; }
        public string ServicingProviderName { set; get; }

        public string IncludePaymentAttribute { set; get; }
        
        public string TimeOfLodgement { set; get; }
        public string TransactionId { set; get; }

        public List<Voucher> Vouchers { set; get; }
        public List<Service> Services;

        public string PaymentChequeClaimant { get; set; }

        public PatientClaim()
        {
            Vouchers = new List<Voucher>();
        }

        private static readonly List<string> ListOfServiceExcludedProperties = new List<string>
        {
            "ServiceId", "DateOfService", "SecondDeviceInd"
        };

        private static readonly List<string> ListOfPatientClaimExcludedProperties = new List<string>
        {
            "DateOfTransmission", "PatientAliasFamilyName", "PatientAliasFirstName", "IncludePaymentAttribute", "PaymentChequeClaimant"
        };

        public IList<_PropertyInfo> GetVoucherProperties()
        {
            return Utils.FilterPropertiesByExcludingList(Vouchers.First().GetType().GetProperties(), new List<string>());
        }

        public IList<_PropertyInfo> GetClaimProperties()
        {
            return new List<_PropertyInfo>(GetType().GetProperties());
        }

        public static bool PatientClaimPropertyIsNotExcluded(_PropertyInfo item)
        {
            return !ListOfPatientClaimExcludedProperties.Contains(item.Name);
        }

        public static bool ServicePropertyIsNotExcluded(_PropertyInfo item)
        {
            return !ListOfServiceExcludedProperties.Contains(item.Name);
        }
    }

    public interface IMedicareOnlineObject
    {
    }    

    public class MedicareOnlineObject : IMedicareOnlineObject
    {
    }

    #region Request Objects

    [Serializable]
    public class PatientVerification : Verification, IPatientDetails
    {
        public override string OPVTypeCde { get; set; } = "PVM";

        public string PatientMedicareCardNum { set; get; }
        public string PatientReferenceNum { set; get; }
        public string EarliestDateOfService { set; get; }
        /// <summary>
        /// avoid being a property so actual medicare don't interfere 
        /// </summary>
        public string Gender;
    }

    [Serializable]
    public class DVAVerification : Verification
    {
        public string PatientGender { set; get; }
        public string PatientAddressLocality { set; get; }
        public string PatientAddressPostcode { set; get; }
        public string PatientAliasFirstName { set; get; }
        public string PatientAliasFamilyName { set; get; }
        public string PatientSecondInitial { set; get; }
        public string VeteranFileNum { set; get; }

    }

    /// <summary>
    /// Online Concession Entitlement Verification
    /// </summary>
    [Serializable]
    public sealed class OCEVerification : PatientVerification
    {
        public string EntitlementId { set; get; }
        public string ServiceDate { set; get; }
        /// <summary>
        /// patientMedicareCardNum and patientReferenceNum are mandatory fields for Online Concession Entitlement Verifications
        /// </summary>
        /// <param name="patientMedicareCardNum"></param>
        /// <param name="patientReferenceNum"></param>
        public OCEVerification(string patientMedicareCardNum, string patientReferenceNum, string entitlementId, string serviceDate)
        {
            PatientReferenceNum = patientReferenceNum;
            PatientMedicareCardNum = patientMedicareCardNum;
            EntitlementId = entitlementId;
            ServiceDate = serviceDate;
        }

        public override string OPVTypeCde {get { return "CEV"; } }
    }

    [Serializable]
    public class Verification
    {
        #region Session Elements

        public string LocationId { set; get; }
        public string transmissionType { set; get; }

        #endregion
        public string PatientFirstName { set; get; }
        public string PatientFamilyName { set; get; }
        public string PatientAliasFirstName { set; get; }
        public string PatientAliasFamilyName { set; get; }
        public string PatientDateOfBirth { set; get; }
        public virtual string OPVTypeCde { set; get; }
        public string ServicingProviderNum { set; get; }        
    }

    #endregion

    #region Output Objects
    [Serializable]
    public class Report
    {
        public string TransactionId { set; get; }
        public string StatusCode { set; get; }

    }

    [Serializable]
    public sealed class PatientVerificationReport : PatientReport
    {
        public int FundStatusCode { set; get; }

        public string CurrentPatientReferenceNum { set; get; }
        public string CurrentPatientMedicareCardNum { set; get; }
        public string PatientFamilyName { set; get; }
        public string MedicareStatusCode { set; get; }
        public string ConcessionStatusCode { set; get; }
        public string MedicareErrorText { set; get; }
        public string MedicareConcessionErrorText { set; get; }


        public override string ToString()
        {
            string report = String.Empty;

            if (MedicareStatusCode != null)
            {
                if (MedicareStatusCode.Trim() == "0")
                { report = report + "\nPatient Eligible for Medicare."; }
                report = report + "\nMedicare Status Code: " + MedicareStatusCode;
                if (MedicareErrorText != null && MedicareErrorText.Trim().Length > 0)
                {
                    report = report + "\n[ " + MedicareErrorText + " ]";
                }

            }
            if (ConcessionStatusCode != null)
            {
                if (ConcessionStatusCode.Trim() == "0")
                { report = report + "\nPatient Eligible for Concession."; }
                report = report + "\nConcession Status Code: " + ConcessionStatusCode;
                if (MedicareConcessionErrorText != null && MedicareConcessionErrorText.Trim().Length > 0)
                {
                    report = report + "\n[ " + MedicareConcessionErrorText + " ]";
                }
            }

            report = report + (!string.IsNullOrWhiteSpace(TransactionId) ?  "\nTransaction Id: " + TransactionId : string.Empty);
            report = report + (!string.IsNullOrWhiteSpace(StatusCode) ? "\nStatus Code: " + StatusCode : string.Empty);

            if (CurrentPatientFirstName != null)
                report = report + "\n\nCurrent Patient First Name: " + CurrentPatientFirstName;
            if (CurrentPatientMedicareCardNum != null)
                report = report + "\nCurrent Patient Medicare Card Num: " + CurrentPatientMedicareCardNum;
            if (CurrentPatientReferenceNum != null)
                report = report + "\nCurrent Patient Reference Num: " + CurrentPatientReferenceNum;

            return report;
        }


    }

    [Serializable]
    public sealed class OnlineConcessionEntitlementVerificationReport : PatientReport
    {
        public string CurrentPatientMedicareCardNum { get; set; }
        public string CurrentPatientReferenceNum { get; set; }
        public string MedicareProcDate { get; set; }
        public string ConcessionStatusCode { get; set; }
    }

    [Serializable]
    public class PatientReport : Report
    {
        public string CurrentPatientFirstName { set; get; }
    }

    [Serializable]
    public class DVAVerificationReport : PatientReport
    {
        public string CurrentPatientFamilyName { set; get; }
        public string CurrentVeteranFileNum { set; get; }
        public string DVAStatusCode { set; get; }
        public string VeteranEntitlementCde { set; get; }

        public string ParseFromOvvResponse(IMedicareServerResult medicareResult)
        {            
            var ovvResponse = medicareResult as POCO.OnlineVeteranVerificationResponse;
            var report = string.Empty;

            if (ovvResponse != null)
            {
                CurrentPatientFamilyName = ovvResponse.Status.CurrentMember.LastName;
                CurrentPatientFirstName = ovvResponse.Status.CurrentMember.FirstName;
                CurrentVeteranFileNum = ovvResponse.Status.CurrentMembership.MemberNum;
                DVAStatusCode = ovvResponse.Status.StatusCode;
                VeteranEntitlementCde = ovvResponse.Status.CurrentMembership.Attributes.Any() ? ovvResponse.Status.CurrentMembership.Attributes.Find(x => x.Name.Equals("veteranEntitlementCde")).Value : null;

                int medicareStatusCode = 0;
                Int32.TryParse(ovvResponse.Status.StatusCode, out medicareStatusCode);
                var config = new ConfigurationManagerUtils();
                report = (ovvResponse.Status.StatusCode == "0") ? "Patient Eligible for Medicare." : config.GetErrorMessage(medicareStatusCode);
                
                report = report + "\nStatus Code: " + ovvResponse.Status.StatusCode;

                if (CurrentPatientFirstName != null)
                    report = report + "\n\nCurrent Patient First Name: " + CurrentPatientFirstName;
                if (CurrentPatientFamilyName != null)
                    report = report + "\nCurrent Patient Family Name: " + CurrentPatientFamilyName;
                if (CurrentVeteranFileNum != null)
                    report = report + "\nCurrent Veteran File Num: " + CurrentVeteranFileNum;
                if (VeteranEntitlementCde != null)
                    report = report + "\nCurrent Entitlement Code: " + VeteranEntitlementCde;
            }

            return report;
        }
    }

    [Serializable]
    public class ProcessingReport
    {
        public string PmsClaimId { set; get; }
        public string DateOfTransmission { set; get; }
        public string PayeeProviderNum { set; get; }
        public string TransactionId { set; get; }

        public List<ProcessingReportElement> prElements { set; get; }

    }

    [Serializable]
    public class StatementAndBenefitReport
    {
        public string PmsClaimId { set; get; }
        public List<StatementAndBenefitRprtElement> prElements { set; get; }

    }

    #endregion

    [Serializable]
    public class StatementAndBenefitRprtElement
    {

        public string ExplanationCode { set; get; }
        public string ServiceBenefitAmount { set; get; }
        public string ServiceId { set; get; }
        public string VoucherId { set; get; }


    }

    [Serializable]
    public class ClaimAssesmentReport
    {


        public string ClaimErrorCode { set; get; }
        public string ClaimErrorLevel { set; get; }
        public string CurrentClaimantMedicareCardNum { set; get; }
        public string CurrentClaimantReferenceNum { set; get; }
        public string CurrentPatientMedicareCardNum { set; get; }
        public string CurrentPatientReferenceNum { set; get; }
        public string PmsClaimId { set; get; }

        public List<ClaimAssesmentReportElement> prElements { set; get; }

    }

    [Serializable]
    public class ClaimAssesmentReportElement
    {

        public string ExplanationCode { set; get; }
        public string ServiceBenefitAmount { set; get; }
        public string ServiceId { set; get; }
        public string VoucherId { set; get; }




    }

    [Serializable]
    public class ProcessingReportElement
    {
        public string PmsClaimId { set; get; }
        public string DateOfTransmission { set; get; }
        public string PayeeProviderNum { set; get; }
        public string ClaimId { set; get; }
        public string ServiceChargeAmount { set; get; }
        public string ClaimChargeAmount { set; get; }
        public string TransactionId { set; get; }
        public string ClaimBenefitPaid { set; get; }
        public string DateOfService { set; get; }
        public string ExplanationCode { set; get; }
        public string DetailsExplanationCode { set; get; }
        public string ItemNum { set; get; }
        public string MedicareCardFlag { set; get; }
        public string PatientFamilyName { set; get; }
        public string PatientFirstName { set; get; }
        public string PatientMedicareCardNum { set; get; }
        public string PatientReferenceNum { set; get; }
        public string ServiceBenefitAmount { set; get; }
        public string ServiceId { set; get; }
        public string ServicingProviderNum { set; get; }
        public string ServicingProviderName { set; get; }
        public string NoOfPatientsSeen { set; get; }
        public string CardFlag { set; get; }
        public string VeteranFileNum { set; get; }
        public string VoucherId { set; get; }
        public string AccountReferenceNum { set; get; }
        public string GstInd { set; get; }



    }

    [Serializable]
    public class PaymentReportTransaction
    {
        private List<PaymentReportElement> _pmrElement;
        public IReportResponse enterpriseReportResponse { get; set; }
        public POCO.ReportRequest enterpriseReportRequest { get; set; }
        public string PayeeProviderNum { set; get; }
        public string PmsClaimId { set; get; }
        public string DateOfTransmission { set; get; }
        public List<PaymentReportElement> pmrElements
        {
            get { return _pmrElement ?? (_pmrElement = new List<PaymentReportElement>()); }
            set { _pmrElement = value; }
        }
    }

    [Serializable]
    public class PaymentReportElement
    {
        public string DateOfTransmission { set; get; }
        public string PayeeProviderNum { set; get; }
        public string BSBCode { set; get; }
        public string BankAccountNum { set; get; }
        public string BankAccountName { set; get; }
        public string ClaimDate { set; get; }
        public string ClaimBenefitPaid { set; get; }
        public string ClaimChargeAmount { set; get; }
        public string DepositAmount { set; get; }
        public string PaymentRunDate { set; get; }
        public string PaymentRunNum { set; get; }
        public string PmsClaimId { set; get; }
        public string TransactionId { set; get; }

    }

    [Serializable]
    public class Voucher
    {
        public string VoucherId { set; get; }
        public string BenefitAssignmentAuthorised { set; get; }
        public string PatientDateOfBirth { set; get; }
        public string PatientFamilyName { set; get; }
        public string PatientFirstName { set; get; }
        public string PatientAliasFamilyName { set; get; }
        public string PatientAliasFirstName { set; get; }
        public string PatientAddressLocality { set; get; }
        public string PatientAddressPostcode { set; get; }
        public string PatientSecondInitial { get; set; }
        public string PatientGender { set; get; }
        public string PatientMedicareCardNum { set; get; }
        public string PatientMedicareCardIssueNum { set; get; }
        public string PatientReferenceNum { set; get; }
        public string ReferralOverrideTypeCde { set; get; }
        public string ReferringProviderNum { set; get; }
        public string RequestingProviderNum { set; get; }
        public string ReferralIssueDate { set; get; }
        public string RequestIssueDate { set; get; }
        public string ReferralPeriod { set; get; }
        public string ReferralPeriodTypeCde { set; get; }
        public string RequestTypeCde { set; get; }
        public string TimeOfService { set; get; }
        public string TreatmentLocationCde { set; get; }
        public string VeteranFileNum { set; get; }
        public string ServicingProviderName { set; get; }
        public string ServicingProviderNum { set; get; }
        public string ServiceTypeCde { set; get; }
        public string SecondDeviceInd { set; get; }
        public string HospitalInd { set; get; }
        public string DateOfService
        {
            set { _dateOfService = value; }
            get
            {
                return _dateOfService ?? DateTime.Now.ToString("ddMMyyyy");
            }
        }
        private string _dateOfService;
        public string AcceptedDisabilityInd { set; get; }
        public string AcceptedDisabilityText { set; get; }
        public string ReferralDateFrom { get; set; }

        public static Type GetVoucherType()
        {
            return typeof(List<Voucher>);
        }
    }

    [Serializable]
    public class Service : IComparable
    {        
        public string ServiceId { set; get; }
        public string AccessionDateTime { set; get; }
        public string AfterCareOverrideInd { set; get; }
        public string ChargeAmount { set; get; }
        public string CollectionDateTime { set; get; }
        public string DuplicateServiceOverrideInd { set; get; }
        public string EquipmentId { set; get; }
        public string FieldQuantity { set; get; }
        public string HospitalInd { set; get; }
        public string ItemNum { set; get; }
        public string SCPId { set; get; }
        public string LSPNum { set; get; }
        public string MultipleProcedureOverrideInd { set; get; }
        public string NoOfPatientsSeen { set; get; }
        public string RestrictiveOverrideCde { set; get; }
        public string Rule3ExemptInd { set; get; }
        public string S4b3ExemptInd { set; get; }
        public string SelfDeemedCde { set; get; }
        public string ServiceText
        {
            get
            {
                return _ServiceText;
            }
            set
            {
                var serviceText = value.Replace('\n', ' ').Replace(">", String.Empty).Replace("<", String.Empty).Replace("*", String.Empty);
                _ServiceText = serviceText.Length > 50 ? serviceText.Substring(0, 50) : serviceText;
            }
        }
        public string TimeDuration { set; get; }
        public string TimeOfService { get; set; }
        public string AccountReferenceNum { get; set; }
        public string DateOfService
        {
            set { _dateOfService = value; }
            get
            {
                return _dateOfService ?? DateTime.Now.ToString("ddMMyyyy");
            }
        }
        private string _dateOfService;
        private string _SecondDeviceInd;
        private string _PatientContribAmt;
        private string _ServiceText;

        public string SecondDeviceInd
        {
            get
            {
                if (_SecondDeviceInd==null)
                    return "N";
                else return _SecondDeviceInd;
            }
            set { _SecondDeviceInd = value; }
        }
        public string PatientContribAmt
        {
            set {
                _PatientContribAmt = value;
                }
            get {
                return string.IsNullOrEmpty(_PatientContribAmt) || _PatientContribAmt.Equals("0") ? string.Empty : _PatientContribAmt;
                }
        }
        public string DistanceKms { get; set; }

        public IList<_PropertyInfo> GetServiceProperties()
        {
            return new List<_PropertyInfo>(this.GetType().GetProperties());
        }

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            Service otherService = obj as Service;
            if (otherService != null)
                return string.IsNullOrWhiteSpace(otherService.DistanceKms) ? 1 : 0;
            return 0;
        }
    }
}