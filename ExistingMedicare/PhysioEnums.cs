﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.ComponentModel;
using Gensolve.Utilities;

namespace Gensolve.PhysioData.Common
{
    /// <summary>
    /// Summary description for Dialog.
    /// </summary>
    public partial class PhysioEnums 
    {
        public enum IntegrationType
        {
            HICAPS,
            TYRO,
            MEDICARE_ONLINE
        }

        public PhysioEnums()
        {
        }

        private static PhysioEnums instance;

        public new static PhysioEnums Instance
        {
            get
            {
                if (instance == null)
                    instance = new PhysioEnums();
                return instance;
            }
        }

        #region PhysioCountry
        public enum PhysioCountry
        {
            NZ,
            AU,
            UK,
        }

        #endregion

        #region Acc32RequestStatus

        //corresponds to pk_physio.f_acc32_status
        public enum Acc32RequestStatus : int
        {
            Created = 0,
            Accepted = 1,
            Rejected = 2,
            Completed = 3,
            VerballyAccepted = 4,
            SendingOnline = 5,
            SentOnline = 6,
            FailedOnlineSend = 7
        }

        public static string Acc32RequestStatusDescription(Acc32RequestStatus ctype)
        {
            return Acc32RequestStatusDescription((int)ctype);
        }

        public static string Acc32RequestStatusDescription(Decimal type)
        {
            return Acc32RequestStatusDescription((int)type);
        }

        public static string Acc32RequestStatusDescription(int status)
        {
            switch (status)
            {
                case (int)Acc32RequestStatus.Created:
                    return "Created";

                case (int)Acc32RequestStatus.Completed:
                    return "Report Mailed";

                case (int)Acc32RequestStatus.Accepted:
                    return "Accepted";

                case (int)Acc32RequestStatus.Rejected:
                    return "Rejected";

                case (int)Acc32RequestStatus.VerballyAccepted:
                    return "Verbally Accepted";

                case (int)Acc32RequestStatus.SendingOnline:
                    return "Sending Online";

                case (int)Acc32RequestStatus.SentOnline:
                    return "Sent Online";

                case (int)Acc32RequestStatus.FailedOnlineSend:
                    return "Failed Online Send";

                default:
                    return "";
            }
        }
        #endregion

        //#region AccessPrivilegeType

        //protected override Gensolve.OrganisationData.Common.OrganisationEnums.AccessPrivilegeType GetAccessPrivilegeTypeEnum()
        //{
        //    if (accessPrivilegeTypeInstance == null)
        //        accessPrivilegeTypeInstance = new AccessPrivilegeType();

        //    return accessPrivilegeTypeInstance;
        //}

        //public new AccessPrivilegeType AccessPrivilegeTypeEnum
        //{
        //    get
        //    {
        //        if (accessPrivilegeTypeInstance == null)
        //            accessPrivilegeTypeInstance = (AccessPrivilegeType)GetAccessPrivilegeTypeEnum();

        //        return (AccessPrivilegeType)accessPrivilegeTypeInstance;
        //    }
        //}

        //public new partial class AccessPrivilegeType : Gensolve.AppointmentData.Common.AppointmentEnums.AccessPrivilegeType
        //{
        //    private const decimal startFrom = Gensolve.AppointmentData.Common.AppointmentEnums.AccessPrivilegeType.AccessPrivilegeTypeStartFrom;

        //    protected new const decimal AccessPrivilegeTypeStartFrom = startFrom + 10000;

        //    #region Main Form

        //    #region Admin Menu 1100

        //    public AccessPrivilegeTypeDetail FDischargeOutcomes = new AccessPrivilegeTypeDetail(startFrom + 1101, "Discharge Outcomes", true);
        //    public AccessPrivilegeTypeDetail FDoseTypes = new AccessPrivilegeTypeDetail(startFrom + 1102, "Dose Types", true);
        //    public AccessPrivilegeTypeDetail FExamTestTypes = new AccessPrivilegeTypeDetail(startFrom + 1103, "Exam Test Types", true);
        //    public AccessPrivilegeTypeDetail FGoalTreatments = new AccessPrivilegeTypeDetail(startFrom + 1104, "Goal Treatments", true);
        //    public AccessPrivilegeTypeDetail FInvestigationTypes = new AccessPrivilegeTypeDetail(startFrom + 1105, "Investigation Types", true);
        //    public AccessPrivilegeTypeDetail FMedNotesColours = new AccessPrivilegeTypeDetail(startFrom + 1106, "Med Notes Colours");
        //    public AccessPrivilegeTypeDetail FPatientGoals = new AccessPrivilegeTypeDetail(startFrom + 1107, "Patient Goals", true);
        //    public AccessPrivilegeTypeDetail FTrainerTypes = new AccessPrivilegeTypeDetail(startFrom + 1108, "Trainer Types", true);
        //    public AccessPrivilegeTypeDetail FReadCodes = new AccessPrivilegeTypeDetail(startFrom + 1109, "Read Codes", true);
        //    public AccessPrivilegeTypeDetail FMedicationTypes = new AccessPrivilegeTypeDetail(startFrom + 1110, "Medication Types", true);
        //    public AccessPrivilegeTypeDetail FMCSPatternTypes = new AccessPrivilegeTypeDetail(startFrom + 1111, "MCS Pattern Types", true);
        //    public AccessPrivilegeTypeDetail FDiabetesRiskTypes = new AccessPrivilegeTypeDetail(startFrom + 1112, "Diabetes Risk Types", true);
        //    public AccessPrivilegeTypeDetail FCustomAssessmentType = new AccessPrivilegeTypeDetail(startFrom + 1406, "Custom Assessment Types", false);
        //    public AccessPrivilegeTypeDetail FOrchardCodes = new AccessPrivilegeTypeDetail(startFrom + 1118, "Orchard Codes", true);

        //    // not on admin menu
        //    public AccessPrivilegeTypeDetail FInternalSportTypes = new AccessPrivilegeTypeDetail(startFrom + 1113, "Sport Types", true);
        //    public AccessPrivilegeTypeDetail FCardLevels = new AccessPrivilegeTypeDetail(startFrom + 1114, "Card Levels", true);

        //    public AccessPrivilegeTypeDetail FInjuryTypes = new AccessPrivilegeTypeDetail(startFrom + 1116, "Injury Types", true);
        //    public AccessPrivilegeTypeDetail FMainProviderReason = new AccessPrivilegeTypeDetail(startFrom + 1117, "Main Provider Reasons", true);
        //    public AccessPrivilegeTypeDetail FTreatmentTypes = new AccessPrivilegeTypeDetail(startFrom + 1118, "Treatment Types", true);

        //    #endregion

        //    #region Reports Menu 1300

        //    public AccessPrivilegeTypeDetail FRConditionReferrals = new AccessPrivilegeTypeDetail(startFrom + 1301, "Condition Referrals");
        //    public AccessPrivilegeTypeDetail FRClientNotSeenOrDischarged = new AccessPrivilegeTypeDetail(startFrom + 1302, "Client Not Seen Or Discharged");
        //    public AccessPrivilegeTypeDetail FRConditionSummary = new AccessPrivilegeTypeDetail(startFrom + 1303, "Condition Summary");
        //    public AccessPrivilegeTypeDetail FRClinicanUniqueClients = new AccessPrivilegeTypeDetail(startFrom + 1304, "Clinican Unique Clients");
        //    public AccessPrivilegeTypeDetail FRWhyChooseUsConditions = new AccessPrivilegeTypeDetail(startFrom + 1305, "Why Choose Us - Conditions");
        //    public AccessPrivilegeTypeDetail FRReferralSourceConditions = new AccessPrivilegeTypeDetail(startFrom + 1306, "Referral Source - Conditions");
        //    public AccessPrivilegeTypeDetail FRClincianApptScheduleDetailed = new AccessPrivilegeTypeDetail(startFrom + 1307, "Clincian Appointment Schedule - Detailed");
        //    public AccessPrivilegeTypeDetail FRClincianApptTreatments = new AccessPrivilegeTypeDetail(startFrom + 1308, "Clincian Appointment Treatments");
        //    public AccessPrivilegeTypeDetail FRClientApptTreatments = new AccessPrivilegeTypeDetail(startFrom + 1309, "Client Appointment Treatments");
        //    public AccessPrivilegeTypeDetail FRApptsWithoutConditions = new AccessPrivilegeTypeDetail(startFrom + 1310, "Appointments Without Conditions");
        //    public AccessPrivilegeTypeDetail FRConditionVisitSummary = new AccessPrivilegeTypeDetail(startFrom + 1311, "Condition Visit Summary");
        //    public AccessPrivilegeTypeDetail FRDischargeSummary = new AccessPrivilegeTypeDetail(startFrom + 1312, "Discharge Summary");
        //    public AccessPrivilegeTypeDetail FRClientConditionGoals = new AccessPrivilegeTypeDetail(startFrom + 1313, "Client Condition Goals");
        //    public AccessPrivilegeTypeDetail FRACC32AwaitingApproval = new AccessPrivilegeTypeDetail(startFrom + 1314, "ACC32 Awaiting Approval");
        //    public AccessPrivilegeTypeDetail FRIntraReferrals = new AccessPrivilegeTypeDetail(startFrom + 1315, "Intra Referrals");
        //    public AccessPrivilegeTypeDetail FRIncompleteExams = new AccessPrivilegeTypeDetail(startFrom + 1316, "Incomplete Exams");
        //    public AccessPrivilegeTypeDetail FRConditionFollowups = new AccessPrivilegeTypeDetail(startFrom + 1317, "Condition Followups");
        //    public AccessPrivilegeTypeDetail FRApptsNotInSchedules = new AccessPrivilegeTypeDetail(startFrom + 1318, "Appointments Not In Schedules");
        //    public AccessPrivilegeTypeDetail FRClinicianSummary = new AccessPrivilegeTypeDetail(startFrom + 1319, "Clinician Summary");
        //    public AccessPrivilegeTypeDetail FROutcomeMeasuresToDo = new AccessPrivilegeTypeDetail(startFrom + 1320, "Outcome Measures To Do");
        //    public AccessPrivilegeTypeDetail FRContractSummary = new AccessPrivilegeTypeDetail(startFrom + 1321, "Contract Summary");
        //    public AccessPrivilegeTypeDetail FRCustomContractDetail = new AccessPrivilegeTypeDetail(startFrom + 1322, "Custom Contract Detail");
        //    public AccessPrivilegeTypeDetail FRCustomContractApptDetail = new AccessPrivilegeTypeDetail(startFrom + 1323, "Custom Contract Appointment Detail");
        //    public AccessPrivilegeTypeDetail FRSportAppointmentTmts = new AccessPrivilegeTypeDetail(startFrom + 1324, "Sport Appointment Treatments");
        //    public AccessPrivilegeTypeDetail FRSplintChargesReport = new AccessPrivilegeTypeDetail(startFrom + 1325, "Splint Charges Report");
        //    public AccessPrivilegeTypeDetail FRPhysioAppointmentSummary = new AccessPrivilegeTypeDetail(startFrom + 1326, "Clinician Appointment Summary");
        //    public AccessPrivilegeTypeDetail FRPhysioAppointmentDetail = new AccessPrivilegeTypeDetail(startFrom + 1327, "Clinician Appointment Detail");
        //    public AccessPrivilegeTypeDetail FRPTPaidAppointments = new AccessPrivilegeTypeDetail(startFrom + 1328, "PT Paid Appointments");
        //    public AccessPrivilegeTypeDetail FRMcsReport = new AccessPrivilegeTypeDetail(startFrom + 1329, "MCS");
        //    public AccessPrivilegeTypeDetail FRContractStatsReport = new AccessPrivilegeTypeDetail(startFrom + 1330, "Contract Stats");
        //    public AccessPrivilegeTypeDetail FRPhoStatsReport = new AccessPrivilegeTypeDetail(startFrom + 1331, "PHO Stats");
        //    public AccessPrivilegeTypeDetail FRContractTracking = new AccessPrivilegeTypeDetail(startFrom + 1332, "Contract Tracking");
        //    public AccessPrivilegeTypeDetail FRSiteContractPayment = new AccessPrivilegeTypeDetail(startFrom + 1333, "Site Contract Payment");
        //    public AccessPrivilegeTypeDetail FRScheduleLineDetail = new AccessPrivilegeTypeDetail(startFrom + 1334, "Schedule Line Detail");
        //    public AccessPrivilegeTypeDetail FRApptsWithoutServiceCharge = new AccessPrivilegeTypeDetail(startFrom + 1335, "Appointments Without Service Charges");
        //    public AccessPrivilegeTypeDetail FRHicapsSummaryReport = new AccessPrivilegeTypeDetail(startFrom + 1336, "Hicaps Summary Report");
        //    public AccessPrivilegeTypeDetail FRTopReadCodeReport = new AccessPrivilegeTypeDetail(startFrom + 1337, "Top Read Code Report");
        //    public AccessPrivilegeTypeDetail FRContractProviderHours = new AccessPrivilegeTypeDetail(startFrom + 1338, "Contract Provider Hours Export");
        //    public AccessPrivilegeTypeDetail FRIncompleteContractCharges = new AccessPrivilegeTypeDetail(startFrom + 1339, "Contract Incomplete Appt Charges");
        //    public AccessPrivilegeTypeDetail FRContractChargesExpenses = new AccessPrivilegeTypeDetail(startFrom + 1340, "Contract Charges and Expenses");
        //    public AccessPrivilegeTypeDetail FRConditionDischargeExport = new AccessPrivilegeTypeDetail(startFrom + 1341, "Condition Discharge Export");
        //    public AccessPrivilegeTypeDetail FRKPIReport = new AccessPrivilegeTypeDetail(startFrom + 1342, "KPI Report");
        //    public AccessPrivilegeTypeDetail FRContractExport = new AccessPrivilegeTypeDetail(startFrom + 1343, "Contract Export");
        //    public AccessPrivilegeTypeDetail FRPhysioAppointmentsAllowedStatus = new AccessPrivilegeTypeDetail(startFrom + 1344, "Condition Appointment Allowed status");
        //    public AccessPrivilegeTypeDetail FRExamOrchardCodes = new AccessPrivilegeTypeDetail(startFrom + 1345, "Exam Orchard Codes");
        //    public AccessPrivilegeTypeDetail FRExamTreatmentTypes = new AccessPrivilegeTypeDetail(startFrom + 1346, "Exam Treatment Types");

        //    public AccessPrivilegeTypeDetail FRContractSummaryLPO = new AccessPrivilegeTypeDetail(startFrom + 1347, "Contract Summary (User is Lead Provider Only)");
        //    public AccessPrivilegeTypeDetail FRContractChargesExpensesLPO = new AccessPrivilegeTypeDetail(startFrom + 1348, "Contract Charges and Expenses (User is Lead Provider Only)");
        //    public AccessPrivilegeTypeDetail FRContractDischargeExportLPO = new AccessPrivilegeTypeDetail(startFrom + 1349, "Contract Discharge Export (User is Lead Provider Only)");
        //    public AccessPrivilegeTypeDetail FRContractExportLPO = new AccessPrivilegeTypeDetail(startFrom + 1350, "Contract Export (User is Lead Provider Only)");
        //    public AccessPrivilegeTypeDetail FRContractProviderHoursLPO = new AccessPrivilegeTypeDetail(startFrom + 1351, "Contract Provider Hours Export (User is Provider Only)");
        //    public AccessPrivilegeTypeDetail FRIncompleteContractChargesLPO = new AccessPrivilegeTypeDetail(startFrom + 1352, "Contract Incomplete Appt Charges (User is Lead Provider Only)");
        //    public AccessPrivilegeTypeDetail FRContractProviderLPO = new AccessPrivilegeTypeDetail(startFrom + 1353, "Contract Provider (User is Lead Provider Only)");
        //    public AccessPrivilegeTypeDetail FRSiteContractPaymentLPO = new AccessPrivilegeTypeDetail(startFrom + 1354, "Site Contract Payment (User is Lead Provider Only)");
        //    public AccessPrivilegeTypeDetail FRContractTrackingLPO = new AccessPrivilegeTypeDetail(startFrom + 1355, "Contract Tracking (User is Lead Provider Only)");
        //    public AccessPrivilegeTypeDetail FRContractStatsReportLPO = new AccessPrivilegeTypeDetail(startFrom + 1356, "Contract Stats (User is Lead Provider Only)");
        //    public AccessPrivilegeTypeDetail FRCustomContractApptDetailLPO = new AccessPrivilegeTypeDetail(startFrom + 1357, "Custom Contract Appointment Detail (User is Provider Only)");
        //    public AccessPrivilegeTypeDetail FRCustomContractDetailLPO = new AccessPrivilegeTypeDetail(startFrom + 1358, "Custom Contract Detail (User is Provider Only)");

        //    public AccessPrivilegeTypeDetail FRContractProvider = new AccessPrivilegeTypeDetail(startFrom + 1359, "Contract Provider");
        //    public AccessPrivilegeTypeDetail FRContractDischargeExport = new AccessPrivilegeTypeDetail(startFrom + 1360, "Contract Discharge Export");

        //    public AccessPrivilegeTypeDetail FRDynamicAssessmentExport = new AccessPrivilegeTypeDetail(startFrom + 1361, "Dynamic Assessment Export");
        //    public AccessPrivilegeTypeDetail FRCustomContractOutcomes = new AccessPrivilegeTypeDetail(startFrom + 1362, "Custom Contract Outcomes");
        //    public AccessPrivilegeTypeDetail FRCustomContractOutcomesLPO = new AccessPrivilegeTypeDetail(startFrom + 1363, "Custom Contract Outcomes (User Provider Only)");

        //    public AccessPrivilegeTypeDetail FRClientConditionAppts = new AccessPrivilegeTypeDetail(startFrom + 1364, "Client Condition Appointments");

        //    #endregion

        //    #region Clients and Conditions 1400

        //    public AccessPrivilegeTypeDetail FClientsConditions = new AccessPrivilegeTypeDetail(startFrom + 1400, "Client's Conditions", true);
        //    //public AccessPrivilegeTypeDetail FConditionsMedNoteSummary = new AccessPrivilegeTypeDetail(startFrom + 1401, "Condition's Med Note Summary");
        //    //public AccessPrivilegeTypeDetail FConditionsReferralDischarges = new AccessPrivilegeTypeDetail(startFrom + 1402, "Condition's Referrals and Discharges", true);
        //    //public AccessPrivilegeTypeDetail FConditionsOutcomeMeasures = new AccessPrivilegeTypeDetail(startFrom + 1403, "Condition's Outcome Measures");
        //    //public AccessPrivilegeTypeDetail FConditionsAcc32s = new AccessPrivilegeTypeDetail(startFrom + 1404, "Condition's ACC32s", true);
        //    public AccessPrivilegeTypeDetail FClientsHealthlink = new AccessPrivilegeTypeDetail(startFrom + 1405, "Client's Healthlink Messages", true);
        //    public AccessPrivilegeTypeDetail FMedicalNotes = new AccessPrivilegeTypeDetail(startFrom + 1406, "Exams/Medical Notes", false);

        //    #endregion

        //    #region Contracts Menu 1500

        //    public AccessPrivilegeTypeDetail FContractsMenu = new AccessPrivilegeTypeDetail(startFrom + 1500, "Contracts Menu");

        //    public AccessPrivilegeTypeDetail FAbpProgrammeTypes = new AccessPrivilegeTypeDetail(startFrom + 1501, "ABP FRP Programme Types", true);
        //    public AccessPrivilegeTypeDetail FAbpContracts = new AccessPrivilegeTypeDetail(startFrom + 1502, "ABP FRP Contracts", true);
        //    public AccessPrivilegeTypeDetail FAbpContractsReport = new AccessPrivilegeTypeDetail(startFrom + 1503, "ABP FRP Contracts Report");

        //    public AccessPrivilegeTypeDetail FCustomContractTypes = new AccessPrivilegeTypeDetail(startFrom + 1504, "Custom Contract Types", true);
        //    public AccessPrivilegeTypeDetail FCustomContracts = new AccessPrivilegeTypeDetail(startFrom + 1505, "Custom Contracts", true);
        //    public AccessPrivilegeTypeDetail FCustomContractViewApptCharges = new AccessPrivilegeTypeDetail(startFrom + 1506, "Show Custom Contract Appt Charges");
        //    public AccessPrivilegeTypeDetail FCustomContractSetSites = new AccessPrivilegeTypeDetail(startFrom + 1507, "Custom Contracts (Sites & Commission Tab)");

        //    public AccessPrivilegeTypeDetail FCustomContractsLPO = new AccessPrivilegeTypeDetail(startFrom + 1508, "Custom Contracts (User is Lead Provider Only)", true);
        //    public AccessPrivilegeTypeDetail FCustomContractsLinked = new AccessPrivilegeTypeDetail(startFrom + 1509, "Custom Contracts (User is Linked or Lead Provider)", true);

        //    public AccessPrivilegeTypeDetail FCustomContractExpenses = new AccessPrivilegeTypeDetail(startFrom + 1510, "Custom Contracts (Expenses Tab)", true);
        //    public AccessPrivilegeTypeDetail FCustomContractSetDiscount = new AccessPrivilegeTypeDetail(startFrom + 1511, "Custom Contracts (Allow Set Discount)", false);
        //    public AccessPrivilegeTypeDetail FCustomContractTypeChargesOnly = new AccessPrivilegeTypeDetail(startFrom + 1512, "Custom Contract Types (View Charges Only)", false);

        //    #endregion

        //    #region ACC45s & Batches Menu 1600

        //    public AccessPrivilegeTypeDetail FACC45sAndBatchesMenu = new AccessPrivilegeTypeDetail(startFrom + 1600, "ACC45's & Batches Menu");
        //    public AccessPrivilegeTypeDetail FACC45s = new AccessPrivilegeTypeDetail(startFrom + 1601, "ACC45s");
        //    public AccessPrivilegeTypeDetail FACC32s = new AccessPrivilegeTypeDetail(startFrom + 1602, "ACC32s");
        //    public AccessPrivilegeTypeDetail FInsurerSchedules = new AccessPrivilegeTypeDetail(startFrom + 1603, "ACC/Insurer Schedules", true);
        //    public AccessPrivilegeTypeDetail FPaymentsRejections = new AccessPrivilegeTypeDetail(startFrom + 1605, "Payments Rejections", true);
        //    public AccessPrivilegeTypeDetail FAcceptanceRejectionReasons = new AccessPrivilegeTypeDetail(startFrom + 1607, "Acceptance Rejection Reasons", true);

        //    public AccessPrivilegeTypeDetail FSendACC45s = new AccessPrivilegeTypeDetail(startFrom + 1608, "Send ACC45s");
        //    public AccessPrivilegeTypeDetail FSendACCSchedules = new AccessPrivilegeTypeDetail(startFrom + 1609, "Send ACC Schedules");

        //    public AccessPrivilegeTypeDetail FACCTravelBilling = new AccessPrivilegeTypeDetail(startFrom + 1610, "ACC Travel Billing", true);

        //    #endregion

        //    #endregion

        //    #region Accounting Form

        //    #region Reports 1800

        //    public AccessPrivilegeTypeDetail AFRClinicianRevenueGenerated = new AccessPrivilegeTypeDetail(startFrom + 1800, "Clinician Revenue Generated");
        //    public AccessPrivilegeTypeDetail AFRClinicianRevenueGeneratedDetail = new AccessPrivilegeTypeDetail(startFrom + 1801, "Clinician Revenue Generated Detail");
        //    public AccessPrivilegeTypeDetail AFRClinicianRevenuePaid = new AccessPrivilegeTypeDetail(startFrom + 1802, "Clinician Revenue Paid");
        //    public AccessPrivilegeTypeDetail AFRClinicianRevenuePaidDetail = new AccessPrivilegeTypeDetail(startFrom + 1803, "Clinician Revenue Paid Detail");
        //    public AccessPrivilegeTypeDetail AFRClinicianApptRevenueGenerated = new AccessPrivilegeTypeDetail(startFrom + 1805, "Clinician Appointment Revenue Generated");
        //    public AccessPrivilegeTypeDetail AFRClientGroupingRevenueReport = new AccessPrivilegeTypeDetail(startFrom + 1806, "Client Grouping Revenue Report");
        //    public AccessPrivilegeTypeDetail AFRInsurerRevenueReport = new AccessPrivilegeTypeDetail(startFrom + 1807, "Insurer Revenue Report");

        //    public AccessPrivilegeTypeDetail AFRClinicianRevenueGeneratedUPO = new AccessPrivilegeTypeDetail(startFrom + 1808, "Clinician Revenue Generated (Users Provider Only)");
        //    public AccessPrivilegeTypeDetail AFRClinicianRevenueGeneratedDetailUPO = new AccessPrivilegeTypeDetail(startFrom + 1809, "Clinician Revenue Generated Detail (Users Provider Only)");
        //    public AccessPrivilegeTypeDetail AFRClinicianRevenuePaidUPO = new AccessPrivilegeTypeDetail(startFrom + 1810, "Clinician Revenue Paid (Users Provider Only)");
        //    public AccessPrivilegeTypeDetail AFRClinicianRevenuePaidDetailUPO = new AccessPrivilegeTypeDetail(startFrom + 1811, "Clinician Revenue Paid Detail (Users Provider Only)");

        //    #endregion

        //    #endregion

        //    #region

        //    #region GPM API Contract Export 3000

        //    public AccessPrivilegeTypeDetail FAPIContractExport = new AccessPrivilegeTypeDetail(startFrom + 3001, "API Contract Export");

        //    #endregion

        //    #region PK App Access 3100

        //    public AccessPrivilegeTypeDetail FAllowPkAccess = new AccessPrivilegeTypeDetail(startFrom + 3100, "PK Patient Portal Access", false);

        //    #endregion

        //    #endregion

        //    #region AddParentChildMappings
        //    protected override void AddParentChildMappings()
        //    {
        //        AddParentChildMappingsCommon();
        //        AddParentChildMappingsSpecialized();
        //        WebReportChildParentMapping();

        //        AddParentChildMapping(Unlimited, FAPIContractExport);
        //    }

        //    protected virtual void AddParentChildMappingsCommon()
        //    {
        //        base.AddParentChildMappings();

        //        #region FAdminMenu
        //        AddParentChildMapping(FAdminMenu, FDischargeOutcomes);
        //        AddParentChildMapping(FAdminMenu, FDoseTypes);
        //        AddParentChildMapping(FAdminMenu, FExamTestTypes);
        //        AddParentChildMapping(FAdminMenu, FGoalTreatments);
        //        AddParentChildMapping(FAdminMenu, FInvestigationTypes);
        //        AddParentChildMapping(FAdminMenu, FMedNotesColours);
        //        AddParentChildMapping(FAdminMenu, FPatientGoals);
        //        AddParentChildMapping(FAdminMenu, FTrainerTypes);
        //        AddParentChildMapping(FAdminMenu, FReadCodes);
        //        AddParentChildMapping(FAdminMenu, FMedicationTypes);
        //        AddParentChildMapping(FAdminMenu, FMCSPatternTypes);
        //        AddParentChildMapping(FAdminMenu, FDiabetesRiskTypes);
        //        AddParentChildMapping(FAdminMenu, FInjuryTypes);

        //        AddParentChildMapping(FAdminMenu, FInternalSportTypes);
        //        AddParentChildMapping(FAdminMenu, FCardLevels);
        //        AddParentChildMapping(FAdminMenu, FTreatmentTypes);
        //        #endregion

        //        #region FReportsMenu
        //        AddParentChildMapping(FReportsMenu, FRConditionReferrals);
        //        AddParentChildMapping(FReportsMenu, FRClientNotSeenOrDischarged);
        //        AddParentChildMapping(FReportsMenu, FRConditionSummary);
        //        AddParentChildMapping(FReportsMenu, FRTopReadCodeReport);
        //        AddParentChildMapping(FReportsMenu, FRClinicanUniqueClients);
        //        AddParentChildMapping(FReportsMenu, FRWhyChooseUsConditions);
        //        AddParentChildMapping(FReportsMenu, FRReferralSourceConditions);
        //        AddParentChildMapping(FReportsMenu, FRClincianApptScheduleDetailed);
        //        AddParentChildMapping(FReportsMenu, FRClincianApptTreatments);
        //        AddParentChildMapping(FReportsMenu, FRClientApptTreatments);
        //        AddParentChildMapping(FReportsMenu, FRApptsWithoutConditions);
        //        AddParentChildMapping(FReportsMenu, FRApptsWithoutServiceCharge);
        //        AddParentChildMapping(FReportsMenu, FRConditionVisitSummary);
        //        AddParentChildMapping(FReportsMenu, FRDischargeSummary);
        //        AddParentChildMapping(FReportsMenu, FRClientConditionGoals);
        //        //AddParentChildMapping(FReportsMenu, FRACC32AwaitingApproval);
        //        AddParentChildMapping(FReportsMenu, FRIntraReferrals);
        //        AddParentChildMapping(FReportsMenu, FRIncompleteExams);
        //        AddParentChildMapping(FReportsMenu, FRConditionFollowups);
        //        AddParentChildMapping(FReportsMenu, FRApptsNotInSchedules);
        //        AddParentChildMapping(FReportsMenu, FRClinicianSummary);
        //        AddParentChildMapping(FReportsMenu, FROutcomeMeasuresToDo);

        //        AddParentChildMapping(FReportsMenu, FRContractSummary);
        //        AddParentChildMapping(FReportsMenu, FRContractSummaryLPO);

        //        AddParentChildMapping(FReportsMenu, FRCustomContractDetail);
        //        AddParentChildMapping(FReportsMenu, FRCustomContractDetailLPO);

        //        AddParentChildMapping(FReportsMenu, FRCustomContractApptDetail);
        //        AddParentChildMapping(FReportsMenu, FRCustomContractApptDetailLPO);

        //        AddParentChildMapping(FReportsMenu, FRCustomContractOutcomes);
        //        AddParentChildMapping(FReportsMenu, FRCustomContractOutcomesLPO);

        //        AddParentChildMapping(FReportsMenu, FRSportAppointmentTmts);
        //        AddParentChildMapping(FReportsMenu, FRSplintChargesReport);
        //        AddParentChildMapping(FReportsMenu, FRPhysioAppointmentSummary);
        //        AddParentChildMapping(FReportsMenu, FRPhysioAppointmentDetail);
        //        AddParentChildMapping(FReportsMenu, FRPTPaidAppointments);
        //        AddParentChildMapping(FReportsMenu, FRMcsReport);
        //        AddParentChildMapping(FReportsMenu, FRScheduleLineDetail);

        //        AddParentChildMapping(FReportsMenu, FRContractStatsReport);
        //        AddParentChildMapping(FReportsMenu, FRContractStatsReportLPO);

        //        //AddParentChildMapping(FReportsMenu, FRPhoStatsReport);

        //        AddParentChildMapping(FReportsMenu, FRContractTracking);
        //        AddParentChildMapping(FReportsMenu, FRContractTrackingLPO);

        //        AddParentChildMapping(FReportsMenu, FRContractProvider);
        //        AddParentChildMapping(FReportsMenu, FRContractProviderLPO);

        //        AddParentChildMapping(FReportsMenu, FRSiteContractPayment);
        //        AddParentChildMapping(FReportsMenu, FRSiteContractPaymentLPO);

        //        AddParentChildMapping(FReportsMenu, FRContractExport);
        //        AddParentChildMapping(FReportsMenu, FRContractExportLPO);

        //        AddParentChildMapping(FReportsMenu, FRPhysioAppointmentsAllowedStatus);

        //        AddParentChildMapping(FReportsMenu, FRContractProviderHours);
        //        AddParentChildMapping(FReportsMenu, FRContractProviderHoursLPO);

        //        AddParentChildMapping(FReportsMenu, FRIncompleteContractCharges);
        //        AddParentChildMapping(FReportsMenu, FRIncompleteContractChargesLPO);

        //        AddParentChildMapping(FReportsMenu, FRContractChargesExpenses);
        //        AddParentChildMapping(FReportsMenu, FRContractChargesExpensesLPO);

        //        AddParentChildMapping(FReportsMenu, FRConditionDischargeExport);

        //        AddParentChildMapping(FReportsMenu, FRContractDischargeExport);
        //        AddParentChildMapping(FReportsMenu, FRContractDischargeExportLPO);

        //        AddParentChildMapping(FReportsMenu, FRKPIReport);
        //        AddParentChildMapping(FReportsMenu, FRExamOrchardCodes);
        //        AddParentChildMapping(FReportsMenu, FRExamTreatmentTypes);

        //        AddParentChildMapping(FReportsMenu, FRDynamicAssessmentExport);

        //        AddParentChildMapping(FReportsMenu, FRClientConditionAppts);

        //        #endregion

        //        #region AFReportsMenu
        //        AddParentChildMapping(AFReportsMenu, AFRClinicianRevenueGenerated);
        //        AddParentChildMapping(AFReportsMenu, AFRClinicianRevenueGeneratedDetail);
        //        AddParentChildMapping(AFReportsMenu, AFRClinicianRevenuePaid);
        //        AddParentChildMapping(AFReportsMenu, AFRClinicianRevenuePaidDetail);
        //        AddParentChildMapping(AFReportsMenu, AFRClinicianApptRevenueGenerated);
        //        AddParentChildMapping(AFReportsMenu, AFRClientGroupingRevenueReport);
        //        AddParentChildMapping(AFReportsMenu, AFRInsurerRevenueReport);

        //        AddParentChildMapping(AFReportsMenu, AFRClinicianRevenueGeneratedUPO);
        //        AddParentChildMapping(AFReportsMenu, AFRClinicianRevenueGeneratedDetailUPO);
        //        AddParentChildMapping(AFReportsMenu, AFRClinicianRevenuePaidUPO);
        //        AddParentChildMapping(AFReportsMenu, AFRClinicianRevenuePaidDetailUPO);

        //        #endregion

        //        #region FClientAccess
        //        AddParentChildMapping(FClientAccess, FClientsConditions);
        //        //AddParentChildMapping(FClientsConditions, FConditionsMedNoteSummary);
        //        //AddParentChildMapping(FClientsConditions, FConditionsReferralDischarges);
        //        //AddParentChildMapping(FClientsConditions, FConditionsOutcomeMeasures);
        //        //AddParentChildMapping(FClientsConditions, FConditionsAcc32s);
        //        AddParentChildMapping(FClientAccess, FClientsHealthlink);
        //        AddParentChildMapping(FClientAccess, FMedicalNotes);
        //        #endregion

        //        #region FContractsMenu

        //        AddParentChildMapping(MainFormAndReports, FContractsMenu);
        //        AddParentChildMapping(FContractsMenu, FAbpProgrammeTypes);
        //        AddParentChildMapping(FContractsMenu, FAbpContracts);
        //        AddParentChildMapping(FContractsMenu, FAbpContractsReport);
        //        AddParentChildMapping(FContractsMenu, FCustomContractTypes);

        //        AddParentChildMapping(FContractsMenu, FCustomContracts);
        //        AddParentChildMapping(FContractsMenu, FCustomContractsLPO);
        //        AddParentChildMapping(FContractsMenu, FCustomContractsLinked);

        //        AddParentChildMapping(FContractsMenu, FCustomContractViewApptCharges);
        //        AddParentChildMapping(FContractsMenu, FCustomContractSetSites);
        //        AddParentChildMapping(FContractsMenu, FCustomContractExpenses);
        //        AddParentChildMapping(FContractsMenu, FCustomContractSetDiscount);
        //        AddParentChildMapping(FContractsMenu, FCustomContractTypeChargesOnly);

        //        #endregion

        //        AddParentChildMapping(MainFormAndReports, FAllowPkAccess);
        //    }

        //    protected virtual void AddParentChildMappingsSpecialized()
        //    {
        //        #region FACC45sAndBatchesMenu
        //        AddParentChildMapping(MainFormAndReports, FACC45sAndBatchesMenu);
        //        AddParentChildMapping(FACC45sAndBatchesMenu, FACC45s);
        //        AddParentChildMapping(FACC45sAndBatchesMenu, FACC32s);
        //        AddParentChildMapping(FACC45sAndBatchesMenu, FInsurerSchedules);
        //        AddParentChildMapping(FACC45sAndBatchesMenu, FPaymentsRejections);
        //        AddParentChildMapping(FACC45sAndBatchesMenu, FAcceptanceRejectionReasons);
        //        AddParentChildMapping(FACC45sAndBatchesMenu, FSendACC45s);
        //        AddParentChildMapping(FACC45sAndBatchesMenu, FSendACCSchedules);
        //        AddParentChildMapping(FACC45sAndBatchesMenu, FACCTravelBilling);
        //        AddParentChildMapping(FReportsMenu, FRPhoStatsReport);
        //        AddParentChildMapping(FReportsMenu, FRACC32AwaitingApproval);
        //        #endregion
        //    }

        //    #endregion
        //}

        //#endregion

        #region PContractSelectionType
        public enum PContractSelectionType : int
        {
            Created = 1,
            Commenced = 2,
            Referred = 3,
            Held = 4,
            Declined = 5,
            [Description("Non Completion")]
            NonCompletion = 6,
            Completed = 7,
            [Description("All Pending")]
            AllPending = 8,
            [Description("All On Hold")]
            AllOnHold = 9,
            [Description("All Incomplete")]
            AllIncomplete = 10,
            [Description("All Unpaid")]
            AllUnpaid = 11,
            [Description("All Approved")]
            AllApproved = 12,
            [Description("Assessment Only")]
            AssessmentOnly = 13,
            Updated = 14
        }
        #endregion

        #region ContractOutcomeDateRestrictionType
        public enum ContractOutcomeDateRestrictionType : int
        {
            OutcomeCreated = 1,
            ServiceEndDate = 2
        }
        #endregion

        #region PContractStatus
        public enum PContractStatus : int
        {
            [Description("Pending Approval")]
            PendingApproval = 1,
            Approved = 2,
            Declined = 3,
            [Description("Non Completion")]
            NonCompletion = 4,
            Completed = 5,
            [Description("On Hold")]
            OnHold = 6,
            [Description("Assessment Only")]
            AssessmentOnly = 7

        }

        public static string PContractStatusDescription(PContractStatus status)
        {
            return PContractStatusDescription((int)status);
        }

        public static string PContractStatusDescription(Decimal status)
        {
            return PContractStatusDescription((int)status);
        }

        public static string PContractStatusDescription(int status)
        {
            switch (status)
            {
                case (int)PContractStatus.PendingApproval:
                    return "Pending Approval";
                case (int)PContractStatus.Approved:
                    return "Approved";
                case (int)PContractStatus.Declined:
                    return "Declined";
                case (int)PContractStatus.NonCompletion:
                    return "Non Completion";
                case (int)PContractStatus.Completed:
                    return "Completed";
                case (int)PContractStatus.OnHold:
                    return "On Hold";
                case (int)PContractStatus.AssessmentOnly:
                    return "Assessment Only";
                default:
                    return "Unknown";
            }
        }

        public static string PContractShortStatusDescription(int status)
        {
            switch (status)
            {
                case (int)PContractStatus.PendingApproval:
                    return "Pending";
                case (int)PContractStatus.Approved:
                    return "Approved";
                case (int)PContractStatus.Declined:
                    return "Declined";
                case (int)PContractStatus.NonCompletion:
                    return "Non Compl";
                case (int)PContractStatus.Completed:
                    return "Completed";
                case (int)PContractStatus.OnHold:
                    return "Held";
                case (int)PContractStatus.AssessmentOnly:
                    return "Assessment";
                default:
                    return "Unknown";
            }
        }
        #endregion

        #region AssessmentOwnerType
        public enum AssessmentOwnerType : int
        {
            Condition = 1,
            AbpContract = 2,
            Contract = 3,
            Exam = 4
        }

        public static string AssessmentOwnerTypeDescription(AssessmentOwnerType type)
        {
            return AssessmentOwnerTypeDescription((int)type);
        }

        public static string AssessmentOwnerTypeDescription(Decimal type)
        {
            return AssessmentOwnerTypeDescription((int)type);
        }

        public static string AssessmentOwnerTypeDescription(int type)
        {
            switch (type)
            {
                case (int)AssessmentOwnerType.Condition:
                    return "Condition";
                case (int)AssessmentOwnerType.AbpContract:
                    return "ABP Contract";
                case (int)AssessmentOwnerType.Contract:
                    return "Contract";
                case (int)AssessmentOwnerType.Exam:
                    return "Exam";
                default:
                    return "Unknown";
            }
        }
        #endregion

        #region PhysioProviderType

        public enum PhysioProviderType : int
        {
            Physiotherapist = 1,
            Osteopath = 2,
            Podiatrist = 3,
            Acupuncturist = 4,
            Chiropractor = 5,
            PersonalTrainer = 6,
            Any = 7,
            Handtherapist = 8,
            GP = 9,
            Specialist = 10,
            ExercisePhysiologist = 11,
            MassageTherapist = 12,
            Psychologist = 13,
            OccupationalTherapist = 14,
            PhysiotherapistEpn = 15,
            DiabetesEducator = 16,
            Dietitian = 17,
            Counselor = 18,
            RemedialMassage = 19,
            Pharmacist = 20,
            SpeechPathology = 21,
            SpeechTherapist = 22,
            PhysioSpecialist = 23
        }
        public static string PhysioProviderTypeDescription(PhysioProviderType type)
        {
            return PhysioProviderTypeDescription((int)type);
        }

        public static string PhysioProviderTypeDescription(Decimal type)
        {
            return PhysioProviderTypeDescription((int)type);
        }

        public static string PhysioProviderTypeDescription(int type)
        {
            switch (type)
            {
                case (int)PhysioProviderType.Physiotherapist: return "Physiotherapist";
                case (int)PhysioProviderType.PhysiotherapistEpn: return "Physiotherapist - EPN";
                case (int)PhysioProviderType.Osteopath: return "Osteopath";
                case (int)PhysioProviderType.Podiatrist: return "Podiatrist";
                case (int)PhysioProviderType.Acupuncturist: return "Acupuncturist";
                case (int)PhysioProviderType.Chiropractor: return "Chiropractor";
                case (int)PhysioProviderType.PersonalTrainer: return "Other Provider";
                case (int)PhysioProviderType.Handtherapist: return "Hand Therapist";
                case (int)PhysioProviderType.GP: return "General Practitioner";
                case (int)PhysioProviderType.Specialist: return "Specialist";
                case (int)PhysioProviderType.ExercisePhysiologist: return "Exercise Physiologist";
                case (int)PhysioProviderType.MassageTherapist: return "Massage Therapist";
                case (int)PhysioProviderType.Psychologist: return "Psychologist";
                case (int)PhysioProviderType.OccupationalTherapist: return "Occupational Therapist";
                case (int)PhysioProviderType.Any: return "Any Clinician";
                case (int)PhysioProviderType.DiabetesEducator: return "Diabetes Educator";
                case (int)PhysioProviderType.Dietitian: return "Dietitian";
                case (int)PhysioProviderType.Counselor: return "Counselor";
                case (int)PhysioProviderType.RemedialMassage: return "Remedial Massage";
                case (int)PhysioProviderType.Pharmacist: return "Pharmacist";
                case (int)PhysioProviderType.SpeechPathology: return "Speech Pathology";
                case (int)PhysioProviderType.SpeechTherapist: return "Speech Therapist";
                case (int)PhysioProviderType.PhysioSpecialist: return "Physio Specialist";
                // case (int)PhysioProviderType.Other: return "Other Provider";
                default: return "Unknown";
            }
        }

        public static string PhysioProviderTypeShortDescription(PhysioProviderType type)
        {
            return PhysioProviderTypeShortDescription((int)type);
        }

        public static string PhysioProviderTypeShortDescription(Decimal type)
        {
            return PhysioProviderTypeShortDescription((int)type);
        }

        public static string PhysioProviderTypeShortDescription(int type)
        {
            switch (type)
            {
                case (int)PhysioProviderType.Physiotherapist: return "Physio";
                case (int)PhysioProviderType.Osteopath: return "Osteo";
                case (int)PhysioProviderType.Podiatrist: return "Podiatrist";
                case (int)PhysioProviderType.Acupuncturist: return "Acupuncturist";
                case (int)PhysioProviderType.Chiropractor: return "Chiropractor";
                case (int)PhysioProviderType.PersonalTrainer: return "Other";
                case (int)PhysioProviderType.Handtherapist: return "Hand Therapist";
                case (int)PhysioProviderType.GP: return "GP";
                case (int)PhysioProviderType.Specialist: return "Specialist";
                case (int)PhysioProviderType.ExercisePhysiologist: return "EP";
                case (int)PhysioProviderType.MassageTherapist: return "Massage Therapist";
                case (int)PhysioProviderType.Psychologist: return "Psychologist";
                case (int)PhysioProviderType.OccupationalTherapist: return "Occupational Therapist";
                case (int)PhysioProviderType.DiabetesEducator: return "Diabetes Educator";
                case (int)PhysioProviderType.Dietitian: return "Dietitian";
                case (int)PhysioProviderType.Counselor: return "Counselor";
                case (int)PhysioProviderType.RemedialMassage: return "Remedial Massage";
                case (int)PhysioProviderType.SpeechPathology: return "Speech Pathology";
                case (int)PhysioProviderType.SpeechTherapist: return "Speech Therapist";
                case (int)PhysioProviderType.PhysioSpecialist: return "Physio Specialist";
                case (int)PhysioProviderType.Any: return "Any";
                default: return "Unknown";
            }
        }

        public static List<decimal> AllPhysioProviderTypes()
        {
            var allTypes = new List<decimal>();
            allTypes.Add((decimal)PhysioProviderType.Physiotherapist);
            allTypes.Add((decimal)PhysioProviderType.PhysiotherapistEpn);
            allTypes.Add((decimal)PhysioProviderType.Osteopath);
            allTypes.Add((decimal)PhysioProviderType.Podiatrist);
            allTypes.Add((decimal)PhysioProviderType.Acupuncturist);
            allTypes.Add((decimal)PhysioProviderType.Chiropractor);
            allTypes.Add((decimal)PhysioProviderType.PersonalTrainer);
            allTypes.Add((decimal)PhysioProviderType.Handtherapist);
            allTypes.Add((decimal)PhysioProviderType.GP);
            allTypes.Add((decimal)PhysioProviderType.Specialist);
            allTypes.Add((decimal)PhysioProviderType.ExercisePhysiologist);
            allTypes.Add((decimal)PhysioProviderType.MassageTherapist);
            allTypes.Add((decimal)PhysioProviderType.Psychologist);
            allTypes.Add((decimal)PhysioProviderType.Any);
            allTypes.Add((decimal)PhysioProviderType.DiabetesEducator);
            allTypes.Add((decimal)PhysioProviderType.Dietitian);
            allTypes.Add((decimal)PhysioProviderType.Counselor);
            allTypes.Add((decimal)PhysioProviderType.RemedialMassage);
            allTypes.Add((decimal)PhysioProviderType.Pharmacist);
            allTypes.Add((decimal)PhysioProviderType.SpeechPathology);
            allTypes.Add((decimal)PhysioProviderType.SpeechTherapist);
            allTypes.Add((decimal)PhysioProviderType.PhysioSpecialist);

            return allTypes;
        }
        #endregion

   //     #region AdminType
   //     protected override Gensolve.OrganisationData.Common.OrganisationEnums.AdminType GetAdminTypeEnum()
   //     {
   //         /*
			//if (adminTypeInstance == null || !typeof(AdminType).IsAssignableFrom(adminTypeInstance.GetType()))
			//	adminTypeInstance = new AdminType();

			//return adminTypeInstance;
			// * */
   //         return new AdminType();
   //     }

   //     public new AdminType AdminTypeEnum
   //     {
   //         get { return (AdminType)GetAdminTypeEnum(); }
   //     }

   //     public new class AdminType : Gensolve.AppointmentData.Common.AppointmentEnums.AdminType
   //     {
   //         public static Hashtable AdminTypeDetails
   //         {
   //             get
   //             {
   //                 if (adminTypeDetails.Count == 0)
   //                 {
   //                     new AdminType();
   //                 }
   //                 return adminTypeDetails;
   //             }
   //         }

   //         private const int startFrom = Gensolve.AppointmentData.Common.AppointmentEnums.AdminType.AdminTypeStartFrom;

   //         protected new const int AdminTypeStartFrom = startFrom + 1000;

   //         public AdminTypeDetail PatientGoal = new AdminTypeDetail(adminTypeDetails, startFrom + 1, "Patient Goal", PhysioEnums.Instance.AccessPrivilegeTypeEnum.FPatientGoals);
   //         public AdminTypeDetail GoalActivity = new AdminTypeDetail(adminTypeDetails, startFrom + 2, "Goal Activity/Plan", PhysioEnums.Instance.AccessPrivilegeTypeEnum.FGoalTreatments);
   //         public AdminTypeDetail DischargeOutcome = new AdminTypeDetail(adminTypeDetails, startFrom + 3, "Discharge Outcome", PhysioEnums.Instance.AccessPrivilegeTypeEnum.FDischargeOutcomes);
   //         public AdminTypeDetail TrainerProviderType = new AdminTypeDetail(adminTypeDetails, startFrom + 4, "Other Provider Type", PhysioEnums.Instance.AccessPrivilegeTypeEnum.FTrainerTypes);
   //         public AdminTypeDetail InternalSportType = new AdminTypeDetail(adminTypeDetails, startFrom + 5, "Sport Type", PhysioEnums.Instance.AccessPrivilegeTypeEnum.FInternalSportTypes);
   //         public AdminTypeDetail CardLevel = new AdminTypeDetail(adminTypeDetails, startFrom + 6, "Card Level", PhysioEnums.Instance.AccessPrivilegeTypeEnum.FCardLevels);
   //         public AdminTypeDetail DoseType = new AdminTypeDetail(adminTypeDetails, startFrom + 7, "Dose Type", PhysioEnums.Instance.AccessPrivilegeTypeEnum.FDoseTypes);
   //         public AdminTypeDetail MCSPatternType = new AdminTypeDetail(adminTypeDetails, startFrom + 8, "MCS Pattern", PhysioEnums.Instance.AccessPrivilegeTypeEnum.FMCSPatternTypes);
   //         public AdminTypeDetail DiabetesRiskType = new AdminTypeDetail(adminTypeDetails, startFrom + 9, "Diabetes Risk Type", PhysioEnums.Instance.AccessPrivilegeTypeEnum.FDiabetesRiskTypes);
   //         public AdminTypeDetail InjuryType = new AdminTypeDetail(adminTypeDetails, startFrom + 11, "Injury Type", PhysioEnums.Instance.AccessPrivilegeTypeEnum.FInjuryTypes);
   //         public AdminTypeDetail MainProviderReason = new AdminTypeDetail(adminTypeDetails, startFrom + 12, "Main Provider Reason", PhysioEnums.Instance.AccessPrivilegeTypeEnum.FMainProviderReason);
   //         public AdminTypeDetail TreatmentType = new AdminTypeDetail(adminTypeDetails, startFrom + 13, "Treatment Type", PhysioEnums.Instance.AccessPrivilegeTypeEnum.FTreatmentTypes);
   //     }

   //     #endregion

        #region TimeframeType
        public enum TimeframeType : int
        {
            Days = 1,
            Weeks = 2,
            Months = 3
        }

        public static string TimeframeTypeDescription(TimeframeType type)
        {
            return TimeframeTypeDescription((int)type);
        }

        public static string TimeframeTypeDescription(Decimal type)
        {
            return TimeframeTypeDescription((int)type);
        }

        public static string TimeframeTypeDescription(int type)
        {
            switch (type)
            {
                case (int)TimeframeType.Days:
                    return "Days";
                case (int)TimeframeType.Weeks:
                    return "Weeks";
                case (int)TimeframeType.Months:
                    return "Months";
                default:
                    return "Unknown";
            }
        }
        #endregion

        //#region LetterTemplateType

        //protected override Gensolve.OrganisationData.Common.OrganisationEnums.LetterTemplateType GetLetterTemplateTypeEnum()
        //{
        //    return new LetterTemplateType();
        //}

        //public new LetterTemplateType LetterTemplateTypeEnum
        //{
        //    get { return (LetterTemplateType)GetLetterTemplateTypeEnum(); }
        //}

        //public new class LetterTemplateType : Gensolve.AppointmentData.Common.AppointmentEnums.LetterTemplateType
        //{
        //    int physioStartFrom = Gensolve.AppointmentData.Common.AppointmentEnums.LetterTemplateType.LetterTemplateTypeStartFrom;

        //    public int AbpContract { get { return physioStartFrom + 1; } }
        //    public int PhysioCondition { get { return physioStartFrom + 2; } }
        //    public int ConditionExam { get { return physioStartFrom + 3; } }
        //    public int ABPExam { get { return physioStartFrom + 4; } }
        //    public int PContract { get { return physioStartFrom + 5; } }
        //    public int PContractExam { get { return physioStartFrom + 6; } }
        //    public int ClientAppointment { get { return physioStartFrom + 7; } }

        //    public LetterTemplateType()
        //    {
        //    }

        //    public override string GetDescription(int type)
        //    {
        //        if (type == AbpContract) return "ABP Contract";
        //        else if (type == PhysioCondition) return "Condition";
        //        else if (type == ConditionExam) return "Condition Exam";
        //        else if (type == ABPExam) return "ABP Exam";
        //        else if (type == PContract) return "Contract";
        //        else if (type == PContractExam) return "Contract Exam";
        //        else if (type == ClientAppointment) return "Client Appointment";
        //        else
        //            return base.GetDescription(type);
        //    }

        //    public override AccessPrivilegeTypeDetail GetAccessRequiredForLetterTemplateType(int type)
        //    {
        //        var privType = PhysioEnums.Instance.AccessPrivilegeTypeEnum;

        //        if (type == AbpContract) return privType.FClientsLettersAndUploads; //why is this letter type defined in org?
        //        else if (type == PhysioCondition) return privType.FClientsLettersAndUploads;
        //        else if (type == ConditionExam) return privType.FClientsLettersAndUploads;
        //        else if (type == ABPExam) return privType.FClientsLettersAndUploads;
        //        else if (type == PContract) return privType.FClientsLettersAndUploads;
        //        else if (type == PContractExam) return privType.FClientsLettersAndUploads;
        //        else if (type == ClientAppointment) return privType.FClientsLettersAndUploads;
        //        else
        //            return base.GetAccessRequiredForLetterTemplateType(type);
        //    }

        //    public override List<decimal> GetAccessTemplateTypes(AccessPrivilegeTypeDetail accessType)
        //    {
        //        var types = base.GetAccessTemplateTypes(accessType);

        //        var privType = PhysioEnums.Instance.AccessPrivilegeTypeEnum;

        //        if (accessType.Id == privType.FClientsLettersAndUploads.Id)
        //        {
        //            types.Add(AbpContract);
        //            types.Add(PhysioCondition);
        //            types.Add(ConditionExam);
        //            types.Add(ABPExam);
        //            types.Add(PContract);
        //            types.Add(PContractExam);
        //            types.Add(ClientAppointment);
        //        }

        //        return types;
        //    }
        //}
        //#endregion

        //#region EntityType

        //protected override Gensolve.OrganisationData.Common.OrganisationEnums.EntityType GetEntityTypeEnum()
        //{
        //    return new EntityType();
        //}
        //public new EntityType EntityTypeEnum
        //{
        //    get { return (EntityType)GetEntityTypeEnum(); }
        //}

        //public new class EntityType : Gensolve.AppointmentData.Common.AppointmentEnums.EntityType
        //{
        //    private const int startFrom = Gensolve.AppointmentData.Common.AppointmentEnums.EntityType.EntityTypeStartFrom;

        //    // ReSharper disable InconsistentNaming
        //    public int PContractAppointment { get { return P_CONTRACT_APPOINTMENT; } }
        //    public int Exam { get { return EXAM; } }
        //    public int AbpContract { get { return ABP_CONTRACT; } }
        //    public int CustomContract { get { return CUSTOM_CONTRACT; } }
        //    public int ClientCondition { get { return CLIENT_CONDITION; } }
        //    public int HISOForm { get { return HISO_FORM; } }
        //    public int InsurerSchedule { get { return INSURER_SCHEDULE; } }
        //    public int Assessment { get { return ASSESSMENT; } }
        //    public int ContractContact { get { return CONTRACT_CONTACT; } }

        //    public const int P_CONTRACT_APPOINTMENT = startFrom + 0;
        //    public const int EXAM = startFrom + 1;
        //    public const int ABP_CONTRACT = startFrom + 2;
        //    public const int CUSTOM_CONTRACT = startFrom + 3;
        //    public const int CLIENT_CONDITION = startFrom + 4;
        //    public const int HISO_FORM = startFrom + 5;
        //    public const int INSURER_SCHEDULE = startFrom + 6;
        //    public const int ASSESSMENT = startFrom + 7;
        //    public const int CONTRACT_CONTACT = startFrom + 8;
        //    // ReSharper restore InconsistentNaming

        //    public override string GetDescription(int type)
        //    {
        //        return _GetDescription(type);
        //    }

        //    public new static string _GetDescription(int type)
        //    {
        //        if (type == P_CONTRACT_APPOINTMENT) return "Contract Appointment";
        //        if (type == EXAM) return "Exam";
        //        if (type == ABP_CONTRACT) return "ABP Contract";
        //        if (type == CUSTOM_CONTRACT) return "Custom Contract";
        //        if (type == CLIENT_CONDITION) return "Client Condition";
        //        if (type == HISO_FORM) return "HISO Form";
        //        if (type == INSURER_SCHEDULE) return "Insurer Schedule";
        //        if (type == ASSESSMENT) return "Assessment";
        //        if (type == CONTRACT_CONTACT) return "Contract Contact";
        //        return Gensolve.AppointmentData.Common.AppointmentEnums.EntityType._GetDescription(type);
        //    }
        //    public override List<int> GetEntitiesTypesToLog()
        //    {
        //        List<int> list = base.GetEntitiesTypesToLog();
        //        list.Add(CUSTOM_CONTRACT);
        //        return list;
        //    }
        //}
        //#endregion

        //#region AppointmentControlType

        //protected override Gensolve.AppointmentData.Common.AppointmentEnums.AppointmentControlType GetAppointmentControlTypeEnum()
        //{
        //    return new AppointmentControlType();
        //}

        //public new AppointmentControlType AppointmentControlTypeEnum
        //{
        //    get { return (AppointmentControlType)GetAppointmentControlTypeEnum(); }
        //}

        //public new class AppointmentControlType : Gensolve.AppointmentData.Common.AppointmentEnums.AppointmentControlType
        //{
        //    private const int startFrom = Gensolve.AppointmentData.Common.AppointmentEnums.AppointmentControlType.AppointmentControlTypeStartFrom;

        //    // ReSharper disable InconsistentNaming
        //    public const int PHYSIO = startFrom + 0;
        //    public const int PERSONAL_TRAINER = startFrom + 1;
        //    public const int ABP_ASSESSMENT = startFrom + 2;
        //    public const int ABP_REHAB = startFrom + 3;
        //    public const int P_CONTRACT = startFrom + 4;
        //    // ReSharper restore InconsistentNaming

        //    [Obsolete("This properties are obsolute pelase use the constant values instead.")]
        //    public int Physio { get { return PHYSIO; } } //100
        //    [Obsolete("This properties are obsolute pelase use the constant values instead.")]
        //    public int PersonalTrainer { get { return PERSONAL_TRAINER; } } // 101
        //    [Obsolete("This properties are obsolute pelase use the constant values instead.")]
        //    public int AbpAssessment { get { return ABP_ASSESSMENT; } } // 102
        //    [Obsolete("This properties are obsolute pelase use the constant values instead.")]
        //    public int AbpRehab { get { return ABP_REHAB; } } // 103
        //    [Obsolete("This properties are obsolute pelase use the constant values instead.")]
        //    public int PContract { get { return P_CONTRACT; } } // 104

        //    public override string GetDescription(int type)
        //    {
        //        switch (type)
        //        {
        //            case PHYSIO:
        //                return "Physio";
        //            case PERSONAL_TRAINER:
        //                return "Personal Trainer";
        //            case ABP_ASSESSMENT:
        //                return "Abp Assessment";
        //            case P_CONTRACT:
        //                return "Abp Rehab";
        //            default:
        //                return base.GetDescription(type);
        //        }
        //    }

        //    public Dictionary<string, int> GetTypes()
        //    {
        //        var consts = this.GetType().GetFields(BindingFlags.Static | BindingFlags.Public).Where(f => f.IsLiteral).ToList();
        //        consts.AddRange(this.GetType().BaseType.GetFields(BindingFlags.Static | BindingFlags.Public).Where(f => f.IsLiteral));

        //        var dictionary = new Dictionary<string, int>();
        //        foreach (FieldInfo c in consts)
        //        {

        //            dictionary.Add(c.Name, (int)c.GetValue(this));
        //        }
        //        return dictionary;
        //    }


        //}
        //#endregion

        //#region AppointmentType

        //// if this ever changes you must update the database package

        //protected override Gensolve.AppointmentData.Common.AppointmentEnums.AppointmentType GetAppointmentTypeEnum()
        //{
        //    return new AppointmentType();
        //}

        //public new AppointmentType AppointmentTypeEnum
        //{
        //    get { return (AppointmentType)GetAppointmentTypeEnum(); }
        //}

        //public new class AppointmentType : Gensolve.AppointmentData.Common.AppointmentEnums.AppointmentType
        //{
        //    private const int startFrom = Gensolve.AppointmentData.Common.AppointmentEnums.AppointmentType.AppointmentTypeStartFrom;

        //    protected new const int AppointmentTypeStartFrom = startFrom + 100;


        //    // ReSharper disable once InconsistentNaming
        //    public const int P_CONTRACT = startFrom + 0;

        //    [Obsolete("This properties are obsolute pelase use the constant values instead.")]
        //    public int PContract { get { return P_CONTRACT; } }

        //    public override string GetDescription(int type)
        //    {
        //        if (type == P_CONTRACT) return "Contract";
        //        return base.GetDescription(type);
        //    }

        //    public Dictionary<string, int> GetTypes()
        //    {
        //        var consts = this.GetType().GetFields(BindingFlags.Static | BindingFlags.Public).Where(f => f.IsLiteral).ToList();
        //        consts.AddRange(this.GetType().BaseType.GetFields(BindingFlags.Static | BindingFlags.Public).Where(f => f.IsLiteral));

        //        var dictionary = new Dictionary<string, int>();
        //        foreach (FieldInfo c in consts)
        //            dictionary.Add(c.Name, (int)c.GetValue(this));

        //        return dictionary;
        //    }
        //}
        //#endregion

        //#region ClientAppointmentType

        //// if this ever changes you must update the database package

        //protected override Gensolve.AppointmentData.Common.AppointmentEnums.ClientAppointmentType GetClientAppointmentTypeEnum()
        //{
        //    return new ClientAppointmentType();
        //}

        //public new ClientAppointmentType ClientAppointmentTypeEnum
        //{
        //    get { return (ClientAppointmentType)GetClientAppointmentTypeEnum(); }
        //}

        //public new class ClientAppointmentType : Gensolve.AppointmentData.Common.AppointmentEnums.ClientAppointmentType
        //{
        //    private const int startFrom = Gensolve.AppointmentData.Common.AppointmentEnums.ClientAppointmentType.ClientAppointmentTypeStartFrom;

        //    protected new const int ClientAppointmentTypeStartFrom = startFrom + 100;

        //    public int Physio { get { return PHYSIO; } } //1
        //    public int PersonalTrainer { get { return PERSONAL_TRAINER; } } //2
        //    public int AbpContract { get { return ABP_CONTRACT; } } // 4

        //    // ReSharper disable InconsistentNaming
        //    public const int PHYSIO = startFrom + 0;
        //    public const int PERSONAL_TRAINER = startFrom + 1;
        //    public const int ABP_CONTRACT = startFrom + 3;
        //    // ReSharper restore InconsistentNaming

        //    public  string GetDescription(int type)
        //    {
        //        if (type == Physio)
        //            return "Clinician";
        //        else if (type == PersonalTrainer)
        //            return "Personal Trainer";
        //        else if (type == AbpContract)
        //            return "Abp Contract";
        //        else
        //            return base.GetDescription(type);
        //    }

        //    public Dictionary<string, int> GetTypes()
        //    {
        //        var consts = this.GetType().GetFields(BindingFlags.Static | BindingFlags.Public).Where(f => f.IsLiteral).ToList();
        //        consts.AddRange(this.GetType().BaseType.GetFields(BindingFlags.Static | BindingFlags.Public).Where(f => f.IsLiteral));

        //        var dictionary = new Dictionary<string, int>();
        //        foreach (FieldInfo c in consts)
        //        {

        //            dictionary.Add(c.Name, (int)c.GetValue(this));
        //        }
        //        return dictionary;
        //    }


        //}
        //#endregion

        #region InsurerScheduleLineType

        public enum InsurerScheduleLineType : int
        {
            Appointment = 1,
            ACC32 = 2,
            MiscItem = 3,
            ContractApptCharge = 4,
            ContractCharge = 5,
            ContractApptProduct = 6,
            ClassAppointment = 7
        }

        public static string InsurerScheduleLineTypeDescription(InsurerScheduleLineType type)
        {
            return InsurerScheduleLineTypeDescription((int)type);
        }

        public static string InsurerScheduleLineTypeDescription(Decimal type)
        {
            return InsurerScheduleLineTypeDescription((int)type);
        }

        public static string InsurerScheduleLineTypeDescription(int type)
        {
            switch (type)
            {
                case (int)InsurerScheduleLineType.Appointment:
                    return "Appointment";
                case (int)InsurerScheduleLineType.ACC32:
                    return "ACC32 Request";
                case (int)InsurerScheduleLineType.MiscItem:
                    return "Misc Billing";
                case (int)InsurerScheduleLineType.ContractApptCharge:
                    return "Contract Appt Charge";
                case (int)InsurerScheduleLineType.ContractCharge:
                    return "Contract Charge";
                case (int)InsurerScheduleLineType.ContractApptProduct:
                    return "Contract Appt Product";
                case (int)InsurerScheduleLineType.ClassAppointment:
                    return "Class Appointment";
                default:
                    return "Unknown";
            }
        }
        #endregion

        #region PhysioConditionType

        public enum PhysioConditionType : int
        {
            ACC = 1,
            Insured = 2,
            Private = 3
        }

        public static string PhysioConditionTypeDescription(PhysioConditionType ctype)
        {
            return PhysioConditionTypeDescription((int)ctype);
        }

        public static string PhysioConditionTypeDescription(int status)
        {
            switch (status)
            {
                case (int)PhysioConditionType.ACC:
                    return "ACC";

                case (int)PhysioConditionType.Insured:
                    return "Insured";

                case (int)PhysioConditionType.Private:
                    return "Private";

                default:
                    return "";
            }
        }
        #endregion

        #region AccContractType
        public enum AccContractType : int
        {
            Regulation = 1,
            Epn = 2,
            HandTherapy = 3,
            Healthwise = 4,
            ClinicalServices = 5,
            CustomContract = 6,
            Ipm = 7, // Interventional Pain Management
            PhySpecialist = 8
        }

        public static string AccContractTypeDescription(decimal ctype)
        {
            return AccContractTypeDescription((int)ctype);
        }

        public static string AccContractTypeDescription(AccContractType ctype)
        {
            return AccContractTypeDescription((int)ctype);
        }

        public static string AccContractTypeDescription(int status)
        {
            switch (status)
            {
                case (int)AccContractType.Regulation:
                    return "Regulation Contract";

                case (int)AccContractType.Epn:
                    return "EPN Contract";

                case (int)AccContractType.HandTherapy:
                    return "Hand Therapy Contract";

                case (int)AccContractType.Healthwise:
                    return "Healthwise Contract";

                case (int)AccContractType.ClinicalServices:
                    return "Clinical Services Contract";

                case (int)AccContractType.CustomContract:
                    return "Custom Contract";

                case (int)AccContractType.Ipm:
                    return "Interventional Pain Management Contract";

                case (int)AccContractType.PhySpecialist:
                    return "Physio Specialist";

                default:
                    return "";
            }
        }

        public static string AccContractTypeShortDescription(decimal ctype)
        {
            return AccContractTypeShortDescription((int)ctype);
        }

        public static string AccContractTypeShortDescription(AccContractType ctype)
        {
            return AccContractTypeShortDescription((int)ctype);
        }

        public static string AccContractTypeShortDescription(int status)
        {
            switch (status)
            {
                case (int)AccContractType.Regulation:
                    return "Regulation";

                case (int)AccContractType.Epn:
                    return "EPN";

                case (int)AccContractType.HandTherapy:
                    return "Hand Therapy";

                case (int)AccContractType.Healthwise:
                    return "Healthwise";

                case (int)AccContractType.ClinicalServices:
                    return "Clinical Services";

                case (int)AccContractType.CustomContract:
                    return "Custom Contract";

                case (int)AccContractType.Ipm:
                    return "IPM";

                case (int)AccContractType.PhySpecialist:
                    return "Phy Specialist";

                default:
                    return "";
            }
        }
        #endregion

        public enum ContractBudgetType : int
        {
            [EnumResourceAttribute("Provider")]
            Provider = 0,
            [EnumResourceAttribute("Budget Group - Hours")]
            BudgetGroupHours = 1,
            [EnumResourceAttribute("Budget Group - Value")]
            BudgetGroupValue = 2
        }


        #region HTMLFormFieldType
        public enum HTMLFormFieldType : int
        {
            TextBox = 1,
            DropDown = 2,
            RadioYes = 3,
            RadioNo = 4,
            RadioNA = 5,
            TextBoxDate = 6,
            TextBoxTrim = 7

        }
        #endregion

        public enum HicapsTxnType : int
        {
            [EnumResourceAttribute("Claim")]
            Claim = 1,
            [EnumResourceAttribute("Cancel")]
            CancelClaim = 2,
            [EnumResourceAttribute("Quote")]
            Quote = 3
        }

        public enum HicapsChargeType : int
        {
            [EnumResourceAttribute("Client Appointment Charge")]
            ClientAppointmentCharge = 1,
            [EnumResourceAttribute("Class Appointment Charge")]
            ClassAppointmentCharge = 2,
        }

        public enum HicapsDeviceSettings : int
        {
            [EnumResourceAttribute("Conected Device")]
            ConnectedDevice = 1
        }

        public enum TyroDeviceSettings
        {
            [EnumResourceAttribute("Connected Device")]
            ConnectedDevice = 1
        }

        public enum MedicareOnlineChargeType : int
        {
            [EnumResourceAttribute("Client Appointment Charge")]
            ClientAppointmentCharge = 1,
            [EnumResourceAttribute("Class Appointment Charge")]
            ClassAppointmentCharge = 2,
        }

        public enum MedicareClaimType : int
        {
            /// <summary>
            /// Fully paid account (EFT) – where the account has been paid in full, payment will be made to
            /// the claimant’s nominated bank account. This payment will be initiated by the claimant swiping
            /// their EFTPOS card. For tyro: Client paid is the same as fullyPaid.
            /// </summary>
            [EnumResourceAttribute("Client paid")]
            ClientPaid = 1,
            /// <summary>
            /// Part paid account (Cheque) – where a claimant has made a part payment contribution
            /// towards the account.
            /// Where Medicare benefits are assessed as payable for a claim, a statement and/or cheque in the
            /// provider’s name will be forwarded to the claimant’s address recorded by Medicare Australia.
            /// The cheque is then forwarded by the claimant to the provider. Where no benefit is assessed as
            /// payable, a statement only will be forwarded to the claimant’s address recorded by Medicare.
            /// </summary>
            [EnumResourceAttribute("Partially paid")]
            PartPaid = 2,
            /// <summary>
            /// Unpaid (Cheque) – where the account is unpaid.
            /// Where Medicare benefits are assessed as payable for a claim, a statement and/or cheque in the
            /// provider’s name will be forwarded to the claimant’s address recorded by Medicare Australia.
            /// The cheque is then forwarded by the claimant to the provider with any outstanding balance.
            /// Where no benefit is assessed as payable, a statement only will be forwarded to the claimant’s
            /// address recorded by Medicare.
            /// </summary>
            [EnumResourceAttribute("Unpaid")]
            Unpaid = 3,
            /// <summary>
            /// A bulk bill claim is where a patient who is eligible for a Medicare benefit(s) assigns his/her
            /// right to the rebate(s) to the servicing provider via the eftPos terminal as full payment for that
            /// service(s) which is then paid directly to the provider’s nominated bank account registered with
            /// Medicare Australia.
            /// It is at the provider’s discretion whether to bulk bill a patient or not.
            /// </summary>
            [EnumResourceAttribute("Bulk Bill")]
            BulkBill = 4
        }
        public enum MedicareOnlineClaimType : int
        {
            [EnumResourceAttribute("Bulk Bill")]
            BulkBill = 1,
            [EnumResourceAttribute("DVA Paperless Streamlined")]
            DVAPaperlessStreamlined = 2,
            [EnumResourceAttribute("DVA Allied Health")]
            DVAAlliedHealth = 3,
            [EnumResourceAttribute("Patient Claim")]
            PatientClaim = 4
        }

        public enum MedicareGenderType : int
        {
            [EnumResourceAttribute("M")]
            Male = 1,
            [EnumResourceAttribute("F")]
            Female = 2,
            [EnumResourceAttribute("O")]
            Other = 3,
            [EnumResourceAttribute("N")]
            Not_stated_inadequately_described = 9
        }

        public enum MedicarePatientValidatorType : int
        {
            [EnumResourceAttribute("PVM")]
            PVM = 1,
            [EnumResourceAttribute("PVF")]
            PVF = 2,
            [EnumResourceAttribute("OPV")]
            OPV = 3

        }

        public enum MedicareTreatmentLocation : int
        {
            NONE = 0,
            [EnumResourceAttribute("V")]
            HomeVisit = 1,
            [EnumResourceAttribute("R")]
            Rooms = 2,
            [EnumResourceAttribute("C")]
            CommunityHealthCentres = 3,
            [EnumResourceAttribute("N")]
            ResidentialCareFacility = 4,
            [EnumResourceAttribute("H")]
            Hospital = 5
        }

        public enum MedicareReconciliationClaimType : int
        {
            [EnumResourceAttribute("MEDICARE ONLINE")]
            MedicareOnline = 1,
            [EnumResourceAttribute("HICAPS")]
            Hicaps = 2,
            [EnumResourceAttribute("TYRO")]
            Tyro = 3,
        }
        public enum MedicareTransmissionType : int
        {
            [EnumResourceAttribute("Direct Claim")]
            DirectClaim = 1,
            [EnumResourceAttribute("Batched")]
            Batched = 2
        }

        public enum MedicareReferralOverride : int
        {
            [EnumResourceAttribute("N")]
            None = 0,
            [EnumResourceAttribute("L")]
            Lost = 1,
            [EnumResourceAttribute("E")]
            Emergency = 2,
            [EnumResourceAttribute("N")]
            NonReferred = 3,
            [EnumResourceAttribute("H")]
            Hospital = 4
        }


        public enum MedicareReferralPeriodType : int
        {
            None = 0,
            [EnumResourceAttribute("S")]
            Standard = 1,
            [EnumResourceAttribute("I")]
            Indefinite = 2,
            [EnumResourceAttribute("N")]
            NonStandard = 3

        }

        public enum MedicareServiceType : int
        {
            [EnumResourceAttribute("S")]
            Specialist = 3,
            [EnumResourceAttribute("P")]
            Pathology = 4,
            [EnumResourceAttribute("D")]
            DiagnosticImaging = 5,
        }

        public enum MedicareItemOverrideCode : int
        {
            [EnumResourceAttribute("AO")]
            NotNormalAfterCare = 1,
            [EnumResourceAttribute("NC")]
            NotForComparison = 2,
            [EnumResourceAttribute("AP")]
            NotADupplicateService = 3
        }

        #region HandDominance
        public enum HandDominance : int
        {
            [EnumResourceAttribute("Left Handed")]
            Left = 1,
            [EnumResourceAttribute("Right Handed")]
            Right = 2,
            [EnumResourceAttribute("Ambidextrous")]
            Ambidextrous = 3
            /*,
			[EnumResourceAttribute("Mixed")]
			Mixed = 3,
			[EnumResourceAttribute("Ambilevous")]
			Ambilevous = 5
			*/
        }
        #endregion

        public enum SignOffExamType : int
        {
            [EnumResourceAttribute("Exam")]
            Exam = 1,
            [EnumResourceAttribute("Dental Exam")]
            DentalExam = 2
        }

        public enum ClassAppointmentType : int
        {
            [EnumResourceAttribute("Standard")]
            Standard = 1,
            [EnumResourceAttribute("Contract")]
            Contract = 2
        }
        public enum ContractDateType : int
        {
            [EnumResourceAttribute("Start Date")]
            StartDate = 0,
            [EnumResourceAttribute("Created Date")]
            CreatedDate = 1,
            [EnumResourceAttribute("Referred Date")]
            ReferredDate = 2,
            [EnumResourceAttribute("Current Date")]
            CurrentDate = 3
        }


#if !NETSTANDARD

        //public static ConfigurableReports ConfigurableReports
        //{
        //    get { return ConfigurableReports.Instance; }
        //}

        //#region SystemUserAccessType

        //protected override Gensolve.OrganisationData.Common.OrganisationEnums.SystemUserAccessType GetSystemUserAccessTypeEnum()
        //{
        //    return new SystemUserAccessType();
        //}

        //public new SystemUserAccessType SystemUserAccessTypeEnum
        //{
        //    get { return (SystemUserAccessType)GetSystemUserAccessTypeEnum(); }
        //}

        //public new class SystemUserAccessType : Gensolve.OrganisationData.Common.OrganisationEnums.SystemUserAccessType
        //{
        //    private const int startFrom = Gensolve.AppointmentData.Common.AppointmentEnums.SystemUserAccessType.SystemUserAccessTypeStartFrom + 1000;

        //    public int ACC45sAndBatches { get { return startFrom + 1; } }

        //    public SystemUserAccessType()
        //    {
        //    }

        //    public override ArrayList GetAllAccessTypes()
        //    {
        //        ArrayList items = base.GetAllAccessTypes();

        //        //only for GPM New Zealand
        //        if (GensolveGlobalInstance.Instance.ProductName == OrganisationEnums.ProductName.GPM)
        //        {
        //            items.Add(ACC45sAndBatches);
        //        }
        //        return items;
        //    }

        //    public override string GetName(int type)
        //    {
        //        if (type == ACC45sAndBatches) return "ACC45s and Batches";
        //        else
        //            return base.GetName(type);
        //    }

        //    public override string GetDescription(int type)
        //    {
        //        if (type == ACC45sAndBatches) return "Access ACC45s and Batches";
        //        else
        //            return base.GetDescription(type);
        //    }
        //}

        //#endregion

        //#region BinaryResourceType

        //protected override Gensolve.OrganisationData.Common.OrganisationEnums.BinaryResourceType GetBinaryResourceTypeEnum()
        //{
        //    return new BinaryResourceType();
        //}

        //public new BinaryResourceType BinaryResourceTypeEnum
        //{
        //    get { return (BinaryResourceType)GetBinaryResourceTypeEnum(); }
        //}

        //public new class BinaryResourceType : Gensolve.AppointmentData.Common.AppointmentEnums.BinaryResourceType
        //{
        //    private const int startFrom = Gensolve.AppointmentData.Common.AppointmentEnums.BinaryResourceType.BinaryResourceTypeStartFrom;

        //    //protected new const int BinaryResourceTypeStartFrom = startFrom + 1000;

        //    public int DischargeReportHeader { get { return startFrom + 0; } }
        //    public int BodyDiagram { get { return startFrom + 0; } }

        //    public override string GetDescription(int type)
        //    {
        //        if (type == DischargeReportHeader) return "Discharge Report Header";
        //        else if (type == BodyDiagram) return "Body Diagram";
        //        else return base.GetDescription(type);
        //    }

        //    public override ArrayList GetVendorTypes()
        //    {
        //        ArrayList types = base.GetVendorTypes();
        //        types.Add(DischargeReportHeader);
        //        return types;
        //    }

        //    public override ArrayList GetSiteTypes()
        //    {
        //        ArrayList types = base.GetSiteTypes();
        //        types.Add(DischargeReportHeader);
        //        return types;
        //    }

        //    public override System.Drawing.Size GetImageDimensions(decimal type)
        //    {
        //        if (type == DischargeReportHeader)
        //            return new System.Drawing.Size(700, 124);
        //        else if (type == BodyDiagram)
        //            return new System.Drawing.Size(298, 408);
        //        else
        //            return base.GetImageDimensions(type);
        //    }
        //}
        //#endregion

        //#region AttachmentType
        //protected override Gensolve.OrganisationData.Common.OrganisationEnums.AttachmentType GetAttachmentTypeEnum()
        //{
        //    return new AttachmentType();
        //}

        //public new AttachmentType AttachmentTypeEnum
        //{
        //    get { return (AttachmentType)GetAttachmentTypeEnum(); }
        //}

        //public new class AttachmentType : Gensolve.AppointmentData.Common.AppointmentEnums.AttachmentType
        //{
        //    private const int startFrom = Gensolve.AppointmentData.Common.AppointmentEnums.AttachmentType.AttachmentTypeStartFrom;

        //    protected new const int AttachmentTypeStartFrom = startFrom + 1000;

        //    public int MedicalNotes { get { return startFrom + 1; } }
        //    public int CustomFormEntry { get { return startFrom + 2; } }

        //    public override string GetDescription(int type)
        //    {
        //        if (type == MedicalNotes) return "Medical Notes";
        //        else if (type == CustomFormEntry) return "Custom Form Entry";
        //        else return base.GetDescription(type);
        //    }
        //}
        //#endregion

        //#region UploadFileType

        //protected override Gensolve.OrganisationData.Common.OrganisationEnums.UploadFileType GetUploadFileTypeEnum()
        //{
        //    return new UploadFileType();
        //}

        //public new UploadFileType UploadFileTypeEnum
        //{
        //    get { return (UploadFileType)GetUploadFileTypeEnum(); }
        //}

        //public new class UploadFileType : Gensolve.AppointmentData.Common.AppointmentEnums.UploadFileType
        //{
        //    private const int startFrom = Gensolve.AppointmentData.Common.AppointmentEnums.UploadFileType.UploadFileTypeStartFrom;

        //    protected new const int UploadFileTypeStartFrom = startFrom + 1000;

        //    public int ConditionNotes { get { return startFrom + 1; } }
        //    public int ABPPlan { get { return startFrom + 2; } }
        //    public int ABPMid { get { return startFrom + 3; } }
        //    public int ABPFinal { get { return startFrom + 4; } }
        //    public int ABPExtension { get { return startFrom + 5; } }
        //    public int MedicarePKI { get { return startFrom + 6; } }
        //    public int HeathlinkAttachment { get { return startFrom + 7; } }
        //    public int EntryRecord { get { return startFrom + 8; } }

        //    public override string GetDescription(int type)
        //    {
        //        if (type == ConditionNotes) return "Condition Medical Notes";
        //        else if (type == ABPPlan) return "Contract Plan Assessment";
        //        else if (type == ABPMid) return "Contract Mid Assessment ";
        //        else if (type == ABPFinal) return "Contract Final Assessment";
        //        else if (type == ABPExtension) return "Contract Extension Assessment";
        //        else if (type == MedicarePKI) return "Medicare PKI File";
        //        else if (type == HeathlinkAttachment) return "Healthlink Attachment";
        //        else if (type == EntryRecord) return "Entry Record Custom Form";
        //        else return base.GetDescription(type);
        //    }
        //}

        //#endregion

        #region PhysioAppointmentType
        public enum PhysioAppointmentType : int
        {
            Physio = 1,
            Pt = 2,
            PContract = 3,
            ABPContract = 4
        }
        #endregion

        #region EmailTemplateType

        //protected override Gensolve.OrganisationData.Common.OrganisationEnums.EmailTemplateType GetEmailTemplateTypeEnum()
        //{
        //    return new EmailTemplateType();
        //}

        //public new EmailTemplateType EmailTemplateTypeEnum
        //{
        //    get { return (EmailTemplateType)GetEmailTemplateTypeEnum(); }
        //}

        //public new class EmailTemplateType : Gensolve.AppointmentData.Common.AppointmentEnums.EmailTemplateType
        //{
        //    private const int startFrom = Gensolve.AppointmentData.Common.AppointmentEnums.EmailTemplateType.EmailTemplateTypeStartFrom;

        //    protected new const int EmailTemplateTypeStartFrom = startFrom + 1000;

        //    public int Contract { get { return startFrom + 1; } }
        //    public int Condition { get { return startFrom + 2; } }

        //    public  string GetDescription(int type)
        //    {
        //        if (type == Contract)
        //            return "Custom Contract";
        //        else if (type == Condition)
        //            return "Condition";
        //        else
        //            return base.GetDescription(type);
        //    }
        //}
        #endregion

        #region ChargePaidByType

        /*
		protected override Gensolve.AppointmentData.Common.AppointmentEnums.ChargePaidByType GetChargePaidByTypeEnum()
        {
            return new ChargePaidByType();
        }

        public new ChargePaidByType ChargePaidByTypeEnum
        {
            get { return (ChargePaidByType)GetChargePaidByTypeEnum(); }
        }

        public new class ChargePaidByType : Gensolve.AppointmentData.Common.AppointmentEnums.ChargePaidByType
        {
            private const int startFrom = Gensolve.AppointmentData.Common.AppointmentEnums.ChargePaidByType.ChargePaidByTypeStartFrom;

            protected new const int ChargePaidByTypeStartFrom = startFrom + 1000;

            public int Insurer { get { return startFrom + 1; } }

            public override string GetDescription(int type)
            {
                if (type == Insurer)
                    return "Insurer";
                else
                    return base.GetDescription(type);
            }
        }
		*/
        #endregion

        #region ACCFormType
        public enum ACCFormType
        {
            ACC32 = 1,
            ACC18 = 2
        }
        #endregion

        #region ACCBillingType

        public enum ACCBillingType : int
        {
            Initial = 1,
            FollowUp = 2,
            InitialExtended = 3,
            FollowUpExtended = 4,
            MileageOver40 = 5,
            TimeOver40 = 6,
            Standard = 7,
            ACC32R = 8,
            HandSplinting = 9,
            HandFollowup = 10,
            HandInitial = 11,
            EPNACC32R = 12,
            HandACC32R = 13,
            MileageOver20 = 14,
            TimeOver20 = 15,
            TimeOverOneHour = 16,
            ACC32A = 17,
            EPNACC32A = 18,
            HandACC32A = 19,
            PhysioStandard = 20,
            OsteoStandard = 21,
            PodiatryStandard = 22,
            AcupuntureStandard = 23,
            ChiropractorStandard = 24,
            OccupationTherapistStandard = 25,
            PhysioStdTelehealthInitial = 26,
            PhysioStdTelehealthFollowup = 27,
            OTTelehealthInitial = 28,
            OTTelehealthFollowup = 29,
            PhotocopyNotesElectronic = 100,
            PhotocopyNotesNotElectronic = 101,
            ComplexNotesSTPR = 102,
            ComplexNotesMEDR = 103,
            ChiropractorXray = 104,
            PodiatryPD10 = 105,
            PodiatryPD11 = 106,
            PodiatryPD12 = 107,
            PodiatryPD13 = 108,
            PT05 = 109,
            GPStandard = 110,
            HandSplintingACC32 = 189,
            ClinicalACC2152 = 190,
            PhysioOrHandACC2152 = 191,
            GPStandard13 = 200,
            GPStandard14 = 202,
            GPTelehealth = 203,
            GPTelehealth14 = 204,
            HandInitialHT01 = 300,
            HandFollouwpHT02 = 301,
            HandSplintingHT03 = 302,
            HandSplintingACC32HT04 = 303,
            HandReactivateHT05 = 304,
            HandPostSurgeryInitialHT11 = 305,
            HandPostSurgeryFollowupHT12 = 306,
            HandSplintingPostSurgeryHT13 = 307,
            PostSurgeryInitialPT11 = 308,
            PostSurgeryFollowupPT12 = 309,
            PhysioStandardPHY7 = 310,
            OffsiteInitial = 318,
            OffsiteFollowup = 319,
            ClassGroupRate = 320,
            CrutchHire = 321,
            CaseReview = 322,
            PhysioXCopayment = 323,
            PhysioSpecialistInitial = 324,
            PhysioSpecialistFollowup = 325,
            MoonBoot = 326,
            KneeBrace = 327,
            PhysEPNTelehealthFollowUp = 328,
            // these have been used but never had an enum
            // 329	Orthotics - non-contracted
            // 330	ACC Surcharge
            // 331	Short-term equipment locally sourced (GP/physio)
            PhysEPNTelehealthInit = 332,
            HandTelehealthInitial = 333,
            HandTelehealthFollowup = 334,
            PhySpecialistTelehealthInitial = 335,
            PhySpecialistTelehealthFollowup = 336,
            ChiroTelehealthInitial = 337,
            ChiroTelehealthFollowup = 338,
            PodiatryTelehealthInitial = 339,
            PodiatryTelehealthFollowup = 340,
            OsteoTelehealthInitial = 341,
            OsteoTelehealthFollowup = 342,
            PhysioRegTelehealth = 400,
            ChiroRegTelehealth = 401,
            OTRegTelehealth = 402,
            PodiatryRegTelehealth = 403,
            OsteoRegTelehealth = 404
        }

        public static string GetMiscBillingTypeDescription(decimal type)
        {
            if (type == (decimal)PhysioEnums.ACCBillingType.PhotocopyNotesElectronic) return "Photocopy Clinical Notes";
            else if (type == (decimal)PhysioEnums.ACCBillingType.ComplexNotesSTPR) return "STPR - Complex Clinical Notes";
            else if (type == (decimal)PhysioEnums.ACCBillingType.ComplexNotesMEDR) return "MEDR - Complex Clinical Notes";
            else if (type == (decimal)PhysioEnums.ACCBillingType.ChiropractorXray) return "Chiropractic Xray";
            else if (type == (decimal)PhysioEnums.ACCBillingType.HandSplinting) return "Hand Splinting (SS)";
            else if (type == (decimal)PhysioEnums.ACCBillingType.HandSplintingACC32) return "Hand Splinting - ACC32 (XS)";
            else if (type == (decimal)PhysioEnums.ACCBillingType.ClinicalACC2152) return "ACC2152 - Treatment Injury Claim";
            else if (type == (decimal)PhysioEnums.ACCBillingType.PhysioOrHandACC2152) return "2152 - Treatment Injury Claim";
            else if (type == (decimal)PhysioEnums.ACCBillingType.HandSplintingHT03) return "Hand Splinting (HT03)";
            else if (type == (decimal)PhysioEnums.ACCBillingType.HandSplintingACC32HT04) return "Hand Splinting - ACC32 (HT04)";
            else if (type == (decimal)PhysioEnums.ACCBillingType.HandSplintingPostSurgeryHT13) return "Hand Splinting-Post Surgery (HT13)";

            return "";
        }
        #endregion

        #region FormEntryType

        public enum FormEntryType : int
        {
            PhysioCondition = 1,
            Contract = 2
        }

        public static string FormEntryTypeDescription(FormEntryType type)
        {
            return FormEntryTypeDescription((int)type);
        }

        public static string FormEntryTypeDescription(Decimal type)
        {
            return FormEntryTypeDescription((int)type);
        }

        public static string FormEntryTypeDescription(int type)
        {
            switch (type)
            {
                case (int)FormEntryType.PhysioCondition:
                    return "Physio Condition";
                case (int)FormEntryType.Contract:
                    return "Contract";
                default:
                    return "Unknown";
            }
        }
        #endregion

        #region FormNodeType

        public enum FormNodeType : int
        {
            Form = 0,
            Question = 1,
            QuestionGroup = 2
        }

        public static string FormNodeTypeDescription(FormNodeType type)
        {
            return FormNodeTypeDescription((int)type);
        }

        public static string FormNodeTypeDescription(Decimal type)
        {
            return FormNodeTypeDescription((int)type);
        }

        public static string FormNodeTypeDescription(int type)
        {
            switch (type)
            {
                case (int)FormNodeType.Question:
                    return "Question";
                case (int)FormNodeType.QuestionGroup:
                    return "QuestionGroup";
                case (int)FormNodeType.Form:
                    return "Form";
                default:
                    return "Unknown";
            }
        }
        #endregion

        #region QuestionType

        public enum QuestionType : int
        {
            YesNo = 1,
            Number = 2,
            NumberRange = 3,
            Date = 4,
            FreeText = 5,
            ListValue = 6,
            ListValueMulti = 7
        }

        public static string QuestionTypeDescription(QuestionType type)
        {
            return QuestionTypeDescription((int)type);
        }

        public static string QuestionTypeDescription(Decimal type)
        {
            return QuestionTypeDescription((int)type);
        }

        public static string QuestionTypeDescription(int type)
        {
            switch (type)
            {
                case (int)QuestionType.YesNo:
                    return "Yes/No";
                case (int)QuestionType.Number:
                    return "Number";
                case (int)QuestionType.NumberRange:
                    return "Number within Range";
                case (int)QuestionType.Date:
                    return "Date";
                case (int)QuestionType.FreeText:
                    return "Free text";
                case (int)QuestionType.ListValue:
                    return "Select value from a List";
                case (int)QuestionType.ListValueMulti:
                    return "Select Multiple values from a List";
                default:
                    return "Unknown";
            }
        }
        #endregion

        #region ComparisonType

        public enum ComparisonType : int
        {
            Equal = 1,
            NotEqual = 2,
            LessThan = 3,
            LessThanOrEqual = 4,
            GreaterThan = 5,
            GreaterThanOrEqual = 6,
        }

        public static string ComparisonTypeDescription(ComparisonType type)
        {
            return ComparisonTypeDescription((int)type);
        }

        public static string ComparisonTypeDescription(Decimal type)
        {
            return ComparisonTypeDescription((int)type);
        }

        public static string ComparisonTypeDescription(int type)
        {
            switch (type)
            {
                case (int)ComparisonType.Equal:
                    return "Equal To";
                case (int)ComparisonType.NotEqual:
                    return "Not Equal To";
                case (int)ComparisonType.LessThan:
                    return "Less Than";
                case (int)ComparisonType.LessThanOrEqual:
                    return "Less Than Or Equal To";
                case (int)ComparisonType.GreaterThan:
                    return "Greater Than";
                case (int)ComparisonType.GreaterThanOrEqual:
                    return "Greater Than Or Equal To";
                default:
                    return "Unknown";
            }
        }
        #endregion

        #region AttributeType

        public enum AttributeType : int
        {
            Age = 1,
            Gender = 2,
            Ethnicity = 3
        }

        public static string AttributeTypeDescription(AttributeType type)
        {
            return AttributeTypeDescription((int)type);
        }

        public static string AttributeTypeDescription(Decimal type)
        {
            return AttributeTypeDescription((int)type);
        }

        public static string AttributeTypeDescription(int type)
        {
            switch (type)
            {
                case (int)AttributeType.Age:
                    return "Age";
                case (int)AttributeType.Ethnicity:
                    return "Ethnicity";
                case (int)AttributeType.Gender:
                    return "Gender";
                default:
                    return "Unknown";
            }
        }
        #endregion        

        #region UKInsurerType

        public enum UKGetPrividerCodeFrom : int
        {
            DEBTOR = 1,
            SITE = 2,
            CLINICIAN = 3
        }

        public static string UKGetProviderCodeFromDescription(UKGetPrividerCodeFrom ctype)
        {
            return UKGetProviderCodeFromDescription((int)ctype);
        }

        public static string UKGetProviderCodeFromDescription(int status)
        {
            switch (status)
            {
                case (int)UKGetPrividerCodeFrom.DEBTOR:
                    return "Debtor";

                case (int)UKGetPrividerCodeFrom.SITE:
                    return "Site";

                case (int)UKGetPrividerCodeFrom.CLINICIAN:
                    return "Clinician/Provider";

                default:
                    return "";
            }
        }
        #endregion

        #region PhysioApptAccStatus

        public enum PhysioApptAccStatus : int
        {
            NotInSchedule = 0,
            InSchedule = 1
        }
        #endregion

        #region AccStatus
        public enum AccStatus : int
        {
            None = 0,
            Incomplete = 1,
            InOutbox = 2,
            //Duplicate = 3,
            //Failed = 4,
            Invalid = 5,
            Receipted = 6,
            Parked = 7,
            HisoSubmitted = 8
        }

        public static string AccStatusDescription(AccStatus status)
        {
            return AccStatusDescription((int)status);
        }

        public static string AccStatusDescription(Decimal status)
        {
            return AccStatusDescription((int)status);
        }

        public static string AccStatusDescription(int status)
        {
            switch (status)
            {
                case (int)AccStatus.Incomplete:
                    return "Incomplete";
                case (int)AccStatus.InOutbox:
                    return "Outbox";
                case (int)AccStatus.Invalid:
                    return "Invalid";
                case (int)AccStatus.Receipted:
                    return "Receipted";
                case (int)AccStatus.Parked:
                    return "Parked";
                case (int)AccStatus.HisoSubmitted:
                    return "Submitted";
                default:
                    return "Unknown";
            }
        }
        #endregion

        #region InsurerScheduleStatus
        public enum InsurerScheduleStatus : int
        {
            //None = 1, 
            InOutbox = 2,
            NotYetPaid = 3,
            TransferFailed = 4,
            Processed = 5
        }

        public static string InsurerScheduleStatusDescription(InsurerScheduleStatus status)
        {
            return InsurerScheduleStatusDescription((int)status);
        }

        public static string InsurerScheduleStatusDescription(Decimal status)
        {
            return InsurerScheduleStatusDescription((int)status);
        }

        public static string InsurerScheduleStatusDescription(int status)
        {
            switch (status)
            {
                case (int)InsurerScheduleStatus.InOutbox:
                    return "Outbox";
                case (int)InsurerScheduleStatus.NotYetPaid:
                    return "Not Yet Paid";
                case (int)InsurerScheduleStatus.Processed:
                    return "Processed";
                case (int)InsurerScheduleStatus.TransferFailed:
                    return "Transfer Failed";
                default:
                    return "Unknown";
            }
        }
        #endregion

        #region PhysioApptCountedToType

        public enum PhysioApptCountedToType : int
        {
            Condition = 0,
            Acc32 = 1,
            PostSurgery = 2
        }
        #endregion

        #region Acc32RequestAccStatus

        public enum Acc32RequestAccStatus : int
        {
            NotInSchedule = 0,
            InSchedule = 1
        }
        #endregion

        #region MiscBillingAccStatus

        public enum MiscBillingAccStatus : int
        {
            NotInSchedule = 0,
            InSchedule = 1
        }
        #endregion

        #region Acc32RequestType

        public enum Acc32RequestType : int
        {
            ACC32R = 1,
            ACC32A = 2
        }

        public static string Acc32RequestTypeDescription(Acc32RequestType type)
        {
            return Acc32RequestTypeDescription((int)type);
        }

        public static string Acc32RequestTypeDescription(Decimal type)
        {
            return Acc32RequestTypeDescription((int)type);
        }

        public static string Acc32RequestTypeDescription(int type)
        {
            switch (type)
            {
                case (int)Acc32RequestType.ACC32R:
                    return "ACC32R";
                case (int)Acc32RequestType.ACC32A:
                    return "ACC32A";
                default:
                    return "Unknown";
            }
        }
        #endregion

        #region Acc32RequestGoalType

        public enum Acc32RequestGoalType : int
        {
            Function = 1,
            SignSymptom = 2
        }

        public static string Acc32RequestGoalTypeDescription(Acc32RequestGoalType type)
        {
            return Acc32RequestGoalTypeDescription((int)type);
        }

        public static string Acc32RequestGoalTypeDescription(Decimal type)
        {
            return Acc32RequestGoalTypeDescription((int)type);
        }

        public static string Acc32RequestGoalTypeDescription(int type)
        {
            switch (type)
            {
                case (int)Acc32RequestGoalType.Function:
                    return "Functions";
                case (int)Acc32RequestGoalType.SignSymptom:
                    return "Signs and Symptoms";
                default:
                    return "Unknown";
            }
        }
        #endregion

        #region Acc32DiagnosisChangeType

        public enum Acc32DiagnosisChangeType : int
        {
            AdditionalDiagnosis = 1,
            AdditionalDiagnosisPlusTreatment = 2,
            TreatmentOnly = 3,
            ChangeDiagnosisOnly = 4,
            SplintingOnly = 5
        }

        public static string Acc32DiagnosisChangeTypeDescription(Acc32DiagnosisChangeType type)
        {
            return Acc32DiagnosisChangeTypeDescription((int)type);
        }

        public static string Acc32DiagnosisChangeTypeDescription(Decimal type)
        {
            return Acc32DiagnosisChangeTypeDescription((int)type);
        }

        public static string Acc32DiagnosisChangeTypeDescription(int type)
        {
            switch (type)
            {
                case (int)Acc32DiagnosisChangeType.AdditionalDiagnosis:
                    return "Additional Diagnosis";
                case (int)Acc32DiagnosisChangeType.ChangeDiagnosisOnly:
                    return "Change Diagnosis Only";
                case (int)Acc32DiagnosisChangeType.AdditionalDiagnosisPlusTreatment:
                    return "Additional Diagnosis Plus Treatment";
                case (int)Acc32DiagnosisChangeType.TreatmentOnly:
                    return "Treatment Only";
                default:
                    return "Unknown";
            }
        }
        #endregion

        #region InsurerApptInvoiceStatus
        public enum InsurerApptInvoiceStatus : int
        {
            None = 1,
            AwaitingApproval = 2,
            Processed = 3
        }

        public static string InsurerApptInvoiceStatusDescription(InsurerApptInvoiceStatus status)
        {
            return InsurerApptInvoiceStatusDescription((int)status);
        }

        public static string InsurerApptInvoiceStatusDescription(Decimal status)
        {
            return InsurerApptInvoiceStatusDescription((int)status);
        }

        public static string InsurerApptInvoiceStatusDescription(int status)
        {
            switch (status)
            {
                case (int)InsurerApptInvoiceStatus.None:
                    return "None";
                case (int)InsurerApptInvoiceStatus.AwaitingApproval:
                    return "Awaiting Insurer Approval";
                case (int)InsurerApptInvoiceStatus.Processed:
                    return "Processed";
                default:
                    return "Unknown";
            }
        }
        #endregion

        #region ScheduleLineStatus
        public enum ScheduleLineStatus : int
        {
            NotYetPaid = 1,
            Paid = 2,
            Rejected = 3,
            Held = 4,
            TransferFailed = 5
        }

        public static string ScheduleLineStatusDescription(InsurerScheduleStatus status)
        {
            return ScheduleLineStatusDescription((int)status);
        }

        public static string ScheduleLineStatusDescription(Decimal status)
        {
            return ScheduleLineStatusDescription((int)status);
        }

        public static string ScheduleLineStatusDescription(int status)
        {
            switch (status)
            {
                case (int)ScheduleLineStatus.NotYetPaid:
                    return "Not Yet Paid";
                case (int)ScheduleLineStatus.Paid:
                    return "Paid";
                case (int)ScheduleLineStatus.Rejected:
                    return "Rejected";
                case (int)ScheduleLineStatus.Held:
                    return "Held";
                case (int)ScheduleLineStatus.TransferFailed:
                    return "Transfer Failed";
                default:
                    return "Unknown";
            }
        }
        #endregion

        #region IncapacityPeriodType
        public enum IncapacityPeriodType : int
        {
            Days = 1,
            Weeks = 2,
            Months = 3
        }

        public static string IncapacityPeriodTypeDescription(IncapacityPeriodType type)
        {
            return IncapacityPeriodTypeDescription((int)type);
        }

        public static string IncapacityPeriodTypeDescription(Decimal type)
        {
            return IncapacityPeriodTypeDescription((int)type);
        }

        public static string IncapacityPeriodTypeDescription(int type)
        {
            switch (type)
            {
                case (int)IncapacityPeriodType.Days:
                    return "Days";
                case (int)IncapacityPeriodType.Weeks:
                    return "Weeks";
                case (int)IncapacityPeriodType.Months:
                    return "Months";
                default:
                    return "Unknown";
            }
        }
        #endregion

        #region ContractType
        public enum ContractType : int
        {
            ABPContract = 1,
            Custom = 2
        }
        #endregion

        #region ContractChargeType
        public enum ContractChargeType : int
        {
            Fixed = 1,
            Variable = 2
        }

        public static string ContractChargeTypeDescription(ContractChargeType type)
        {
            return ContractChargeTypeDescription((int)type);
        }

        public static string ContractChargeTypeDescription(Decimal type)
        {
            return ContractChargeTypeDescription((int)type);
        }

        public static string ContractChargeTypeDescription(int type)
        {
            switch (type)
            {
                case (int)ContractChargeType.Fixed:
                    return "Fixed";
                case (int)ContractChargeType.Variable:
                    return "Variable";
                default:
                    return "Unknown";
            }
        }
        #endregion

        #region ABPProgramTimingType
        public enum ABPProgramTimingType : int
        {
            Days = 1,
            Weeks = 2,
            Months = 3
        }

        public static string ABPProgramTimingTypeDescription(ABPProgramTimingType status)
        {
            return ABPProgramTimingTypeDescription((int)status);
        }

        public static string ABPProgramTimingTypeDescription(Decimal status)
        {
            return ABPProgramTimingTypeDescription((int)status);
        }

        public static string ABPProgramTimingTypeDescription(int status)
        {
            switch (status)
            {
                case (int)ABPProgramTimingType.Days:
                    return "Days";
                case (int)ABPProgramTimingType.Weeks:
                    return "Weeks";
                case (int)ABPProgramTimingType.Months:
                    return "Months";
                default:
                    return "Unknown";
            }
        }
        #endregion

        #region ABPProgramApptType
        public enum ABPProgramApptType : int
        {
            PlanOfAction = 1,
            MidTerm = 2,
            FinalAssessment = 3,
            AbpRehab = 4,
            Extension = 5
        }

        public static string ABPProgramApptTypeDescription(ABPProgramApptType status)
        {
            return ABPProgramApptTypeDescription((int)status);
        }

        public static string ABPProgramApptTypeDescription(Decimal status)
        {
            return ABPProgramApptTypeDescription((int)status);
        }

        public static string ABPProgramApptTypeDescription(int status)
        {
            switch (status)
            {
                case (int)ABPProgramApptType.PlanOfAction:
                    return "Plan Of Action";
                case (int)ABPProgramApptType.MidTerm:
                    return "Mid Term";
                case (int)ABPProgramApptType.FinalAssessment:
                    return "Final Assessment";
                case (int)ABPProgramApptType.AbpRehab:
                    return "ABP Rehab";
                case (int)ABPProgramApptType.Extension:
                    return "Extension";
                default:
                    return "Unknown";
            }
        }
        #endregion

        #region AbpContractStatus
        public enum AbpContractStatus : int
        {
            Referred = 0,
            Approved = 1,
            Declined = 2,
            Unsuitable = 3,
            NonCompletion = 4,
            Completed = 5
        }

        public static string AbpContractStatusDescription(AbpContractStatus status)
        {
            return AbpContractStatusDescription((int)status);
        }

        public static string AbpContractStatusDescription(Decimal status)
        {
            return AbpContractStatusDescription((int)status);
        }

        public static string AbpContractStatusDescription(int status)
        {
            switch (status)
            {
                case (int)AbpContractStatus.Referred:
                    return "Referred";
                case (int)AbpContractStatus.Approved:
                    return "Approved";
                case (int)AbpContractStatus.Declined:
                    return "Declined";
                case (int)AbpContractStatus.Unsuitable:
                    return "Unsuitable";
                case (int)AbpContractStatus.NonCompletion:
                    return "Non Completion";
                case (int)AbpContractStatus.Completed:
                    return "Completed";
                default:
                    return "Unknown";
            }
        }
        #endregion

        #region AbpReportType
        public enum AbpReportType : int
        {
            Plan = 1,
            Mid = 2,
            Final = 3,
            NonCompletion = 4,
            Extension = 5
        }
        #endregion

        #region InjuryType
        public enum InjuryType : int
        {
            KneeInjury = 1,
            ShoulderInjury = 2,
            //No longer User.. Just Spine
            //BackInjury = 3,
            AnkleInjury = 4,
            SpineInjury = 5,
            ElbowInjury = 6,
            WristInjury = 7,
            HipInjury = 8,
            GenericInjury = 9,
            KneeInjuryLeft = 10,
            KneeInjuryRight = 11,
            ShoulderInjuryLeft = 12,
            ShoulderInjuryRight = 13,
            AnkleInjuryLeft = 14,
            AnkleInjuryRight = 15,
            ElbowInjuryLeft = 16,
            ElbowInjuryRight = 17,
            WristInjuryLeft = 18,
            WristInjuryRight = 19,
            HipInjuryLeft = 20,
            HipInjuryRight = 21,
            //  podiatry assessments
            PodDiabetes = 22,
            PodBiomechanical = 23,
            PodPalliative = 24,
            PodNeurological = 25,
            PodPaedatric = 26,
            //  hand assessments,
            Hand = 27,
            //HandLeft = 27,??
            //HandRight = 28,??
            Thumb = 29,
            //ThumbLeft = 29, ??
            //ThumbRight = 30 ??
            MCSInjury = 31,
            MckenziePeripheral = 32,
            MckenzieCervical = 33,
            MckenzieLumbar = 34,
            MckenzieThoracic = 35,
            Custom = 99
        }

        #region InjuryTypeDescription
        public static string InjuryTypeDescription(InjuryType type)
        {
            return InjuryTypeDescription((int)type);
        }

        public static string InjuryTypeDescription(Decimal type)
        {
            return InjuryTypeDescription((int)type);
        }

        public static string InjuryTypeDescription(int type)
        {
            switch (type)
            {
                case (int)InjuryType.KneeInjury: return "Knee Injury";
                case (int)InjuryType.KneeInjuryLeft: return "Knee Injury (Left)";
                case (int)InjuryType.KneeInjuryRight: return "Knee Injury (Right)";
                case (int)InjuryType.ShoulderInjury: return "Shoulder Injury";
                case (int)InjuryType.ShoulderInjuryLeft: return "Shoulder Injury (Left)";
                case (int)InjuryType.ShoulderInjuryRight: return "Shoulder Injury (Right)";
                case (int)InjuryType.AnkleInjury: return "Ankle Injury";
                case (int)InjuryType.AnkleInjuryLeft: return "Ankle Injury (Left)";
                case (int)InjuryType.AnkleInjuryRight: return "Ankle Injury (Right)";
                case (int)InjuryType.SpineInjury: return "Spine Injury";
                case (int)InjuryType.ElbowInjury: return "Elbow Injury";
                case (int)InjuryType.ElbowInjuryLeft: return "Elbow Injury (Left)";
                case (int)InjuryType.ElbowInjuryRight: return "Elbow Injury (Right)";
                case (int)InjuryType.WristInjury: return "Wrist Injury";
                case (int)InjuryType.WristInjuryLeft: return "Wrist Injury (Left)";
                case (int)InjuryType.WristInjuryRight: return "Wrist Injury (Right)";
                case (int)InjuryType.HipInjury: return "Hip Injury";
                case (int)InjuryType.HipInjuryLeft: return "Hip Injury (Left))";
                case (int)InjuryType.HipInjuryRight: return "Hip Injury (Right";
                case (int)InjuryType.GenericInjury: return "Generic Injury";
                case (int)InjuryType.PodDiabetes: return "Diabetes Assessment";
                case (int)InjuryType.PodBiomechanical: return "Biomechanical Assessment";
                case (int)InjuryType.PodPalliative: return "Palliative Assessment";
                case (int)InjuryType.PodNeurological: return "Paediatric Assessment";
                case (int)InjuryType.PodPaedatric: return "Neurological Assessment";
                case (int)InjuryType.Hand: return "Hand Injury ";
                //case (int)InjuryType.HandLeft:				return "Hand Injury (Left)";
                //case (int)InjuryType.HandRight:				return "Hand Injury (Right)";
                case (int)InjuryType.Thumb: return "Thumb Injury ";
                //case (int)InjuryType.ThumbLeft:				return "Thumb Injury (Left)";
                //case (int)InjuryType.ThumbRight:			return "Thumb Injury (Right)";
                case (int)InjuryType.MCSInjury: return "MCS Injury ";
                case (int)InjuryType.MckenziePeripheral: return "Extremity";
                case (int)InjuryType.MckenzieCervical: return "Cervical Spine";
                case (int)InjuryType.MckenzieLumbar: return "Lumbar Spine";
                case (int)InjuryType.MckenzieThoracic: return "Thoracic Spine";
                case (int)InjuryType.Custom: return "Custom";
                default: return "Unknown";
            }
        }
        #endregion

        //#region InjuryTypeShortDescription
        //public static string InjuryTypeShortDescription(InjuryType type)
        //{
        //    return InjuryTypeShortDescription((int)type);
        //}

        //public static string InjuryTypeShortDescription(PhysioDataSet dsPhysio, Decimal type, decimal customType)
        //{
        //    return InjuryTypeShortDescription(dsPhysio, (int)type, customType);
        //}

        //public static string InjuryTypeShortDescription(Decimal type)
        //{
        //    return InjuryTypeShortDescription(new PhysioDataSet(), (int)type, 0);
        //}

        //public static string InjuryTypeShortDescription(PhysioDataSet dsPhysio, int type, decimal customType)
        //{
        //    switch (type)
        //    {
        //        case (int)InjuryType.KneeInjury: return "Knee";
        //        case (int)InjuryType.KneeInjuryLeft: return "Knee (Left)";
        //        case (int)InjuryType.KneeInjuryRight: return "Knee (Right)";
        //        case (int)InjuryType.ShoulderInjury: return "Shoulder";
        //        case (int)InjuryType.ShoulderInjuryLeft: return "Shoulder (Left)";
        //        case (int)InjuryType.ShoulderInjuryRight: return "Shoulder (Right)";
        //        case (int)InjuryType.AnkleInjury: return "Ankle";
        //        case (int)InjuryType.AnkleInjuryLeft: return "Ankle (Left)";
        //        case (int)InjuryType.AnkleInjuryRight: return "Ankle (Right)";
        //        case (int)InjuryType.SpineInjury: return "Spine";
        //        case (int)InjuryType.ElbowInjury: return "Elbow";
        //        case (int)InjuryType.ElbowInjuryLeft: return "Elbow (Left)";
        //        case (int)InjuryType.ElbowInjuryRight: return "Elbow (Right)";
        //        case (int)InjuryType.WristInjury: return "Wrist";
        //        case (int)InjuryType.WristInjuryLeft: return "Wrist (Left)";
        //        case (int)InjuryType.WristInjuryRight: return "Wrist (Right)";
        //        case (int)InjuryType.HipInjury: return "Hip";
        //        case (int)InjuryType.HipInjuryLeft: return "Hip (Left)";
        //        case (int)InjuryType.HipInjuryRight: return "Hip (Right)";
        //        case (int)InjuryType.GenericInjury: return "Generic";
        //        case (int)InjuryType.PodDiabetes: return "Diabetes";
        //        case (int)InjuryType.PodBiomechanical: return "Biomechanical";
        //        case (int)InjuryType.PodPalliative: return "Palliative";
        //        case (int)InjuryType.PodNeurological: return "Paediatric";
        //        case (int)InjuryType.PodPaedatric: return "Neurological";
        //        case (int)InjuryType.Hand: return "Hand";
        //        //case (int)InjuryType.HandLeft:				return "Hand (Left)";
        //        //case (int)InjuryType.HandRight:				return "Hand (Right)";
        //        case (int)InjuryType.Thumb: return "Thumb";
        //        //case (int)InjuryType.ThumbLeft:				return "Thumb (Left)";
        //        //case (int)InjuryType.ThumbRight:			return "Thumb (Right)";
        //        case (int)InjuryType.MCSInjury: return "MCS";
        //        case (int)InjuryType.MckenziePeripheral: return "Extremity";
        //        case (int)InjuryType.MckenzieCervical: return "Cervical Spine";
        //        case (int)InjuryType.MckenzieLumbar: return "Lumbar Spine";
        //        case (int)InjuryType.MckenzieThoracic: return "Thoracic Spine";
        //        case (int)InjuryType.Custom: return "Custom";
        //        // case (int)InjuryType.Custom: return  CustomInjuryTypeShortDescription(customType);
        //        default: return "Unknown";
        //    }
        //}
        //#endregion

        #region InjuryTypeSideDescription
        public static string InjuryTypeSideDescription(InjuryType type)
        {
            return InjuryTypeSideDescription((int)type);
        }

        public static string InjuryTypeSideDescription(Decimal type)
        {
            return InjuryTypeSideDescription((int)type);
        }

        public static string InjuryTypeSideDescription(int type)
        {
            switch (type)
            {
                case (int)InjuryType.AnkleInjuryLeft:
                case (int)InjuryType.ElbowInjuryLeft:
                case (int)InjuryType.HipInjuryLeft:
                case (int)InjuryType.KneeInjuryLeft:
                case (int)InjuryType.ShoulderInjuryLeft:
                case (int)InjuryType.WristInjuryLeft:
                    //case (int)InjuryType.HandLeft:
                    //case (int)InjuryType.ThumbLeft: 
                    return "Left";
                case (int)InjuryType.AnkleInjuryRight:
                case (int)InjuryType.ElbowInjuryRight:
                case (int)InjuryType.HipInjuryRight:
                case (int)InjuryType.KneeInjuryRight:
                case (int)InjuryType.ShoulderInjuryRight:
                case (int)InjuryType.WristInjuryRight:
                    //case (int)InjuryType.HandRight:
                    //case (int)InjuryType.ThumbRight:
                    return "Right";
                default: return "";
            }
        }
        #endregion

        #region GetBaseInjuryType
        public static decimal GetBaseInjuryType(InjuryType type)
        {
            return GetBaseInjuryType((int)type);
        }

        public static decimal GetBaseInjuryType(Decimal type)
        {
            return GetBaseInjuryType((int)type);
        }

        public static decimal GetBaseInjuryType(int type)
        {
            switch (type)
            {
                case (int)InjuryType.AnkleInjuryLeft:
                case (int)InjuryType.AnkleInjuryRight: return (decimal)InjuryType.AnkleInjury;

                case (int)InjuryType.ElbowInjuryRight:
                case (int)InjuryType.ElbowInjuryLeft: return (decimal)InjuryType.ElbowInjury;

                case (int)InjuryType.HipInjuryLeft:
                case (int)InjuryType.HipInjuryRight: return (decimal)InjuryType.HipInjury;

                case (int)InjuryType.KneeInjuryLeft:
                case (int)InjuryType.KneeInjuryRight: return (decimal)InjuryType.KneeInjury;

                case (int)InjuryType.ShoulderInjuryLeft:
                case (int)InjuryType.ShoulderInjuryRight: return (decimal)InjuryType.ShoulderInjury;

                case (int)InjuryType.WristInjuryLeft:
                case (int)InjuryType.WristInjuryRight: return (decimal)InjuryType.WristInjury;

                default: return (decimal)type;
            }
        }
        #endregion

        #region GetAnkleInjuryTypes
        public static ArrayList GetAnkleInjuryTypes()
        {
            ArrayList types = new ArrayList();

            types.Add((decimal)InjuryType.AnkleInjury);
            types.Add((decimal)InjuryType.AnkleInjuryLeft);
            types.Add((decimal)InjuryType.AnkleInjuryRight);

            return types;
        }
        #endregion

        #region GetSpineInjuryTypes
        public static ArrayList GetSpineInjuryTypes()
        {
            ArrayList types = new ArrayList();

            types.Add((decimal)InjuryType.SpineInjury);

            return types;
        }
        #endregion

        #region GetElbowInjuryTypes
        public static ArrayList GetElbowInjuryTypes()
        {
            ArrayList types = new ArrayList();

            types.Add((decimal)InjuryType.ElbowInjury);
            types.Add((decimal)InjuryType.ElbowInjuryLeft);
            types.Add((decimal)InjuryType.ElbowInjuryRight);

            return types;
        }
        #endregion

        #region GetGenericInjuryTypes
        public static ArrayList GetGenericInjuryTypes()
        {
            ArrayList types = new ArrayList();

            types.Add((decimal)InjuryType.GenericInjury);

            return types;
        }
        #endregion

        #region GetHipInjuryTypes
        public static ArrayList GetHipInjuryTypes()
        {
            ArrayList types = new ArrayList();

            types.Add((decimal)InjuryType.HipInjury);
            types.Add((decimal)InjuryType.HipInjuryLeft);
            types.Add((decimal)InjuryType.HipInjuryRight);

            return types;
        }
        #endregion

        #region GetKneeInjuryTypes
        public static ArrayList GetKneeInjuryTypes()
        {
            ArrayList types = new ArrayList();

            types.Add((decimal)InjuryType.KneeInjury);
            types.Add((decimal)InjuryType.KneeInjuryLeft);
            types.Add((decimal)InjuryType.KneeInjuryRight);

            return types;
        }
        #endregion

        #region GetShoulderInjuryTypes
        public static ArrayList GetShoulderInjuryTypes()
        {
            ArrayList types = new ArrayList();

            types.Add((decimal)InjuryType.ShoulderInjury);
            types.Add((decimal)InjuryType.ShoulderInjuryLeft);
            types.Add((decimal)InjuryType.ShoulderInjuryRight);

            return types;
        }
        #endregion

        #region GetWristInjuryTypes
        public static ArrayList GetWristInjuryTypes()
        {
            ArrayList types = new ArrayList();

            types.Add((decimal)InjuryType.WristInjury);
            types.Add((decimal)InjuryType.WristInjuryLeft);
            types.Add((decimal)InjuryType.WristInjuryRight);

            return types;
        }
        #endregion

        #region GetPodDiabetesInjuryTypes
        public static ArrayList GetPodDiabetesInjuryTypes()
        {
            ArrayList types = new ArrayList();

            types.Add((decimal)InjuryType.PodDiabetes);

            return types;
        }
        #endregion

        #region GetPodBiomechInjuryTypes
        public static ArrayList GetPodBiomechInjuryTypes()
        {
            ArrayList types = new ArrayList();

            types.Add((decimal)InjuryType.PodBiomechanical);

            return types;
        }
        #endregion

        #region GetThumbInjuryTypes
        public static ArrayList GetThumbInjuryTypes()
        {
            ArrayList types = new ArrayList();

            types.Add((decimal)InjuryType.Thumb);

            return types;
        }
        #endregion

        #region GetHandInjuryTypes
        public static ArrayList GetHandInjuryTypes()
        {
            ArrayList types = new ArrayList();

            types.Add((decimal)InjuryType.Hand);

            return types;
        }
        #endregion

        #region GetMCSInjuryTypes
        public static ArrayList GetMCSInjuryTypes()
        {
            ArrayList types = new ArrayList();

            types.Add((decimal)InjuryType.MCSInjury);

            return types;
        }
        #endregion

        #region GetMckenzieInjuryTypes
        public static ArrayList GetMckenzieInjuryTypes()
        {
            ArrayList types = new ArrayList();

            types.Add((decimal)InjuryType.MckenzieCervical);
            types.Add((decimal)InjuryType.MckenzieLumbar);
            types.Add((decimal)InjuryType.MckenziePeripheral);
            types.Add((decimal)InjuryType.MckenzieThoracic);

            return types;
        }
        #endregion

        #endregion

        #region InjurySide
        public enum InjurySide : int
        {
            Left = 0,
            Right = 1
        }

        public static string InjurySideDescription(InjurySide type)
        {
            return InjurySideDescription((int)type);
        }

        public static string InjurySideDescription(Decimal type)
        {
            return InjurySideDescription((int)type);
        }

        public static string InjurySideDescription(int type)
        {
            switch (type)
            {
                case (int)InjurySide.Left: return "Left";
                case (int)InjurySide.Right: return "Right";
                default: return "Unknown";
            }
        }
        #endregion

        #region BodyDiagram
        public enum BodyDiagramType : int
        {
            FullBody = 1,
            Hand = 2,
            Foot = 3,
            Spine = 4,
            Cranial = 5,
            Thumb = 6
        }
        #endregion

        #region Body Diagram Description
        public static string BodyDiagramDescription(BodyDiagramType type)
        {
            return BodyDiagramDescription((int)type);
        }

        public static string BodyDiagramDescription(Decimal type)
        {
            return BodyDiagramDescription((int)type);
        }

        public static string BodyDiagramDescription(int type)
        {
            switch (type)
            {
                case (int)BodyDiagramType.FullBody: return "Full Body";
                case (int)BodyDiagramType.Hand: return "Hand";
                case (int)BodyDiagramType.Foot: return "Foot";
                case (int)BodyDiagramType.Spine: return "Spine";
                case (int)BodyDiagramType.Cranial: return "Cranial";
                default: return "Unknown";
            }
        }
        #endregion

        #region GetBodyDiagramTypes
        public static ArrayList GetBodyDiagramTypes()
        {
            ArrayList types = new ArrayList();

            types.Add((decimal)BodyDiagramType.FullBody);
            types.Add((decimal)BodyDiagramType.Hand);
            types.Add((decimal)BodyDiagramType.Foot);
            types.Add((decimal)BodyDiagramType.Spine);
            types.Add((decimal)BodyDiagramType.Cranial);

            return types;
        }
        #endregion

        #region AssessmentType
        public enum AssessmentType : int
        {
            Standard = 1,
            Podiatry = 2,
            Simple = 3,
            Doctor = 4,
            Mckenzie = 5,
            Sotap = 6,
            Rehab = 7,
            Soaper = 8,
            SimpleConsult = 9,
            HandWritten = 10,
            Custom = 99
            //HandTherapy = 4
        }

        public static string AssessmentTypeDescription(AssessmentOwnerType type)
        {
            return AssessmentTypeDescription((int)type);
        }

        public static string AssessmentTypeDescription(Decimal type)
        {
            return AssessmentTypeDescription((int)type);
        }

        public static string AssessmentTypeDescription(int type)
        {
            switch (type)
            {
                case (int)AssessmentType.Standard:
                    return "Standard";
                case (int)AssessmentType.Podiatry:
                    return "Podiatry";
                case (int)AssessmentType.Simple:
                    return "Simple";
                case (int)AssessmentType.Doctor:
                    return "Doctor";
                case (int)AssessmentType.Mckenzie:
                    return "McKenzie";
                case (int)AssessmentType.Sotap:
                    return "SOTAP";
                case (int)AssessmentType.Soaper:
                    return "SOAPER";
                case (int)AssessmentType.SimpleConsult:
                    return "Simple Consultation";
                case (int)AssessmentType.Rehab:
                    return "Rehab";
                case (int)AssessmentType.HandWritten:
                    return "Image Notes";
                //case (int)AssessmentType.HandTherapy:
                //     return "Hand Therapy";
                default:
                    return "Unknown";
            }
        }
        #endregion

        #region TextShortcutRestrictionType

        //protected override Gensolve.OrganisationData.Common.OrganisationEnums.TextShortcutRestrictionType GetTextShortcutRestrictionTypeEnum()
        //{
        //    return new TextShortcutRestrictionType();
        //}

        //public new TextShortcutRestrictionType TextShortcutRestrictionTypeEnum
        //{
        //    get { return (TextShortcutRestrictionType)GetTextShortcutRestrictionTypeEnum(); }
        //}

        //public new class TextShortcutRestrictionType : Gensolve.AppointmentData.Common.AppointmentEnums.TextShortcutRestrictionType
        //{
        //    int gpmStartFrom = Gensolve.AppointmentData.Common.AppointmentEnums.TextShortcutRestrictionType.TextShortcutRestrictionTypeStartFrom;

        //    public int AllInjuries { get { return gpmStartFrom + 1; } }
        //    public int AnkleInjury { get { return gpmStartFrom + 2; } }
        //    //public int BackInjury		{ get { return physioStartFrom + 3; } }
        //    public int SpineInjury { get { return gpmStartFrom + 4; } }
        //    public int ElbowInjury { get { return gpmStartFrom + 5; } }
        //    public int GenericInjury { get { return gpmStartFrom + 6; } }
        //    public int HipInjury { get { return gpmStartFrom + 7; } }
        //    public int KneeInjury { get { return gpmStartFrom + 8; } }
        //    public int ShoulderInjury { get { return gpmStartFrom + 9; } }
        //    public int WristInjury { get { return gpmStartFrom + 10; } }

        //    public int PersonalTrainerNotes { get { return gpmStartFrom + 11; } }
        //    public int TreatmentNotes { get { return gpmStartFrom + 12; } }
        //    public int PlanNotes { get { return gpmStartFrom + 13; } }
        //    public int AggravatingFactors { get { return gpmStartFrom + 14; } }
        //    public int ExerciseNotes { get { return gpmStartFrom + 15; } }
        //    public int SportDetails { get { return gpmStartFrom + 16; } }
        //    public int WeeklyActivity { get { return gpmStartFrom + 17; } }
        //    public int Medication { get { return gpmStartFrom + 18; } }
        //    public int Surgery { get { return gpmStartFrom + 19; } }
        //    public int SocialFactors { get { return gpmStartFrom + 20; } }
        //    public int Diagnosis { get { return gpmStartFrom + 21; } }
        //    public int DiffDiagnosis { get { return gpmStartFrom + 22; } }
        //    public int CurrentSymptoms { get { return gpmStartFrom + 23; } }
        //    public int DischargeTmtSummary { get { return gpmStartFrom + 24; } }
        //    public int DischargePlan { get { return gpmStartFrom + 25; } }
        //    public int Acc32GoalsSigns { get { return gpmStartFrom + 26; } }
        //    public int Acc32GoalsSymptoms { get { return gpmStartFrom + 27; } }
        //    public int ObjObservation { get { return gpmStartFrom + 28; } }
        //    public int ObjFindings { get { return gpmStartFrom + 29; } }
        //    public int ObjPalpation { get { return gpmStartFrom + 30; } }
        //    public int ObjOtherComments { get { return gpmStartFrom + 31; } }

        //    public int Region { get { return gpmStartFrom + 32; } }
        //    public int ContraIndications { get { return gpmStartFrom + 33; } }
        //    public int OtherDetails { get { return gpmStartFrom + 34; } }
        //    public int ClinicalFindings { get { return gpmStartFrom + 35; } }

        //    public int ACC32InitialDiag { get { return gpmStartFrom + 36; } }
        //    public int ACC32CurrentDiag { get { return gpmStartFrom + 37; } }
        //    public int ACC32CurrentCondition { get { return gpmStartFrom + 38; } }
        //    public int ACC32NotResolved { get { return gpmStartFrom + 39; } }
        //    public int ACC32GoalsSandS { get { return gpmStartFrom + 40; } }
        //    public int ACC32GoalsFunction { get { return gpmStartFrom + 41; } }
        //    public int ACC32LimitationSandS { get { return gpmStartFrom + 42; } }
        //    public int ACC32LimitationFunction { get { return gpmStartFrom + 43; } }
        //    public int ACC32Recommendation { get { return gpmStartFrom + 44; } }

        //    public int ExamGoal { get { return gpmStartFrom + 45; } }
        //    public int ExamGoalTmt { get { return gpmStartFrom + 46; } }

        //    public int DischargeOutcomeDetails { get { return gpmStartFrom + 47; } }

        //    public int HandInjury { get { return gpmStartFrom + 48; } }
        //    public int ThumbInjury { get { return gpmStartFrom + 49; } }
        //    public int PodBiomechanical { get { return gpmStartFrom + 50; } }
        //    public int PodDiabetes { get { return gpmStartFrom + 51; } }

        //    public int AbpFrpLimitations { get { return gpmStartFrom + 52; } }
        //    public int AbpFrpStatusReason { get { return gpmStartFrom + 53; } }
        //    public int AbpFrpPlanOfActionChanges { get { return gpmStartFrom + 54; } }

        //    public int FrpSubjective { get { return gpmStartFrom + 55; } }
        //    public int FrpDisability { get { return gpmStartFrom + 56; } }
        //    public int FrpUnderstanding { get { return gpmStartFrom + 57; } }
        //    public int FrpReferralGoals { get { return gpmStartFrom + 58; } }
        //    public int FrpClientGoals { get { return gpmStartFrom + 59; } }
        //    public int FrpGoals { get { return gpmStartFrom + 60; } }
        //    public int FrpProgress { get { return gpmStartFrom + 61; } }
        //    public int FrpComments { get { return gpmStartFrom + 62; } }
        //    public int FrpDailyActivity { get { return gpmStartFrom + 63; } }
        //    public int FrpWorkAbility { get { return gpmStartFrom + 64; } }
        //    public int FrpStrategies { get { return gpmStartFrom + 65; } }
        //    public int FrpRehabilitation { get { return gpmStartFrom + 66; } }
        //    public int FrpExtensionComments { get { return gpmStartFrom + 67; } }

        //    public int AbpMedicalInfo { get { return gpmStartFrom + 68; } }
        //    public int AbpAlternativeOutcome { get { return gpmStartFrom + 69; } }
        //    public int AbpReferralReason { get { return gpmStartFrom + 70; } }
        //    public int AbpManagePain { get { return gpmStartFrom + 71; } }
        //    public int AbpOtherInfo { get { return gpmStartFrom + 72; } }

        //    public int ACC32InjuryCause { get { return gpmStartFrom + 73; } }

        //    public int DischargeFollowUp { get { return gpmStartFrom + 74; } }

        //    public int AssessmentGenericNotes { get { return gpmStartFrom + 75; } }

        //    public int Mechanism { get { return gpmStartFrom + 76; } }
        //    public int Analysis { get { return gpmStartFrom + 77; } }
        //    public int CurrentHistory { get { return gpmStartFrom + 78; } }
        //    public int Investigations { get { return gpmStartFrom + 79; } }
        //    public int ProvisionalDiagnosis { get { return gpmStartFrom + 80; } }
        //    public int PrimaryDiagnosis { get { return gpmStartFrom + 81; } }
        //    public int SecondaryDiagnosis { get { return gpmStartFrom + 82; } }
        //    public int ReferralReason { get { return gpmStartFrom + 83; } }

        //    public int ExamNotes { get { return gpmStartFrom + 200; } }
        //    public int VocationalHistory { get { return gpmStartFrom + 201; } }
        //    public int ClinicalSummary { get { return gpmStartFrom + 202; } }
        //    public int Education { get { return gpmStartFrom + 203; } }
        //    public int Review { get { return gpmStartFrom + 204; } }
        //    public int Objective { get { return gpmStartFrom + 205; } }

        //    public int GeneralHealth { get { return gpmStartFrom + 500; } }
        //    public int HealthOtherNotes { get { return gpmStartFrom + 501; } }

        //    public int Acc18Treatment { get { return gpmStartFrom + 600; } }
        //    public int Acc18Complications { get { return gpmStartFrom + 601; } }
        //    public int Acc18Restrictions { get { return gpmStartFrom + 602; } }
        //    public int Acc18AlternativeTasks { get { return gpmStartFrom + 603; } }

        //    public int TestMovementsDuring { get { return gpmStartFrom + 700; } }
        //    public int TestMovementsAfter { get { return gpmStartFrom + 701; } }
        //    public int Posture { get { return gpmStartFrom + 702; } }
        //    public int Neurological { get { return gpmStartFrom + 703; } }
        //    public int MovementLossPain { get { return gpmStartFrom + 704; } }
        //    public int StaticTests { get { return gpmStartFrom + 705; } }
        //    public int ProvisionalClassification { get { return gpmStartFrom + 706; } }
        //    public int PrincipleManagement { get { return gpmStartFrom + 707; } }
        //    public int History { get { return gpmStartFrom + 708; } }
        //    public int FunctionalDisability { get { return gpmStartFrom + 709; } }
        //    public int Stresses { get { return gpmStartFrom + 710; } }
        //    public int CervicalDifferentialTest { get { return gpmStartFrom + 711; } }

        //    public int ReviewAppropriate { get { return gpmStartFrom + 750; } }
        //    public int ReviewRecommendations { get { return gpmStartFrom + 751; } }
        //    public int ReviewInvestigations { get { return gpmStartFrom + 752; } }

        //    public int RestrictedNote { get { return gpmStartFrom + 800; } }

        //    public TextShortcutRestrictionType()
        //    {
        //    }

        //    public  string GetDescription(decimal type)
        //    {
        //        if (type == AllInjuries) return "ALL Injury Types";
        //        else if (type == AnkleInjury) return "Injury - Ankle";
        //        else if (type == SpineInjury) return "Injury - Spine";
        //        else if (type == ElbowInjury) return "Injury - Elbow";
        //        else if (type == GenericInjury) return "Injury - Generic";
        //        else if (type == HandInjury) return "Injury - Hand";
        //        else if (type == HipInjury) return "Injury - Hip";
        //        else if (type == KneeInjury) return "Injury - Knee";
        //        else if (type == ShoulderInjury) return "Injury - Shoulder";
        //        else if (type == ThumbInjury) return "Injury - Thumb";
        //        else if (type == WristInjury) return "Injury - Wrist";
        //        else if (type == PodBiomechanical) return "Field - Biomechanical";
        //        else if (type == PodDiabetes) return "Field - Diabetes";
        //        else if (type == PersonalTrainerNotes) return "Field - Personal Trainer Notes";
        //        else if (type == TreatmentNotes) return "Field - Treatment Notes";
        //        else if (type == PlanNotes) return "Field - Plan Notes";
        //        else if (type == AggravatingFactors) return "Field - Aggravating Factors";
        //        else if (type == SportDetails) return "Field - Sport Details";
        //        else if (type == WeeklyActivity) return "Field - Weekly Activity";
        //        else if (type == Medication) return "Field - Medication";
        //        else if (type == Surgery) return "Field - Surgery";
        //        else if (type == SocialFactors) return "Field - Social Factors";
        //        else if (type == ExerciseNotes) return "Field - Exercise Notes";
        //        else if (type == Diagnosis) return "Field - Diagnosis Notes";
        //        else if (type == DiffDiagnosis) return "Field - Differential Diagnosis Notes";
        //        else if (type == CurrentSymptoms) return "Field - Current Symptoms";
        //        else if (type == DischargeTmtSummary) return "Field - Discharge Tmt Summary";
        //        else if (type == DischargePlan) return "Field - Discharge Further Plan";
        //        else if (type == ObjObservation) return "Field - Objective Observation";
        //        else if (type == ObjFindings) return "Field - Objective Findings";
        //        else if (type == ObjPalpation) return "Field - Objective Palpation";
        //        else if (type == ObjOtherComments) return "Field - Objective Other Comments";
        //        else if (type == Region) return "Field - Region";
        //        else if (type == ContraIndications) return "Field - Contra Indications";
        //        else if (type == OtherDetails) return "Field - Other Details";
        //        else if (type == ClinicalFindings) return "Field - Clinical Findings";
        //        else if (type == ACC32InitialDiag) return "Field - ACC32 Initial Diagnosis";
        //        else if (type == ACC32CurrentDiag) return "Field - ACC32 Current Diagnosis";
        //        else if (type == ACC32InjuryCause) return "Field - ACC32 Caused by Injury";
        //        else if (type == ACC32CurrentCondition) return "Field - ACC32 Current Condition Relation";
        //        else if (type == ACC32NotResolved) return "Field - ACC32 Why not Resolved";
        //        else if (type == ACC32GoalsSandS) return "Field - ACC32 Goals, Signs and Symptoms";
        //        else if (type == ACC32GoalsFunction) return "Field - ACC32 Goals, Function";
        //        else if (type == ACC32LimitationSandS) return "Field - ACC32 Limitations, Signs and Symptoms";
        //        else if (type == ACC32LimitationFunction) return "Field - ACC32 Limitations, Function";
        //        else if (type == ACC32Recommendation) return "Field - ACC32 Recommendation";
        //        else if (type == ExamGoal) return "Field - Exam Goal";
        //        else if (type == ExamGoalTmt) return "Field - Exam Goal Treatment";
        //        else if (type == DischargeOutcomeDetails) return "Field - Discharge Outcome Details";
        //        else if (type == AbpFrpStatusReason) return "Field - ABP Status Reason";
        //        else if (type == FrpSubjective) return "Field - FRP FRP Subjective Objective Assessment";
        //        else if (type == AbpFrpLimitations) return "Field - ABP/FRP Client Limitations";
        //        else if (type == AbpFrpPlanOfActionChanges) return "Field - ABP/FRP Plan Of Action Changes";
        //        else if (type == FrpDisability) return "Field - FRP Pain Related Disability";
        //        else if (type == FrpUnderstanding) return "Field - FRP Client Understanding";
        //        else if (type == FrpReferralGoals) return "Field - FRP Referral Goals";
        //        else if (type == FrpClientGoals) return "Field - FRP Client Goals";
        //        else if (type == FrpGoals) return "Field - FRP Goals";
        //        else if (type == FrpProgress) return "Field - FRP Progress";
        //        else if (type == FrpComments) return "Field - FRP Comments";
        //        else if (type == FrpDailyActivity) return "Field - FRP Ability to return to daily activities";
        //        else if (type == FrpWorkAbility) return "Field - FRP Ability to return to work";
        //        else if (type == FrpStrategies) return "Field - FRP Strategies";
        //        else if (type == FrpRehabilitation) return "Field - FRP Other Rehabilitation";
        //        else if (type == FrpExtensionComments) return "Field - FRP Extension Comments";
        //        else if (type == AbpMedicalInfo) return "Field - ABP Medical Information";
        //        else if (type == AbpAlternativeOutcome) return "Field - ABP Alternative Outcome";
        //        else if (type == AbpReferralReason) return "Field - ABP Referral Reason";
        //        else if (type == AbpManagePain) return "Field - ABP Ability to self manage pain";
        //        else if (type == AbpOtherInfo) return "Field - ABP Other Info";
        //        else if (type == DischargeFollowUp) return "Field - Discharge - Follow Up";
        //        else if (type == AssessmentGenericNotes) return "Field - Assessment Generic Notes";

        //        else if (type == Mechanism) return "Field - Mechanism / Presenting Complaint";
        //        else if (type == Analysis) return "Field - Analysis";
        //        else if (type == CurrentHistory) return "Field - Current History";
        //        else if (type == GeneralHealth) return "Field - General Health";
        //        else if (type == Investigations) return "Field - Investigations";
        //        else if (type == ProvisionalDiagnosis) return "Field - Provisional Diagnosis";
        //        else if (type == PrimaryDiagnosis) return "Field - Primary Diagnosis";
        //        else if (type == SecondaryDiagnosis) return "Field - Secondary Diagnosis";
        //        else if (type == ReferralReason) return "Field - Referral Reason";

        //        else if (type == HealthOtherNotes) return "Field - Client Notes";

        //        else if (type == ExamNotes) return "Field - Other Notes";
        //        else if (type == VocationalHistory) return "Field - Vocational History";
        //        else if (type == ClinicalSummary) return "Field - Clinical Summary";
        //        else if (type == Education) return "Field - Education";
        //        else if (type == Review) return "Field - Review";
        //        else if (type == Objective) return "Field - Objective";

        //        else if (type == Acc18AlternativeTasks) return "Field - ACC18 Alternative Tasks";
        //        else if (type == Acc18Complications) return "Field - ACC18 Complications";
        //        else if (type == Acc18Restrictions) return "Field - ACC18 Restrictions";
        //        else if (type == Acc18Treatment) return "Field - ACC18 Treatment";

        //        else if (type == TestMovementsDuring) return "Field - Test Movements During";
        //        else if (type == TestMovementsAfter) return "Field - Test Movements After";
        //        else if (type == Posture) return "Field - Posture";
        //        else if (type == Neurological) return "Field - Neurological";
        //        else if (type == MovementLossPain) return "Field - Movement Loss Pain";
        //        else if (type == StaticTests) return "Field - Static Tests";
        //        else if (type == ProvisionalClassification) return "Field - Provisional Classification";
        //        else if (type == PrincipleManagement) return "Field - Principle Management";
        //        else if (type == History) return "Field - History";
        //        else if (type == FunctionalDisability) return "Field - Functional Disability";
        //        else if (type == Stresses) return "Field - Stresses";
        //        else if (type == CervicalDifferentialTest) return "Field - Cervical Differential Test";

        //        else if (type == ReviewAppropriate) return "Field - Review Appropriate";
        //        else if (type == ReviewRecommendations) return "Field - Review Recommendations";
        //        else if (type == ReviewInvestigations) return "Field - Review Investigations";

        //        else if (type == RestrictedNote) return "Field - Restricted Note";

        //        return base.GetDescription(type);
        //    }

        //    //public  List<decimal> GetRestrictions(ProductName product)
        //    //{
        //    //    List<decimal> items = base.GetRestrictions(product);
        //    //    items.Add(PersonalTrainerNotes);
        //    //    items.Add(AllInjuries);
        //    //    items.Add(TreatmentNotes);
        //    //    items.Add(ExerciseNotes);
        //    //    items.Add(PlanNotes);
        //    //    items.Add(AggravatingFactors);
        //    //    items.Add(SportDetails);
        //    //    items.Add(WeeklyActivity);
        //    //    items.Add(Medication);
        //    //    items.Add(Surgery);
        //    //    items.Add(SocialFactors);
        //    //    items.Add(Diagnosis);
        //    //    items.Add(DiffDiagnosis);
        //    //    items.Add(CurrentSymptoms);
        //    //    items.Add(DischargeTmtSummary);
        //    //    items.Add(DischargePlan);
        //    //    items.Add(ObjObservation);
        //    //    items.Add(ObjFindings);
        //    //    items.Add(ObjPalpation);
        //    //    items.Add(ObjOtherComments);
        //    //    items.Add(AnkleInjury);
        //    //    items.Add(SpineInjury);
        //    //    items.Add(ElbowInjury);
        //    //    items.Add(GenericInjury);
        //    //    items.Add(HandInjury);
        //    //    items.Add(HipInjury);
        //    //    items.Add(KneeInjury);
        //    //    items.Add(ShoulderInjury);
        //    //    items.Add(ThumbInjury);
        //    //    items.Add(WristInjury);
        //    //    items.Add(PodBiomechanical);
        //    //    items.Add(PodDiabetes);
        //    //    items.Add(Region);
        //    //    items.Add(ContraIndications);
        //    //    items.Add(OtherDetails);
        //    //    items.Add(ClinicalFindings);
        //    //    if (product == ProductName.GPM)
        //    //    {
        //    //        items.Add(ACC32InitialDiag);
        //    //        items.Add(ACC32CurrentDiag);
        //    //        items.Add(ACC32InjuryCause);
        //    //        items.Add(ACC32CurrentCondition);
        //    //        items.Add(ACC32NotResolved);
        //    //        items.Add(ACC32GoalsSandS);
        //    //        items.Add(ACC32GoalsFunction);
        //    //        items.Add(ACC32LimitationSandS);
        //    //        items.Add(ACC32LimitationFunction);
        //    //        items.Add(ACC32Recommendation);
        //    //    }
        //    //    items.Add(ExamGoal);
        //    //    items.Add(ExamGoalTmt);
        //    //    items.Add(DischargeOutcomeDetails);
        //    //    if (product == ProductName.GPM)
        //    //    {
        //    //        items.Add(AbpFrpStatusReason);
        //    //        items.Add(AbpFrpLimitations);
        //    //        items.Add(AbpFrpStatusReason);
        //    //        items.Add(AbpFrpPlanOfActionChanges);
        //    //        items.Add(FrpSubjective);
        //    //        items.Add(FrpDisability);
        //    //        items.Add(FrpUnderstanding);
        //    //        items.Add(FrpReferralGoals);
        //    //        items.Add(FrpClientGoals);
        //    //        items.Add(FrpGoals);
        //    //        items.Add(FrpProgress);
        //    //        items.Add(FrpComments);
        //    //        items.Add(FrpDailyActivity);
        //    //        items.Add(FrpWorkAbility);
        //    //        items.Add(FrpStrategies);
        //    //        items.Add(FrpRehabilitation);
        //    //        items.Add(FrpExtensionComments);
        //    //        items.Add(AbpMedicalInfo);
        //    //        items.Add(AbpAlternativeOutcome);
        //    //        items.Add(AbpReferralReason);
        //    //        items.Add(AbpManagePain);
        //    //        items.Add(AbpOtherInfo);
        //    //    }
        //    //    items.Add(DischargeFollowUp);
        //    //    items.Add(AssessmentGenericNotes);
        //    //    items.Add(Mechanism);
        //    //    items.Add(Analysis);
        //    //    items.Add(CurrentHistory);
        //    //    items.Add(GeneralHealth);
        //    //    items.Add(Investigations);
        //    //    items.Add(ProvisionalDiagnosis);
        //    //    items.Add(PrimaryDiagnosis);
        //    //    items.Add(SecondaryDiagnosis);
        //    //    items.Add(ReferralReason);
        //    //    items.Add(HealthOtherNotes);
        //    //    items.Add(ExamNotes);
        //    //    items.Add(VocationalHistory);
        //    //    items.Add(ClinicalSummary);
        //    //    items.Add(Education);
        //    //    items.Add(Review);
        //    //    items.Add(Objective);
        //    //    if (product == ProductName.GPM)
        //    //    {
        //    //        items.Add(Acc18AlternativeTasks);
        //    //        items.Add(Acc18Complications);
        //    //        items.Add(Acc18Restrictions);
        //    //        items.Add(Acc18Treatment);
        //    //    }

        //    //    items.Add(TestMovementsDuring);
        //    //    items.Add(TestMovementsAfter);
        //    //    items.Add(Posture);
        //    //    items.Add(Neurological);
        //    //    items.Add(MovementLossPain);
        //    //    items.Add(StaticTests);
        //    //    items.Add(ProvisionalClassification);
        //    //    items.Add(PrincipleManagement);
        //    //    items.Add(History);
        //    //    items.Add(FunctionalDisability);
        //    //    items.Add(Stresses);
        //    //    items.Add(CervicalDifferentialTest);

        //    //    items.Add(ReviewAppropriate);
        //    //    items.Add(ReviewRecommendations);
        //    //    items.Add(ReviewInvestigations);

        //    //    items.Add(RestrictedNote);

        //    //    return items;
        //    //}

        //    public int GetGroup(int type)
        //    {
        //        if (type == PersonalTrainerNotes
        //            || type == CurrentSymptoms
        //            || type == TreatmentNotes
        //            || type == PlanNotes
        //            || type == AggravatingFactors
        //            || type == ExerciseNotes
        //            || type == SportDetails
        //            || type == WeeklyActivity
        //            || type == Medication
        //            || type == Surgery
        //            || type == SocialFactors
        //            || type == DischargeTmtSummary
        //            || type == DischargePlan
        //            || type == ObjObservation
        //            || type == ObjFindings
        //            || type == ObjPalpation
        //            || type == Region
        //            || type == ContraIndications
        //            || type == OtherDetails
        //            || type == ACC32InitialDiag
        //            || type == ACC32CurrentDiag
        //            || type == ACC32InjuryCause
        //            || type == ACC32CurrentCondition
        //            || type == ACC32NotResolved
        //            || type == ACC32GoalsSandS
        //            || type == ACC32GoalsFunction
        //            || type == ACC32LimitationSandS
        //            || type == ACC32LimitationFunction
        //            || type == ACC32Recommendation
        //            || type == ExamGoal
        //            || type == ExamGoalTmt
        //            || type == DischargeOutcomeDetails
        //            || type == AbpFrpStatusReason
        //            || type == FrpSubjective
        //            || type == AbpFrpStatusReason
        //            || type == AbpFrpLimitations
        //            || type == AbpFrpPlanOfActionChanges
        //            || type == FrpDisability
        //            || type == FrpUnderstanding
        //            || type == FrpReferralGoals
        //            || type == FrpClientGoals
        //            || type == FrpGoals
        //            || type == FrpProgress
        //            || type == FrpComments
        //            || type == FrpDailyActivity
        //            || type == FrpWorkAbility
        //            || type == FrpStrategies
        //            || type == FrpRehabilitation
        //            || type == FrpExtensionComments
        //            || type == AbpMedicalInfo
        //            || type == AbpAlternativeOutcome
        //            || type == AbpReferralReason
        //            || type == AbpManagePain
        //            || type == AbpOtherInfo
        //            || type == DischargeFollowUp
        //            || type == AssessmentGenericNotes
        //            || type == Mechanism
        //            || type == Analysis
        //            || type == GeneralHealth
        //            || type == Investigations
        //            || type == ProvisionalDiagnosis
        //            || type == HealthOtherNotes
        //            || type == ExamNotes
        //            || type == Education
        //            || type == Review
        //            || type == Objective
        //            || type == Acc18AlternativeTasks
        //            || type == Acc18Complications
        //            || type == Acc18Restrictions
        //            || type == Acc18Treatment
        //            || type == TestMovementsDuring
        //            || type == TestMovementsAfter
        //            || type == Posture
        //            || type == Neurological
        //            || type == MovementLossPain
        //            || type == StaticTests
        //            || type == ProvisionalClassification
        //            || type == PrincipleManagement
        //            || type == History
        //            || type == FunctionalDisability
        //            || type == Stresses
        //            || type == CervicalDifferentialTest
        //            || type == ReviewAppropriate
        //            || type == ReviewRecommendations
        //            || type == ReviewInvestigations
        //            || type == RestrictedNote
        //        )
        //            return 1;
        //        else if (type == AllInjuries
        //            || type == AnkleInjury
        //            || type == SpineInjury
        //            || type == ElbowInjury
        //            || type == GenericInjury
        //            || type == HipInjury
        //            || type == HandInjury
        //            || type == KneeInjury
        //            || type == ShoulderInjury
        //            || type == ThumbInjury
        //            || type == WristInjury
        //            || type == PodDiabetes
        //            || type == PodBiomechanical)
        //            return 2;
        //        else
        //            return base.GetGroup(type);
        //    }
        //}
        #endregion

        //#region ClassAppointmentPayByType

        //protected override Gensolve.AppointmentData.Common.AppointmentEnums.ClassAppointmentPayByType GetClassAppointmentPayByTypeEnum()
        //{
        //    return new ClassAppointmentPayByType();
        //}

        //public new ClassAppointmentPayByType ClassAppointmentPayByTypeEnum
        //{
        //    get { return (ClassAppointmentPayByType)GetClassAppointmentPayByTypeEnum(); }
        //}

        //public new class ClassAppointmentPayByType : Gensolve.AppointmentData.Common.AppointmentEnums.ClassAppointmentPayByType
        //{
        //    private const int startFrom = Gensolve.AppointmentData.Common.AppointmentEnums.ClassAppointmentPayByType.ClassAppointmentPayByTypeStartFrom;

        //    protected new const int ClassAppointmentPayByTypeStartFrom = startFrom + 1000;

        //    public int Contract { get { return startFrom + 0; } }
        //    public int ConditionDebtor { get { return startFrom + 1; } }

        //    public string GetDescription(int type)
        //    {
        //        if (type == Contract) return "Contract";
        //        else return base.GetDescription(type);
        //    }
        //}

        //#endregion

        #region Severity

        public enum Severity : int
        {
            Low = 1,
            Moderate = 2,
            High = 3
        }

        public static string SeverityDescription(Severity severity)
        {
            return SeverityDescription((int)severity);
        }

        public static string SeverityDescription(Decimal severity)
        {
            return SeverityDescription((int)severity);
        }

        public static string SeverityDescription(int severity)
        {
            switch (severity)
            {
                case (int)Severity.Low: return "Low";
                case (int)Severity.Moderate: return "Moderate";
                case (int)Severity.High: return "High";
                default: return "Unknown";
            }
        }

        public static string SeverityShortDescription(Severity severity)
        {
            return SeverityShortDescription((int)severity);
        }

        public static string SeverityShortDescription(Decimal severity)
        {
            return SeverityShortDescription((int)severity);
        }

        public static string SeverityShortDescription(int severity)
        {
            switch (severity)
            {
                case (int)Severity.Low: return "Low";
                case (int)Severity.Moderate: return "Mod";
                case (int)Severity.High: return "High";
                default: return "Unknown";
            }
        }
        #endregion

        #region Irritability

        public enum Irritability : int
        {
            Low = 1,
            Moderate = 2,
            High = 3
        }

        public static string IrritabilityDescription(Irritability irritability)
        {
            return IrritabilityDescription((int)irritability);
        }

        public static string IrritabilityDescription(Decimal irritability)
        {
            return IrritabilityDescription((int)irritability);
        }

        public static string IrritabilityDescription(int irritability)
        {
            switch (irritability)
            {
                case (int)Irritability.Low: return "Low";
                case (int)Irritability.Moderate: return "Moderate";
                case (int)Irritability.High: return "High";
                default: return "Unknown";
            }
        }

        public static string IrritabilityShortDescription(Irritability irritability)
        {
            return IrritabilityShortDescription((int)irritability);
        }

        public static string IrritabilityShortDescription(Decimal irritability)
        {
            return IrritabilityShortDescription((int)irritability);
        }

        public static string IrritabilityShortDescription(int irritability)
        {
            switch (irritability)
            {
                case (int)Irritability.Low: return "Low";
                case (int)Irritability.Moderate: return "Mod";
                case (int)Irritability.High: return "High";
                default: return "Unknown";
            }
        }
        #endregion

        #region Nature

        public enum Nature : int
        {
            Inflammatory = 1,
            Mechanical = 2,
            Other = 3
        }

        public static string NatureDescription(Nature nature)
        {
            return NatureDescription((int)nature);
        }

        public static string NatureDescription(Decimal nature)
        {
            return NatureDescription((int)nature);
        }

        public static string NatureDescription(int nature)
        {
            switch (nature)
            {
                case (int)Nature.Inflammatory: return "Inflammatory";
                case (int)Nature.Mechanical: return "Mechanical";
                case (int)Nature.Other: return "Other";
                default: return "Unknown";
            }
        }

        public static string NatureShortDescription(Nature nature)
        {
            return NatureShortDescription((int)nature);
        }

        public static string NatureShortDescription(Decimal nature)
        {
            return NatureShortDescription((int)nature);
        }

        public static string NatureShortDescription(int nature)
        {
            switch (nature)
            {
                case (int)Nature.Inflammatory: return "Inflam";
                case (int)Nature.Mechanical: return "Mod";
                case (int)Nature.Other: return "High";
                default: return "Unknown";
            }
        }
        #endregion

        #region Stage

        public enum Stage : int
        {
            Acute = 1,
            Subacute = 2,
            Chronic = 3
        }

        public static string StageDescription(Stage stage)
        {
            return StageDescription((int)stage);
        }

        public static string StageDescription(Decimal stage)
        {
            return StageDescription((int)stage);
        }

        public static string StageDescription(int stage)
        {
            switch (stage)
            {
                case (int)Stage.Acute: return "Acute";
                case (int)Stage.Subacute: return "Subacute";
                case (int)Stage.Chronic: return "Chronic";
                default: return "Unknown";
            }
        }
        #endregion

        #region IntraPainType

        public enum IntraPainType : int
        {
            Acute = 1,
            SubAcute = 2,
            Chronic = 3
        }

        public static string IntraPainTypeDescription(Severity type)
        {
            return IntraPainTypeDescription((int)type);
        }

        public static string IntraPainTypeDescription(Decimal type)
        {
            return IntraPainTypeDescription((int)type);
        }

        public static string IntraPainTypeDescription(int type)
        {
            switch (type)
            {
                case (int)IntraPainType.Acute: return "Acute";
                case (int)IntraPainType.SubAcute: return "Sub Acute";
                case (int)IntraPainType.Chronic: return "Chronic";
                default: return "Unknown";
            }
        }

        #endregion

        //#region MergeField

        //protected override Gensolve.OrganisationData.Common.OrganisationEnums.MergeField GetMergeFieldEnum()
        //{
        //    return new MergeField();
        //}

        //public new MergeField MergeFieldEnum
        //{
        //    get { return (MergeField)GetMergeFieldEnum(); }
        //}

        //public new class MergeField : Gensolve.AppointmentData.Common.AppointmentEnums.MergeField
        //{
        //    private const int startFrom = Gensolve.AppointmentData.Common.AppointmentEnums.MergeField.MergeFieldStartFrom;

        //    protected new const int MergeFieldStartFrom = startFrom + 1000;

        //    public int Occupation { get { return startFrom + 1; } }
        //    public int EthnicGroup { get { return startFrom + 3; } }
        //    public int DischargedDate { get { return startFrom + 4; } }
        //    public int UndischargedDate { get { return startFrom + 5; } }
        //    public int SportClient { get { return startFrom + 6; } }

        //    public int DateConditionCreated { get { return startFrom + 500; } }
        //    public int DateOfInjury { get { return startFrom + 501; } }
        //    public int HasReadCode { get { return startFrom + 502; } }
        //    public int HasConditionAppointmentByProvider { get { return startFrom + 503; } }
        //    public int HasConditionAppointmentByProviderType { get { return startFrom + 504; } }
        //    public int IsDischarged { get { return startFrom + 505; } }
        //    public int HasAppointmentsExpiring { get { return startFrom + 506; } }
        //    public int ConditionType { get { return startFrom + 507; } }
        //    public int Insurer { get { return startFrom + 508; } }
        //    public int Sport { get { return startFrom + 509; } }
        //    public int Scene { get { return startFrom + 510; } }
        //    public int Location { get { return startFrom + 511; } }
        //    public int AlreadyRegistered { get { return startFrom + 512; } }
        //    public int AtWork { get { return startFrom + 513; } }
        //    public int InNZ { get { return startFrom + 514; } }
        //    public int MotorVehicleInvolved { get { return startFrom + 515; } }
        //    public int ReadCodeInjuryType { get { return startFrom + 516; } }
        //    public int InjuryDescription { get { return startFrom + 517; } }
        //    public int ConditionReferralDate { get { return startFrom + 518; } }
        //    public int ConditionReferralExpiryDate { get { return startFrom + 519; } }

        //    //public int HasNumberOfApptsRemaining { get { return startFrom + 518; } }
        //    //public int HasACC32ofStatus { get { return startFrom + 519; } }	

        //    public int DateContractCreated { get { return startFrom + 800; } }
        //    public int HasContractAppointmentByProvider { get { return startFrom + 801; } }
        //    public int ContractType { get { return startFrom + 803; } }
        //    public int ContractStatus { get { return startFrom + 804; } }
        //    public int ContractStatusDate { get { return startFrom + 805; } }
        //    public int DateContractReferred { get { return startFrom + 806; } }
        //    public int DateContractCommenced { get { return startFrom + 807; } }
        //    public int DateContractPlanDue { get { return startFrom + 808; } }
        //    public int DateContractPlanCompleted { get { return startFrom + 809; } }
        //    public int DateContractProgressDue { get { return startFrom + 810; } }
        //    public int DateContractProgressCompleted { get { return startFrom + 811; } }
        //    public int DateContractFinalDue { get { return startFrom + 812; } }
        //    public int DateContractFinalCompleted { get { return startFrom + 813; } }
        //    public int DateContractServiceCompletion { get { return startFrom + 814; } }
        //    public int DateContractDischarged { get { return startFrom + 815; } }
        //    public int DateContractDischargeEntered { get { return startFrom + 816; } }
        //    public int ManagementSite { get { return startFrom + 817; } }


        //    public string GetDescription(int type, decimal cultureType)
        //    {
        //        if (type == Occupation) return "Occupation";
        //        else if (type == EthnicGroup) return "Ethnic Group";
        //        else if (type == DischargedDate) return "Discharged Date";
        //        else if (type == UndischargedDate) return "Undischarge Date";
        //        else if (type == SportClient) return "Sport";

        //        else if (type == DateConditionCreated) return "Condition Created Date";
        //        else if (type == DateOfInjury) return "Date of Injury";
        //        else if (type == HasReadCode) return "Has Read Code";
        //        else if (type == HasConditionAppointmentByProvider) return "Has Appointment By Provider";
        //        else if (type == HasConditionAppointmentByProviderType) return "Has Appointment By Provider Type";
        //        else if (type == IsDischarged) return "Is Discharged";
        //        else if (type == HasAppointmentsExpiring) return "Has Appointments Expiring";
        //        else if (type == ConditionType) return "Condition Type";
        //        else if (type == Insurer) return "Insurer";
        //        else if (type == Sport) return "Sport";
        //        else if (type == Scene) return "Scene";
        //        else if (type == Location) return "Location";
        //        else if (type == AlreadyRegistered) return "Already Registered";
        //        else if (type == AtWork) return "At Work";
        //        else if (type == InNZ) return "In NZ";
        //        else if (type == MotorVehicleInvolved) return "Motor Vehicle Involved";
        //        else if (type == ReadCodeInjuryType) return "Read Code Injury Type";
        //        else if (type == InjuryDescription) return "Injury Description";
        //        else if (type == ConditionReferralDate) return "Referral Date";
        //        else if (type == ConditionReferralExpiryDate) return "Referral Expiry Date";

        //        //else if (type == HasNumberOfApptsRemaining) return "Has X Appointments Left";
        //        //else if (type == HasACC32ofStatus) return "Has ACC32 with Status";			

        //        else if (type == DateContractCreated) return "Contract Created Date";
        //        else if (type == HasContractAppointmentByProvider) return "Has Appointment By Provider";
        //        //else if (type == HasContractAppointmentByProviderType) return "Has Appointment By Provider Type";
        //        else if (type == ContractType) return "Contract Type";
        //        else if (type == ContractStatus) return "Contract Status";
        //        else if (type == ContractStatusDate) return "Contract Status Date";

        //        else if (type == DateContractReferred) return "Referred Date";
        //        else if (type == DateContractCommenced) return "Contract Start Date";
        //        else if (type == DateContractPlanDue) return "Plan Due Date";
        //        else if (type == DateContractPlanCompleted) return "Plan Completed Date";
        //        else if (type == DateContractProgressDue) return "Progress Due Date";
        //        else if (type == DateContractProgressCompleted) return "Progress Completed Date";
        //        else if (type == DateContractFinalDue) return "Final Due Date";
        //        else if (type == DateContractFinalCompleted) return "Final Completed Date";
        //        else if (type == DateContractServiceCompletion) return "Contract Service Completion Date";
        //        else if (type == DateContractDischarged) return "Contract Discharge Date";
        //        else if (type == DateContractDischargeEntered) return "Contract Discharge Entered Date";
        //        else if (type == ManagementSite) return "Management Site";

        //        else
        //            return base.GetDescription(type, cultureType);
        //    }
        //}

        //#endregion

        #region ROMType

        public enum ROMType : int
        {
            Active = 1,
            Passive = 2
        }

        public static string ROMTypeDescription(ROMType type)
        {
            return ROMTypeDescription((int)type);
        }

        public static string ROMTypeDescription(Decimal type)
        {
            return ROMTypeDescription((int)type);
        }

        public static string ROMTypeDescription(int type)
        {
            switch (type)
            {
                case (int)ROMType.Active: return "AROM";
                case (int)ROMType.Passive: return "PROM";
                default: return "Unknown";
            }
        }

        #endregion

        #region ExamDisplayType

        public enum ExamDisplayType : int
        {
            Text = 1,
            ROM = 2
        }

        public static string ExamDisplayTypeDescription(ROMType type)
        {
            return ExamDisplayTypeDescription((int)type);
        }

        public static string ExamDisplayTypeDescription(Decimal type)
        {
            return ExamDisplayTypeDescription((int)type);
        }

        public static string ExamDisplayTypeDescription(int type)
        {
            switch (type)
            {
                case (int)ExamDisplayType.Text: return "Text";
                case (int)ExamDisplayType.ROM: return "ROM";
                default: return "Unknown";
            }
        }

        #endregion

        #region ExamField

        public enum ExamField : int
        {
            CurrentSymptoms = 1,
            TreatmentDetails = 2,
            ExerciseDetails = 3,
            ICD9Diagnostics = 4,
            Analysis = 5,
            CurrentHistory = 6,
            PhysicalTherapyDiagnosis = 7,
            PlanReview = 8,
            VASBest = 9,
            VASWorst = 10,
            DailyPain = 11,
            EasingFactors = 12,
            NoChangeFactors = 13,
            AggravatingFactors = 14,
            AreaOtherDetails = 15,
            AnkleObs = 16,
            AnklePalp = 17,
            AnkleFindings = 18,
            AnkleOther = 19,
            GenericObs = 20,
            GenericPalp = 21,
            GenericFindings = 22,
            GenericOther = 23,
            HipObs = 24,
            HipPalp = 25,
            HipFindings = 26,
            HipOther = 27,
            KneeObs = 28,
            KneePalp = 29,
            KneeFindings = 30,
            KneeOther = 31,
            ShoulderObs = 32,
            ShoulderPalp = 33,
            ShoulderFindings = 34,
            ShoulderOther = 35,
            SpineObs = 36,
            SpinePalp = 37,
            SpineFindings = 38,
            SpineOther = 39,
            WristObs = 40,
            WristPalp = 41,
            WristFindings = 42,
            WristOther = 43,
            ElbowObs = 44,
            ElbowPalp = 45,
            ElbowFindings = 46,
            ElbowOther = 47,
            ThumbObs = 48,
            ThumbPalp = 49,
            ThumbFindings = 50,
            ThumbOther = 51,
            HandObs = 52,
            HandPalp = 53,
            HandFindings = 54,
            HandOther = 55,
            ExamOtherNotes = 56,
            ResponseToLastTreatment = 57,
            ClinicalSummary = 58,
            VocationalHistory = 59,
            Education = 60,
            Review = 61,

            AnkleAromDF = 62,
            AnkleAromPF = 63,
            AnkleAromInv = 64,
            AnkleAromEv = 65,
            AnkleAromToeExtn = 66,
            AnkleAromToeFlxn = 67,

            AnklePromDF = 68,
            AnklePromPF = 69,
            AnklePromInv = 70,
            AnklePromEv = 71,
            AnklePromToeExtn = 72,
            AnklePromToeFlxn = 73,

            ElbowAromExtn = 74,
            ElbowAromFlxn = 75,
            ElbowAromPron = 76,
            ElbowAromSupn = 77,

            ElbowPromExtn = 78,
            ElbowPromFlxn = 79,
            ElbowPromPron = 80,
            ElbowPromSupn = 81,

            HipAromAbduction = 82,
            HipAromAdduction = 83,
            HipAromEr = 84,
            HipAromExtn = 85,
            HipAromFlxn = 86,
            HipAromIr = 87,

            HipPromEr = 88,
            HipPromExtn = 89,
            HipPromFlxn = 90,
            HipPromIr = 91,
            HipPromQuadrant = 92,

            KneeAromExtn = 93,
            KneeAromFlxn = 94,

            KneePromExtn = 95,
            KneePromFlxn = 96,

            ShoulderAromAbduction = 97,
            ShoulderAromAdduction = 98,
            ShoulderAromEr = 99,
            ShoulderAromEr90 = 100,
            ShoulderAromFlxn = 101,
            ShoulderAromIr90 = 102,

            ShoulderPromAbduction = 103,
            ShoulderPromAdduction = 104,
            ShoulderPromEr = 105,
            ShoulderPromEr90 = 106,
            ShoulderPromFlxn = 107,
            ShoulderPromIr90 = 108,

            WristAromExtn = 109,
            WristAromFlxn = 110,
            WristAromPron = 111,
            WristAromRad = 112,
            WristAromSupn = 113,
            WristAromUlnar = 114,

            WristPromExtn = 115,
            WristPromFlxn = 116,
            WristPromPron = 117,
            WristPromRad = 118,
            WristPromSupn = 119,
            WristPromUlnar = 120,

            Objective = 121,
            CustomField = 122,
            RestrictedNotes = 123
        }

        //public static bool GetExamFieldFromDynamicSystemField(decimal systemField, out decimal examField)
        //{
        //    bool matched = false;
        //    examField = -1;

        //    if (systemField == (decimal)Gensolve.PhysioData.Common.DynamicForms.DynamicSystemFieldType.AggravatingFactors)
        //    {
        //        matched = true;
        //        examField = (decimal)ExamField.AggravatingFactors;
        //    }
        //    else if (systemField == (decimal)Gensolve.PhysioData.Common.DynamicForms.DynamicSystemFieldType.EasingFactors)
        //    {
        //        matched = true;
        //        examField = (decimal)ExamField.EasingFactors;
        //    }
        //    else if (systemField == (decimal)Gensolve.PhysioData.Common.DynamicForms.DynamicSystemFieldType.NoChangeFactors)
        //    {
        //        matched = true;
        //        examField = (decimal)ExamField.NoChangeFactors;
        //    }
        //    else if (systemField == (decimal)Gensolve.PhysioData.Common.DynamicForms.DynamicSystemFieldType.CurrentSymptoms)
        //    {
        //        matched = true;
        //        examField = (decimal)ExamField.CurrentSymptoms;
        //    }
        //    else if (systemField == (decimal)Gensolve.PhysioData.Common.DynamicForms.DynamicSystemFieldType.CurrentHistory)
        //    {
        //        matched = true;
        //        examField = (decimal)ExamField.CurrentHistory;
        //    }
        //    else if (systemField == (decimal)Gensolve.PhysioData.Common.DynamicForms.DynamicSystemFieldType.Education)
        //    {
        //        matched = true;
        //        examField = (decimal)ExamField.Education;
        //    }
        //    else if (systemField == (decimal)Gensolve.PhysioData.Common.DynamicForms.DynamicSystemFieldType.Review)
        //    {
        //        matched = true;
        //        examField = (decimal)ExamField.Review;
        //    }
        //    else if (systemField == (decimal)Gensolve.PhysioData.Common.DynamicForms.DynamicSystemFieldType.Objective)
        //    {
        //        matched = true;
        //        examField = (decimal)ExamField.Objective;
        //    }
        //    else if (systemField == (decimal)Gensolve.PhysioData.Common.DynamicForms.DynamicSystemFieldType.ClinicalSummary)
        //    {
        //        matched = true;
        //        examField = (decimal)ExamField.ClinicalSummary;
        //    }
        //    else if (systemField == (decimal)Gensolve.PhysioData.Common.DynamicForms.DynamicSystemFieldType.Analysis)
        //    {
        //        matched = true;
        //        examField = (decimal)ExamField.Analysis;
        //    }
        //    else if (systemField == (decimal)Gensolve.PhysioData.Common.DynamicForms.DynamicSystemFieldType.Treatment)
        //    {
        //        matched = true;
        //        examField = (decimal)ExamField.TreatmentDetails;
        //    }

        //    return matched;
        //}


        public static ExamDisplayType GetExamDisplayType(ExamField examField)
        {
            switch (examField)
            {
                case ExamField.VASBest:
                case ExamField.VASWorst:
                case ExamField.AnkleAromDF:
                case ExamField.AnkleAromPF:
                case ExamField.AnkleAromInv:
                case ExamField.AnkleAromEv:
                case ExamField.AnkleAromToeExtn:
                case ExamField.AnkleAromToeFlxn:
                case ExamField.AnklePromDF:
                case ExamField.AnklePromPF:
                case ExamField.AnklePromInv:
                case ExamField.AnklePromEv:
                case ExamField.AnklePromToeExtn:
                case ExamField.AnklePromToeFlxn:
                case ExamField.ElbowAromExtn:
                case ExamField.ElbowAromFlxn:
                case ExamField.ElbowAromPron:
                case ExamField.ElbowAromSupn:
                case ExamField.ElbowPromExtn:
                case ExamField.ElbowPromFlxn:
                case ExamField.ElbowPromPron:
                case ExamField.ElbowPromSupn:

                case ExamField.HipAromAbduction:
                case ExamField.HipAromAdduction:
                case ExamField.HipAromEr:
                case ExamField.HipAromExtn:
                case ExamField.HipAromFlxn:
                case ExamField.HipAromIr:
                case ExamField.HipPromEr:
                case ExamField.HipPromExtn:
                case ExamField.HipPromFlxn:
                case ExamField.HipPromIr:
                case ExamField.HipPromQuadrant:

                case ExamField.KneeAromExtn:
                case ExamField.KneeAromFlxn:
                case ExamField.KneePromExtn:
                case ExamField.KneePromFlxn:

                case ExamField.ShoulderAromAbduction:
                case ExamField.ShoulderAromAdduction:
                case ExamField.ShoulderAromEr:
                case ExamField.ShoulderAromEr90:
                case ExamField.ShoulderAromFlxn:
                case ExamField.ShoulderAromIr90:

                case ExamField.ShoulderPromAbduction:
                case ExamField.ShoulderPromAdduction:
                case ExamField.ShoulderPromEr:
                case ExamField.ShoulderPromEr90:
                case ExamField.ShoulderPromFlxn:
                case ExamField.ShoulderPromIr90:

                case ExamField.WristAromExtn:
                case ExamField.WristAromFlxn:
                case ExamField.WristAromPron:
                case ExamField.WristAromRad:
                case ExamField.WristAromSupn:
                case ExamField.WristAromUlnar:
                case ExamField.WristPromExtn:
                case ExamField.WristPromFlxn:
                case ExamField.WristPromPron:
                case ExamField.WristPromRad:
                case ExamField.WristPromSupn:
                case ExamField.WristPromUlnar:

                    return ExamDisplayType.ROM;

                default:
                    return ExamDisplayType.Text;


            }
        }

        public static string ExamFieldDescription(ExamField type)
        {
            return ExamFieldDescription((int)type);
        }

        public static string ExamFieldDescription(Decimal type)
        {
            return ExamFieldDescription((int)type);
        }

        public static string ExamFieldDescription(int type)
        {
            switch (type)
            {
                case (int)ExamField.Analysis: return "Analysis";
                case (int)ExamField.CurrentHistory: return "Current History";
                case (int)ExamField.CurrentSymptoms: return "Current Symptoms";
                case (int)ExamField.ExerciseDetails: return "Exercise Details";
                case (int)ExamField.ICD9Diagnostics: return "ICD9 Diagnostics";
                case (int)ExamField.PhysicalTherapyDiagnosis: return "Physical Therapy Diagnosis";
                case (int)ExamField.PlanReview: return "Plan Review";
                case (int)ExamField.TreatmentDetails: return "Treatment Details";
                case (int)ExamField.VASBest: return "VAS Best";
                case (int)ExamField.VASWorst: return "VAS Worst";
                case (int)ExamField.DailyPain: return "Daily Pain";
                case (int)ExamField.EasingFactors: return "Easing Factors";
                case (int)ExamField.NoChangeFactors: return "No Change Factors";
                case (int)ExamField.AggravatingFactors: return "Aggravating Factors";
                case (int)ExamField.AreaOtherDetails: return "Area Other Details";
                case (int)ExamField.AnkleObs: return "Observation";
                case (int)ExamField.AnklePalp: return "Palpation / Physiological Movement";
                case (int)ExamField.AnkleFindings: return "Description / Findings";
                case (int)ExamField.AnkleOther: return "Other Comments / Key Changes";
                case (int)ExamField.ElbowObs: return "Observation";
                case (int)ExamField.ElbowPalp: return "Palpation / Physiological Movement";
                case (int)ExamField.ElbowFindings: return "Description / Findings";
                case (int)ExamField.ElbowOther: return "Other Comments / Key Changes";
                case (int)ExamField.GenericObs: return "Observation";
                case (int)ExamField.GenericPalp: return "Palpation / Physiological Movement";
                case (int)ExamField.GenericFindings: return "Description / Findings";
                case (int)ExamField.GenericOther: return "Other Comments / Key Changes";
                case (int)ExamField.KneeObs: return "Observation";
                case (int)ExamField.KneePalp: return "Palpation / Physiological Movement";
                case (int)ExamField.KneeFindings: return "Description / Findings";
                case (int)ExamField.KneeOther: return "Other Comments / Key Changes";
                case (int)ExamField.ShoulderObs: return "Observation";
                case (int)ExamField.ShoulderPalp: return "Palpation / Physiological Movement";
                case (int)ExamField.ShoulderFindings: return "Description / Findings";
                case (int)ExamField.ShoulderOther: return "Other Comments / Key Changes";
                case (int)ExamField.SpineObs: return "Observation";
                case (int)ExamField.SpinePalp: return "Palpation / Physiological Movement";
                case (int)ExamField.SpineFindings: return "Description / Findings";
                case (int)ExamField.SpineOther: return "Other Comments / Key Changes";
                case (int)ExamField.WristObs: return "Observation";
                case (int)ExamField.WristPalp: return "Palpation / Physiological Movement";
                case (int)ExamField.WristFindings: return "Description / Findings";
                case (int)ExamField.WristOther: return "Other Comments / Key Changes";
                case (int)ExamField.ThumbObs: return "Observation";
                case (int)ExamField.ThumbPalp: return "Palpation / Physiological Movement";
                case (int)ExamField.ThumbFindings: return "Description / Findings";
                case (int)ExamField.ThumbOther: return "Other Comments / Key Changes";
                case (int)ExamField.HandObs: return "Observation";
                case (int)ExamField.HandPalp: return "Palpation / Physiological Movement";
                case (int)ExamField.HandFindings: return "Description / Findings";
                case (int)ExamField.HandOther: return "Other Comments / Key Changes";
                case (int)ExamField.ResponseToLastTreatment: return "Response To Last Treatment";
                case (int)ExamField.ClinicalSummary: return "Clinical Summary";
                case (int)ExamField.VocationalHistory: return "Vocational History";
                case (int)ExamField.Education: return "Education";
                case (int)ExamField.Review: return "Review";

                case (int)ExamField.AnkleAromDF: return "Arom Dorsi Flexion";
                case (int)ExamField.AnkleAromPF: return "Arom Plantar Flexion";
                case (int)ExamField.AnkleAromInv: return "Arom Inverion";
                case (int)ExamField.AnkleAromEv: return "Arom Everson";
                case (int)ExamField.AnkleAromToeExtn: return "Arom Toe Extenstion";
                case (int)ExamField.AnkleAromToeFlxn: return "Arom Toe Flexion";

                case (int)ExamField.AnklePromDF: return "Prom Dorsi Flexion";
                case (int)ExamField.AnklePromPF: return "Prom Plantar Flexion";
                case (int)ExamField.AnklePromInv: return "Prom Inverion";
                case (int)ExamField.AnklePromEv: return "Prom Everson";
                case (int)ExamField.AnklePromToeExtn: return "Prom Toe Extenstion";
                case (int)ExamField.AnklePromToeFlxn: return "Prom Toe Flexion";

                case (int)ExamField.ElbowAromExtn: return "Arom Extension";
                case (int)ExamField.ElbowAromFlxn: return "Arom Flexion";
                case (int)ExamField.ElbowAromPron: return "Arom Pronation";
                case (int)ExamField.ElbowAromSupn: return "Arom Supination";

                case (int)ExamField.ElbowPromExtn: return "Prom Extension";
                case (int)ExamField.ElbowPromFlxn: return "Prom Flexion";
                case (int)ExamField.ElbowPromPron: return "Prom Pronation";
                case (int)ExamField.ElbowPromSupn: return "Prom Supination";

                case (int)ExamField.HipAromAbduction: return "Arom Abduction";
                case (int)ExamField.HipAromAdduction: return "Arom Adduction";
                case (int)ExamField.HipAromEr: return "Arom ER";
                case (int)ExamField.HipAromExtn: return "Arom Extension";
                case (int)ExamField.HipAromFlxn: return "Arom Flexion";
                case (int)ExamField.HipAromIr: return "Arom IR";

                case (int)ExamField.HipPromEr: return "Prom ER";
                case (int)ExamField.HipPromExtn: return "Prom Extension";
                case (int)ExamField.HipPromFlxn: return "Prom Flexion";
                case (int)ExamField.HipPromIr: return "Prom IR";
                case (int)ExamField.HipPromQuadrant: return "Prom Quadrant";

                case (int)ExamField.KneeAromExtn: return "Arom Extension";
                case (int)ExamField.KneeAromFlxn: return "Arom Flexion";
                case (int)ExamField.KneePromExtn: return "Prom Extension";
                case (int)ExamField.KneePromFlxn: return "Prom Flexion";

                case (int)ExamField.ShoulderAromAbduction: return "Arom Abduction";
                case (int)ExamField.ShoulderAromAdduction: return "Arom Adduction";
                case (int)ExamField.ShoulderAromEr: return "Arom ER Neutral";
                case (int)ExamField.ShoulderAromEr90: return "Arom ER @ 90 AB";
                case (int)ExamField.ShoulderAromFlxn: return "Arom Flexion";
                case (int)ExamField.ShoulderAromIr90: return "Arom IR @ 90 AB";

                case (int)ExamField.ShoulderPromAbduction: return "Prom Abduction";
                case (int)ExamField.ShoulderPromAdduction: return "Prom Adduction";
                case (int)ExamField.ShoulderPromEr: return "Prom ER Neutral";
                case (int)ExamField.ShoulderPromEr90: return "Prom ER @ 90 AB";
                case (int)ExamField.ShoulderPromFlxn: return "Prom Flexion";
                case (int)ExamField.ShoulderPromIr90: return "Prom IR @ 90 AB";

                case (int)ExamField.WristAromExtn: return "Arom Extension";
                case (int)ExamField.WristAromFlxn: return "Arom Flexion";
                case (int)ExamField.WristAromPron: return "Arom Pronation";
                case (int)ExamField.WristAromRad: return "Arom Rad Dev";
                case (int)ExamField.WristAromSupn: return "Arom Supination";
                case (int)ExamField.WristAromUlnar: return "Arom Ulnar Dev";

                case (int)ExamField.WristPromExtn: return "Prom Extension";
                case (int)ExamField.WristPromFlxn: return "Prom Flexion";
                case (int)ExamField.WristPromPron: return "Prom Pronation";
                case (int)ExamField.WristPromRad: return "Prom Rad Dev";
                case (int)ExamField.WristPromSupn: return "Prom Supination";
                case (int)ExamField.WristPromUlnar: return "Prom Ulnar Dev";

                case (int)ExamField.Objective: return "Objective";

                case (int)ExamField.RestrictedNotes: return "Restricted Notes";

                default: return "Unknown";
            }
        }
        #endregion

        #region ExamResponseToLastTreatment

        public enum ExamResponseToLastTreatment : int
        {
            NA = 0,
            Improving = 1,
            Worse = 2,
            NoChange = 3,
            AdverseReaction = 4
        }

        public static string ExamResponseToLastTreatmentDescription(ExamResponseToLastTreatment type)
        {
            return ExamResponseToLastTreatmentDescription((int)type);
        }

        public static string ExamResponseToLastTreatmentDescription(Decimal type)
        {
            return ExamResponseToLastTreatmentDescription((int)type);
        }

        public static string ExamResponseToLastTreatmentDescription(int type)
        {
            switch (type)
            {
                case (int)ExamResponseToLastTreatment.NA: return "N/A";
                case (int)ExamResponseToLastTreatment.Improving: return "Improving";
                case (int)ExamResponseToLastTreatment.Worse: return "Worse";
                case (int)ExamResponseToLastTreatment.NoChange: return "No Change";
                case (int)ExamResponseToLastTreatment.AdverseReaction: return "Adverse Reaction";
                default: return "";
            }
        }
        #endregion

        #region ExamTestType

        public enum ExamTestType : int
        {
            Special = 1,
            Strength = 2,
            Neuro = 3
        }

        public static string ExamTestTypeDescription(ExamTestType type)
        {
            return ExamTestTypeDescription((int)type);
        }

        public static string ExamTestTypeDescription(Decimal type)
        {
            return ExamTestTypeDescription((int)type);
        }

        public static string ExamTestTypeDescription(int type)
        {
            switch (type)
            {
                case (int)ExamTestType.Special: return "Special";
                case (int)ExamTestType.Strength: return "Strength";
                case (int)ExamTestType.Neuro: return "Neuro";
                default: return "Unknown";
            }
        }

        #endregion

        #region ContractRateType

        public enum ContractRateType : int
        {
            PerUnit = 0,
            PerHour = 1,
            AdHoc = 2,
            Fixed = 3
        }
        public static string ContractRateTypeDescription(ContractRateType type)
        {
            return ContractRateTypeDescription((int)type);
        }

        public static string ContractRateTypeDescription(Decimal type)
        {
            return ContractRateTypeDescription((int)type);
        }

        public static string ContractRateTypeDescription(int type)
        {
            switch (type)
            {
                case (int)ContractRateType.PerUnit: return "Per Unit";
                case (int)ContractRateType.PerHour: return "Per Hour";
                case (int)ContractRateType.AdHoc: return "Ad Hoc";
                default: return "Unknown";
            }
        }
        #endregion

        #region BackRomPosition
        public enum BackRomPosition : int
        {
            Standing = 1,
            Sitting = 2,
            Lying = 3
        }
        #endregion

        #region OutcomeMeasureType

        public enum OutcomeMeasureType : int
        {
            Activity = 1,
            Pain = 2,
            Other = 3,
            GRC = 4
        }
        #endregion

        #region Discharge State

        public enum DischargeStatus
        {
            Discharged, Undischarged, PartiallyDischarged
        }

        public static string DischargeStatusDescription(DischargeStatus status)
        {
            if (status == DischargeStatus.Discharged)
                return "Discharged";
            else if (status == DischargeStatus.Undischarged)
                return "Not Discharged";
            else if (status == DischargeStatus.PartiallyDischarged)
                return "Partially Discharged";
            else return "";
        }

        public enum DischargeAction
        {
            Undischarge, Discharge
        }

        public static DischargeAction GetDischargeAction(string dischargeDescription)
        {
            if (DischargeActionDescription(DischargeAction.Discharge) == dischargeDescription) return DischargeAction.Discharge;
            if (DischargeActionDescription(DischargeAction.Undischarge) == dischargeDescription) return DischargeAction.Undischarge;
            else return DischargeAction.Discharge;
        }

        public static string DischargeActionDescription(Decimal status)
        {
            return DischargeActionDescription((int)status);
        }

        public static string DischargeActionDescription(DischargeAction status)
        {
            return DischargeActionDescription((int)status);
        }

        public static string DischargeActionDescription(int status)
        {
            switch (status)
            {
                case (int)DischargeAction.Discharge: return "Discharge";
                case (int)DischargeAction.Undischarge: return "Undischarge";
                default: return "Unknown";
            }
        }

        #endregion

        #region ExamReportHeaderType
        public enum ExamReportHeaderType
        {
            NoHeader = 1,
            InvoiceImage = 2,
            EmptySpace = 3
        }
        #endregion
        #region MedicarePkiFiles
        public enum MedicarePkiFiles
        {
            fac_encrypt_p12 = 1,
            fac_sign_p12 = 2,
            //trust_p12 = 3,
            hic_encrypt_crt = 4,
            hic_sign_crt = 5,
            Test_MCA_Enc_crt = 6,
            Test_MCA_sign_crt = 7,
            tmaoca_new_crt = 8,
            //tmaoca_crt = 9,
            tmarca_crt,
            mca_sign_crt,
            mca_encrypt_crt
        }
        #endregion
        #region MedicareBScheduleType
        public enum MedicareBScheduleType
        {
            Medicare = 1,
            DVAAllied = 2
        }
        #endregion

        #region ReadCodeAvailableType
        public enum ReadCodeAvailableType
        {
            AllConditions = 1,
            PrivateConditions = 2,
            ACCAndInsuredConditions = 3,
            InsuredConditions = 4
        }
        #endregion

        #region OrchardCodeAvailableType
        public enum OrchardCodeAvailableType
        {
            AllConditions = 1,
            PrivateConditions = 2,
            InsuredConditions = 3
        }
        #endregion

        #region DebtorContractBillingType
        public enum DebtorContractBillingType
        {
            StandardInvoices = 1,
            InsurerSchedules = 2,
            ACCSchedules = 3
        }
        #endregion

        #region PhysioFullHistoryType

        public enum PhysioFullHistoryType : int
        {
            ACC32 = 1,
            Appointment = 2,
            Condition = 3,
            Discharge = 4,
            Event = 5,
            Invoice = 6,
            Letter = 7,
            Upload = 8,
            HLMessage = 9,
            Exam = 10,
            Payment = 11,
            Statement = 12,
            Medication = 13,
            Investigation = 14,
            FormEntry = 15,
            MedicalNote = 16,
            ClassHistory = 17,
            CashSale = 18,
            SMS = 19,
            ContractAppointmentProduct = 20,
            ClientAppointmentProduct = 21,
            BusinessInvoice = 22,
            MedicareOnline = 23,
            ConditionReview = 24
        }

        public static bool IsClinicianType(PhysioFullHistoryType type)
        {
            switch (type)
            {
                case PhysioFullHistoryType.Condition:
                case PhysioFullHistoryType.Discharge:
                case PhysioFullHistoryType.Investigation:
                case PhysioFullHistoryType.Medication:
                case PhysioFullHistoryType.Exam:
                case PhysioFullHistoryType.Appointment:
                    return true;
                default:
                    return false;
            }
        }

        public static bool IsAccountingType(PhysioFullHistoryType type)
        {
            switch (type)
            {
                case PhysioFullHistoryType.Payment:
                case PhysioFullHistoryType.Statement:
                case PhysioFullHistoryType.Invoice:
                    return true;
                default:
                    return false;
            }
        }

        public static bool IsMiscType(PhysioFullHistoryType type)
        {
            return (!IsClinicianType(type) && !IsAccountingType(type));
        }

        public static string PhysioFullHistoryTypeDescription(PhysioFullHistoryType ctype)
        {
            return PhysioFullHistoryTypeDescription((int)ctype);
        }

        public static string PhysioFullHistoryTypeDescription(decimal ctype)
        {
            return PhysioFullHistoryTypeDescription((int)ctype);
        }

        public static string PhysioFullHistoryTypeDescription(int status)
        {
            switch (status)
            {
                case (int)PhysioFullHistoryType.ACC32: return "Acc32";
                case (int)PhysioFullHistoryType.Appointment: return "Appointment";
                case (int)PhysioFullHistoryType.Condition: return "Condition";
                case (int)PhysioFullHistoryType.Discharge: return "Discharge";
                case (int)PhysioFullHistoryType.Event: return "Event";
                case (int)PhysioFullHistoryType.Letter: return "Letter";
                case (int)PhysioFullHistoryType.Upload: return "Upload";
                case (int)PhysioFullHistoryType.HLMessage: return "Healthlink Message";
                case (int)PhysioFullHistoryType.Exam: return "Exam";
                case (int)PhysioFullHistoryType.Payment: return "Payment";
                case (int)PhysioFullHistoryType.Statement: return "Statement";
                case (int)PhysioFullHistoryType.Medication: return "Medication";
                case (int)PhysioFullHistoryType.Investigation: return "Investigation";
                case (int)PhysioFullHistoryType.FormEntry: return "Form Entry";
                case (int)PhysioFullHistoryType.MedicalNote: return "Medical Note";
                case (int)PhysioFullHistoryType.CashSale: return "Cash Sale";
                case (int)PhysioFullHistoryType.SMS: return "SMS";
                case (int)PhysioFullHistoryType.ContractAppointmentProduct: return "Contract Product";
                case (int)PhysioFullHistoryType.ClientAppointmentProduct: return "Appt Product";
                case (int)PhysioFullHistoryType.BusinessInvoice: return "Invoice";
                case (int)PhysioFullHistoryType.MedicareOnline: return "Medicare Online";
                case (int)PhysioFullHistoryType.ConditionReview: return "ConditionReview";

                default:
                    return "";
            }
        }
        #endregion

        #region PhysioClientMedicalNotesType
        public enum PhysioClientMedicalNotesType : int
        {
            Exam = 1,
            ClinicalNote = 2
        }

        public static string PhysioClientMedicalNotesTypeDescription(PhysioClientMedicalNotesType ctype)
        {
            return PhysioClientMedicalNotesTypeDescription((int)ctype);
        }

        public static string PhysioClientMedicalNotesTypeDescription(decimal ctype)
        {
            return PhysioClientMedicalNotesTypeDescription((int)ctype);
        }

        public static string PhysioClientMedicalNotesTypeDescription(int status)
        {
            switch (status)
            {
                case (int)PhysioClientMedicalNotesType.Exam: return "Notes / Exam";
                case (int)PhysioClientMedicalNotesType.ClinicalNote: return "Clinical Note";
                default:
                    return "";
            }
        }
        #endregion

        #region ShowRedFlagsType
        public enum ShowRedFlagsType : int
        {
            DontShow = 0,
            ShowOwn = 1,
            ShowAll = 2
        }
        #endregion

        #region DefaultUserSites
        public enum DefaultUserSiteType : int
        {
            SessionSite = 0,
            AllSites = 1,
            SpecifiedSite = 2
        }

        public static string DefaultUserSiteTypeDescription(DefaultUserSiteType ctype)
        {
            return DefaultUserSiteTypeDescription((int)ctype);
        }

        public static string DefaultUserSiteTypeDescription(decimal ctype)
        {
            return DefaultUserSiteTypeDescription((int)ctype);
        }

        public static string DefaultUserSiteTypeDescription(int status)
        {
            switch (status)
            {
                case (int)DefaultUserSiteType.SessionSite: return "Current Site";
                case (int)DefaultUserSiteType.AllSites: return "All Sites";
                case (int)DefaultUserSiteType.SpecifiedSite: return "Specified Site";

                default:
                    return "";
            }
        }

        #endregion

        #region OutcomeMeasureDateTypes
        public enum OutcomeMeasureDateType : int
        {
            ACC32 = 0,
            Discharge = 1,
            SubsequentVisit = 2,
            TwelfthVisit = 3
        }

        public static string OutcomeMeasureDateTypeDescription(OutcomeMeasureDateType ctype)
        {
            return OutcomeMeasureDateTypeDescription((int)ctype);
        }

        public static string OutcomeMeasureDateTypeDescription(decimal ctype)
        {
            return OutcomeMeasureDateTypeDescription((int)ctype);
        }

        public static string OutcomeMeasureDateTypeDescription(int status)
        {
            switch (status)
            {
                case (int)OutcomeMeasureDateType.ACC32: return "ACC32 Visit";
                case (int)OutcomeMeasureDateType.Discharge: return "Discharge Visit";
                case (int)OutcomeMeasureDateType.SubsequentVisit: return "Subsequent Visit";
                case (int)OutcomeMeasureDateType.TwelfthVisit: return "12th Visit";

                default:
                    return "";
            }
        }
        #endregion

        #region InvestigationOptionFieldType
        public enum InvestigationOptionFieldType : int
        {
            Checkbox = 1,
            Text = 2
        }
        #endregion

        #region VocServiceType
        public enum VocServiceType : int
        {
            JobPlacement = 1,
            StandAloneWorkAssessment = 2,
            StayAtWork = 3,
            WorkReadiness = 4,
            StayAtWork2 = 5,
            BackToWork2 = 6
        }
        #endregion

        #region VocStage
        public enum VocStage : int
        {
            Completion = 1,
            SustainedOutcome = 2
        }
        #endregion

        #region VocLevel
        public enum VocLevel : int
        {
            Level1 = 1,
            Level2 = 2,
            Level3 = 3,
            Level4 = 4,
            JobBrokerage = 5,
            JobSearchService = 6,
            NotApplicable = 7
        }

        public static string VocLevelDescription(VocLevel ctype)
        {
            return VocLevelDescription((int)ctype);
        }

        public static string VocLevelDescription(decimal ctype)
        {
            return VocLevelDescription((int)ctype);
        }

        public static string VocLevelDescription(int status)
        {
            switch (status)
            {
                case (int)VocLevel.Level1: return "Level 1";
                case (int)VocLevel.Level2: return "Level 2";
                case (int)VocLevel.Level3: return "Level 3";
                case (int)VocLevel.Level4: return "Level 4";
                case (int)VocLevel.JobBrokerage: return "Job Brokerage";
                case (int)VocLevel.JobSearchService: return "Job Search Service";
                case (int)VocLevel.NotApplicable: return "Not Applicable";

                default:
                    return "";
            }
        }

        #endregion


        #region ConditionLovDisplay

        public enum ConditionLovDisplay : int
        {
            DOI = 1,
            ReferralDate = 2,
            DOIAndReferralDate = 3,
            ReferralDateAndDOI = 4
        }

        public static string ConditionLovDisplayDescription(ConditionLovDisplay type)
        {
            return ConditionLovDisplayDescription((int)type);
        }

        public static string ConditionLovDisplayDescription(Decimal type)
        {
            return ConditionLovDisplayDescription((int)type);
        }

        public static string ConditionLovDisplayDescription(int type)
        {
            switch (type)
            {
                case (int)ConditionLovDisplay.DOI:
                    return "Date of Injury";
                case (int)ConditionLovDisplay.ReferralDate:
                    return "Referral Date";
                case (int)ConditionLovDisplay.DOIAndReferralDate:
                    return "DOI and Referral Date";
                case (int)ConditionLovDisplay.ReferralDateAndDOI:
                    return "Referral Date and DOI";
                default:
                    return "Unknown";
            }
        }
        #endregion


        public enum OutcomeSendStatus : int
        {
            [EnumResourceAttribute("Created")]
            Created = 1,
            [EnumResourceAttribute("Sent")]
            Sent = 2,
            [EnumResourceAttribute("Received")]
            Received = 3,
            [EnumResourceAttribute("Failed")]
            Failed = 4
        }

        public enum HisoFormStatus : int
        {
            [EnumResourceAttribute("Created")]
            Created = 1,
            [EnumResourceAttribute("Parked")]
            Parked = 2,
            [EnumResourceAttribute("Submitted")]
            Submitted = 3
        }

        #region Options of GST 

        public enum GSTOptions : int
        {
            [EnumResourceAttribute("GST Exempt")]
            Exempt = -1,
            [EnumResourceAttribute("Price Excludes GST")]
            Excludes = 0,
            [EnumResourceAttribute("Price Includes GST")]
            Includes = 1
        }

        #endregion


        #region BodyDiagram Types
        public enum BodyDiagramDrawAs : int
        {
            [EnumResourceAttribute("Free Hand")]
            FreeHand = 0,
            [EnumResourceAttribute("Tick")]
            Tick = 1,
            [EnumResourceAttribute("X Mark")]
            XMark = 2,
            [EnumResourceAttribute("Circle")]
            Circle = 3
        }
        #endregion

        public enum InsurerScheduleType : int
        {
            [EnumResourceAttribute("Insurer")]
            Insurer = 0,
            [EnumResourceAttribute("ACC")]
            ACC = 1,
            [EnumResourceAttribute("GMS")]
            GMS = 2
        }


        public enum WebLimitPeriods : int
        {
            [EnumResourceAttribute("Days")]
            Days = 1,
            [EnumResourceAttribute("Weeks")]
            Weeks = 2,
            [EnumResourceAttribute("Months")]
            Months = 3,
            [EnumResourceAttribute("Years")]
            Years = 4
        }

        public enum ACCLodgementType : int
        {
            [EnumResourceAttribute("Applet")]
            Applet = 0,
            [EnumResourceAttribute("HISO")]
            HISO = 1,
            [EnumResourceAttribute("Api")]
            Api = 2
        }

        public enum ACC47LodgementType : int
        {
            [EnumResourceAttribute("Applet")]
            Applet = 0,
            [EnumResourceAttribute("Api")]
            Api = 1
        }

        public enum Adaptor : int
        {
            CA,
            ESA
        }

        public enum PhysioAppointmentLocation : int
        {
            [EnumResourceAttribute("In Clinic")]
            InClinic = 0,
            [EnumResourceAttribute("Offsite")]
            Offsite = 1,
            [EnumResourceAttribute("Telehealth")]
            Telehealth = 2,
            [EnumResourceAttribute("Telehealth Initial")]
            TelehealthInitial = 3
        }
#endif
    }
}
