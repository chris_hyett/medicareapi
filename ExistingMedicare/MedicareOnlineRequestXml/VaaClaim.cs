﻿using System.Xml;
using System;
using System.Xml.Serialization;
using Gensolve.MedicareOnline.POCO;
using System.Collections.Generic;
// ReSharper disable InconsistentNaming

namespace Gensolve.MedicareOnline.MedicareOnlineRequestXml
{
    [XmlRoot(ElementName = "servicingProvider")]
    public class ServicingProvider
    {
        [XmlAttribute(AttributeName = "providerNum")]
        public string ProviderNum { get; set; }
    }

    [XmlRoot(ElementName = "afterCare")]
    public class Aftercare
    {
        [XmlAttribute(AttributeName = "overrideInd")]
        public string OverrideInd { get; set; }
    }

    [XmlRoot(ElementName = "treatmentLocation")]
    public class TreatmentLocation
    {
        [XmlAttribute(AttributeName = "locationCde")]
        public string LocationCde { get; set; }
        [XmlAttribute(AttributeName = "admittedInd")]
        public string AdmittedInd { get; set; }
    }

    [XmlRoot(ElementName = "service")]
    public class Service
    {
        #region Private Fields

        private Aftercare _aftercare;
        private TreatmentLocation _treatmentLocation;

        #endregion

        #region Attributes

        [XmlAttribute(AttributeName = "itemNum")]
        public string ItemNum { get; set; }

        [XmlAttribute(AttributeName = "accessionDateTime")]
        public string AccessionDateTime { get; set; }

        [XmlAttribute(AttributeName = "collectionDateTime")]
        public string CollectionDateTime { get; set; }

        [XmlAttribute(AttributeName = "scpId")]
        public string ScpId { get; set; }

        [XmlAttribute(AttributeName = "chargeAmount")]
        public string ChargeAmount { get; set; }

        [XmlAttribute(AttributeName = "serviceId")]
        public string ServiceId { get; set; }

        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }

        [XmlAttribute(AttributeName = "accountReferenceNum")]
        public string AccountReferenceNum { get; set; }

        [XmlAttribute(AttributeName = "dateOfService")]
        public string DateOfService { get; set; }

        [XmlAttribute(AttributeName = "distanceKms")]
        public string DistanceKms { get; set; }

        [XmlAttribute(AttributeName = "duplicateServiceOverrideInd")]
        public string DuplicateServiceOverrideInd { get; set; }

        [XmlAttribute(AttributeName = "multipleProcedureOverrideInd")]
        public string MultipleProcedureOverrideInd { get; set; }

        [XmlAttribute(AttributeName = "noOfPatientsSeen")]
        public string NoOfPatientsSeen { get; set; }

        [XmlAttribute(AttributeName = "rule3ExemptInd")]
        public string Rule3ExemptInd { get; set; }

        [XmlAttribute(AttributeName = "selfDeemedCde")]
        public string SelfDeemedCde { get; set; }

        [XmlAttribute(AttributeName = "equipmentId")]
        public string EquipmentId { get; set; }

        [XmlAttribute(AttributeName = "fieldQuantity")]
        public string FieldQuantity { get; set; }

        [XmlAttribute(AttributeName = "fieldQty")]
        public string FieldQty { get; set; }

        [XmlAttribute(AttributeName = "serviceText")]
        public string ServiceText { get; set; }

        [XmlAttribute(AttributeName = "lspNum")]
        public string LspNum { get; set; }

        [XmlAttribute(AttributeName = "timeOfService")]
        public string TimeOfService { get; set; }

        [XmlAttribute(AttributeName = "s4b3ExemptInd")]
        public string S4b3ExemptInd { get; set; }

        [XmlAttribute(AttributeName = "aftercareOverrideInd")]
        public string AftercareOverrideInd { get; set; }

        [XmlAttribute(AttributeName = "dischargeDate")]
        public string DischargeDate { get; set; }

        /// <summary>
        /// Charge Amount for patient Claims
        /// </summary>
        [XmlAttribute(AttributeName = "charge")]
        public string Charge { get; set; }

        [XmlAttribute(AttributeName = "hospitalInd")]
        public string HospitalInd { get; set; }

        /// <summary>
        /// itemNum for patient Claims
        /// </summary>
        [XmlAttribute(AttributeName = "item")]
        public string Item { get; set; }

        /// <summary>
        /// No of patient seen for patient claims
        /// </summary>
        [XmlAttribute(AttributeName = "patientQty")]
        public string PatientQty { get; set; }

        [XmlAttribute(AttributeName = "patientContribAmt")]
        public string PatientContribAmt { get; set; }

        [XmlAttribute(AttributeName = "restrictiveOverrideCde")]
        public string RestrictiveOverrideCde { get; set; }

        [XmlAttribute(AttributeName = "specimenCollectionPointId")]
        public string SpecimenCollectionPointId { get; set; }

        [XmlAttribute(AttributeName = "text")]
        public string Text { get; set; }

        [XmlAttribute(AttributeName = "timeDuration")]
        public string TimeDuration { get; set; }

        [XmlAttribute(AttributeName = "secondDeviceInd")]
        public string SecondDeviceInd { get; set; }

        #endregion

        #region Elements

        [XmlElement(ElementName = "afterCare")]
        public Aftercare Aftercare
        {
            get { return _aftercare ?? (_aftercare = new Aftercare()); }
            set { _aftercare = value; }
        }

        [XmlElement(ElementName = "treatmentLocation")]
        public TreatmentLocation TreatmentLocation
        {
            get { return _treatmentLocation ?? (_treatmentLocation = new TreatmentLocation()); }
            set { _treatmentLocation = value; }
        }

        

        #endregion

        #region Public Methods

        public static Service Create()
        {
            return new Service();
        }

        #endregion
    }
     

    [XmlRoot(ElementName = "identity")]
    public class Identity
    {
        [XmlAttribute(AttributeName = "lastName")]
        public string LastName { get; set; }

        [XmlAttribute(AttributeName = "firstName")]
        public string FirstName { get; set; }

        [XmlAttribute(AttributeName ="secondInitial")]
        public string SecondInitial { get; set; }
    }

    [XmlRoot(ElementName = "alias")]
    public class Alias
    {
        [XmlAttribute(AttributeName = "lastName")]
        public string LastName { get; set; }
        [XmlAttribute(AttributeName = "firstName")]
        public string FirstName { get; set; }
    }

    [XmlRoot(ElementName = "residentialAddress")]
    public class ResidentialAddress
    {
        [XmlAttribute(AttributeName = "postcode")]
        public string Postcode { get; set; }

        [XmlAttribute(AttributeName = "locality")]
        public string Locality { get; set; }
    }

    [XmlRoot(ElementName = "dvaMemebrship")]
    public class DvaMembership
    {
        [XmlAttribute(AttributeName = "memberNum")]
        public string MemberNum { get; set; }
    }

    [XmlRoot(ElementName = "patient")]
    public class Patient
    {
        #region Private Fields

        private ResidentialAddress _residentialAddress;
        private Alias _alias;
        private Identity _identity;
        private DvaMembership _dvaMembership; 

        #endregion

        #region Attributes

        /* Patient Claim attributes */
        [XmlAttribute(AttributeName = "lastName")]
        public string LastName { get; set; }

        [XmlAttribute(AttributeName = "firstName")]
        public string FirstName { get; set; }

        [XmlAttribute(AttributeName = "quotedMedicareCardNum")]
        public string QuotedMedicareCardNum { get; set; }

        [XmlAttribute(AttributeName = "quotedMedicareCardIssueNum")]
        public string QuotedMedicareCardIssueNum { get; set; }

        [XmlAttribute(AttributeName = "quotedSubnumerate")]
        public string QuotedSubnumerate { get; set; }
        /**/

        [XmlAttribute(AttributeName = "gender")]
        public string Gender { get; set; }
        [XmlAttribute(AttributeName = "dateOfBirth")]
        public string DateOfBirth { get; set; } 

        #endregion

        #region Elements

        [XmlElement(ElementName = "identity")]
        public Identity Identity
        {
            get { return _identity ?? (_identity = new Identity()); }
            set { _identity = value; }
        }

        [XmlElement(ElementName = "alias")]
        public Alias Alias
        {
            get { return _alias ?? (_alias = new Alias()); }
            set { _alias = value; }
        }

        [XmlElement(ElementName = "residentialAddress")]
        public ResidentialAddress ResidentialAddress
        {
            get { return _residentialAddress ?? (_residentialAddress = new ResidentialAddress()); }
            set { _residentialAddress = value; }
        }

        [XmlElement(ElementName = "dvaMembership")]
        public DvaMembership DvaMembership
        {
            get { return _dvaMembership ?? (_dvaMembership = new DvaMembership()); }
            set { _dvaMembership = value; }
        }

        #endregion
    }

    [XmlRoot(ElementName = "provider")]
    public class Provider
    {
        [XmlAttribute(AttributeName = "providerNum")]
        public string ProviderNum { get; set; }
    }

    [XmlRoot(ElementName = "referral")]
    public class Referral
    {
        #region Private Fields

        private Provider _provider;

        #endregion

        #region Attributes

        [XmlAttribute(AttributeName = "periodTypeCde")]
        public string PeriodTypeCde { get; set; }

        [XmlAttribute(AttributeName = "period")]
        public string Period { get; set; }

        [XmlAttribute(AttributeName = "dateOfIssue")]
        public string DateOfIssue { get; set; }

        [XmlAttribute(AttributeName = "issuedDate")]
        public string IssuedDate { get; set; }

        [XmlAttribute(AttributeName = "periodType")]
        public string PeriodType { get; set; }

        #endregion

        #region Elements

        [XmlElement(ElementName = "provider")]
        public Provider Provider
        {
            get { return _provider ?? (_provider = new Provider()); }
            set { _provider = value; }
        } 

        #endregion
    }

    [XmlRoot(ElementName = "request")]
    public class Request
    {
        #region Private Fields

        private Provider _provider;

        #endregion

        #region Attributes

        [XmlAttribute(AttributeName = "dateOfIssue")]
        public string DateOfIssue { get; set; }

        #endregion

        #region Elements

        [XmlElement(ElementName = "provider")]
        public Provider Provider
        {
            get { return _provider ?? (_provider = new Provider()); }
            set { _provider = value; }
        } 

        #endregion
    }

    [XmlRoot(ElementName = "clinicalCondition")]
    public class ClinicalCondition
    {
        #region Attributes

        [XmlAttribute(AttributeName = "treatedText")]
        public string TreatedText { get; set; }

        [XmlAttribute(AttributeName = "whiteCardInd")]
        public string WhiteCardInd { get; set; } 

        #endregion
    }

    [XmlRoot(ElementName = "serviceProvider")]
    public class ServiceProvider
    {
        [XmlAttribute(AttributeName = "providerNum")]
        public string ProviderNum { get; set; }
    }  

    [XmlRoot(ElementName = "payeeProvider")]
    public class PayeeProvider
    {
        [XmlAttribute(AttributeName = "providerNum")]
        public string ProviderNum { get; set; }
    }

    [XmlRoot(ElementName = "dvaClaim", Namespace = "http://hic.gov.au/hiconline/dva/version-6")]
    public class DvaClaim : IMedicareOnlineObject
    {
        #region Private Fields

        private ServicingProvider _servicingProvider;
        private PayeeProvider _payeeProvider;
        private List<MedicalEvent> _medicalEvent;

        #endregion

        #region Attributes

        [XmlAttribute(AttributeName = "claimId")]
        public string ClaimId { get; set; }

        [XmlAttribute(AttributeName = "dateOfClaimCertified")]
        public string DateOfClaimCertified { get; set; }

        [XmlAttribute(AttributeName = "claimCertifiedInd")]
        public string ClaimCertifiedInd { get; set; }

        [XmlAttribute(AttributeName = "dateOfAuthorisation")]
        public string DateOfAuthorisation { get; set; }

        [XmlAttribute(AttributeName = "serviceTypeCde")]
        public string ServiceTypeCde { get; set; }

        [XmlAttribute(AttributeName = "ns1", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Ns1 { get; set; }

        #endregion

        #region Elements

        [XmlElement(ElementName = "servicingProvider")]
        public ServicingProvider ServicingProvider
        {
            get { return _servicingProvider ?? (_servicingProvider = new ServicingProvider()); }
            set { _servicingProvider = value; }
        }

        [XmlElement(ElementName = "medicalEvent")]
        public List<MedicalEvent> MedicalEvent
        {
            get { return _medicalEvent ?? (_medicalEvent = new List<MedicalEvent>()); }
            set { _medicalEvent = value; }
        }

        [XmlElement(ElementName = "payeeProvider")]
        public PayeeProvider PayeeProvider
        {
            get { return _payeeProvider ?? (_payeeProvider = new PayeeProvider()); }
            set { _payeeProvider = value; }
        }

        #endregion

        public static DvaClaim Create()
        {
            return new DvaClaim();
        }
    }

    [XmlRoot(ElementName = "vaaClaim", Namespace = "http://hic.gov.au/hiconline/dva/version-6")]
    public class VaaClaim : IMedicareOnlineObject
    {
        private PayeeProvider _payeeProvider;
        private List<MedicalEvent> _medicalEvent;

        [XmlElement(ElementName = "medicalEvent")]
        public List<MedicalEvent> MedicalEvent
        {
            get { return _medicalEvent ?? (_medicalEvent = new List<MedicalEvent>()); }
            set { _medicalEvent = value; }
        }        

        [XmlAttribute(AttributeName = "claimId")]
        public string ClaimId { get; set; }

        [XmlAttribute(AttributeName = "dateOfClaimCertified")]
        public string DateOfClaimCertified { get; set; }

        [XmlAttribute(AttributeName = "claimCertifiedInd")]
        public string ClaimCertifiedInd { get; set; }

        [XmlAttribute(AttributeName = "dateOfAuthorisation")]
        public string DateOfAuthorisation { get; set; }

        [XmlElement(ElementName = "payeeProvider")]
        public PayeeProvider PayeeProvider
        {
            get { return _payeeProvider ??(_payeeProvider=new PayeeProvider()); }
            set { _payeeProvider = value; }
        }

        [XmlAttribute(AttributeName = "ns1", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Ns1 { get; set; }

        [XmlAttribute(AttributeName = "serviceTypeCde")]
        public string ServiceTypeCde { get; set; }

        public static VaaClaim Create()
        {
            return new VaaClaim();
        }
    }
}
