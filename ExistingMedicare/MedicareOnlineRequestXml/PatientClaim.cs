﻿using POCO = Gensolve.MedicareOnline.POCO;
using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming

namespace Gensolve.MedicareOnline.MedicareOnlineRequestXml
{
    [XmlRoot(ElementName = "PatientClaim")]
    public class PatientClaim : IMedicareOnlineObject
    {
        #region Private Fields

        private List<POCO.MedicalEvent> _medicalEvent;
        private Claimant _claimant;
        private Payment _payment;
        
        #endregion
        
        #region Public Methods

        public static PatientClaim Create()
        {
            return new PatientClaim();
        } 

        #endregion

        #region Attributes

        [XmlAttribute(AttributeName = "accountPaidInd")]
        public string AccountPaidInd { get; set; }

        [XmlAttribute(AttributeName = "registrationId")]
        public string RegistrationId { get; set; }

        [XmlAttribute(AttributeName = "lodgementDate")]
        public string LodgementDate { get; set; }

        [XmlAttribute(AttributeName = "lodgementTime")]
        public string LodgementTime { get; set; }
        
        [XmlAttribute(AttributeName = "submissionAuthorityInd")]
        public string SubmissionAuthorityInd { get; set; }

        #endregion

        #region Elements

        [XmlElement(ElementName = "Payment")]
        public Payment Payment
        {
            get { return _payment ?? (_payment = new Payment()); }
            set { _payment = value; }
        }

        [XmlElement(ElementName = "Claimant")]
        public Claimant Claimant
        {
            get { return _claimant ?? (_claimant = new Claimant()); }
            set { _claimant = value; }
        }

        [XmlElement("MedicalEvent")]
        public List<POCO.MedicalEvent> MedicalEvent
        {
            get { return _medicalEvent ?? (_medicalEvent = new List<POCO.MedicalEvent>()); }
            set { _medicalEvent = value; }
        } 

        #endregion
    }

    

    [XmlRoot(ElementName = "Claimant")]
    public class Claimant
    {
        #region Private Fields

        private Address _address;

        #endregion

        #region Elements

        [XmlElement(ElementName = "Address")]
        public Address Address
        {
            get { return _address ?? (_address = new Address()); }
            set { _address = value; }
        } 

        #endregion

        #region Attributes

        [XmlAttribute(AttributeName = "dateOfBirth")]
        public string DateOfBirth { get; set; }

        [XmlAttribute(AttributeName = "lastName")]
        public string LastName { get; set; }

        [XmlAttribute(AttributeName = "firstName")]
        public string FirstName { get; set; }

        [XmlAttribute(AttributeName = "quotedMedicareCardNum")]
        public string QuotedMedicareCardNum { get; set; }

        [XmlAttribute(AttributeName = "quotedMedicareCardIssueNum")]
        public string QuotedMedicareCardIssueNum { get; set; }

        [XmlAttribute(AttributeName = "quotedSubnumerate")]
        public string QuotedSubnumerate { get; set; }

        [XmlAttribute(AttributeName = "phoneNum")]
        public string PhoneNum { get; set; } 

        #endregion

    }

    [XmlRoot(ElementName = "Address")]
    public class Address
    {
        #region Attributes

        [XmlAttribute(AttributeName = "addressLine1")]
        public string AddressLine1 { get; set; }

        [XmlAttribute(AttributeName = "addressLine2")]
        public string AddressLine2 { get; set; }

        [XmlAttribute(AttributeName = "locality")]
        public string Locality { get; set; }

        [XmlAttribute(AttributeName = "postCode")]
        public string PostCode { get; set; } 

        #endregion
    }

    [XmlRoot(ElementName = "Payment")]
    public sealed class Payment
    {
        #region Private Fields
        private PaymentChequeClaimant _paymentChequeClaimant;
        private PaymentEftClaimant _paymentEftClaimant;
        private PaymentChequeProvider _paymentChequeProvider;

        #endregion

        #region Elements     
        [XmlElement(ElementName = "PaymentChequeClaimant")]
        public PaymentChequeClaimant PaymentChequeClaimant
        {
            get { return _paymentChequeClaimant ?? (_paymentChequeClaimant = new PaymentChequeClaimant()); }
            set { _paymentChequeClaimant = value; }
        }

        [XmlElement(ElementName = "PaymentChequeProvider")]
        public PaymentChequeProvider PaymentChequeProvider
        {
            get { return _paymentChequeProvider ?? (_paymentChequeProvider = new PaymentChequeProvider()); }
            set { _paymentChequeProvider = value; }
        }        

        [XmlElement(ElementName = "PaymentEFTClaimant")]
        public PaymentEftClaimant PaymentEftClaimant
        {
            get { return _paymentEftClaimant ?? (_paymentEftClaimant = new PaymentEftClaimant()); }
            set { _paymentEftClaimant = value; }
        }        
        #endregion
    }

    [XmlRoot(ElementName = "PaymentChequeClaimant")]
    public sealed class PaymentChequeClaimant
    {
        #region Attributes
        [XmlAttribute(AttributeName = "include")]
        public string Include { get; set; }
        #endregion
    }

    [XmlRoot(ElementName = "PaymentChequeProvider")]
    public sealed class PaymentChequeProvider
    {
        #region Attributes
        [XmlAttribute(AttributeName = "accountReference")]
        public string AccountReference { get; set; }

        [XmlAttribute(AttributeName = "include")]
        public string Include { get; set; }
        #endregion
    }

    [XmlRoot(ElementName = "PaymentEFTClaimant")]
    public sealed class PaymentEftClaimant
    {
        #region Attributes

        [XmlAttribute(AttributeName = "accountNum")]
        public string AccountNum { get; set; }

        [XmlAttribute(AttributeName = "accountName")]
        public string AccountName { get; set; }

        [XmlAttribute(AttributeName = "bsbCode")]
        public string BSBCode { get; set; }

        [XmlAttribute(AttributeName = "include")]
        public string Include { get; set; }
        #endregion
    }
     
}
