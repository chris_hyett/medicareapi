﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Gensolve.MedicareOnline.MedicareOnlineRequestXml
{
    [XmlRoot(ElementName = "medicalEvent")]
    public class MedicalEvent
    {
        #region Private Fields

        private ClinicalCondition _clinicalCondition;        
        private List<Service> _service;
        private Patient _patient;
        private Referral _referral;
        private Request _request;
        private ServicingProvider _servicingProvider;
        private PayeeProvider _payeeProvider;
        private ServiceProvider _serviceProvider;
        private TreatmentLocation _treatmentLocation;

        #endregion

        #region Attributes

        [XmlAttribute(AttributeName = "timeOfService")]
        public string TimeOfService { get; set; }

        [XmlAttribute(AttributeName = "dateOfService")]
        public string DateOfService { get; set; }

        [XmlAttribute(AttributeName = "serviceTypeCde")]
        public string ServiceTypeCde { get; set; }

        [XmlAttribute(AttributeName = "referralOverrideTypeCde")]
        public string ReferralOverrideTypeCde { get; set; }

        [XmlAttribute(AttributeName = "medicalEventId")]
        public string MedicalEventId { get; set; }

        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }

        [XmlAttribute(AttributeName = "HospitalInd")]
        public string HospitalInd { get; set; }

        [XmlAttribute(AttributeName = "requestOverrideTypeCde")]
        public string RequestOverrideTypeCde { get; set; }

        [XmlAttribute(AttributeName = "medicalEventDate")]
        public string MedicalEventDate { get; set; }

        [XmlAttribute(AttributeName = "referralOverrideType")]
        public string ReferralOverrideType { get; set; }

        [XmlAttribute(AttributeName = "medicalEventTime")]
        public string MedicalEventTime { get; set; }

        [XmlAttribute(AttributeName = "AcceptedDisabilityText")]
        public string AcceptedDisabilityText { get; set; }

        [XmlAttribute(AttributeName = "AcceptedDisabilityInd")]
        public string AcceptedDisabilityInd { get; set; }

        #endregion

        #region Elements

        [XmlElement(ElementName = "servicingProvider", Order = 2)]
        public ServicingProvider ServicingProvider
        {
            get { return _servicingProvider??(_servicingProvider = new ServicingProvider()); }
            set { _servicingProvider = value; }
        }

        [XmlElement(ElementName = "service", Order = 1)]
        public List<Service> Service
        {
            get { return _service??(_service= new List<Service>()); }
            set { _service = value; }
        }

        [XmlElement(ElementName = "treatmentLocation", Order = 5)]
        public TreatmentLocation TreatmentLocation
        {
            get { return _treatmentLocation??(_treatmentLocation = new TreatmentLocation()); }
            set { _treatmentLocation = value; }
        }

        [XmlElement(ElementName = "patient", Order = 3)]
        public Patient Patient
        {
            get { return _patient??(_patient=new Patient()); }
            set { _patient = value; }
        }

        [XmlElement(ElementName = "referral", Order = 4)]
        public Referral Referral
        {
            get { return _referral??(_referral=new Referral()); }
            set { _referral = value; }
        }

        [XmlElement(ElementName = "request", Order = 6)]
        public Request Request
        {
            get { return _request??(_request= new Request()); }
            set { _request = value; }
        }

        [XmlElement(ElementName = "clinicalCondition", Order = 7)]
        public ClinicalCondition ClinicalCondition
        {
            get { return _clinicalCondition ?? (_clinicalCondition = new ClinicalCondition()); }
            set { _clinicalCondition = value; }
        }

        [XmlElement(ElementName = "payeeProvider", Order = 9)]
        public PayeeProvider PayeeProvider
        {
            get { return _payeeProvider??(_payeeProvider = new PayeeProvider()); }
            set { _payeeProvider = value; }
        }

        [XmlElement(ElementName = "serviceProvider", Order = 10)]
        public ServiceProvider ServiceProvider
        {
            get { return _serviceProvider??(_serviceProvider=new ServiceProvider()); }
            set { _serviceProvider = value; }
        }        

        #endregion

        #region Public Methods

        public static MedicalEvent Create()
        {
            return new MedicalEvent();
        } 

        #endregion
    }
}