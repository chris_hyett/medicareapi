﻿using Gensolve.MedicareOnline.Interfaces;

namespace Gensolve.MedicareOnline.MedicareOnlineResponseXml
{
    public class SimpleBoolResponse : IMedicareServerResult
    {
        public bool Result { get; set; }
        public string XmlString { get; set; }
        public string XmlRequest { get; set; }
        public string XmlResponse { get; set; }
        public string TransactionId { get; set; }
        public void LoadXmlString(string xmlString)
        {
            throw new System.NotImplementedException();
        }
    }
}
