﻿using System;
using System.Runtime.Serialization;

namespace Gensolve.MedicareOnline
{
    #region MedicareException
    [Serializable]
    public class MedicareException : Exception
    {
        public int code { get; set; }
        protected MedicareException(SerializationInfo info, StreamingContext context) : base(info, context)
        {

        }
        public MedicareException(string message) : base(message) { }

        public MedicareException(string message, int code) : base(message)
        {
            this.code = code;
        }

        public MedicareException(string message, Exception innerException) : base(message) { }
    }
    #endregion
}