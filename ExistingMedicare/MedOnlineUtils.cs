﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;


namespace Gensolve.MedicareOnline
{
    public class MedOnlineUtils
    {
        public static bool FormatDateIsValid(string value)
        {
            if (string.IsNullOrEmpty(value))
                return true;

            var valid = true;
            try
            {
                var day = short.Parse(value.Substring(0, 2));
                if (day < 0 || day > 31)
                    valid = false;
                var month = short.Parse(value.Substring(2, 2));
                if (month < 0 || month > 12)
                    valid = false;
                var year = short.Parse(value.Substring(4, 4));
                if (year < 1900 || year > 2100)
                    valid = false;
            }
            catch (Exception)
            {
                throw new Exception("Bad format exception only DDMMYYYY allowed.");
            }
            if (!valid)
                throw new Exception("Bad format exception only DDMMYYYY allowed.");
            return true;
        }

        public static bool ValidateMedicareProviderNumber(string providerNumber)
        {
            /*
             * The Medicare provider number comprises:
             *  - six digits (provider stem)
             *  -  a practice location character (one alphanum char)
             *  -  a check-digit (one alpha character)
             */
            var locTable = "0123456789ABCDEFGHJKLMNPQRTUVWXY";
            var checkTable = "YXWTLKJHFBA";
            var weights = new int[] { 3, 5, 8, 4, 2, 1 };
            var re = @"^(\d{5,6})([" + locTable + "])([" + checkTable + "])$";

            providerNumber = Regex.Replace(providerNumber.Trim().ToUpper(), @"/[^\dA-Z]/", "");
            var matches = new List<string>();
            if (pregMatch(new Regex(re, RegexOptions.Compiled), providerNumber, out matches))
            {
                var stem = matches[1];

                // accommodate dropping of leading 0 
                if (stem.Length == 5) { stem = "0" + stem; }

                var location = matches[2];
                var checkDigit = matches[3][0];

                var plv = locTable.IndexOf(location);
                var sum = plv * 6;

                for (int i = 0; i < weights.Length; i++)
                {
                    sum += int.Parse(stem[i].ToString()) * weights[i];
                }

                var checkDigitNumber = sum % 11;

                if (checkDigit == checkTable[checkDigitNumber])
                {
                    return true;
                }
            }
            return false;
        }

        public static bool pregMatch(Regex regex, string input, out List<string> matches)
        {
            var match = regex.Match(input);
            var groups = (from object g in match.Groups select g.ToString()).ToList();

            matches = groups;
            return match.Success;
        }
    }


    public interface IConfigurationManagerSettings
    {       
        string GetAppSettingsValue(string key);
        void LoadErrors();
        string GetErrorMessage(int errorCode);
    }

    public class ConfigurationManagerUtils : IConfigurationManagerSettings
    {
        public Hashtable MedicareErrors;

        public string GetAppSettingsValue(string key)
        {
            System.Configuration.AppSettingsReader confAppSettings = new System.Configuration.AppSettingsReader();
            return (string)(confAppSettings.GetValue(key, typeof(string)));            
        }

        public void LoadErrors()
        {
            MedicareErrors = new Hashtable();
            string strMedErrorList = GetAppSettingsValue("MedicareErrorListPath");

            using (StreamReader sr = new StreamReader(strMedErrorList))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    int i, errorCode;
                    if ((i = line.IndexOf('=')) == -1)
                        continue;
                    string code = line.Substring(0, i);

                    if (Int32.TryParse(code, out errorCode))
                    {
                        if (!MedicareErrors.Contains(errorCode))
                            MedicareErrors.Add(errorCode, line.Substring(i + 1));
                    }
                    else
                    {
                        //Infrastructure.Diagnostics.TraceUtils.WriteLineError("Invalid number for Medicare Error Code", this.GetType());
                    }
                }
            }            
        }

        public string GetErrorMessage(int errorCode)
        {
            if(MedicareErrors == null || MedicareErrors.Count == 0)
                LoadErrors();
            return MedicareErrors[errorCode] != null ? string.Format("{0}:{1}", errorCode, MedicareErrors[errorCode]) : errorCode.ToString();
        }
    }
}