﻿namespace Gensolve.MedicareOnline.Interfaces
{
    public interface IMedicareOnlineVerificationResponseHandler : IMedicareOnlineResponseHandler
    {
        string GenerateReport(IConfigurationManagerSettings config); 
    }
}
