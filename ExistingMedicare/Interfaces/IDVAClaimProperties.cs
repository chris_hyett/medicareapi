﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Gensolve.MedicareOnline
{
    public interface IDvaClaimProperties
    {
        /// <summary>
        /// Filter properties for every type of claim.
        /// </summary>
        /// <returns></returns>
        IList<_PropertyInfo> GetVoucherProperties();

        IList<_PropertyInfo> GetClaimProperties();

        Type GetVoucherType();
    }
}