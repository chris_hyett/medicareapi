﻿using RestSharp;
namespace Gensolve.MedicareOnline.Interfaces
{
    public interface  IMedicareOnlineResponseHandler
    {
        void ProcessResponse(IRestResponse response);

        bool WasOK { get; set; }
    }
}
