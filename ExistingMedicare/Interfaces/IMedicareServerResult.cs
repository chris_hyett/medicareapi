﻿using System;
namespace Gensolve.MedicareOnline.Interfaces
{    
    public interface IMedicareServerResult
    {
        string TransactionId { get; set; }
        string XmlRequest { get; set; }
        string XmlResponse { get; set; }
        void LoadXmlString(string xmlString);
    }

    public interface IReportResponse {

    }
    
    public interface IEsaResponse : IMedicareServerResult {
        
        string Content { get; set; }
        string StatusCode { get; set; }

        //string TransactionId { get; set; }        
    }

    [Serializable]
    public class EsaResponse : IEsaResponse
    {
        public string StatusCode { get; set; }
        public string TransactionId { get; set; }
        public string XmlRequest { get; set; }
        public string XmlResponse { get; set; }

        public EsaResponse()
        {

        }

        public string Content
        {
            get; set;
        }


        public void LoadXmlString(string xmlString)
        {
            throw new NotImplementedException();
        }
    }
}
