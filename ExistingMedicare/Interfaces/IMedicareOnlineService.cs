﻿using Gensolve.MedicareOnline.POCO;
using Gensolve.PhysioData.Common.Medicare;

namespace Gensolve.MedicareOnline.Interfaces
{
    /// <summary>
    /// Defines interface for new service to wrap Medicare Onlines new web services which replace the old adapter 
    /// </summary>
    public interface IMedicareOnlineService
    {
        void OnlinePatientVerification(PatientVerification patientVerification);
        
        void OnlineConcessionVerification(OCEVerification oceVerification);

        void OnlineVeteranVerification(DVAVerification dvaVerification);

        void PatientClaimInteractive(Gensolve.MedicareOnline.MedicareOnlineRequestXml.PatientClaim xmlData, MedicareOnlineClaim medicareOnlineClaim);

        void DirectBillClaim(DirectBillClaim xmlData, MedicareOnlineClaim medicareOnlineClaim);

        // Reminders of methods to be added 
        /*
        void EnterpriseConcessionVerification(EnterpriseConcessionEntitlementVerificationRequest xmlData, IMedicareOnlineEnterpriseSender IMedicareOnlineEnterpriseSender);
        void BulkBillProcessingReport(DPRReportRequest xmlData, IMedicareOnlineEnterpriseSender IMedicareOnlineEnterpriseSender);
        void BulkBillPaymentReport(DPYReportRequest xmlData, IMedicareOnlineEnterpriseSender IMedicareOnlineEnterpriseSender);
        void PatientClaimStoredAndForward(MedicareOnlineRequestXml.PatientClaim xmlData);
        void DvaMedicalPaperlessClaims(DvaClaim xmlData);
        void DvaProcessingReport(DvaReportRequest xmlData, IMedicareOnlineEnterpriseSender IMedicareOnlineEnterpriseSender);
        void DvaPaymentReport(DvaReportRequest xmlData, IMedicareOnlineEnterpriseSender medicareOnlineEnterpriseSender);
        void EnterpriseVeteranVerificationRequest(EnterpriseVeteranVerificationRequest xmlData);
        void VaaClaim(VaaClaim xmlData);
        */
    }
}
