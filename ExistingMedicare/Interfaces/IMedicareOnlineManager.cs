﻿using Gensolve.Utilities;
using System;
using System.Collections.Generic;
using Gensolve.PhysioData.Common.Medicare;

namespace Gensolve.MedicareOnline
{
    public interface IMedicareOnlineManager : IMedicareDll, IPKIeSignatureCore, IProcessingReport, IPaymentReport, IStoreCertificate
    {
        bool HelloMedicareOnline(string foo);
        
        int SendContent(int sessionId, string transmissionType, string passphrase);
        int isReportAvailable(int sessionId);
        int authoriseContent(int sessionId, string hcipassword, IntPtr ptrAtrContent);
        int getContent(int sessionId, IntPtr ptrXMLContent, int bufferSize);
        int createReport(int sessionId, string reportName, string selectionCriteria);
        int createSessionId(string pathstore, string psipass, out int sessionId);
        int createBusinessObjectId(string businessObjectType, int sessionId, out string path);
        int DVALessStrLinedClaimByObjectProperties(int sessionId, DVAPaperLessStrmLinedClaim DVAPlessClaim, bool IsBatching, string hcipassword, string locationId, out string xmlbatch);
        string MOErrorText(int errorCode);
        Results PatientClaimByObjectProperties(int sessionId, PatientClaim patientClaim, bool Isbatching, string hcipassword, string locationId, out string xmlbatch, out StatementAndBenefitReport stmReport, out ClaimAssesmentReport cassreport, out bool succesClaim, out string transactionId);
        int DVAAlliedHealthClaimByObjectProperties(int sessionId, DVAAlliedHealthClaim DVAAlliedClaim, bool Isbatching, string hcipassword, string locationId, out string xmlbatch);
        int DvaAlliedHealthClaimByObjectProperties(int sessionId, DvaAlliedHealthClaim32 alliedClaim, string locationId, out string transactionId);
        int PatientClaimByObjectProperties(int sessionId, PatientClaim32 patientClaim, string locationId, out string transactionId);
        int DvaPaperlessClaimByObjectProperties(int sessionId, DvaPaperlessClaim32 paperlessClaim32, string locationId, out string transactionId);
        int sendBatchTransmissionContent(int sessionId, string locationId, string xmlstr, int transmissionType, out string dateStr);
        int BulkBillClaimByObjectProperties(int sessionId, BillClaim bClaim, bool isbatching, string hcipassword, string locationId, out string xmlbatch, out string transactionId);
        int BulkBillClaimByObjectProperties(int sessionId, BulkBillClaim32 bbClaim, string locationId, out string transactionId);
        int CreateBulkBillClaim(int sessionId, DateTime authorisationDate, TimeZone tz);
        Results DoDVAVerification(string storepath, DVAVerification dvaObject);
        string DoOnlineEntitlementClientVerification(int sessionId, OCEVerification oceVerification);

        string DoOnlinePatientVerification(int sessionId, PatientVerification pv);
        int createSession(string pathstore, string locationId);
    }

    public interface IProcessingReport : IDisposable {
        ProcessingReport CreateDVAProcessingReport(MedicareAttributes medicareAttributes, string payeeProviderNum, string pmsClaimId, string dateOfTransmission);
        ProcessingReport CreateBBProcessingReport(MedicareAttributes medicareAttributes, string payeeProviderNum, string pmsClaimId, string dateOfTransmission);
        ProcessingReport CreateBBHicapsReport(int sessionId, string payeeProviderNum, List<string> transactionIds, string dateOfTransmission);        
    }

    public interface IPaymentReport : IDisposable
    {
        PaymentReportTransaction CreateBBHicapsPaymentReport(int sessionId, string payeeProviderNum, List<string> transactionIds, string dateOfTransmission);
        PaymentReportTransaction CreateBBPaymentReport(MedicareAttributes medicareAttributes, string payeeProviderNum, string pmsClaimId, string dateOfTransmission);
        PaymentReportTransaction CreateDVAPaymentReport(MedicareAttributes medicareAttributes, string payeeProviderNum, string pmsClaimId, string dateOfTransmission);

    }

    public interface IMedicareDll : IDisposable
    {
        int loadEasyclaim();
        int createSessionEasyclaim(IntPtr ptr, string pathstore, string psipass);
        int getNextReportRow(int sessionId);
        int setBusinessObjectElement(int sessionId, string path, string name, string elementValue);
        int setBusinessObjectElement(int sessionId, string path, string name, decimal elementValue);
        int CreateBusinessObject(int sessionId, string type, string parentPath, string objectId, int bufferSize);
        int getUniqueId(int sessionId, IntPtr ptrUniqueId, int bufferSize);
        int sendTransmission(int sessionId);
        int addContent(int sessionId, string xmlstr);
        int setTransmissionElement(int sessionId, string elementName, string timeStr);
        int resetSession(int sessionId);
        int setSessionElement(int sessionId, string key, string value);
        int createTransmission(int sessionId, string TransmissionContent);        
        string GetReportElement(int sessionId, string path, int size);
        int acceptContent(int sessionId, string HciPassphrase);
    }

    public interface IPKIeSignatureCore {
        string CreatePSIStore(string storepath);
        string ImportPSIP12(byte[] _file, string storepath, string passpsicrt);
        string ImportPSICertificate(byte[] _file, string storepath);
        void PsiRemoveCertificates(string storepath);
        List<CertificateAttr> PsiGetCertificatesAtr(string storepath);
        void ReleaseSession();

    }

    public interface IStoreCertificate {
        void CreateFolder(string folderstore);
        bool FileExists(string storepath);
        string GetStoragePath();
        string GetPassword();

        string GetEnvironmentType();
    }
    
    [Serializable]
    public class MedicareAttributes
    {
        public string environmentType;
        public string storePath;
        public string LocationId;

        public MedicareAttributes(string storepath, string locationId)
        {
            storePath = storepath;
            LocationId = locationId;
        }
    }
}
