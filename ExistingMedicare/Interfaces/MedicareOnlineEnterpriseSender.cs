﻿using System.Threading.Tasks;

namespace Gensolve.MedicareOnline.Interfaces
{
    public interface IMedicareOnlineEnterpriseSender
    {
        Task<IEsaResponse> MakePostApiCallTransaction(string xmlData, string medicareRemoteContentType);

        Task<IEsaResponse> MakePostApiCallTransactionWithRemoteActionHeader(string xmlData, string medicareRemoteContentType,
            string remoteAction);
        
        Task<IEsaResponse> MakePostApiCallTransactionWithRemoteActionHeaderWithResponse(string xmlData, string medicareRemoteContentType, string remoteAction);
    }
}
