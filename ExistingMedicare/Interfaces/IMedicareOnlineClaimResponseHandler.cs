﻿namespace Gensolve.MedicareOnline.Interfaces
{
    public interface IMedicareOnlineClaimResponseHandler : IMedicareOnlineResponseHandler
    {
        IEsaResponse GenerateResult();
    }
}
