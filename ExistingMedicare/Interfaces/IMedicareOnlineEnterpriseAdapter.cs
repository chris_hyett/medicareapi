﻿using Gensolve.MedicareOnline.MedicareOnlineRequestXml;
using Gensolve.MedicareOnline.POCO;

namespace Gensolve.MedicareOnline.Interfaces
{
    public interface IMedicareOnlineEnterpriseAdapter
    {
        IMedicareServerResult OnlineConcessionVerification(OnlineConcessionEntitlementVerificationRequest xmlData, IMedicareOnlineEnterpriseSender IMedicareOnlineEnterpriseSender);
        IMedicareServerResult EnterpriseConcessionVerification(EnterpriseConcessionEntitlementVerificationRequest xmlData, IMedicareOnlineEnterpriseSender IMedicareOnlineEnterpriseSender);
        //IMedicareServerResult GeneralImmunisation(ProviderClaim xmlData, IMedicareOnlineEnterpriseSender IMedicareOnlineEnterpriseSender);
        //IMedicareServerResult HistoryImmunisation(ProviderClaim xmlData, IMedicareOnlineEnterpriseSender IMedicareOnlineEnterpriseSender);
        //IMedicareServerResult NextDueDate(ProviderClaim xmlData, IMedicareOnlineEnterpriseSender IMedicareOnlineEnterpriseSender);
        IMedicareServerResult DirectBillClaim(DirectBillClaim xmlData, IMedicareOnlineEnterpriseSender IMedicareOnlineEnterpriseSender);
        IMedicareServerResult BulkBillProcessingReport(DPRReportRequest xmlData, IMedicareOnlineEnterpriseSender IMedicareOnlineEnterpriseSender);
        IMedicareServerResult BulkBillPaymentReport(DPYReportRequest xmlData, IMedicareOnlineEnterpriseSender IMedicareOnlineEnterpriseSender);
        IMedicareServerResult PatientClaimInteractive(MedicareOnlineRequestXml.PatientClaim xmlData);
        IMedicareServerResult PatientClaimStoredAndForward(MedicareOnlineRequestXml.PatientClaim xmlData);
        IMedicareServerResult DvaMedicalPaperlessClaims(DvaClaim xmlData);
        IMedicareServerResult DvaProcessingReport(DvaReportRequest xmlData, IMedicareOnlineEnterpriseSender IMedicareOnlineEnterpriseSender);
        IMedicareServerResult DvaPaymentReport(DvaReportRequest xmlData, IMedicareOnlineEnterpriseSender medicareOnlineEnterpriseSender);
        IMedicareServerResult OnlineVeteranVerification(OnlineVeteranVerificationRequest xmlData);
        IMedicareServerResult EnterpriseVeteranVerificationRequest(EnterpriseVeteranVerificationRequest xmlData);
        IMedicareServerResult VaaClaim(VaaClaim xmlData);
        IMedicareServerResult OnlinePatientVerification(OnlinePatientVerificationRequest opv, IMedicareOnlineEnterpriseSender IMedicareOnlineEnterpriseSender);
    }
}
