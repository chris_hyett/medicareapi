﻿using Gensolve.MedicareOnline;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using Gensolve.MedicareOnline.ObjectClasses;
using System.Runtime.InteropServices;

namespace Gensolve.PhysioData.Common.Medicare
{
    public interface IDvaClaimProperties
    {
        /// <summary>
        /// Filter properties for every type of claim.
        /// </summary>
        /// <returns></returns>
        IList<_PropertyInfo> GetVoucherProperties();

        IList<_PropertyInfo> GetClaimProperties();

        Type GetVoucherType();
    }

    public interface IReferralOverrideDetails
    {
        string ReferralOverrideTypeCde { get; set; }
    }

    public interface IRequestingDetails
    {
        string RequestingProviderNum { get; set; }
        string RequestTypeCde { get; set; }
        string RequestIssueDate { get; set; }
    }

    public interface IClaimantDetails
    {
        string ClaimantAddressLine1 { get; set; }
        string ClaimantAddressLine2 { get; set; }
        string ClaimantAddressLocality { get; set; }
        string ClaimantAddressPostcode { get; set; }
        string ClaimantDateOfBirth { get; set; }
        string ClaimantFamilyName { get; set; }
        string ClaimantFirstName { get; set; }
        string ClaimantMedicareCardNum { get; set; }
        string ClaimantReferenceNum { get; set; }
    }

    public interface IPatientDetails {

        string PatientDateOfBirth { get; set; }
        string PatientAliasFamilyName { get; set; }
        string PatientAliasFirstName { get; set; }
        string PatientFamilyName { get; set; }
        string PatientFirstName { get; set; }
        string PatientMedicareCardNum { get; set; }
        string PatientReferenceNum { get; set; }
    }

    public interface IReferralDetails : IRequestingDetails, IReferralOverrideDetails
    {
        string ReferringProviderNum { get; set; }
        string ReferralIssueDate { get; set; }
        string ReferralPeriodTypeCde { get; set; }
        string ReferralDateFrom { get; set; }
        string ReferralPeriod { get; set; }
    }
    #region DVA Paperless
    [Serializable]
    public class DvaPaperlessClaim : DvaClaim
    {
        public override string BusinessObjectType
        {
            get { return "BusinessObjectTypes.DVAClaimRequest"; }
            set { }
        }

        public new List<DvaPaperlessService> Services;

        public new List<DvaPaperlessVoucher> Vouchers;

        public string ServiceTypeCde { get; set; }
        public string ServicingProviderNum { get; set; }

        public void AddVoucher(DvaPaperlessVoucher bbVoucher)
        {
            if (Vouchers == null)
                Vouchers = new List<DvaPaperlessVoucher>();
            Vouchers.Add(bbVoucher);
        }

        public void AddService(DvaPaperlessService bbService)
        {
            if (Services == null)
                Services = new List<DvaPaperlessService>();
            Services.Add(bbService);
        }
    }

    [Serializable]
    public class DvaPaperlessVoucher : DvaVoucher
    {
        public DvaPaperlessVoucher(string objectId) : base(objectId)
        {
        }

        public string TimeOfService { get; set; }
        public string TreatmentLocationCde { get; set; }
        public string TransactionId { get; set; }
        public new string RequestingProviderNum { get { return string.Empty; } set { } }
        public new string RequestIssueDate { get { return string.Empty; } set { } }
    }
    [Serializable]
    public class DvaPaperlessService : DvaService
    {
        public DvaPaperlessService(string parentVoucherId) : base(parentVoucherId)
        {
        }

        public string ObjectId;
        public string AfterCareOverrideInd { get; set; }
        public string CollectionDateTime { get; set; }
        public string FieldQuantity { get; set; }
        public string LSPNum { get; set; }
        public string Rule3ExemptInd { get; set; }
        public string S4b3ExemptInd { get; set; }
        public string SCPId { get; set; }
    }

    #endregion

    #region DVA Claim

    [Serializable]
    public class DvaClaim : ClaimHeader
    {
        public DvaClaim()
        {
            this.PathOfParent = string.Empty;
            this.ObjectId = string.Empty;
        }
        public override string BusinessObjectType {
            get { return "BusinessObjectTypes.VAAClaimRequest"; }
            set { }
        }

        private string _authorisationDate;

        public string AuthorisationDate
        {
            get { return _authorisationDate; }
            set { if (FormatDateIsValid(value)) _authorisationDate = value; }
        }
        public string PayeeProviderNum { get; set; }
        public string ClaimCertifiedInd { get { return "Y"; } set {} }
        public string ClaimCertifiedDate { get; set; }
        public string PmsClaimId { get; set; }

        public List<DvaVoucher> Vouchers { get; private set; }
        public List<DvaService> Services { get; private set; }
        public string TransactionId { get; set; }

        public void AddVoucher(DvaVoucher bbVoucher)
        {
            if (Vouchers == null)
                Vouchers = new List<DvaVoucher>();
            Vouchers.Add(bbVoucher);
        }

        public void AddService(DvaService bbService)
        {
            if (Services == null)
                Services = new List<DvaService>();
            Services.Add(bbService);
        }
    }

    [Serializable]
    public class DvaService : MedOnlineService
    {
        public DvaService(string parentVoucherId) : base(parentVoucherId)
        {
        }

        public string AccountReferenceNum { get; set; }
        public string AdmissionDate { get; set; }
        public decimal ChargeAmount { get; set; }
        public string DateOfService { get; set; }
        public string DischargeDate { get; set; }
        public string DistanceKms { get; set; }
        public string DuplicateServiceOverrideInd { get; set; }
        public string EquipmentId { get; set; }
        public string ItemNum { get; set; }
        public string MultipleProcedureOverrideInd { get; set; }
        public decimal NoOfPatientsSeen { get; set; }
        public string NumberOfTeeth { get; set; }
        public string OpticalScript { get; set; }
        public string SecondDeviceInd { get { return (!string.IsNullOrWhiteSpace(DistanceKms)) ? string.Empty : "N"; } set { } }
        public string SelfDeemedCde { get; set; }
        public string ServiceId { get; set; }
        public string ServiceText { get; set; }
        public string TimeDuration { get; set; }
        public string TimeOfService { get; set; }
        public string ToothNum { get; set; }
        public string UpperLowerJawCde { get; set; }
    }

    [Serializable]
    public class DvaVoucher : IReferralDetails
    {
        private string _referralPeriodTypeCde;

        public DvaVoucher(string objectId)
        {
            ObjectId = objectId;
        }
        public string ObjectId { get; private set; }
        public string AcceptedDisabilityInd { get; set; }
        public string AcceptedDisabilityText { get; set; }
        public string HospitalInd { get; set; }
        public string PatientAddressLocality { get; set; }
        public string PatientAddressPostcode { get; set; }
        public string PatientAliasFirstName { get; set; }
        public string PatientAliasFamilyName { get; set; }
        public string PatientDateOfBirth { get; set; }
        public string PatientFamilyName { get; set; }
        public string PatientFirstName { get; set; }
        public string PatientGender { get; set; }
        public string PatientSecondInitial { get; set; }
        public string ReferralIssueDate { get; set; }
        public string ReferralOverrideTypeCde { get; set; }
        public string ReferralPeriod { get; set; }
        public string ReferralDateFrom { get; set; }
        public string DateOfService { get; set; }


        public string ReferralPeriodTypeCde
        {
            get { return _referralPeriodTypeCde; }
            set { if(value.Length>1)
                    throw new Exception("Length can't be more than one character");
                _referralPeriodTypeCde = value; }
        }

        public string ReferringProviderNum { get; set; }
        public string RequestingProviderNum { get; set; }
        public string RequestIssueDate { get; set; }
        public string RequestOverrideTypeCde { get; set; }
        public string ServiceTypeCde { get { return "J"; } set { } }
        public string ServicingProviderNum { get; set; }
        public string VeteranFileNum { get; set; }
        public decimal VoucherId { get { return Decimal.Parse(ObjectId); } set {} }

        /// <summary>
        /// not necesarry in Dva
        /// </summary>
        public string RequestTypeCde
        {
            get { return string.Empty; }

            set
            {
                
            }
        }        
    }

    #endregion

    [Serializable]
    public class ClaimHeader
    {
        public virtual string BusinessObjectType { get; set; }
        public string PathOfParent { get; set; }
        public string ObjectId { get; set; }

        protected static bool FormatDateIsValid(string value)
        {
            if (string.IsNullOrEmpty(value))
                return true;

            var valid = true;
            try
            {
                var day = short.Parse(value.Substring(0, 2));
                if (day < 0 || day > 31)
                    valid = false;
                var month = short.Parse(value.Substring(2, 2));
                if (month < 0 || month > 12)
                    valid = false;
                var year = short.Parse(value.Substring(4, 4));
                if (year < 1900 || year > 2100)
                    valid = false;
            }
            catch (Exception)
            {
                throw new Exception("Bad format exception only DDMMYYYY allowed.");
            }
            if (!valid)
                throw new Exception("Bad format exception only DDMMYYYY allowed.");
            return true;
        }
    }

    #region Patient Claim
    [Serializable]
    public class PatientClaim32 : IClaimHeader, IPatientDetails, IClaimantDetails
    {
        public PatientClaim32()
        {
            this.PathOfParent = string.Empty;
            this.ObjectId = string.Empty;
        }

        #region Header Data Elements

        public string BusinessObjectType
        {
            get { return "BusinessObjectTypes.StoreForwardPatientClaim"; }
            set { }

        }
        private string _payeeProviderNum;
        public string ServicingProviderNum { get; set; }
        public string PayeeProviderNum { get { return ServicingProviderNum == _payeeProviderNum ? string.Empty : _payeeProviderNum; } set { _payeeProviderNum = value; } }
        public string ServiceTypeCde { get; set; }
        public string TransactionId { get; set; }

        public List<PatientClaimVoucher> Vouchers { get; private set; }
        public List<PatientClaimService> Services { get; private set; }
        public string AccountPaidInd { get; set; }
        public string ClaimSubmissionAuthorised { get { return "Y"; } }
        public string DateOfLodgement { get; set; }
        public string PatientDateOfBirth { get; set; }

        public string PatientFamilyName
        {
            get;

            set;
        }

        public string PatientFirstName
        {
            get;

            set;
        }

        public string PatientMedicareCardNum
        {
            get;

            set;
        }

        public string PatientReferenceNum
        {
            get;

            set;
        }
        public string PatientAliasFirstName { get; set; }
        public string PatientAliasFamilyName { get; set; }
        public string TimeOfLodgement { get; set; }

        public string ClaimantAddressLine1
        {
            get;set;
        }

        public string ClaimantAddressLine2
        {
            get;set;
        }

        public string ClaimantAddressLocality
        {
            get;set;
        }

        public string ClaimantAddressPostcode
        {
            get;set;
        }

        public string ClaimantDateOfBirth
        {
            get;set;
        }

        public string ClaimantFamilyName
        {
            get;set;
        }

        public string ClaimantFirstName
        {
            get;set;
        }

        public string ClaimantMedicareCardNum
        {
            get;set;
        }

        public string ClaimantReferenceNum
        {
            get;set;
        }

        public string AccountReferenceId { get; set; }
        public string BankAccountName { get; set; }
        public string BankAccountNum { get; set; }
        public string BsbCode { get; set; }
        public string ContactPhoneNum { get; set; }

        public string PathOfParent
        {
            get;set;
        }

        public string ObjectId
        {
            get;set;
        }

        public void AddVoucher(PatientClaimVoucher pcVoucher)
        {
            if (Vouchers == null)
                Vouchers = new List<PatientClaimVoucher>();
            Vouchers.Add(pcVoucher);
        }

        public void AddService(PatientClaimService pcService)
        {
            if (Services == null)
                Services = new List<PatientClaimService>();
            Services.Add(pcService);
        }

        public bool Validate()
        {
            var isValid = true;
            //ReferringProviderNum, ReferralIssueDate, and ReferralPeriodTypeCde 
            foreach (var voucher in Vouchers)
            {
                isValid &= !string.IsNullOrEmpty(voucher.ReferralIssueDate);
                isValid &= !string.IsNullOrEmpty(voucher.ReferringProviderNum);
                isValid &= !string.IsNullOrEmpty(voucher.ReferralPeriodTypeCde);
            }
            return isValid;
        }

        public string BSBCode { set; get; }
        public string ServicingProviderName { set; get; }
        #endregion

    }
    [Serializable]
    public class PatientClaimVoucher : IReferralDetails
    {
        public string ObjectId { get; set; }

        public PatientClaimVoucher(string ObjectId)
        {
            this.ObjectId = ObjectId;
        }

        public string BusinessObjectType { get { return "Voucher"; } set { } }
        public string DateOfService { get; set; }

        public string ReferralDateFrom
        {
            get; set;
            
        }

        public string ReferralIssueDate
        {
            get;set;
        }

        public string ReferralOverrideTypeCde
        {
            get;set;
        }

        public string ReferralPeriod
        {
            get;set;
        }

        public string ReferralPeriodTypeCde
        {
            get; set;
        }

        public string ReferringProviderNum
        {
            get; set;
        }

        public string RequestingProviderNum
        {
            get; set;
        }

        public string RequestIssueDate
        {
            get; set;
        }

        public string RequestTypeCde
        {
            get; set;
        }

        public string TimeOfService { get; set; }
    }
    [Serializable]

    public class PatientClaimService : MedOnlineService, IClaimHeader
    {
        public PatientClaimService(string parentVoucherId) : base(parentVoucherId)
        {
            
        }

        public string ObjectId { get; set; }
        
        public string AccessionDateTime { get; set; }
        public string AfterCareOverrideInd { get; set; }
        public string CollectionDateTime { get; set; }
        public string ChargeAmount { get; set; }
        public string DuplicateServiceOverrideInd { get; set; }
        public string EquipmentId { get; set; }
        public string FieldQuantity { get; set; }
        public string HospitalInd { get; set; }
        public string ItemNum { get; set; }
        public string SCPId { get; set; }
        public string LSPNum { get; set; }
        public string MultipleProcedureOverrideInd { get; set; }
        public string NoOfPatientsSeen { get; set; }
        public string PatientContribAmt { get; set; }
        public string RestrictiveOverrideCde { get; set; }
        public string Rule3ExemptInd { get; set; }
        public string S4b3ExemptInd { get; set; }
        public string SelfDeemedCde { get; set; }
        public string ServiceText { get; set; }
        public string TimeDuration { get; set; }

        public string TransactionId
        {
            get;set;
        }

        public string BusinessObjectType
        {
            get { return "Service"; } set { }
        }

        public string PathOfParent { get { return "./" + ParentVoucherId; } set { } }
    }
    #endregion

    #region Bulk Bill Claim
    [Serializable]
    public class BulkBillClaim : ClaimHeader
    {
        public BulkBillClaim()
        {
            this.PathOfParent = string.Empty;
            this.ObjectId = string.Empty;
        }
        private string _authorisationDate;


        #region Header Data Elements

        public override string BusinessObjectType
        {
            get { return "BusinessObjectTypes.DirectBillClaim"; }
            set { }

        }
        public string AuthorisationDate
        {
            get { return _authorisationDate; }
            set { if (FormatDateIsValid(value)) _authorisationDate = value; }
        }

        public string PmsClaimId { get; set; }
        public string ServicingProviderNum { get; set; }
        public string PayeeProviderNum { get; set; }
        public string ServiceTypeCde { get; set; }
        public string TransactionId { get; set; }

        public List<BulkBillVoucher> Vouchers { get; private set; }
        public List<BulkBillService> Services { get; private set; }

        public void AddVoucher(BulkBillVoucher bbVoucher)
        {
            if (Vouchers == null)
                Vouchers = new List<BulkBillVoucher>();
            Vouchers.Add(bbVoucher);
        }

        public void AddService(BulkBillService bbService)
        {
            if (Services == null)
                Services = new List<BulkBillService>();
            Services.Add(bbService);
        }

        public bool Validate()
        {
            var isValid = true;
            //ReferringProviderNum, ReferralIssueDate, and ReferralPeriodTypeCde 
            foreach (var voucher in Vouchers)
            {
                isValid &= !string.IsNullOrEmpty(voucher.ReferralIssueDate);
                isValid &= !string.IsNullOrEmpty(voucher.ReferringProviderNum);
                isValid &= !string.IsNullOrEmpty(voucher.ReferralPeriodTypeCde);
            }
            return isValid;
        }

        #endregion

    }
        
    [Serializable]
    public class BulkBillVoucher : IReferralDetails, IPatientDetails
    {
        #region Fields
        private string _referralOverrideTypeCde;
        private string _patientDateOfBirth;

        private string _referralIssueDate;
        private string _requestIssueDate;
        private string _referringProviderNum;
        private string _referralPeriodTypeCde;
        private string _requestingProviderNum;
        private string _requestTypeCde;
        #endregion

        public BulkBillVoucher(string objectId)
        {
            ObjectId = objectId;
        }

        public string BusinessObjectType { get { return "Voucher"; } }

        public string PathOfParent { get { return "."; } }

        public string ObjectId { get; private set; }

        public string BenefitAssignmentAuthorised { get { return "Y"; } }

        /// <summary>
        /// date
        /// </summary>
        public string PatientDateOfBirth
        {
            get { return ReturnDateInMedicareFormat(_patientDateOfBirth); }
            set { if (FormatDateIsValid(value)) _patientDateOfBirth = value; }
        }

        public string PatientFamilyName { get; set; }

        public string PatientFirstName { get; set; }

        public string PatientMedicareCardNum { get; set; }

        public string PatientReferenceNum { get; set; }

        public string DateOfService { get; set; }

        #region Referral Override 1 of 3
        public string ReferralOverrideTypeCde
        {
            get { return _referralOverrideTypeCde; }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    ClearReferringDetails();
                    ClearRequestsDetails();
                }
                _referralOverrideTypeCde = value;
            }
        }
        #endregion

        #region Referral Items 2 of 3
        public string ReferringProviderNum
        {
            get { return _referringProviderNum; }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    ClearOverrideCodeDetails();
                    ClearRequestsDetails();
                }
                _referringProviderNum = value;
            }
        }

        public string ReferralIssueDate
        {
            get { return ReturnDateInMedicareFormat(_referralIssueDate); }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    ClearOverrideCodeDetails();
                    ClearRequestsDetails();
                }
                if (FormatDateIsValid(value)) _referralIssueDate = value;
            }
        }

        public string ReferralPeriodTypeCde
        {
            get { return _referralPeriodTypeCde; }
            set
            {
                if (value.Length > 1)
                    throw new Exception("Length can't be more than one character");

                if (!string.IsNullOrWhiteSpace(value))
                {
                    ClearOverrideCodeDetails();
                    ClearRequestsDetails();
                }
                _referralPeriodTypeCde = value;
            }
        }
        #endregion

        #region Requesting Items 3 of 3
        public string RequestingProviderNum
        {
            get { return _requestingProviderNum; }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    ClearOverrideCodeDetails();
                    ClearReferringDetails();
                }
                _requestingProviderNum = value;
            }
        }

        public string RequestIssueDate
        {
            get { return ReturnDateInMedicareFormat(_requestIssueDate); }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    ClearOverrideCodeDetails();
                    ClearReferringDetails();
                }
                if (!FormatDateIsValid(value)) _requestIssueDate = value;
            }
        }

        public string RequestTypeCde
        {
            get { return _requestTypeCde; }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    ClearOverrideCodeDetails();
                    ClearReferringDetails();
                }
                _requestTypeCde = value;
            }
        }

        public string ReferralDateFrom
        {
            get; set;
        }

        public string ReferralPeriod
        {
            get;set;
        }

        public string PatientAliasFamilyName
        {
            get;set;
        }

        public string PatientAliasFirstName
        {
            get; set;
        }
        #endregion

        #region Private Methods

        private void ClearRequestsDetails()
        {
            _requestIssueDate = String.Empty;
            _requestTypeCde = String.Empty;
            _requestingProviderNum = String.Empty;
        }

        private void ClearReferringDetails()
        {
            _referralIssueDate = String.Empty;
            _referralPeriodTypeCde = String.Empty;
            _referringProviderNum = String.Empty;
        }

        private void ClearOverrideCodeDetails()
        {
            _referralPeriodTypeCde = String.Empty;
        }

        private string ReturnDateInMedicareFormat(string value)
        {
            
            return value == null ? String.Empty : (value.Length > 7 ? value.Substring(0, 8) : string.Empty);
        }

        private static bool FormatDateIsValid(string value)
        {
            if (string.IsNullOrEmpty(value))
                return true;

            var valid = true;
            try
            {
                var day = short.Parse(value.Substring(0, 2));
                if (day < 0 || day > 31)
                    valid = false;
                var month = short.Parse(value.Substring(2, 2));
                if (month < 0 || month > 12)
                    valid = false;
                var year = short.Parse(value.Substring(4, 4));
                if (year < 1900 || year > 2100)
                    valid = false;
            }
            catch (Exception)
            {
                throw new Exception("Bad format exception only DDMMYYYY allowed.");
            }
            if (!valid)
                throw new Exception("Bad format exception only DDMMYYYY allowed.");
            return true;
        }
        #endregion
    }

    [Serializable]
    public class BulkBillService : MedOnlineService
    {
        #region Fields

        private string _accessionDateTime;
        private string _collectionDateTime;
        private string _dateOfService;
        #endregion

        public BulkBillService(string parentVoucherId) : base(parentVoucherId)
        {
            
        }

        /// <summary>
        /// date
        /// </summary>
        public string DateOfService
        {
            get { return ReturnDateInMedicareFormat(_dateOfService); }
            set { if (FormatDateIsValid(value)) _dateOfService = value; }
        }

        public string BusinessObjectType { get { return "Service"; } }
        public string PathOfParent { get { return "./" + ParentVoucherId; } }
        public string ObjectId { get; set; }

        /// <summary>
        /// DDMMYYYYHHMM or DDMMYYYY HHMMSS
        /// </summary>
        public string AccessionDateTime
        {
            get { return _accessionDateTime; }
            set { _accessionDateTime = value; }
        }

        public string AfterCareOverrideInd { get; set; }

        public decimal ChargeAmount { get; set; }

        /// <summary>
        /// DDMMYYYYHHMM or DDMMYYYY HHMMSS
        /// </summary>
        public string CollectionDateTime
        {
            get { return _collectionDateTime; }
            set { _collectionDateTime = value; }
        }

        public string DuplicateServiceOverrideInd { get; set; }
        public short EquipmentId { get; set; }
        public short FieldQuantity { get; set; }
        public string HospitalInd { get; set; }
        public string ItemNum { get; set; }
        public decimal LSPNum { get; set; }
        public string MultipleProcedureOverrideInd { get; set; }
        public short NoOfPatientsSeen { get; set; }
        public string RestrictiveOverrideCde { get; set; }
        public string Rule3ExemptInd { get; set; }
        public string S4b3ExemptInd { get; set; }
        public string SelfDeemedCde { get; set; }
        public string ServiceText { get; set; }
        public short TimeDuration { get; set; }

        private string ReturnDateInMedicareFormat(string value)
        {
            return value.Length > 7 ? value.Substring(0, 8) : string.Empty;
        }

        private static bool FormatDateIsValid(string value)
        {
            if (string.IsNullOrEmpty(value))
                return true;

            var valid = true;
            try
            {
                var day = short.Parse(value.Substring(0, 2));
                if (day < 0 || day > 31)
                    valid = false;
                var month = short.Parse(value.Substring(2, 2));
                if (month < 0 || month > 12)
                    valid = false;
                var year = short.Parse(value.Substring(4, 4));
                if (year < 1900 || year > 2100)
                    valid = false;
            }
            catch (Exception)
            {
                throw new Exception("Bad format exception only DDMMYYYY allowed.");
            }
            if (!valid)
                throw new Exception("Bad format exception only DDMMYYYY allowed.");
            return true;
        }

    }

    [Serializable]
    public class MedOnlineService
    {
        public string ParentVoucherId;

        public MedOnlineService(string parentVoucherId)
        {
            ParentVoucherId = parentVoucherId;
        }
    }

    #endregion




    [KnownType(typeof(CAMedicareOnlineClaimObject))]
    [KnownType(typeof(ESAMedicareOnlineClaimObject))]
    [KnownType(typeof(MedicareOnlineClaimObject))]
    public abstract class MedicareOnlineClaim
    {
        public TimeZoneInfo SiteTzi { get; set; }
        public virtual DateTime DateOfTransmission { get; set; }
        public virtual DateTime AuthorisationDate { get; set; }
        public virtual DateTime ClaimCertifiedDate { get; set; }
        public virtual PhysioEnums.Adaptor AdaptorType { get; set; }

        public decimal Id;
        public bool IsConcession;
        public string providerId;
        public string patientId;
        public string PatientFamilyName;
        public string PatientFirstName;
        public string PatientAliasFamilyName;
        public string PatientAliasFirstName;
        public string PatientAddressLocality;
        public string PatientAddressPostcode;
        public string PatientSecondInitial;
        public virtual string ClaimantMedCardNum { get; set; }        
        public string ClaimantIRN;
        public virtual string PatientMedCardNum { get; set; }
        public string PatientIRN;
        public string ServicingProviderNum;
        public string ServicingProviderName;
        public string PayeeProviderNum;
        public string PayeeProviderName;
        public string ReferringProviderNum;
        public string RequestingProviderNum;
        public string PatientGender;
        public string VeteranFileNum;
        public string AccountPaidInd;
        public decimal physioProviderId;
        public decimal debtorId;
        public decimal paymentTypeId;
        public decimal clientId;
        public decimal vendorId;
        public decimal bankAccountId;
        public virtual DateTime ReferralIssueDate { get { return new DateTime(_ReferralIssueDate); } set { _ReferralIssueDate = value.Ticks; } }
        public long _ReferralIssueDate { get; private set; }
        public string TimeOfService;
        public string HospitalInd;
        public virtual DateTime DateOfService
        {
            get { return new DateTime(_dateOfService); }
            set
            {
                _dateOfService = value.Ticks;
            }
        }
        public long _dateOfService { get; private set; }


        public DateTime? ClaimantDOB
        {
            get { return _claimantDoB.HasValue ? new DateTime?(new DateTime(_claimantDoB.Value)) : null; }
            set
            {
                _claimantDoB = value.HasValue ? value.Value.Ticks : (long?)null;
            }
        }
        public long? _claimantDoB { get; private set; }
        public DateTime PatientDateOfBirth;
        public DateTime RequestIssueDate
        {
            get { return new DateTime(_requestIssueDate); }
            set
            {
                _requestIssueDate = value.Ticks;
            }
        }

        public long _requestIssueDate { get; private set; }
        public DateTime CollectionDateTime
        {
            get { return new DateTime(_collectionDateTime); }
            set
            {
                _collectionDateTime = value.Ticks;
            }
        }


        public long _collectionDateTime { get; private set; }
        public string ReferralPeriodTypeCde;
        public string RequestTypeCde;
        public string ClaimantFamilyName;
        public string ClaimantFirstName;
        public string ClaimantAddresLine;
        public string ClaimantAddresLocality;
        public string ClaimantAddresPostcode;
        public string BankAccountName;
        public string BankAccountNum;
        public string BSBCode;
        public bool includeBankInfo;
        public bool includeClaimantInfo;
        public string ServiceId;
        public string AccessionDateTime;
        public string AfterCareOverrideInd;
        public string ChargeAmount;
        public string DuplicateServiceOverrideInd;
        public decimal siteId;
        public string PatientDateOfBirthStr;
        public string DateOfServiceStr;
        public string TransactionDateStr;
        public string patientName;
        public bool IsBatching;
        public bool isUsingPrevClaim;
        public bool isIndividualCrt;
        public string HciPassword;
        public string DVAConditionTreated;
        public string HospitalDetails;
        public string AcceptedDisabilityInd;
        public string AcceptedDisabilityText;
        public string PeriodTypeText;

        public PhysioEnums.MedicareOnlineClaimType claimType = PhysioEnums.MedicareOnlineClaimType.BulkBill;
        public PhysioEnums.MedicareServiceType serviceType = PhysioEnums.MedicareServiceType.Specialist;
        public PhysioEnums.MedicareReferralOverride ReferralOverrideTypeCde = PhysioEnums.MedicareReferralOverride.NonReferred;
        public PhysioEnums.MedicareTreatmentLocation TreatmentLocationCde = PhysioEnums.MedicareTreatmentLocation.CommunityHealthCentres;
        public PhysioEnums.MedicareReferralPeriodType ReferralPeriodType;

        public List<MedicareOnlineClaimObjectItem> ChargesToClaim;
        public decimal GpmClientId;

        //public TransactionResponse TransactionResponse
        //{
        //    get
        //    {

        //        if (_transactionResponse == null)
        //            _transactionResponse = new TransactionResponse();
        //        return _transactionResponse;
        //    }
        //    set
        //    {
        //        _transactionResponse = value;
        //    }
        //}

        public string RestrictiveOverrideCode { get; set; }

        //private TransactionResponse _transactionResponse;

        public String JsonRequestObject { get; set; }
        public String JsonClassSerialized { get; set; }
        public string ReferralDateFrom { get; set; }

        public void SerializeRequestObject(Object requestObject)
        {
            //JsonRequestObject = JsonConvert.SerializeObject(requestObject).ZipMe();
            JsonClassSerialized = requestObject.GetType().FullName;
        }

        //public static IMedicareOnlineObject GetObjectFromJson(Reports.RepPhysioDataSet.VR_MEDICARE_STATEMENT_CLAIMRow medicareStatementRow)
        //{
        //    if (!medicareStatementRow.IsJSON_REQUEST_CLASSNull()
        //            && !medicareStatementRow.IsJSON_REQUESTNull())
        //    {
        //        string unzippedString = medicareStatementRow.JSON_REQUEST.UnzipMe();
        //        if (medicareStatementRow.JSON_REQUEST_CLASS.Equals("Gensolve.MedicareOnline.PatientClaim"))
        //        {
        //            return JsonConvert.DeserializeObject<MedicareOnline.PatientClaim>(unzippedString);
        //        }
        //        else if (medicareStatementRow.JSON_REQUEST_CLASS.Equals("Gensolve.MedicareOnline.BillClaim"))
        //        {
        //            return JsonConvert.DeserializeObject<MedicareOnline.BillClaim>(unzippedString);
        //        }
        //        else if (medicareStatementRow.JSON_REQUEST_CLASS.Equals("Gensolve.MedicareOnline.DVAAlliedHealthClaim"))
        //        {
        //            return JsonConvert.DeserializeObject<MedicareOnline.DVAAlliedHealthClaim>(unzippedString);
        //        }
        //        else if (medicareStatementRow.JSON_REQUEST_CLASS.Equals("Gensolve.MedicareOnline.DVAPaperLessStrmLinedClaim"))
        //        {
        //            return JsonConvert.DeserializeObject<MedicareOnline.DVAPaperLessStrmLinedClaim>(unzippedString);
        //        }
        //    }
        //    return null;
        //}

    }

    public class MedicareOnlineClaimObject : MedicareOnlineClaim
    {        

        public override DateTime DateOfService
        {
            get
            {
                return new DateTime(_dateOfService);
            }
            set
            {
                _dateOfService = value.Ticks;
            }
        }
        public new long _dateOfService { get; private set; }
    }

    public class CAMedicareOnlineClaimObject : MedicareOnlineClaim
    {        
        public new long _dateOfService { get; private set; }
        public override DateTime DateOfService
        {
            get
            {
                return new DateTime(_dateOfService);
            }
            set
            {
                _dateOfService = value.Ticks;
            }
        }

        public override DateTime DateOfTransmission
        {
            get
            {
                return (SiteTzi == null) ? DateTime.Now : TimeZoneInfo.ConvertTime(DateTime.Now, SiteTzi);
            }
            set { }
        }

        public override DateTime AuthorisationDate
        {
            get
            {
                return (SiteTzi == null) ? DateTime.Now : TimeZoneInfo.ConvertTime(DateTime.Now, SiteTzi);
            }
            set { }
        }

        public new long _ReferralIssueDate { get; private set; }
        public override DateTime ReferralIssueDate
        {
            get
            {
                return new DateTime(_ReferralIssueDate);
            }
            set
            {
                _ReferralIssueDate = value.Ticks;
            }
        }

        public override DateTime ClaimCertifiedDate
        {
            get
            {
                return (SiteTzi == null) ? DateTime.Now : TimeZoneInfo.ConvertTime(DateTime.Now, SiteTzi);
            }
            set { }
        }

        public override PhysioEnums.Adaptor AdaptorType
        {
            get
            {
                return PhysioEnums.Adaptor.CA;
            }
        }
    }

    public class ESAMedicareOnlineClaimObject : MedicareOnlineClaim
    {        

        public new long _dateOfService { get; private set; }
        public override DateTime DateOfService
        {
            get
            {
                return new DateTime(_dateOfService);                
            }
            set
            {
                _dateOfService = value.Ticks;
            }
        }

        public new long _ReferralIssueDate { get; private set; }
        public override DateTime ReferralIssueDate
        {
            get
            {                
                return  new DateTime(_ReferralIssueDate);                
            }
            set
            {
                _ReferralIssueDate = value.Ticks;
            }
        }

        public override DateTime DateOfTransmission
        {
            get
            {                
                return (SiteTzi == null) ? DateTime.Now : TimeZoneInfo.ConvertTime(DateTime.Now, SiteTzi);
            }
            set { }
        }

        public override DateTime AuthorisationDate
        {
            get
            {
                return (SiteTzi == null) ? DateTime.Now : TimeZoneInfo.ConvertTime(DateTime.Now, SiteTzi);                
            }
            set { }
        }

        public override DateTime ClaimCertifiedDate
        {
            get
            {
                return (SiteTzi == null) ? DateTime.Now : TimeZoneInfo.ConvertTime(DateTime.Now, SiteTzi);                
            }
            set { }
        }

        public override PhysioEnums.Adaptor AdaptorType
        {
            get
            {
                return PhysioEnums.Adaptor.ESA;
            }
        }

    }


    public abstract class MedicareOnlineClaimFactory
    {
        public abstract MedicareOnlineClaim GetMedicareOnlineClaim();
    }


    public class CAMedicareOnlineClaimObjectFactory : MedicareOnlineClaimFactory
    {
        public override MedicareOnlineClaim GetMedicareOnlineClaim()
        {
            return new CAMedicareOnlineClaimObject();
        }
    }

    public class ESAMedicareOnlineClaimObjectFactory : MedicareOnlineClaimFactory
    {
        public override MedicareOnlineClaim GetMedicareOnlineClaim()
        {
            return new ESAMedicareOnlineClaimObject();
        }
    }



    public class MedicareOnlineClaimObjectItem
    {
        public decimal chargeId = -1;
        public string itemOverrideCode = "";
        public string lspNum = "";
        public string contribPatientAmount = "0";
        public decimal totalAmnt = 0;
        public decimal paidAmnt = 0;
        public DateTime PatientDateOfBirth;
        public string TimeOfService;
        public string ItemNum = "";
        public string AccountReferenceNum = "";
        public string AfterCareOverrideInd = "";
        public string DuplicateServiceOverrideInd = "";
        public string MultipleProcedureOverrideInd = "";
        public string TimeDuration = "";
        public string serviceText { get; set; }
        public string RestrictiveOverrideCode { get; set; }
        public string SecondDeviceInd { get; set; }

        public string NoOfPatientsSeen = "";
        public string DistanceKms { get; set; }
        public string HospitalInd { get; set; }
        public string Qty { get; set; }

        public string claimServiceId;
        public DateTime DateOfService;

        public decimal isClientAppt;
        public decimal isProduct = 0;
    }
           

    [Serializable]
    public class SerializedReport
    {
        #region Properties
        public string Header { get; set; }
        public string Body { get; set; }
        public BulkBillProcessingReport BulkBillProcessingReport { get; set; }
        public DvaProcessingReport DvaProcessingReport { get; set; }
        public List<PaymentReport> PaymentReports { get; set; }

        public static Hashtable MedicareErrors = new Hashtable();
        static System.Resources.ResourceManager reasonRM;
        #endregion

        public SerializedReport(string header, string body)
        {
            Header = header;
            Body = body;
            Initialize();
        }

        private void Initialize()
        {
            BulkBillProcessingReport = new BulkBillProcessingReport();
            DvaProcessingReport = new DvaProcessingReport();
            PaymentReports = new List<PaymentReport>();
        }

        //public void ParseProcessingBulkBillReport(DPRReportResponse response, IConfigurationManagerSettings config)
        //{           
        //    var bbReport = this.BulkBillProcessingReport;

        //    bbReport.ClaimBenefitPaid = response.DirectBillClaim.ClaimBenefit;
        //    bbReport.ClaimCharge = response.DirectBillClaim.ClaimCharge;
        //    bbReport.PmsClaimId = response.DirectBillClaim.ClaimId;

        //    var firstMedicalEvent = response.DirectBillClaim.MedicalEvent.FirstOrDefault();
            
        //    bbReport.ServicingProviderNum = firstMedicalEvent != null ? firstMedicalEvent.ServiceProvider.ProviderNum : string.Empty;

        //    var prElementList = new List<BulkBillProcessingReportElement>();

        //    foreach (var medicalEvent in response.DirectBillClaim.MedicalEvent)
        //    {
        //        //var medicalEvent = response.DirectBillClaim.MedicalEvent.First();
        //        var patient = medicalEvent.Patient;
                
        //        foreach (var service in medicalEvent.Service)
        //        {
        //            var element = new BulkBillProcessingReportElement();
        //            var genElement = new ProcessingReportElement();

        //            genElement.ClaimChargeAmount = element.ChargeAmount = service.Charge;
        //            genElement.ServiceBenefitAmount = element.ServiceBenefitAmount = service.Benefit;

        //            genElement.ServiceId = element.ServiceId = service.Id;                    
        //            genElement.ItemNum = element.ItemNum = service.Item;
        //            genElement.ExplanationCode = element.ExplanationCode = service.AssessmentExplanation != null ? service.AssessmentExplanation.Code : string.Empty;
        //            genElement.DetailsExplanationCode = element.DetailsExplanationCode = service.AssessmentExplanation.Code != null ? config.GetErrorMessage(int.Parse(service.AssessmentExplanation.Code)) : string.Empty;
        //            if (element.ExplanationCode != null && element.ExplanationCode.Equals(element.DetailsExplanationCode)) element.DetailsExplanationCode = string.Empty;
        //            element.MedicareCardFlag = patient.MedicareCardStatusCode;
        //            element.NoOfPatientsSeen = "0";
        //            element.PatientFamilyName = patient.LastName;
        //            element.PatientFirstName = patient.FirstName;
        //            element.PatientMedicareCardNum = patient.CurrentMedicareCardNum;
        //            element.PatientReferenceNum = patient.CurrentMedicareCardIssueNum;
        //            genElement.DateOfService = element.DateOfService = medicalEvent.MedicalEventDate;

        //            prElementList.Add(element);
        //            bbReport.GenericElements.Add(genElement);
        //        }
        //    }

        //    bbReport.Elements = prElementList;
        //}

        //public static PaymentReportTransaction ParsePaymentBulkBillReport(DPYReportRequest request, DPYReportResponse response)
        //{

        //    PaymentReportTransaction pyt = new PaymentReportTransaction();
        //    pyt.DateOfTransmission = request.Date;
        //    pyt.PayeeProviderNum = response.Provider.ProviderNum;
        //    pyt.PmsClaimId = request.ClaimId;

        //    foreach (var summary in response.Payment.PaymentEFTProvider)
        //    {
        //        DirectBillClaim bc = summary.DirectBillClaim.FirstOrDefault(x => x.ClaimId == request.ClaimId);

        //        var element = new PaymentReportElement();
        //        element.BankAccountName = summary.AccountName;
        //        element.BankAccountNum = summary.AccountNum;
        //        element.BSBCode = summary.BsbCode;
        //        element.ClaimBenefitPaid = bc != null ? bc.ClaimBenefit : "";
        //        element.ClaimChargeAmount = string.Empty;

        //        element.ClaimDate = bc != null ? bc.LodgementDate : "";
        //        element.DateOfTransmission = request.Date;
        //        element.DepositAmount = response.Payment.PaymentAmount;
        //        element.PmsClaimId = request.ClaimId;
        //        element.PaymentRunNum = response.Payment.PaymentRunNum;
        //        element.PaymentRunDate = response.Payment.PaymentRunDate;
        //        pyt.pmrElements.Add(element);
        //    }

        //    pyt.enterpriseReportRequest = request;
        //    pyt.enterpriseReportResponse = response;

        //    return pyt;
        //}

        //public static PaymentReportTransaction ParseMedicarePaymentBulkBillReport(DPYReportRequest request, DPYReportResponse response)
        //{

        //    PaymentReportTransaction pyt = new PaymentReportTransaction();
        //    pyt.DateOfTransmission = request.Date;
        //    pyt.PayeeProviderNum = response.Provider.ProviderNum;
        //    pyt.PmsClaimId = request.ClaimId;            

        //    foreach (var summary in response.Payment.PaymentEFTProvider)
        //    {
        //        foreach(var bc in summary.DirectBillClaim)
        //        {
        //            var element = new PaymentReportElement();
        //            element.BankAccountName = summary.AccountName;
        //            element.BankAccountNum = summary.AccountNum;
        //            element.BSBCode = summary.BsbCode;
        //            element.ClaimBenefitPaid = bc.ClaimBenefit;
        //            element.ClaimChargeAmount = bc.ClaimCharge;
        //            element.ClaimDate = bc.LodgementDate;
        //            element.DateOfTransmission = request.Date;
        //            element.DepositAmount = response.Payment.PaymentAmount;
        //            element.PmsClaimId = bc.ClaimId;
        //            element.PaymentRunNum = response.Payment.PaymentRunNum;
        //            element.PaymentRunDate = response.Payment.PaymentRunDate;
        //            pyt.pmrElements.Add(element);
        //        }                
        //    }

        //    pyt.enterpriseReportRequest = request;
        //    pyt.enterpriseReportResponse = response;

        //    return pyt;
        //}


        //public static PaymentReportTransaction ParsePaymentDvaReport(DvaReportRequest request, DvaPaymentReportResponse response) {

        //    PaymentReportTransaction pyt = new PaymentReportTransaction();
        //    pyt.DateOfTransmission = response.ClaimSummary.First().ClaimDate;
        //    pyt.PayeeProviderNum = request.PayeeProvider.ProviderNum;
        //    pyt.PmsClaimId = request.ClaimId;

        //    foreach(var summary in response.ClaimSummary)
        //    {
        //        var element = new PaymentReportElement();
        //        element.BankAccountName = response.PaymentRun.Account.BankAccountName;
        //        element.BankAccountNum = response.PaymentRun.Account.BankAccountNum;
        //        element.BSBCode = response.PaymentRun.Account.BsbCode;
        //        element.ClaimBenefitPaid = summary.ClaimBenefitPaid;
        //        element.ClaimChargeAmount = string.Empty;

        //        element.ClaimDate = SetEsaTimeToMedOnlineFormatTime(summary.ClaimDate);
        //        element.DateOfTransmission = request.Date;
        //        element.DepositAmount = response.PaymentRun.DepositAmount;
        //        element.PmsClaimId = summary.PmsClaimId;
        //        element.PaymentRunNum = response.PaymentRun.PaymentRunNum;
        //        element.PaymentRunDate = SetEsaTimeToMedOnlineFormatTime(response.PaymentRun.PaymentRunDate);
        //        pyt.pmrElements.Add(element);
        //    }

        //    pyt.enterpriseReportRequest = request;
        //    pyt.enterpriseReportResponse = response;

        //    return pyt;
        //}        

        private static string SetEsaTimeToMedOnlineFormatTime(string claimDate)
        {
            char[] splitter = new char[3] { '-', '+', ':' };
            var splittedTime = claimDate.Split(splitter);
            return splittedTime[2]+splittedTime[1]+splittedTime[0];
        }

    //    public void ParseProcessingDvaReport(DvaProcessingReportResponse dvaReportResponse)
    //    {
    //        var prReport = this.DvaProcessingReport;
    //        prReport.PmsClaimId = dvaReportResponse.Claim.ClaimId;
    //        decimal benefit, chargeAmount = 0;
    //        prReport.ClaimBenefitPaid = decimal.TryParse(dvaReportResponse.Claim.Benefit, out benefit) ? (benefit / 100).ToString("C2") : dvaReportResponse.Claim.Benefit;
    //        prReport.ClaimChargeAmount = decimal.TryParse(dvaReportResponse.Claim.ChargeAmount, out chargeAmount) ? (chargeAmount/ 100).ToString("C2") : dvaReportResponse.Claim.ChargeAmount; 
    //        //var medicalEvent = dvaReportResponse.Claim.MedicalEvent.First();
    //        var prElementList = new List<DvaProcessingReportElement>();

    //        foreach (var medicalEvent in dvaReportResponse.Claim.MedicalEvent)
    //        {
    //            foreach (var service in medicalEvent.Service)
    //            {
    //                var prElement = new DvaProcessingReportElement();
    //                var genElement = new ProcessingReportElement();

    //                prElement.ServiceId = service.ServiceId;

    //                if (prElementList.Where(x => x.ServiceId == prElement.ServiceId).Count() > 0)
    //                    continue;

    //                genElement.ServiceId = service.ServiceId;
    //                genElement.DateOfService = prElement.DateOfService = DateTime.ParseExact(service.DateOfService, "yyyy-MM-dd+HH:mm", System.Globalization.CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");
    //                genElement.ExplanationCode = prElement.ExplanationCode = service.ExplanationCode;
    //                if (!string.IsNullOrEmpty(service.ExplanationCode))
    //                    genElement.DetailsExplanationCode = prElement.DetailsExplanationCode = CodeResources.ReasonCodes.ResourceManager.GetString(prElement.ExplanationCode);
    //                prElement.ItemNum = service.ItemNum;
    //                genElement.ItemNum = prElement.ItemNum = (prElement.ItemNum ?? string.Empty).Trim().TrimStart('0');
    //                var patient = medicalEvent.Patient;
    //                prElement.MedicareCardFlag = patient.CardFlag;
    //                prElement.NoOfPatientsSeen = service.NoOfPatientsSeen;
    //                prElement.PatientFamilyName = patient.Identity.LastName;
    //                prElement.PatientFirstName = patient.Identity.FirstName;

    //                benefit = chargeAmount = 0;
    //                prElement.ServiceBenefitAmount = decimal.TryParse(service.Benefit, out benefit) ? (benefit / 100).ToString("C2") : service.Benefit;
    //                genElement.ServiceBenefitAmount = service.Benefit;
    //                prElement.ServiceChargeAmount = decimal.TryParse(service.ChargeAmount, out chargeAmount) ? (chargeAmount / 100).ToString("C2") : service.ChargeAmount;
    //                genElement.ServiceChargeAmount = service.ChargeAmount;
    //                prElement.ClaimChargeAmount = prReport.ClaimChargeAmount; //dvaReportResponse.Claim.ChargeAmount;
    //                genElement.ClaimChargeAmount = service.ItemNum.Equals("KM") ? "0" : service.ChargeAmount;
    //                prElement.ServicingProviderNum = medicalEvent.ServiceProvider.ProviderNum;
    //                //prElement.ServicingProviderName = GetReportElement(sessionId, "ServicingProviderName", 40);
    //                //prElement.DateOfTransmission = DateOfTransmission;
    //                prElement.CardFlag = patient.CardFlag;
    //                prElement.VeteranFileNum = patient.DvaMembership.MemberNum;
    //                prElement.VoucherId = medicalEvent.Id;
    //                prElement.AccountReferenceNum = service.AccountReferenceNum;


    //                prElementList.Add(prElement);
    //                prReport.GenericElements.Add(genElement);
    //            }
    //        }

    //        prReport.Elements = prElementList;            
    //    }
    }

    [Serializable]
    public class PaymentReport
    {
        public string BankAccountName { get; set; }
        public string BankAccountNum { get; set; }
        public string BsbCode { get; set; }
        public string ClaimBenefitPaid { get; set; }
        public string ClaimDate { get; set; }
        public string DepositAmount { get; set; }
        public string Header { get; set; }
        public string PaymentRunDate { get; set; }
        public string PaymentRunNum { get; set; }
        public string PmsClaimId { get; set; }
    }

    [Serializable]
    public class DvaProcessingReport
    {
        private List<ProcessingReportElement> _genericElements;

        public string ClaimBenefitPaid { get; set; }
        public string PmsClaimId { get; set; }
        public string ClaimChargeAmount { get; set; }

        public List<DvaProcessingReportElement> Elements { get; set; }
        
        public List<ProcessingReportElement> GenericElements
        {
            get { return _genericElements ?? (_genericElements = new List<ProcessingReportElement>()); }
            set { _genericElements = value; }
        }

        public DvaProcessingReport()
        {
            Elements = new List<DvaProcessingReportElement>();
        }
    }

    [Serializable]
    public class DvaProcessingReportElement
    {
        public string DateOfService { get; set; }
        public string ExplanationCode { get; set; }
        public string ServiceChargeAmount { get; set; }
        public string DetailsExplanationCode { get;set; }
        public string ItemNum { get; set; }
        public string MedicareCardFlag { get; set; }
        public string NoOfPatientsSeen { get; set; }
        public string PatientFamilyName { get; set; }
        public string PatientFirstName { get; set; }
        public string PatientMedicareCardNum { get;set; }
        public string PatientReferenceNum { get;set; }
        public string ServiceBenefitAmount { get;set; }
        public string ServicingProviderNum { get; set; }
        public string ServicingProviderName { get; set; }
        public string DateOfTransmission { get; set; }
        public string CardFlag { get; set; }
        public string VeteranFileNum { get; set; }
        public string VoucherId { get; set; }
        public string AccountReferenceNum { get; set; }
        public string ClaimChargeAmount { get; set; }
        public string ServiceId { get; set; }
    }

    [Serializable]
    public class BulkBillProcessingReport
    {
        private List<BulkBillProcessingReportElement> _elements;
        private List<ProcessingReportElement> _genericElements;

        public string ClaimBenefitPaid { get; set; }
        public string ServicingProviderNum { get; set; }
        public string PmsClaimId { get; set; }

        public List<BulkBillProcessingReportElement> Elements
        {
            get { return _elements ?? (_elements = new List<BulkBillProcessingReportElement>()); }
            set { _elements = value; }
        }

        public List<ProcessingReportElement> GenericElements
        {
            get { return _genericElements ?? (_genericElements = new List<ProcessingReportElement>()); }
            set { _genericElements = value; }
        }

        public BulkBillProcessingReport()
        {
            this.Elements = new List<BulkBillProcessingReportElement>();
        }

        public string ClaimBenefitPaidTotal
        {
            get {
                decimal benefit = 0;
                return decimal.TryParse(ClaimBenefitPaid, out benefit) ? (benefit / 100).ToString("C2") : ClaimBenefitPaid;
            }
        }

        public decimal ClaimBenefitAmount
        {
            get
            {
                try
                {
                    decimal sum = Elements.Sum(x => long.Parse(x.ChargeAmount));
                    return decimal.Round(decimal.Divide(sum,100),2, MidpointRounding.AwayFromZero);
                }
                catch { return 0; }
            }
        }

        public string ClaimCharge { get; set; }
    }

    public class BulkBillProcessingReportElement
    {
        public string ChargeAmount { get; set; }
        public string DateOfService { get; set; }
        public string ExplanationCode { get; set; }
        public string DetailsExplanationCode { get; set; }
        public string ItemNum { get; set; }
        public string MedicareCardFlag { get; set; }
        public string NoOfPatientsSeen { get; set; }
        public string PatientFamilyName { get; set; }
        public string PatientFirstName { get; set; }
        public string PatientMedicareCardNum { get; set; }
        public string PatientReferenceNum { get; set; }
        public string ServiceBenefitAmount { get; set; }
        public string ServiceId { get; set; }

    }

}
