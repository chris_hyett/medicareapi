﻿using System;
using System.Collections.Generic;
using Gensolve.MedicareOnline.ObjectClasses;
using System.Linq;


namespace Gensolve.MedicareOnline
{
    [Serializable]
    public class SerializedReport
    {

        #region Properties
        public string Header { get; set; }
        public string Body { get; set; }

        public BulkBillProcessingReport BulkBillProcessingReport { get; set; }
        public DvaProcessingReport DvaProcessingReport { get; set; }
        public List<PaymentReport> PaymentReports { get; set; }
        #endregion
        #region Constructors

        public SerializedReport(DvaProcessingReport dvaProcessingReport)
        {
            this.Header = "DVA Processing Report";
            this.Body = "Please refer to the DVA processing report object in this class";
            this.DvaProcessingReport = dvaProcessingReport;
        }

        public SerializedReport(BulkBillProcessingReport bulkBillProcessingReport)
        {
            this.Header = "Bulk Bill Processing Report";
            this.Body = "Please refer to the bulkbill processing report object in this class";
            this.BulkBillProcessingReport = bulkBillProcessingReport;
        }

        public SerializedReport(string header, string body)
        {
            Header = header;
            Body = body;
        }

        public SerializedReport(string header, PaymentReportElement paymentReportElement)
        {
            var pre = paymentReportElement;
            var paymentReport = new PaymentReport();
            paymentReport.BsbCode = pre.BSBCode;
            paymentReport.BankAccountNum = pre.BankAccountNum;
            paymentReport.BankAccountName = pre.BankAccountName;
            paymentReport.ClaimBenefitPaid = Utils.ParseMedicareAmntToCurrency(pre.ClaimBenefitPaid);
            paymentReport.ClaimDate = Utils.GetFormattedDate(pre.ClaimDate);
            paymentReport.DepositAmount = Utils.ParseMedicareAmntToCurrency(pre.DepositAmount);
            paymentReport.PaymentRunDate = Utils.GetFormattedDate(pre.PaymentRunDate);
            paymentReport.PaymentRunNum = pre.PaymentRunNum;
            paymentReport.PmsClaimId = pre.PmsClaimId;
            paymentReport.Header = header;

            if(PaymentReports == null)
                PaymentReports = new List<PaymentReport>();

            PaymentReports.Add(paymentReport);
            
            Header = header;
            Body = SerializePaymentReportElement(paymentReportElement);
        }
        public SerializedReport(string header, IEnumerable<PaymentReportElement> paymentReportElement)
        {
            Header = header;
            Body = SerializePaymentReportElement(paymentReportElement);
        }

        public SerializedReport(string header, ProcessingReport processingReport, object moTxnRow, object cpdRow, object moTxnItemRow)
        {
            PaymentReports = new List<PaymentReport>();
            
        }
        
        public SerializedReport(string header, List<PaymentReportElement> paymentReportElement)
        {
            Header = header;
            Body = SerializePaymentReportElement(paymentReportElement);
        }



        #endregion

        #region Private Methods

        private string SerializePaymentReportElement(IEnumerable<PaymentReportElement> listOfPayments)
        {
            string report = string.Empty;
            foreach (var paymentReportElement in listOfPayments)
            {
                var paymentReport = new PaymentReport();
                paymentReport.BsbCode = paymentReportElement.BSBCode;
                paymentReport.BankAccountNum = paymentReportElement.BankAccountNum;
                paymentReport.BankAccountName = paymentReportElement.BankAccountName;
                paymentReport.PayeeProviderNum = paymentReportElement.PayeeProviderNum;
                paymentReport.ClaimBenefitPaid = Utils.ParseMedicareAmntToCurrency(paymentReportElement.ClaimBenefitPaid);
                paymentReport.ClaimDate = Utils.GetFormattedDate(paymentReportElement.ClaimDate);
                paymentReport.DepositAmount = Utils.ParseMedicareAmntToCurrency(paymentReportElement.DepositAmount);
                paymentReport.PaymentRunDate = Utils.GetFormattedDate(paymentReportElement.PaymentRunDate);
                paymentReport.PaymentRunNum = paymentReportElement.PaymentRunNum;
                paymentReport.PmsClaimId = paymentReportElement.PmsClaimId;

                if (PaymentReports == null)
                    PaymentReports = new List<PaymentReport>();

                PaymentReports.Add(paymentReport);

                if (!string.IsNullOrEmpty(report))
                    report += "\n\n-----\n";
                report = report + "\nBSB Code: " + paymentReportElement.BSBCode;
                report = report + "\nBank Account Num: " + paymentReportElement.BankAccountNum;
                report = report + "\nBank Account Name: " + paymentReportElement.BankAccountName;
                report = report + "\nClaim Benefit Paid: " + Utils.ParseMedicareAmntToCurrency(paymentReportElement.ClaimBenefitPaid);
                report = report + "\nClaim Date: " + Utils.GetFormattedDate(paymentReportElement.ClaimDate);
                report = report + "\nDeposit Amount: " + Utils.ParseMedicareAmntToCurrency(paymentReportElement.DepositAmount);
                report = report + "\nPayment Run Date: " + Utils.GetFormattedDate(paymentReportElement.PaymentRunDate);
                report = report + "\nPayment Run Number: " + paymentReportElement.PaymentRunNum;
                report = string.IsNullOrEmpty(paymentReportElement.PmsClaimId) ? report : report + "\nPms Claim Id: " + paymentReportElement.PmsClaimId;
            }
            
            return report;
        }

        private string SerializeProcessingReport(ProcessingReport pr, object moTxnRow, object cpdRow, object moTxnItemRow)
        {
            return null;

        }

        private static string SetValuesForReport(string reportElement, string gpmElement)
        {
            return (string.IsNullOrWhiteSpace(reportElement) ? gpmElement : reportElement);
        }

        private string SerializePaymentReportElement(PaymentReportElement pre)
        {
            string report = "\nBSB Code: " + pre.BSBCode;
            report = report + "\nBank Account Num: " + pre.BankAccountNum;
            report = report + "\nBank Account Name: " + pre.BankAccountName;
            report = report + "\nClaim Benefit Paid: " + Utils.ParseMedicareAmntToCurrency(pre.ClaimBenefitPaid);
            report = report + "\nClaim Date: " + Utils.GetFormattedDate(pre.ClaimDate);
            report = report + "\nDeposit Amount: " + Utils.ParseMedicareAmntToCurrency(pre.DepositAmount);
            report = report + "\nPayment Run Date: " + Utils.GetFormattedDate(pre.PaymentRunDate);
            report = report + "\nPayment Run Number: " + pre.PaymentRunNum;
            report = string.IsNullOrEmpty(pre.PmsClaimId) ? report : report + "\nPms Claim Id: " + pre.PmsClaimId;

            return report;

        } 
        #endregion

    }

    [Serializable]
    public class DvaProcessingReport
    {
        public string ClaimBenefitPaid { get; internal set; }
        public string PmsClaimId { get; internal set; }
        public string ClaimChargeAmount { get; internal set; }
        public List<DvaProcessingReportElement> Elements { get; set; }

        public void AddElement(DvaProcessingReportElement dvaProcessingReportElement)
        {
            if (this.Elements == null)
                Elements = new List<DvaProcessingReportElement>();

            Elements.Add(dvaProcessingReportElement);
        }
    }

    [Serializable]
    public class DvaProcessingReportElement
    {
        public string DateOfService { get; internal set; }
        public string ExplanationCode { get; internal set; }
        public string ServiceChargeAmount { get; internal set; }
        public string DetailsExplanationCode { get; set; }
        public string ItemNum { get; internal set; }
        public string MedicareCardFlag { get; internal set; }
        public string NoOfPatientsSeen { get; internal set; }
        public string PatientFamilyName { get; internal set; }
        public string PatientFirstName { get; internal set; }
        public string PatientMedicareCardNum { get; set; }
        public string PatientReferenceNum { get; set; }
        public string ServiceBenefitAmount { get; set; }
        public string ServicingProviderNum { get; internal set; }
        public string ServicingProviderName { get; internal set; }
        public string DateOfTransmission { get; internal set; }
        public string CardFlag { get; internal set; }
        public string VeteranFileNum { get; internal set; }
        public string VoucherId { get; internal set; }
        public string AccountReferenceNum { get; internal set; }
        public string ClaimChargeAmount { get; internal set; }
        public string ServiceId { get; internal set; }
    }

    [Serializable]
    public class PaymentReport
    {
        public string BankAccountName { get; internal set; }
        public string BankAccountNum { get; internal set; }
        public string BsbCode { get; internal set; }
        public string ClaimBenefitPaid { get; internal set; }
        public string ClaimDate { get; internal set; }
        public string DepositAmount { get; internal set; }
        public string PaymentRunDate { get; internal set; }
        public string PaymentRunNum { get; internal set; }
        public string PmsClaimId { get; internal set; }
        public string PayeeProviderNum { get; internal set; }
        public string Header { get; set; }
    }

    [Serializable]
    public class BulkBillProcessingReport
    {
        public string ClaimBenefitPaid { get; set; }
        public string ServicingProviderNum { get; set; }
        
        public List<BulkBillProcessingReportElement> Elements { get; set; }
        public string PmsClaimId { get; set; }

        internal void AddElement(BulkBillProcessingReportElement bbpElement)
        {
            if (this.Elements == null)
                Elements = new List<BulkBillProcessingReportElement>();

            Elements.Add(bbpElement);
        }
    }

    public class BulkBillProcessingReportElement
    {
        public string ChargeAmount{ get; set; }
        public string DateOfService{ get; set; }
        public string ExplanationCode{ get; set; }
        public string ItemNum{ get; set; }
        public string MedicareCardFlag{ get; set; }
        public string NoOfPatientsSeen{ get; set; }
        public string PatientFamilyName{ get; set; }
        public string PatientFirstName{ get; set; }
        public string PatientMedicareCardNum{ get; set; }
        public string PatientReferenceNum{ get; set; }
        public string ServiceBenefitAmount{ get; set; }
        public string ServiceId{ get; set; }
        public string DetailsExplanationCode { get; set; }

    }
}