﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

namespace Gensolve.MedicareOnline.ObjectClasses
{
    public class Utils
    {
        public static IList<_PropertyInfo> FilterPropertiesByList(PropertyInfo[] properties, List<string> propertyList)
        {
            var propertyInfoList = new List<_PropertyInfo>(properties);
            return propertyInfoList.Where(propertyInfo => propertyList.Contains(propertyInfo.Name)).ToList();
        }

        public static IList<_PropertyInfo> FilterPropertiesByExcludingList(PropertyInfo[] properties, List<string> excludeThis)
        {
            var propertyInfoList = new List<_PropertyInfo>(properties);
            return propertyInfoList.Where(propertyInfo => !excludeThis.Contains(propertyInfo.Name)).ToList();
        }

        public static string ParseMedicareAmntToCurrency(string amount)
        {
            decimal centvalue = -1;
            bool result = decimal.TryParse(amount, out centvalue);
            if (result)
            {
                decimal dollar = Math.Round(centvalue / 100, 2);
                return string.Format("{0:C}", dollar);
            }
            return string.Empty;
        }

        public static readonly string MedicareFormatDate = "ddMMyyyy";

        public static string GetFormattedDate(string date)
        {
            if(string.IsNullOrEmpty(date))
                return String.Empty;
            
            return DateTime.ParseExact(date, MedicareFormatDate, CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");
        }

        public static string GetFormattedDate(DateTime time)
        {
            return time.ToString(MedicareFormatDate);
        }

        internal static string ParseNumber(string explanationCode)
        {
            int code = 0;
            var tryParse = int.TryParse(explanationCode, out code);
            return string.Format("{0}", tryParse ? code : 0) ;
        }
    }

    public enum MedicareOnlineClaimType : int
    {

        BulkBill = 1,
        DVAPaperlessStreamlined = 2,
        DVAAlliedHealth = 3,
        PatientClaim = 4
    }
}
