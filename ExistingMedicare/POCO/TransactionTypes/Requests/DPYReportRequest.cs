﻿using System.Xml;
using System.Xml.Serialization;

namespace Gensolve.MedicareOnline.POCO
{
    [XmlRoot(ElementName = "DPYReportRequest")]
    public class DPYReportRequest : ReportRequest, IMedicareOnlineObject
    {
        #region Attributes

        [XmlAttribute(AttributeName = "claimId")]
        public string ClaimId { get; set; }
        [XmlAttribute(AttributeName = "date")]
        public string Date { get; set; }

        #endregion

        #region Elements

        [XmlElement(ElementName = "Provider")]
        public Provider Provider
        {
            get { return _provider??(_provider = new Provider()); }
            set { _provider = value; }
        } 

        #endregion

        //public DPYReportRequest(PhysioDataSet.MEDICARE_ONLINE_TXNRow moTxnRow)
        //{
        //    ClaimId = moTxnRow.PMSCLAIM_ID;
        //    Date = moTxnRow.TRANSACTION_DATE_STR;
        //    Provider.ProviderNum = moTxnRow.PROVIDER_NUMBER_ID;
        //}

        public DPYReportRequest() { }
    }
}
