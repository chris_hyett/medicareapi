﻿using System;
using System.Xml;
using System.Xml.Serialization;
using Gensolve.PhysioData;

namespace Gensolve.MedicareOnline.POCO
{
    [XmlRoot(ElementName = "dvaReportRequest", Namespace = "http://hic.gov.au/hiconline/dva/version-6")]
    public class DvaReportRequest : ReportRequest
    {

        #region Elements
        [XmlElement(ElementName = "payeeProvider")]
        public Provider PayeeProvider { get { return _provider ?? (_provider = new Provider()); } set { _provider = value; } }

        [XmlAttribute(AttributeName = "claimId")]
        public string ClaimId { get; set; }

        [XmlAttribute(AttributeName = "dateOfLodgement")]
        public string Date { get; set; }

        [XmlAttribute(AttributeName = "ns1", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Ns1 { get; set; }
        #endregion

        public DvaReportRequest()
        {

        }

        public DvaReportRequest(object moTxnRow)
        {           
        }

        public DvaReportRequest(string pmsclaim_id, string transaction_date_str, string provider_number)
        {
            string MedicareFormat = "ddMMyyyy";
            ClaimId = pmsclaim_id;
            try
            {
                Date = DateTime.ParseExact(transaction_date_str, MedicareFormat, System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd+HH:mm");
            }
            catch (Exception e)
            {
                Date = transaction_date_str;
            }

            PayeeProvider.ProviderNum = provider_number;
        }
    }
}
