﻿using System.Xml;
using System.Xml.Serialization;

namespace Gensolve.MedicareOnline.POCO
{    
    [XmlRoot(ElementName = "onlineVeteranVerificationRequest", Namespace = "http://hic.gov.au/hiconline/dva/version-6")]
    public class OnlineVeteranVerificationRequest : IMedicareOnlineObject
    {
        private SerializableOVVPatient _patient;
        private ServicingProvider _servicingProvider;
        [XmlAttribute(AttributeName = "OPVTypeCde")]
        public string OPVTypeCde { get; set; }
        [XmlAttribute(AttributeName = "ns1", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Ns1 { get; set; }

        [XmlElement(ElementName = "patient")]
        public SerializableOVVPatient Patient { get { return _patient ?? (_patient = new SerializableOVVPatient()); } set { _patient = value; } }
        [XmlElement(ElementName = "servicingProvider")]
        public ServicingProvider ServicingProvider { get { return _servicingProvider ?? (_servicingProvider = new ServicingProvider()); } set { _servicingProvider = value; } }


    }
}
