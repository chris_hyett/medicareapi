﻿using System.Xml;
using System.Xml.Serialization;

namespace Gensolve.MedicareOnline.POCO
{
    [XmlRoot(ElementName = "onlineConcessionEntitlementVerificationRequest", Namespace = "http://hic.gov.au/hiconline/cev/version-1", IsNullable = false)]
    public class OnlineConcessionEntitlementVerificationRequest : IMedicareOnlineObject 
    {
        private Person _person;
        private Provider _provider;

        [XmlElement(ElementName = "person")]
        public Person Person
        {
            get { return _person ?? (_person = new Person()); }
            set { _person = value; }
        }

        [XmlElement(ElementName = "provider")]
        public Provider Provider
        {
            get { return _provider ?? (_provider = new Provider()); }
            set { _provider = value; }
        }

        [XmlAttribute(AttributeName = "ns1", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Ns1 { get; set; }
        [XmlAttribute(AttributeName = "dateOfService")]
        public string DateOfService { get; set; }
        [XmlAttribute(AttributeName = "OPVTypeCde")]
        public string OPVTypeCde { get; set; }

        public OnlineConcessionEntitlementVerificationRequest() { }
    }
}
