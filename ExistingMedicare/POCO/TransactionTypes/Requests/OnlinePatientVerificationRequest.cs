﻿
using System;
using System.Xml.Serialization;

namespace Gensolve.MedicareOnline.POCO
{
    [XmlRoot(ElementName = "patientVerificationRequest", Namespace = "http://hic.gov.au/hiconline/hiconline/version-4", IsNullable = false)]
    public class OnlinePatientVerificationRequest : IMedicareOnlineObject
    {
        private Patient _patient;
        private Provider _provider;
        private readonly string MedicareFormat = "ddMMyyyy";

        public OnlinePatientVerificationRequest()
        {

        }

        private string SetDateInMedOnlineServerFormat(string dateInClientAdapterFormat)
        {
            return DateTime.ParseExact(dateInClientAdapterFormat, MedicareFormat, System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd+HH:mm");
        }

        public OnlinePatientVerificationRequest(PatientVerification patientVerification)
        {
            EarliestDateOfService = patientVerification.EarliestDateOfService;
            Patient.HealthFund.Organisation = string.Empty; //When using PVM this field is not required
            OPVTypeCde = patientVerification.OPVTypeCde; //PVM – Medicare Only

            Patient.Alias.LastName = patientVerification.PatientAliasFamilyName;
            Patient.Alias.FirstName = patientVerification.PatientAliasFirstName;
            Patient.DateOfBirth = SetDateInMedOnlineServerFormat(patientVerification.PatientDateOfBirth);
            Patient.Identity.LastName = patientVerification.PatientFamilyName;
            Patient.Identity.FirstName = patientVerification.PatientFirstName;
            Patient.Gender = patientVerification.Gender;
            Patient.Medicare.MemberNum = patientVerification.PatientMedicareCardNum;
            Patient.Medicare.MemberRefNum = patientVerification.PatientReferenceNum;
            Provider.ProviderNum = patientVerification.ServicingProviderNum;
        }
                
        [XmlAttribute(AttributeName = "earliestDateOfService")]
        public string EarliestDateOfService { get; set; }

        [XmlAttribute(AttributeName = "OPVTypeCde")]
        public string OPVTypeCde { get; set; }

        [XmlElement("patient")]
        public Patient Patient
        {
            get { return _patient??(_patient = new Patient()); }
            set { _patient = value; }
        }

        [XmlElement("provider")]
        public Provider Provider
        {
            get { return _provider??(_provider = new Provider()); }
            set { _provider = value; }
        }
    }
}
