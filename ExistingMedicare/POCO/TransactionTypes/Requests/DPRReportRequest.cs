﻿using System;
using System.Xml;
using System.Xml.Serialization;
using Gensolve.PhysioData;

namespace Gensolve.MedicareOnline.POCO
{    

    [XmlRoot(ElementName = "DPRReportRequest")]
    public class DPRReportRequest : ReportRequest
    {
        [XmlAttribute(AttributeName = "claimId")]
        public string ClaimId { get; set; }

        #region Elements
        [XmlElement(ElementName = "Provider")]
        public Provider Provider { get { return _provider ?? (_provider = new Provider()); } set { _provider = value; } }


        //not used here.
        [XmlAttribute(AttributeName = "date")]
        public string Date
        {
            get;set;
        }
        #endregion

        public DPRReportRequest(object moTxnRow)
        {
            ClaimId = null;
            Date = null;
            Provider.ProviderNum = null;
        }

        public DPRReportRequest() { }
    }
}
