﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml;

namespace Gensolve.MedicareOnline.POCO
{
    [XmlRoot(ElementName = "DirectBillClaim")]
    public class DirectBillClaim : MedicareOnlineObject
    {
        #region Private Fields

        private List<MedicalEvent> _medicalEvent; 

        #endregion

        #region Constructors
        public static DirectBillClaim Create()
        {
            return new DirectBillClaim();
        }
        #endregion

        #region Attributes

        [XmlAttribute(AttributeName = "claimId")]
        public string ClaimId { get; set; }

        /// <summary>
        /// Must be set
        ///• Must be a valid value:
        ///     P = Pathology services
        ///     S = Specialist
        ///     O = General
        /// </summary>
        [XmlAttribute(AttributeName = "serviceType")]
        public string ServiceType { get; set; }

        [XmlAttribute(AttributeName = "authorisationDate")]
        public string AuthorisationDate { get; set; }

        [XmlAttribute(AttributeName = "claimBenefit")]
        public string ClaimBenefit { get; set; }

        [XmlAttribute(AttributeName = "lodgementDate")]
        public string LodgementDate { get; set; }

        [XmlAttribute(AttributeName = "claimCharge")]
        public string ClaimCharge { get; set; }
                
        [XmlAttribute("xml:space=preserve", AttributeName = "registrationId")]
        public string RegistrationId { get; set; }

        #endregion

        #region Elements

        [XmlElement(ElementName = "MedicalEvent")]
        public List<MedicalEvent> MedicalEvent
        {
            get
            {
                if (_medicalEvent == null)
                    _medicalEvent = new List<MedicalEvent>();
                return _medicalEvent;
            }
            set { _medicalEvent = value; }
        }
                
        #endregion
    }
}