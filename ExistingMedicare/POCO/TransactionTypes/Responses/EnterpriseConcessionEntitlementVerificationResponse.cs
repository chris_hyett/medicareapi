﻿using System.Xml;
using Gensolve.MedicareOnline.Interfaces;

namespace Gensolve.MedicareOnline.POCO
{
    public class EnterpriseConcessionEntitlementVerificationResponse : XmlDocument, IMedicareServerResult
    {
        public string TransactionId { get; set; }
        public string XmlRequest { get; set; }
        public string XmlResponse { get; set; }
        //Properties of the XML response medicare online document
        public void LoadXmlString(string xmlString)
        {
            LoadXml(xmlString);
        }
    }
}
