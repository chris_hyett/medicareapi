﻿using System;
using System.Xml;
using Gensolve.MedicareOnline.Interfaces;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.IO;

namespace Gensolve.MedicareOnline.POCO
{
    [XmlRoot(ElementName = ("dvaProcessingReportResponse"))]
    public class DvaProcessingReportResponse : IMedicareServerResult
    {
        private Claim _claim;

        [XmlElement(ElementName ="claim")]
        public Claim Claim
        {
            get { return _claim??(_claim = new Claim()); }
            set { _claim = value; }
        }

        //XML properties
        public string TransactionId { get; set; }
        public string XmlRequest { get; set; }
        public string XmlResponse { get; set; }

        public void LoadXmlString(string xmlString)
        {
            XmlSerializer serializer = new XmlSerializer(this.GetType());
            xmlString = MedOnlinePocoHelper.RemoveHeader(xmlString);
            xmlString = MedOnlinePocoHelper.RemoveNamespaceFromXml(xmlString);

            using (TextReader reader = new StringReader(xmlString))
            {
                var result = (DvaProcessingReportResponse)serializer.Deserialize(reader);
                Claim = result.Claim;
            }
        }        
    }

    [XmlRoot(ElementName = "claim")]
    public class Claim
    {
        private List<DvaMedicalEvent> _medicalEvent;

        [XmlElement(ElementName = "medicalEvent")]
        public List<DvaMedicalEvent> MedicalEvent { get { return _medicalEvent??(_medicalEvent = new List<DvaMedicalEvent>()); } set { _medicalEvent = value; } }
        [XmlAttribute(AttributeName = "chargeAmount")]
        public string ChargeAmount { get; set; }
        [XmlAttribute(AttributeName = "claimId")]
        public string ClaimId { get; set; }
        [XmlAttribute(AttributeName = "benefit")]
        public string Benefit { get; set; }
    }
}
