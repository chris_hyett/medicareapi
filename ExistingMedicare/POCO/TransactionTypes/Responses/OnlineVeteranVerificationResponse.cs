﻿using System.Xml;
using Gensolve.MedicareOnline.Interfaces;
using System.Xml.Serialization;
using System.IO;
namespace Gensolve.MedicareOnline.POCO
{
    [XmlRoot(ElementName ="onlineVeteranVerificationResponse")]
    public class OnlineVeteranVerificationResponse : IMedicareServerResult
    {
        #region Private Fields

        private DvaStatus _medicareStatus;
        private HealthFundStatus _healthFundStatus;

        #endregion

        #region Attributes

        [XmlAttribute(AttributeName = "statusCde")]
        public string StatusCde { get; set; }

        [XmlAttribute(AttributeName = "DVAProcDate")]
        public string DvaProcDate { get; set; }



        #endregion

        #region Elements

        [XmlElement(ElementName = "status")]
        public DvaStatus Status { get { return _medicareStatus ?? (_medicareStatus = new DvaStatus()); } set { _medicareStatus = value; } }

        [XmlElement(ElementName = "healthFundStatus")]
        public HealthFundStatus HealthFundStatus { get { return _healthFundStatus ?? (_healthFundStatus = new HealthFundStatus()); } set { _healthFundStatus = value; } }

        #endregion
        //Xml properties
        public string TransactionId { get; set; }
        public string XmlRequest { get; set; }
        public string XmlResponse { get; set; }
        public void LoadXmlString(string xmlString)
        {
            if (string.IsNullOrWhiteSpace(xmlString))
                return;
            XmlSerializer serializer = new XmlSerializer(this.GetType());

            xmlString = MedOnlinePocoHelper.RemoveHeader(xmlString);
            xmlString = MedOnlinePocoHelper.RemoveNamespaceFromXml(xmlString);

            using (TextReader reader = new StringReader(xmlString))
            {
                var result = (OnlineVeteranVerificationResponse)serializer.Deserialize(reader);
                this.StatusCde = result.StatusCde;
                this.DvaProcDate = result.DvaProcDate;
                this.Status = result.Status;
                this.HealthFundStatus = result.HealthFundStatus;
            }

        }
    }
        
}
