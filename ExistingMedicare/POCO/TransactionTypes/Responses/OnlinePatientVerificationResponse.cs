﻿using System.Xml;
using Gensolve.MedicareOnline.Interfaces;
using System.Xml.Serialization;
using System.IO;

namespace Gensolve.MedicareOnline.POCO
{
    [XmlRoot(ElementName = "patientVerificationResponse")]
    public class OnlinePatientVerificationResponse : IMedicareServerResult
    {

        #region Private Fields

        private MedicareStatus _medicareStatus;
        private HealthFundStatus _healthFundStatus;

        #endregion

        #region Attributes

        [XmlAttribute(AttributeName = "statusCde")]
        public string StatusCde { get; set; }

        public string XmlRequest { get; set; }
        public string XmlResponse { get; set; }
        public string TransactionId { get; set; }

        #endregion

        #region Elements

        [XmlElement(ElementName = "medicareStatus")]
        public MedicareStatus MedicareStatus { get { return _medicareStatus ?? (_medicareStatus = new MedicareStatus()); } set { _medicareStatus = value; } }

        [XmlElement(ElementName = "healthFundStatus")]
        public HealthFundStatus HealthFundStatus { get { return _healthFundStatus ?? (_healthFundStatus = new HealthFundStatus()); } set { _healthFundStatus = value; } } 

        #endregion

        //Properties of the XML response medicare online document
        public void LoadXmlString(string xmlString)
        {
            if (string.IsNullOrWhiteSpace(xmlString))
                return;

            XmlSerializer serializer = new XmlSerializer(typeof(OnlinePatientVerificationResponse));

            xmlString = MedOnlinePocoHelper.RemoveHeader(xmlString);
            xmlString = MedOnlinePocoHelper.RemoveNamespaceFromXml(xmlString);

            using (TextReader reader = new StringReader(xmlString))
            {
                var result = (OnlinePatientVerificationResponse)serializer.Deserialize(reader);
                this.StatusCde = result.StatusCde;
                this.MedicareStatus = result.MedicareStatus;
                this.HealthFundStatus = result.HealthFundStatus;
            }
        }

        
    }
}