﻿using System.Xml;
using Gensolve.MedicareOnline.Interfaces;
using System.Xml.Serialization;
using System;
using System.IO;

namespace Gensolve.MedicareOnline.POCO
{
    [XmlRoot(ElementName = "DPYReportResponse")]
    public class DPYReportResponse : IReportResponse, IMedicareOnlineObject, IMedicareServerResult
    {
        #region Private Fields

        private Provider _provider;

        #endregion

        #region Attributes   
        public string TransactionId { get; set; }
        public string XmlRequest { get; set; }
        public string XmlResponse { get; set; }

        #endregion

        #region Elements

        [XmlElement(ElementName = "Provider")]
        public Provider Provider { get { return _provider ?? (_provider = new Provider()); } set { _provider = value; } }
        [XmlElement(ElementName = "Payment")]
        public Payment Payment { get; set; }

        #endregion

        #region IMedicareServerResult Implementation

        //Properties of the XML response medicare online document
        public void LoadXmlString(string xmlString)
        {
            LoadXml(xmlString);
        }

        #endregion

        #region Private Methods

        private void LoadXml(string xmlString)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(DPYReportResponse));
            using (TextReader reader = new StringReader(xmlString))
            {
                DPYReportResponse result = (DPYReportResponse)serializer.Deserialize(reader);
                
                Provider = result.Provider;
                Payment = result.Payment;
            }
        } 

        #endregion
    }
}
