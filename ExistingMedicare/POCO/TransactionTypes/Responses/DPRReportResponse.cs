﻿using System.Xml;
using Gensolve.MedicareOnline.Interfaces;
using System.Xml.Serialization;
using Gensolve.MedicareOnline.POCO;
using System.IO;

namespace Gensolve.MedicareOnline.POCO
{
    [XmlRoot(ElementName = "DPRReportResponse")]
    public class DPRReportResponse : IReportResponse, IMedicareServerResult
    {
        private DirectBillClaim _directBillClaim;

        [XmlElement(ElementName = "DirectBillClaim")]
        public DirectBillClaim DirectBillClaim { get { return _directBillClaim??(_directBillClaim = new DirectBillClaim()); } set { _directBillClaim = value; } }

        //Properties of the XML response medicare online document
        public string TransactionId { get; set; }
        public string XmlRequest { get; set; }
        public string XmlResponse { get; set; }
        public void LoadXmlString(string xmlString)
       { 
            XmlSerializer serializer = new XmlSerializer(typeof(DPRReportResponse));
            using (TextReader reader = new StringReader(xmlString))
            {
                DPRReportResponse result = (DPRReportResponse)serializer.Deserialize(reader);
                this.DirectBillClaim = result.DirectBillClaim;                
            }
        }

        public DPRReportResponse() { }
    }
}
