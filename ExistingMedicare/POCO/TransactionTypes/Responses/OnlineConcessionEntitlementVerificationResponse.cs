﻿using System.Xml;
using Gensolve.MedicareOnline.Interfaces;
using System.Xml.Serialization;
using System.IO;
using System;
using System.Collections.Generic;

namespace Gensolve.MedicareOnline.POCO
{

    [XmlRoot(ElementName = "onlineConcessionEntitlementVerificationResponse")]
    public class OnlineConcessionEntitlementVerificationResponse : IMedicareServerResult
    {
        #region Private Fields
        private MedicareStatus _medicareStatus;
        private ConcessionStatus _concessionStatus;
        #endregion

        #region Elements

        [XmlElement("medicareStatus")]
        public MedicareStatus medicareStatus { get { return _medicareStatus ?? (_medicareStatus = new MedicareStatus()); } set { _medicareStatus = value; } }

        [XmlElement("concessionStatus")]
        public ConcessionStatus concessionStatus { get { return _concessionStatus ?? (_concessionStatus = new ConcessionStatus()); } set { _concessionStatus = value; } }

        #endregion

        public string TransactionId { get; set; }
        public string XmlRequest { get; set; }
        public string XmlResponse { get; set; }

        //Properties of the XML response medicare online document
        public void LoadXmlString(string xmlString)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(OnlineConcessionEntitlementVerificationResponse));
            xmlString = MedOnlinePocoHelper.RemoveHeader(xmlString);
            xmlString = MedOnlinePocoHelper.RemoveNamespaceFromXml(xmlString);

            using (TextReader reader = new StringReader(xmlString))
            {
                OnlineConcessionEntitlementVerificationResponse result = (OnlineConcessionEntitlementVerificationResponse)serializer.Deserialize(reader);
                this.medicareStatus = result.medicareStatus;
                this.concessionStatus = result.concessionStatus;
            }
        }
        
        public OnlineConcessionEntitlementVerificationResponse() { }
    }

    public class Status {

        private CurrentMember _currentMember;
        private CurrentMembership _currentMembership;

        [XmlElement(ElementName ="currentMember")]
        public CurrentMember CurrentMember { get { return _currentMember ?? (_currentMember = new CurrentMember()); } set { _currentMember = value; } }

        [XmlElement(ElementName ="currentMembership")]
        public CurrentMembership CurrentMembership { get { return _currentMembership ?? (_currentMembership = new CurrentMembership()); } set { _currentMembership = value; } }

        [XmlAttribute(AttributeName ="statusCode")]
        public string StatusCode { get; set; }
    }

    public class DvaStatus : Status
    {        
    }

    public class ConcessionStatus : Status
    {        
    }

    public class HealthFundStatus : Status
    {
    }
    
    public class MedicareStatus : Status
    {
        
    }

    public class CurrentMembership
    {
        private List<Attributes> _attributes;

        [XmlElement(ElementName = ("attributes"))]
        public List<Attributes> Attributes { get { return _attributes ?? (_attributes = new List<Attributes>()); } set { _attributes = value; } }

        [XmlAttribute("memberNum")]
        public string MemberNum { get; set; }

        [XmlAttribute("memberRefNum")]
        public string MemberRefNum { get; set; }
    }

    public class Attributes : CurrentMembership
    {
        [XmlAttribute(AttributeName = "value")]
        public string Value;

        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
    }

    public class CurrentMember
    {
        [XmlAttribute("firstName")]
        public string FirstName { get; set; }

        [XmlAttribute("lastname")]
        public string LastName { get; set; }
    }
}
