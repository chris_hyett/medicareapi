﻿using System;
using System.Xml.Serialization;
using Gensolve.MedicareOnline.Interfaces;
using System.Collections.Generic;

namespace Gensolve.MedicareOnline.POCO
{
    [XmlRoot(ElementName = "dvaPaymentReportResponse")]
    public class DvaPaymentReportResponse : IReportResponse, IMedicareServerResult
    {
        #region Private Fields

        private PaymentRun _paymentRun;
        private List<ClaimSummary> _claimSummary;

        #endregion

        #region Elements

        [XmlElement("paymentRun")]
        public PaymentRun PaymentRun
        {
            get { return _paymentRun??(_paymentRun = new PaymentRun()); }
            set { _paymentRun = value; }
        }

        [XmlElement("claimSummary")]
        public List<ClaimSummary> ClaimSummary
        {
            get { return _claimSummary ?? (_claimSummary = new List<ClaimSummary>()); }
            set { _claimSummary = value; }
        }

        #endregion
        public string TransactionId { get; set; }
        public string XmlRequest { get; set; }
        public string XmlResponse { get; set; }
        public void LoadXmlString(string xmlString)
        {
            XmlSerializer serializer = new XmlSerializer(this.GetType());
            xmlString = MedOnlinePocoHelper.RemoveHeader(xmlString);
            xmlString = MedOnlinePocoHelper.RemoveNamespaceFromXml(xmlString);

            using (var reader = new System.IO.StringReader(xmlString))
            {
                var result = (DvaPaymentReportResponse)serializer.Deserialize(reader);
                PaymentRun = result.PaymentRun;
                ClaimSummary = result.ClaimSummary;
            }
        }
    }
}

