﻿using System.Xml.Serialization;

namespace Gensolve.MedicareOnline.POCO
{
    [XmlRoot(ElementName = "person")]
    public class Person 
    {
        private Identity _identity;
        private Medicare _medicare;
        private Concession _concession;

        [XmlElement(ElementName = "identity")]
        public Identity Identity
        {
            get { return _identity ?? (_identity = new Identity()); }
            set { _identity = value; }
        }

        [XmlElement(ElementName = "medicare")]
        public Medicare Medicare
        {
            get { return _medicare ?? (_medicare = new Medicare()); }
            set { _medicare = value; }
        }

        [XmlElement(ElementName = "concession")]
        public Concession Concession
        {
            get { return _concession ?? (_concession = new Concession()); }
            set { _concession = value; }
        }

        [XmlAttribute(AttributeName = "dateOfBirth")]
        public string DateOfBirth { get; set; }
    }
}