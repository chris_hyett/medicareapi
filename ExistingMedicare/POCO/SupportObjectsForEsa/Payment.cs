﻿using System.Xml.Serialization;
using System.Collections.Generic;

namespace Gensolve.MedicareOnline.POCO
{
    [XmlRoot(ElementName = "Payment")]
    public class Payment
    {

        #region Private Fields
        private List<PaymentEFTProvider> _paymentEFTProvider;
        #endregion
        #region Attributes

        [XmlAttribute(AttributeName = "paymentAmount")]
        public string PaymentAmount { get; set; }

        [XmlAttribute(AttributeName = "paymentRunDate")]
        public string PaymentRunDate { get; set; }

        [XmlAttribute(AttributeName = "paymentRunNum")]
        public string PaymentRunNum { get; set; }

        #endregion

        #region Elements

        [XmlElement(ElementName = "PaymentEFTProvider")]
        public List<PaymentEFTProvider> PaymentEFTProvider
        {
            get { return _paymentEFTProvider??(_paymentEFTProvider = new List<PaymentEFTProvider>()); }
            set { _paymentEFTProvider = value; }
        } 

        #endregion
    }
}