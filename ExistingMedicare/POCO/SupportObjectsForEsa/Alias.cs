﻿using System.Xml.Serialization;

namespace Gensolve.MedicareOnline.POCO
{
    [XmlRoot(ElementName = "alias")]
    public class Alias
    {
        [XmlAttribute(AttributeName = "firstName")]
        public string FirstName { get; set; }
        [XmlAttribute(AttributeName = "lastName")]
        public string LastName { get; set; }
    }
}