﻿using System.Xml.Serialization;

namespace Gensolve.MedicareOnline.POCO
{
    [XmlRoot(ElementName = "paymentRun")]
    public class PaymentRun
    {
        #region Private Fields
        private Account _account;        
        #endregion

        #region Attributes
        [XmlAttribute("depositAmount")]
        public string DepositAmount { get; set; }

        [XmlAttribute("paymentRunDate")]
        public string PaymentRunDate { get; set; }

        [XmlAttribute("paymentRunNum")]
        public string PaymentRunNum { get; set; }
        #endregion

        #region Elements
        [XmlElement("account")]
        public Account Account {
            get { return _account??(_account = new Account()); } set { _account = value; }
        }
        
        #endregion

    }
}