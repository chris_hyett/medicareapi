﻿using System.Xml.Serialization;

namespace Gensolve.MedicareOnline.POCO
{
    [XmlRoot(ElementName = "AssessmentExplanation")]
    public class AssessmentExplanation
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
    }
}