﻿using System.Xml.Serialization;

namespace Gensolve.MedicareOnline.POCO
{
    [XmlRoot(ElementName = "Request")]
    public class Request
    {
        [XmlElement(ElementName = "DiagnosticImagingRequest")]
        public DiagnosticImagingRequest DiagnosticImagingRequest { get; set; }
    }
}