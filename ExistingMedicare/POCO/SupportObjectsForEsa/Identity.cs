﻿using System.Xml.Serialization;

namespace Gensolve.MedicareOnline.POCO
{
    [XmlRoot(ElementName = "identity")]
    public class Identity 
    {
        [XmlAttribute(AttributeName = "lastName")]
        public string LastName { get; set; }
        [XmlAttribute(AttributeName = "firstName")]
        public string FirstName { get; set; }
        [XmlAttribute(AttributeName = "secondInitial")]
        public string SecondInitial { get; set; }
    }
}