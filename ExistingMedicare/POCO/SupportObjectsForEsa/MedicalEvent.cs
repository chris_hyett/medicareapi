﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Gensolve.MedicareOnline.POCO
{
    /// <summary>
    /// MedicalEvent also known as Voucher
    /// </summary>
    [XmlRoot(ElementName = "MedicalEvent")]    
    public sealed class MedicalEvent : GeneralMedicalEvent
    {
        private Patient _patient;

        #region Elements

        [XmlElement(ElementName = "Service")]
        public List<Service> Service
        {
            get { return _service ?? (_service = new List<Service>()); }
            set { _service = value; }
        }

        [XmlElement(ElementName = "Patient")]
        public Patient Patient { get { return _patient ?? (_patient = new Patient()); } set { _patient = value; } }

        [XmlElement(ElementName = "ServiceProvider")]
        public ServiceProvider ServiceProvider { get { return _serviceProvider ?? (_serviceProvider = new ServiceProvider()); } set { _serviceProvider = value; } }       

        #endregion
    }
}