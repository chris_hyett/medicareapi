﻿namespace Gensolve.MedicareOnline
{
    [System.Serializable]
    public class CertificateAttr
    {
        public string Issuer { set; get; }
        public string Subject { set; get; }
        public string SerialNumber { set; get; }
        public string CommonName { set; get; }
        public string NotBefore { set; get; }
        public string NotAfter { set; get; }
        public string EnCryptionAlgorithm { set; get; }
        public string SignatureAlgorithm { set; get; }
        public string UserNotice { set; get; }
        public string Version { set; get; }
        public string Location { set; get; }

        public string VerifyAttr { set; get; }

    }
}
