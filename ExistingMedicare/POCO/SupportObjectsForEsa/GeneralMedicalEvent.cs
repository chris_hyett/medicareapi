﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Gensolve.MedicareOnline.POCO
{
    public abstract class GeneralMedicalEvent
    {
        #region Fields

        protected ServiceProvider _serviceProvider;
        private PayeeProvider _payeeProvider;        
        private Referral _referral;
        protected List<Service> _service;

        #endregion

        #region Attributes

        [XmlAttribute(AttributeName = "medicalEventId")]
        public string MedicalEventId { get; set; }

        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }

        [XmlAttribute(AttributeName = "medicalEventDate")]
        public string MedicalEventDate { get; set; }

        [XmlAttribute(AttributeName = "medicalEventTime")]
        public string MedicalEventTime { get; set; }

        [XmlAttribute(AttributeName = "referralOverrideType")]
        public string ReferralOverrideType { get; set; }

        [XmlAttribute(AttributeName = "submissionAuthorityInd")]
        public string SubmissionAuthorityInd { get; set; }

        #endregion

        #region Elements

        [XmlElement(ElementName = "Referral")]
        public Referral Referral { get { return _referral ?? (_referral = new Referral()); } set { _referral = value; } }
        
        [XmlElement(ElementName = "PayeeProvider")]
        public PayeeProvider PayeeProvider { get { return _payeeProvider ?? (_payeeProvider = new PayeeProvider()); } set { _payeeProvider = value; } }
        
        #endregion

        #region Public Methods

        public static MedicalEvent Create()
        {
            return new MedicalEvent();
        }

        #endregion
    }
}