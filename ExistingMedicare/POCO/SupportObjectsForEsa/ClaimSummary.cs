﻿using System.Xml.Serialization;
namespace Gensolve.MedicareOnline.POCO
{
    public class ClaimSummary
    {
        #region Attributes
        [XmlAttribute("dateOfLodgement")]
        public string ClaimDate { get; set; }

        [XmlAttribute("benefit")]
        public string ClaimBenefitPaid { get; set; }

        [XmlAttribute("claimId")]
        public string PmsClaimId { get; set; }
        #endregion
    }
}