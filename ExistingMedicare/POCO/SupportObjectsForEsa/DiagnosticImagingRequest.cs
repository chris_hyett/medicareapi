﻿using System.Xml.Serialization;

namespace Gensolve.MedicareOnline.POCO
{
    [XmlRoot(ElementName = "DiagnosticImagingRequest")]
    public class DiagnosticImagingRequest
    {
        [XmlAttribute(AttributeName = "requestDate")]
        public string RequestDate { get; set; }

        [XmlElement(ElementName = "Provider")]
        public Provider Provider { get; set; }
    }
}