﻿using System.Xml.Serialization;

namespace Gensolve.MedicareOnline.POCO
{
    [XmlRoot(ElementName = "Service")]
    public class Service
    {
        #region Private Fields

        private AssessmentExplanation _assessmentExplanation;

        #endregion

        #region Attributes

        [XmlAttribute(AttributeName = "noOfPatientsSeen")]
        public string NoOfPatientsSeen { get; set; }

        [XmlAttribute(AttributeName = "serviceId")]
        public string ServiceId { get; set; }
        
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }

        [XmlAttribute(AttributeName = "item")]
        public string Item { get; set; }

        [XmlAttribute(AttributeName = "charge")]
        public string Charge { get; set; }

        [XmlAttribute(AttributeName = "duplicateServiceOverrideInd")]
        public string DuplicateServiceOverrideInd { get; set; }

        [XmlAttribute(AttributeName = "multipleProcedureOverrideInd")]
        public string MultipleProcedureOverrideInd { get; set; }

        [XmlAttribute(AttributeName = "aftercareOverrideInd")]
        public string AftercareOverrideInd { get; set; }

        [XmlAttribute(AttributeName = "accessionDateTime")]
        public string AccessionDateTime { get; set; }

        [XmlAttribute(AttributeName = "collectionDateTime")]
        public string CollectionDateTime { get; set; }
        
        [XmlAttribute(AttributeName = "equipmentId")]
        public string EquipmentId { get; set; }

        [XmlAttribute(AttributeName = "fieldQty")]
        public string FieldQty { get; set; }

        [XmlAttribute(AttributeName = "hospitalInd")]
        public string HospitalInd { get; set; }

        [XmlAttribute(AttributeName = "specimenCollectionPointId")]
        public string SpecimenCollectionPointId { get; set; }
        
        [XmlAttribute(AttributeName = "patientQty")]
        public string PatientQty { get; set; }

        [XmlAttribute(AttributeName = "restrictiveOverrideCde")]
        public string RestrictiveOverrideCde { get; set; }

        [XmlAttribute(AttributeName = "rule3ExemptInd")]
        public string Rule3ExemptInd { get; set; }

        [XmlAttribute(AttributeName = "s4b3ExemptInd")]
        public string S4b3ExemptInd { get; set; }

        [XmlAttribute(AttributeName = "selfDeemedInd")]
        public string SelfDeemedInd { get; set; }

        [XmlAttribute(AttributeName = "text")]
        public string Text { get; set; }

        [XmlAttribute(AttributeName = "serviceText")]
        public string ServiceText { get; set; }

        [XmlAttribute(AttributeName = "LspNum")]
        public string LspNum { get; set; }

        [XmlAttribute(AttributeName = "timeDuration")]
        public string TimeDuration { get; set; }

        [XmlAttribute(AttributeName = "benefit")]
        public string Benefit { get; set; }

        [XmlAttribute(AttributeName = "explanationCode")]
        public string ExplanationCode { get; set; }

        [XmlAttribute(AttributeName = "detailsExplanationCode")]
        public string DetailsExplanationCode { get; set; }

        [XmlAttribute(AttributeName = "chargeAmount")]
        public string ChargeAmount { get; set; }

        [XmlAttribute(AttributeName = "dateOfService")]
        public string DateOfService { get; set; }

        [XmlAttribute(AttributeName = "itemNum")]
        public string ItemNum { get; set; }

        [XmlAttribute(AttributeName = "AccountReferenceNum")]
        public string AccountReferenceNum { get; set; }

        [XmlAttribute(AttributeName = "patientContribAmt")]
        public string PatientContribAmt { get; set; }

        [XmlAttribute(AttributeName = "selfDeemedCde")]
        public string SelfDeemedCde { get; set; }

        #endregion

        #region Elements

        [XmlElement(ElementName = "AssessmentExplanation")]
        public AssessmentExplanation AssessmentExplanation { get { return _assessmentExplanation??(_assessmentExplanation = new AssessmentExplanation()); } set { _assessmentExplanation = value; } }

        #endregion

        #region Constructor

        public static Service Create()
        {
            return new Service();
        }

        #endregion
    }
}