﻿using System.Xml.Serialization;

namespace Gensolve.MedicareOnline.POCO
{
    [XmlRoot(ElementName = "Patient")]
    public class GeneralPatient 
    {
        #region Private Fields

        private Alias _alias;        
        private HealthFund _healthFund;        
        private ResidentialAddress _residentialAddress;        

        #endregion

        protected Identity _identity;

        #region Attributes

        [XmlAttribute(AttributeName = "firstName")]
        public string FirstName { get; set; }
        
        [XmlAttribute(AttributeName = "lastName")]
        public string LastName { get; set; }
        
        [XmlAttribute(AttributeName = "quotedSubnumerate")]
        public string QuotedSubnumerate { get; set; }

        [XmlAttribute(AttributeName = "quotedMedicareCardNum")]
        public string QuotedMedicareCardNum { get; set; }
        
        [XmlAttribute(AttributeName = "quotedMedicareCardIssueNum")]
        public string QuotedMedicareCardIssueNum { get; set; }

        [XmlAttribute(AttributeName = "dateOfBirth")]
        public string DateOfBirth { get; set; }

        [XmlAttribute(AttributeName = "gender")]
        public string Gender { get; set; }

        [XmlAttribute(AttributeName = "currentMedicareCardNum")]
        public string CurrentMedicareCardNum { get; set; }

        [XmlAttribute(AttributeName = "currentSubnumerate")]
        public string CurrentSubnumerate { get; set; }

        [XmlAttribute(AttributeName = "currentMedicareCardIssueNum")]
        public string CurrentMedicareCardIssueNum { get; set; }

        [XmlAttribute(AttributeName = "medicareCardStatusCode")]
        public string MedicareCardStatusCode { get; set; }

        [XmlAttribute(AttributeName = "cardFlag")]
        public string CardFlag { get; set; }

        #endregion

        #region Elements

        [XmlElement(ElementName = "Alias")]
        public Alias Alias
        {
            get { return _alias ?? (_alias = new Alias()); }
            set { _alias = value; }
        }
        
        [XmlElement(ElementName = "healthFund")]
        public HealthFund HealthFund
        {
            get { return _healthFund ?? (_healthFund = new HealthFund()); }
            set { _healthFund = value; }
        }        

        [XmlElement(ElementName = "ResidentialAddress")]
        public ResidentialAddress ResidentialAddress
        {
            get { return _residentialAddress ?? (_residentialAddress = new ResidentialAddress()); }
            set { _residentialAddress = value; }
        }       
                
        #endregion

    }
}