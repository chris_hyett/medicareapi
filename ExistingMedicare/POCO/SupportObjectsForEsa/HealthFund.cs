﻿using System.Xml.Serialization;

namespace Gensolve.MedicareOnline.POCO
{
    [XmlRoot(ElementName = "healthFund")]
    public class HealthFund
    {
        [XmlAttribute(AttributeName = "memberNumber")]
        public string MemberNumber { get; set; }

        [XmlAttribute(AttributeName = "memberRefNumber")]
        public string MemberRefNumber { get; set; }

        [XmlAttribute(AttributeName = "organisation")]
        public string Organisation { get; set; }
    }
}