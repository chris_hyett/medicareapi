﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Gensolve.MedicareOnline.POCO
{
    [XmlRoot(ElementName = "PaymentEFTProvider")]
    public class PaymentEFTProvider
    {
        #region Private Fields

        private List<DirectBillClaim> _directBillClaim;

        #endregion

        #region Attributes

        [XmlAttribute(AttributeName = "accountName")]
        public string AccountName { get; set; }

        [XmlAttribute(AttributeName = "accountNum")]
        public string AccountNum { get; set; }

        [XmlAttribute(AttributeName = "bsbCode")]
        public string BsbCode { get; set; }

        #endregion

        #region Elements

        [XmlElement(ElementName = "DirectBillClaim")]
        public List<DirectBillClaim> DirectBillClaim
        {
            get { return _directBillClaim ?? (_directBillClaim = new List<DirectBillClaim>()); }
            set { _directBillClaim = value; }
        }

        #endregion
    }
}