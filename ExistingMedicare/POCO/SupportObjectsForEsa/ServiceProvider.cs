﻿using System.Xml.Serialization;

namespace Gensolve.MedicareOnline.POCO
{
    [XmlRoot(ElementName = "ServiceProvider")]
    public class ServiceProvider
    {
        #region Attributes

        [XmlAttribute(AttributeName = "providerNum")]
        public string ProviderNum { get; set; }

        #endregion

        #region Elements

        [XmlElement(ElementName = "Request")]
        public Request Request { get; set; }

        #endregion
    }
}