﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Gensolve.MedicareOnline.POCO
{
    public sealed class DvaMedicalEvent : GeneralMedicalEvent
    {
        private DvaPatient _patient;
        
        #region Elements

        [XmlElement(ElementName = "service")]
        public List<Service> Service
        {
            get { return _service ?? (_service = new List<Service>()); }
            set { _service = value; }
        }

        [XmlElement(ElementName = "patient")]
        public DvaPatient Patient { get { return _patient ?? (_patient = new DvaPatient()); } set { _patient = value; } }

        [XmlElement(ElementName = "servicingProvider")]
        public ServiceProvider ServiceProvider { get { return _serviceProvider ?? (_serviceProvider = new ServiceProvider()); } set { _serviceProvider = value; } }       

        #endregion
    }
}