﻿using System.Xml.Serialization;

namespace Gensolve.MedicareOnline.POCO
{
    public sealed class DvaPatient : GeneralPatient
    {
        private DvaMembership _dvaMembership;

        [XmlElement(ElementName = "identity")]
        public Identity Identity
        {
            get { return _identity ?? (_identity = new Identity()); }
            set { _identity = value; }
        }

        [XmlElement(ElementName = "dvaMembership")]
        public DvaMembership DvaMembership
        {
            get { return _dvaMembership ?? (_dvaMembership = new DvaMembership()); }
            set { _dvaMembership = value; }
        }
    }

    public sealed class SerializableOVVPatient
    {
        private Alias _alias;
        private HealthFund _healthFund;
        private ResidentialAddress _residentialAddress;
        private DvaMembership _dvaMembership;
        private Identity _identity;

        [XmlAttribute(AttributeName = "dateOfBirth")]
        public string DateOfBirth { get; set; }

        [XmlAttribute(AttributeName = "gender")]
        public string Gender { get; set; }


        [XmlElement(ElementName = "identity", Order = 1)]
        public Identity Identity
        {
            get { return _identity ?? (_identity = new Identity()); }
            set { _identity = value; }
        }

        [XmlElement(ElementName = "alias", Order = 2)]
        public Alias Alias
        {
            get { return _alias ?? (_alias = new Alias()); }
            set { _alias = value; }
        }

        [XmlElement(ElementName = "residentialAddress", Order = 3)]
        public ResidentialAddress ResidentialAddress
        {
            get { return _residentialAddress ?? (_residentialAddress = new ResidentialAddress()); }
            set { _residentialAddress = value; }
        }

        [XmlElement(ElementName = "dvaMembership", Order = 4)]
        public DvaMembership DvaMembership
        {
            get { return _dvaMembership ?? (_dvaMembership = new DvaMembership()); }
            set { _dvaMembership = value; }
        }

    }
}