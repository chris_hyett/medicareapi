﻿using System.Xml.Serialization;

namespace Gensolve.MedicareOnline.POCO
{
    public sealed class Patient : GeneralPatient {

        private Medicare _medicare;

        [XmlElement(ElementName = "identity")]
        public Identity Identity
        {
            get { return _identity ?? (_identity = new Identity()); }
            set { _identity = value; }
        }

        [XmlElement(ElementName = "medicare")]
        public Medicare Medicare
        {
            get { return _medicare ?? (_medicare = new Medicare()); }
            set { _medicare = value; }
        }
    }
}