﻿using System.Xml.Serialization;

namespace Gensolve.MedicareOnline.POCO
{
    [XmlRoot(ElementName = "dvaMembership")]
    public class DvaMembership
    {
        [XmlAttribute(AttributeName = "memberNum")]
        public string MemberNum { get; set; }
    }
}