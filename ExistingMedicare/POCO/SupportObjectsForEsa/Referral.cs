﻿using System.Xml.Serialization;

namespace Gensolve.MedicareOnline.POCO
{
    [XmlRoot(ElementName = "Referral")]
    public class Referral
    {
        Provider _provider;

        [XmlAttribute(AttributeName = "issuedDate")]
        public string IssuedDate { get; set; }
        [XmlAttribute(AttributeName = "periodType")]
        public string PeriodType { get; set; }

        [XmlElement(ElementName = "Provider")]
        public Provider Provider
        {
            get
            {
                if (_provider == null)
                    _provider = new Provider(); return _provider;
            }
            set { _provider = value; }
        }
    }
}