﻿using System.Xml.Serialization;

namespace Gensolve.MedicareOnline.POCO
{
    public class Account
    {
        [XmlAttribute("bankAccountName")]
        public string BankAccountName { get; set; }

        [XmlAttribute("bankAccountNum")]
        public string BankAccountNum { get; set; }

        [XmlAttribute("bsbCode")]
        public string BsbCode { get; set; }
    }
}