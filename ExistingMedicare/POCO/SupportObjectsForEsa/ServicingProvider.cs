﻿using System.Xml.Serialization;

namespace Gensolve.MedicareOnline.POCO
{
    [XmlRoot(ElementName = "ServicingProvider")]
    public class ServicingProvider {
        [XmlAttribute(AttributeName = "providerNum")]
        public string ProviderNum { get; set; }

    }
}