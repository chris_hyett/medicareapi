﻿using System.Xml.Serialization;

namespace Gensolve.MedicareOnline.POCO
{
    [XmlRoot(ElementName = "concession")]
    public class Concession 
    {
        [XmlAttribute(AttributeName = "memberNum")]
        public string MemberNum { get; set; }
    }
}