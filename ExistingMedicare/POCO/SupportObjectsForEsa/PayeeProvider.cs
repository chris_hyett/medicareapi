﻿using System.Xml.Serialization;

namespace Gensolve.MedicareOnline.POCO
{
    [XmlRoot(ElementName = "PayeeProvider")]
    public class PayeeProvider
    {
        [XmlAttribute(AttributeName = "providerNum")]
        public string ProviderNum { get; set; }
    }
}