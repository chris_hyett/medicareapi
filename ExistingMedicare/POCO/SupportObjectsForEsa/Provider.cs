﻿using System.Xml.Serialization;

namespace Gensolve.MedicareOnline.POCO
{
    [XmlRoot(ElementName = "Provider")]
    public class Provider
    {
        [XmlAttribute(AttributeName = "providerNum")]
        public string ProviderNum { get; set; }
    }
}