﻿using System.Xml.Serialization;

namespace Gensolve.MedicareOnline.POCO
{
    [XmlRoot(ElementName = "medicare")]
    public class Medicare
    {
        [XmlAttribute(AttributeName = "memberRefNum")]
        public string MemberRefNum { get; set; }
        [XmlAttribute(AttributeName = "memberNum")]
        public string MemberNum { get; set; }
    }
}