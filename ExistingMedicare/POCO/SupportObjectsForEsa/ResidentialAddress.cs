﻿using System.Xml.Serialization;

namespace Gensolve.MedicareOnline.POCO
{
    [XmlRoot(ElementName = "residentialAddress")]
    public class ResidentialAddress
    {
        [XmlAttribute(AttributeName = "postcode")]
        public string Postcode { get; set; }
        [XmlAttribute(AttributeName = "locality")]
        public string Locality { get; set; }
    }
}