﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gensolve.MedicareOnline.POCO
{
    public static class MedOnlinePocoHelper
    {
        public static string RemoveHeader(string xmlString)
        {
            if (!HasHeader(xmlString))
                return xmlString;

            var modifiedString = xmlString.Substring(xmlString.IndexOf('>') + 1);
            modifiedString = modifiedString.Substring(modifiedString.IndexOf("<"));

            return modifiedString;
        }

        private static bool HasHeader(string xmlString)
        {
            return xmlString.Trim().StartsWith("<?xml");
        }

        internal static string RemoveNamespaceFromXml(string xmlString)
        {
            xmlString = xmlString.Replace("xmlns:tns=\"http://hic.gov.au/hiconline/hiconline/version-4\"", string.Empty).Trim();
            xmlString = xmlString.Replace("xmlns:ns1=\"http://hic.gov.au/hiconline/dva/version-6\"", string.Empty).Trim();
            xmlString = xmlString.Replace("xmlns:ns1=\"http://hic.gov.au/hiconline/cev/version-1\"", string.Empty).Trim();

            xmlString = xmlString.Replace("ns1:", string.Empty).Trim();
            xmlString = xmlString.Replace("tns:", string.Empty).Trim();
            
            return xmlString;
        }
    }
}
