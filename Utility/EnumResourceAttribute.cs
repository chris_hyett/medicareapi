﻿using System;
using System.Reflection;

namespace Gensolve.Utilities
{
    public interface IAttribute<T>
        {
            T Value { get; }
            T Key { get; }
        }

        [AttributeUsage(AttributeTargets.Field)]
        public sealed class EnumResourceAttribute : Attribute, IAttribute<String>
        {

            public EnumResourceAttribute(string strValue)
            {
                this.strValue = strValue;
            }

            #region IAttribute<string> Members
            private string strValue;
            public string Value
            {
                get { return strValue; }
            }
            public string Key
            {
                get { return ""; }
            }
            #endregion
        }

        public class EnumResourceAttributeReader
        {
            public static R GetAttributeValue<T, R>(Enum @enum)
            {
                R attributeValue = default(R);

                if (@enum != null)
                {
                    FieldInfo fi = @enum.GetType().GetField(@enum.ToString());
                    if (fi != null)
                    {
                        T[] attributes = fi.GetCustomAttributes(typeof(T), false) as T[];
                        if (attributes != null && attributes.Length > 0)
                        {
                            IAttribute<R> attribute = attributes[0] as IAttribute<R>;
                            attributeValue = attribute.Value;
                        }
                    }
                }
                return attributeValue;
            }
        }
    }
